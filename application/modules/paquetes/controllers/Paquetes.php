<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Paquetes extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->model('M_paquetes', 'mp');
    $this->load->library(array('session', 'table', 'email'));
    $this->load->helper(array('form', 'html', 'validation', 'url', 'astillero'));

    date_default_timezone_set('America/Mexico_City');

    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
    if (!PermisoModulo('paquetes')) {
      redirect(site_url('login'));
    }
  }
  public function completar_paquetes()
  {
    $data['info'] = $this->mp->getPaquetes();
    $data['titulo'] = 'Lista de paquetes incompletos';
    $this->blade->render('completar_paquetes', $data);
  }
  public function generar_paquetes($id = 0)
  {
    $id = decrypt($id);
    if ($id == 0) {
      $info = new Stdclass();
      $info_usuario = new Stdclass();
      $info_ubicacion = new Stdclass();
      $info_auto = new Stdclass();
      $info_horario = new Stdclass();
      $info_facturacion = new Stdclass();
      $data['mapa_latitud'] = '';
      $data['mapa_longitud'] = '';
      $data['id_usuario'] = 0;
      $data['id_ubicacion'] = 0;
      $data['id_auto'] = 0;
      $data['id_facturacion'] = 0;
      $data['id_horario'] = 0;
      $data['id_tipo_auto'] = '';
      $disabled = '';
      $data['paquetes_incompletos'] = $this->mp->existenPaquetesIncompletos();
    } else {
      $info = $this->mp->getById($id, 'servicio_lavado');
      $info_usuario = $this->mp->getById($info->id_usuario, 'usuarios');
      $info_ubicacion = $this->mp->getById($info->id_ubicacion, 'ubicacion_servicio');
      $data['mapa_latitud'] = $info_ubicacion->latitud;
      $data['mapa_longitud'] = $info_ubicacion->longitud;
      $info_auto = $this->mp->getById($info->id_auto, 'autos');
      $info_horario = $this->mp->getById($info->id_horario, 'horarios_lavadores');
      $info_facturacion = $this->mp->getDatosFacturacionByIdUsuario($info->id_usuario);
      $data['id_usuario'] = $info->id_usuario;
      $data['id_ubicacion'] = $info->id_ubicacion;
      $data['id_auto'] = $info->id_auto;
      $data['id_horario'] = $info->id_horario;
      $data['id_tipo_auto'] = $this->mp->getTipoAuto($info_auto->id_modelo);
      if ($info_facturacion) {
        $data['id_facturacion'] = $info_facturacion->id;
      } else {
        $data['id_facturacion'] = 0;
      }
      $disabled = 'disabled';
    }
    if ($this->input->post()) {
      //datos del vehículo
      $this->form_validation->set_rules('placas', 'placas', 'trim|required');
      $this->form_validation->set_rules('id_color', 'color', 'trim|required');
      $this->form_validation->set_rules('id_modelo', 'modelo', 'trim|required');
      $this->form_validation->set_rules('id_anio', 'año', 'trim|required');
      $this->form_validation->set_rules('id_marca', 'marca', 'trim|required');
      $this->form_validation->set_rules('numero_int', 'número interior', 'trim');
      $this->form_validation->set_rules('numero_ext', 'número exterior', 'trim|required');
      $this->form_validation->set_rules('colonia', 'colonia', 'trim|required');
      $this->form_validation->set_rules('id_estado', 'estado', 'trim|required');
      $this->form_validation->set_rules('latitud', 'latitud', 'trim|required');
      $this->form_validation->set_rules('longitud', 'longitud', 'trim|required');
      $this->form_validation->set_rules('calle', 'calle', 'trim|required');
      $this->form_validation->set_rules('kilometraje', 'kilometraje', 'trim|numeric');
      $this->form_validation->set_rules('numero_serie', 'numero serie', 'trim|exact_length[17]');
      //servicio
      // if($_POST['id']==0){
      //   $this->form_validation->set_rules('fecha', 'fecha', 'trim|required');
      //   $this->form_validation->set_rules('hora', 'hora', 'trim|required');
      // }
      //$this->form_validation->set_rules('id_servicio', 'servicio', 'trim|required');
      //usuario
      $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
      $this->form_validation->set_rules('apellido_paterno', 'apellido paterno', 'trim|required');
      $this->form_validation->set_rules('apellido_materno', 'apellido materno', 'trim|required');
      $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required');
      $this->form_validation->set_rules('telefono', 'telefono', 'trim|exact_length[10]|required|numeric');
      $this->form_validation->set_rules('id_sucursal', 'sucursal', 'trim|required');
      $this->form_validation->set_rules('id_municipio', 'municipio', 'trim|required');
      //Ubicación
      $this->form_validation->set_rules('id_sucursal_ubicacion', 'sucursal', 'trim|required');
      //facturacion
      if (isset($_POST['check_facturacion'])) {
        $this->form_validation->set_rules('referencia', 'referencia', 'trim|required');
        $this->form_validation->set_rules('razon_social', 'razon_social', 'trim|required');
        $this->form_validation->set_rules('domicilio', 'domicilio', 'trim|required');
        $this->form_validation->set_rules('email_facturacion', 'email', 'trim|valid_email|required');
        $this->form_validation->set_rules('id_cfdi', 'CFDI', 'trim|required');
        $this->form_validation->set_rules('id_metodo_pago', 'método de pago', 'trim|required');
        $this->form_validation->set_rules('rfc', 'R.F.C', 'trim|required');
        $this->form_validation->set_rules('cp', 'C.P.', 'trim|required|numeric');
      }
      $this->form_validation->set_rules('id_paquete', 'paquete', 'trim|required');

      if ($this->form_validation->run()) {
        $this->mp->guardarServicio();
      } else {
        $errors = array(
          //Datos del vehículo
          'placas' => form_error('placas'),
          'id_color' => form_error('id_color'),
          'id_modelo' => form_error('id_modelo'),
          'id_anio' => form_error('id_anio'),
          'id_marca' => form_error('id_marca'),
          'kilometraje' => form_error('kilometraje'),
          'numero_serie' => form_error('numero_serie'),
          //Ubicación
          'numero_int' => form_error('numero_int'),
          'numero_ext' => form_error('numero_ext'),
          'colonia' => form_error('colonia'),
          'id_estado' => form_error('id_estado'),
          'latitud' => form_error('latitud'),
          'longitud' => form_error('longitud'),
          'calle' => form_error('calle'),
          //Servicio
          'fecha' => form_error('fecha'),
          'hora' => form_error('hora'),
          'id_servicio' => form_error('id_servicio'),
          //usuario
          'nombre' => form_error('nombre'),
          'apellido_paterno' => form_error('apellido_paterno'),
          'apellido_materno' => form_error('apellido_materno'),
          'email' => form_error('email'),
          'telefono' => form_error('telefono'),
          'id_sucursal' => form_error('id_sucursal'),
          'id_municipio' => form_error('id_municipio'),
          //'password' => form_error('password'),
          //facturación
          'referencia' => form_error('referencia'),
          'razon_social' => form_error('razon_social'),
          'domicilio' => form_error('domicilio'),
          'email_facturacion' => form_error('email_facturacion'),
          'id_cfdi' => form_error('id_cfdi'),
          'id_metodo_pago' => form_error('id_metodo_pago'),
          'rfc' => form_error('rfc'),
          'cp' => form_error('cp'),
          'id_paquete' => form_error('id_paquete'),
          //ubicación
          'id_sucursal_ubicacion' => form_error('id_sucursal_ubicacion')
        );
        echo json_encode($errors);
        exit();
      }
    }
    //datos del vehículo
    $data['placas'] = form_input('placas', set_value('placas', exist_obj($info_auto, 'placas')), 'class="form-control" id="placas"');
    $data['kilometraje'] = form_input('kilometraje', set_value('kilometraje', exist_obj($info_auto, 'kilometraje')), 'class="form-control numeric" id="kilometraje" maxlength="10"');
    $data['numero_serie'] = form_input('numero_serie', set_value('numero_serie', exist_obj($info_auto, 'numero_serie')), 'class="form-control" id="numero_serie" maxlength="17"');
    $data['id_color'] = form_dropdown('id_color', array_combos($this->Mgeneral->get_table('cat_colores'), 'id', 'color', TRUE), set_value('id_color', exist_obj($info_auto, 'id_color')), 'class="form-control busqueda" id="id_color"');
    $data['id_anio'] = form_dropdown('id_anio', array_combos($this->Mgeneral->get_table('cat_anios'), 'id', 'anio', TRUE), set_value('id_anio', exist_obj($info_auto, 'id_anio')), 'class="form-control busqueda" id="id_anio"');

    if ($id == 0) {
      $lista_modelos = array();
    } else {
      $lista_modelos = $this->mp->getModelosByMarca(set_value('id_marca', exist_obj($info_auto, 'id_marca')));
    }
    $data['id_marca'] = form_dropdown('id_marca', array_combos($this->Mgeneral->get_table('cat_marcas'), 'id', 'marca', TRUE), set_value('id_marca', exist_obj($info_auto, 'id_marca')), 'class="form-control busqueda" id="id_marca"');
    $data['id_modelo'] = form_dropdown('id_modelo', array_combos($lista_modelos, 'id', 'modelo', TRUE), set_value('id_modelo', exist_obj($info_auto, 'id_modelo')), 'class="form-control busqueda" id="id_modelo"');
    //datos de ubicación
    $data['numero_int'] = form_input('numero_int', set_value('numero_int', exist_obj($info_ubicacion, 'numero_int')), 'class="form-control" id="numero_int"');
    $data['numero_ext'] = form_input('numero_ext', set_value('numero_ext', exist_obj($info_ubicacion, 'numero_ext')), 'class="form-control" id="numero_ext"');
    $data['colonia'] = form_input('colonia', set_value('colonia', exist_obj($info_ubicacion, 'colonia')), 'class="form-control" id="colonia"');
    $data['id_estado'] = form_dropdown('id_estado', array_combos($this->Mgeneral->get_table('estados'), 'id', 'estado', TRUE), set_value('id_estado', exist_obj($info_ubicacion, 'id_estado')), 'class="form-control busqueda" id="id_estado"');
    $data['calle'] = form_input('calle', set_value('calle', exist_obj($info_ubicacion, 'calle')), 'class="form-control" id="calle"');
    if ($id == 0) {
      $lista_sucursales = array();
      $lista_municipios = array();
    } else {
      $lista_sucursales = $this->mp->getSucursalesByEstado(set_value('id_estado', exist_obj($info_ubicacion, 'id_estado')));
      $lista_municipios = $this->mp->getMunicipiosByEstado(set_value('id_estado', exist_obj($info_ubicacion, 'id_estado')));
    }
    
    $data['id_sucursal_ubicacion'] = form_dropdown('id_sucursal_ubicacion', array_combos($lista_sucursales, 'id', 'sucursal', TRUE), set_value('id_sucursal', exist_obj($info_ubicacion, 'id_sucursal')), 'class="form-control busqueda" id="id_sucursal_ubicacion"');
    $data['id_municipio'] = form_dropdown('id_municipio', array_combos($lista_municipios, 'id', 'municipio', TRUE), set_value('id_municipio', exist_obj($info_ubicacion, 'id_municipio')), 'class="form-control busqueda" id="id_municipio"');
    $data['latitud'] = form_input('latitud', set_value('latitud', exist_obj($info_ubicacion, 'latitud')), 'class="form-control" id="latitud" readonly');
    $data['longitud'] = form_input('longitud', set_value('longitud', exist_obj($info_ubicacion, 'longitud')), 'class="form-control" id="longitud" readonly');
    //Datos del servicio
    $data['id_paquete'] = form_dropdown('id_paquete', array_combos($this->Mgeneral->get_table('paquetes'), 'id', 'paquete', TRUE), set_value('id_paquete', exist_obj($info, 'id_paquete')), 'class="form-control busqueda" id="id_paquete" ' . $disabled . '');
    $data['comentarios'] = form_textarea('comentarios', set_value('comentarios', exist_obj($info, 'comentarios')), 'class="form-control" rows="2" id="comentarios"');
    $data['id_servicio'] = form_dropdown('id_servicio', array_combos($this->Mgeneral->get_table('servicios'), 'servicioId', 'servicioNombre', TRUE), set_value('id_servicio', exist_obj($info, 'id_servicio')), 'class="form-control busqueda" id="id_servicio"');
    //Datos del usuario
    $data['nombre'] = form_input('nombre', set_value('nombre', exist_obj($info_usuario, 'nombre')), 'class="form-control js_user" id="nombre"');
    $data['apellido_paterno'] = form_input('apellido_paterno', set_value('apellido_paterno', exist_obj($info_usuario, 'apellido_paterno')), 'class="form-control js_user" id="apellido_paterno"');
    $data['apellido_materno'] = form_input('apellido_materno', set_value('apellido_materno', exist_obj($info_usuario, 'apellido_materno')), 'class="form-control js_user" id="apellido_materno"');
    $data['email'] = form_input('email', set_value('email', exist_obj($info_usuario, 'email')), 'class="form-control js_user" id="email"');
    $data['telefono'] = form_input('telefono', set_value('telefono', exist_obj($info_usuario, 'telefono')), 'class="form-control js_user" id="telefono" maxlength="10');
    $data['password'] = form_input('password', set_value('password', exist_obj($info_usuario, 'password')), 'class="form-control js_user" id="password"');
    $data['id_sucursal'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), set_value('id_sucursal', exist_obj($info_usuario, 'id_sucursal')), 'class="form-control js_user busqueda" id="id_sucursal"');
    //Datos facturación
    $data['referencia'] = form_input('referencia', set_value('referencia', exist_obj($info_facturacion, 'referencia')), 'class="form-control" id="referencia"');
    $data['cp'] = form_input('cp', set_value('cp', exist_obj($info_facturacion, 'cp')), 'class="form-control" id="cp"');
    $data['razon_social'] = form_input('razon_social', set_value('razon_social', exist_obj($info_facturacion, 'razon_social')), 'class="form-control" id="razon_social"');
    $data['domicilio'] = form_input('domicilio', set_value('domicilio', exist_obj($info_facturacion, 'domicilio')), 'class="form-control" id="domicilio"');
    $data['email_facturacion'] = form_input('email_facturacion', set_value('email_facturacion', exist_obj($info_facturacion, 'email_facturacion')), 'class="form-control" id="email_facturacion"');
    $data['id_cfdi'] = form_dropdown('id_cfdi', array_combos($this->Mgeneral->get_table('cat_cfdi'), 'id', 'cfdi', TRUE), set_value('id_cfdi', exist_obj($info_facturacion, 'id_cfdi')), 'class="form-control busqueda" id="id_cfdi"');
    $data['rfc'] = form_input('rfc', set_value('rfc', exist_obj($info_facturacion, 'rfc')), 'class="form-control" id="rfc"');
    $data['id_metodo_pago'] = form_dropdown('id_metodo_pago', array_combos($this->Mgeneral->get_table('cat_metodo_pago'), 'id', 'metodo_pago', TRUE), set_value('id_metodo_pago', exist_obj($info_facturacion, 'id_metodo_pago')), 'class="form-control busqueda" id="id_metodo_pago"');
    $clientes = $this->db->select('id,concat(nombre," ",apellido_paterno," ",apellido_materno) as cliente')->get('usuarios')->result();
    $data['id_usuario_list'] = form_dropdown('id_usuario_list', array_combos_message($clientes, 'id', 'cliente', 'Cliente nuevo'), set_value('id_usuario_list', exist_obj($info_facturacion, 'id_usuario_list')), 'class="form-control busqueda" id="id_usuario_list"');

    $data['id'] = $id;
    $this->blade->render('nuevo_paquete', $data);
  }
  public function getAutoById()
  {
    $auto =  $this->mp->getById($_POST['id_auto'], 'autos');
    echo json_encode(array('auto' => $auto));
  }
  public function getMunicipios()
  {
    $municipios = $this->mp->getMunicipiosByEstado($_POST['id_estado']);
    echo json_encode($municipios);
  }
  public function getModelos()
  {
    $modelos = $this->mp->getModelosByMarca($_POST['id_marca']);
    echo json_encode($modelos);
  }
  public function getFechasDisponiblesLavador()
  {
    $fechas = $this->mp->getFechasDisponiblesLavador($_POST['id_lavador']);
    echo json_encode($fechas);
  }
  public function tablero_lavadores()
  {
    $datos['fecha'] = date('Y-m-d');
    $datos['tiempo_aumento'] = 60;
    $datos['valor'] = 11;
    $datos['tabla1'] = $this->mp->tablero_lavadores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 0); // el último argumento es para saber si hace el cociente en base a 0 o a 1
    $datos['tabla2'] = $this->mp->tablero_lavadores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 1); // el último argumento es para saber si hace el cociente en base a 0 o a 1
    $this->blade->render('tablero_lavadores', $datos);
  }
  public function tabla_horarios_lavadores()
  {
    $datos['fecha'] = $this->input->post('fecha');
    $datos['tiempo_aumento'] = 60;
    $datos['valor'] = 11;
    $datos['tabla1'] = $this->mp->tablero_lavadores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 0);
    $datos['tabla2'] = $this->mp->tablero_lavadores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 1);
    $this->blade->render('tabla_horarios_lavadores', $datos);
  }
  public function getDatosUsuarios()
  {
    $datos_usuario = $this->mp->getDataUser($_POST['id_usuario']);
    $autos = $this->mp->getVehiculosUser($_POST['id_usuario']);
    echo json_encode(array('usuario' => $datos_usuario, 'autos' => $autos));
  }
  public function getVehiculos()
  {
    $data['autos'] = $this->mp->getVehiculosUser($_POST['id_usuario']);
    $this->blade->render('autos', $data);
  }
  //Obtener vehículo por placa
  public function getAutoByPlacas()
  {
    $auto = $this->mp->getAutoByPlacas($_POST['placas']);
    echo json_encode(array('auto' => $auto));
  }
  //Obtener tipo de auto
  public function getTipoAuto()
  {
    echo $this->mp->getTipoAuto($_POST['id_modelo']);
  }
  public function detalle_paquete()
  {
    $data['precio'] = $this->mp->getPrecioPaquete($_POST['id_tipo_auto'], $_POST['id_paquete']);
    $dias_paquete = $this->mp->getDiasPaquete($_POST['id_paquete']);
    $newdate = strtotime('+' . $dias_paquete . ' day', strtotime(date('Y-m-d')));
    $fecha_fin = date('Y-m-d', $newdate);
    $data['detalle_paquete'] = $this->mp->descripcionPaquete($_POST['id_paquete']);
    $paquete = $this->mp->getDataPaquete($_POST['id_paquete']);
    $data['nombre_paquete'] = $paquete->paquete;
    $data['descripcion_paquete'] = $paquete->descripcion;
    $data['id_servicio_lavado'] = $_POST['id_servicio_lavado'];
    $data['fechas_lavadores'] = $this->mp->getFechasLavadores('', $fecha_fin);
    //debug_var($data['detalle_paquete']);die();
    $this->blade->render('detalle_paquete', $data);
  }
  //Función para guardar guardar los horarios del paquete
  public function guardarHorario()
  {
    $horario_lavador = $this->mp->getIdLavador();

    if (!$horario_lavador) {
      echo json_encode(array('exito' => false, 'mensaje' => 'El horario ya fue ocupado, elige otro'));
      exit();
    }
    if ($_POST['principal']) {
      $array_servicio = [
        "id_lavador" => $horario_lavador->id_lavador,
        "id_horario" => $horario_lavador->id,
        "id_servicio" => $_POST['id_servicio'],
        "fecha_programacion" => $horario_lavador->fecha . ' ' . $horario_lavador->hora,
        'pos' => $_POST['pos']
      ];
      //Actualizar el horario
      $this->db->where('id', $_POST['id'])->update('servicio_lavado', $array_servicio);
      $this->db->where('id', $horario_lavador->id)->set('ocupado', 1)->update('horarios_lavadores');
      $id_servicio_save = $_POST['id'];
    } else {
      //Replicar el servicio
      $servicio_actual = $this->mp->getById($_POST['id'], 'servicio_lavado');
      $servicio = [
        'id_usuario' => $servicio_actual->id_usuario,
        'id_auto' => $servicio_actual->id_auto,
        'id_servicio' => $_POST['id_servicio'],
        'puntos' => '',
        'comentarios' => $servicio_actual->comentarios,
        'calificacion' => '',
        'created_at' => date('Y-m-d H:i:s'),
        'status' => 1,
        'latitud_lavador' => '',
        'longitud_lavador' => '',
        'id_ubicacion' => $servicio_actual->id_ubicacion,
        'nombre_cliente' => '',
        'evidencia' => $servicio_actual->evidencia,
        'path_evidencia' => $servicio_actual->path_evidencia,
        'origen' => $servicio_actual->origen,
        'completo' => 0,
        "id_lavador" => $horario_lavador->id_lavador,
        "id_horario" => $horario_lavador->id,
        "fecha_programacion" => $horario_lavador->fecha . ' ' . $horario_lavador->hora,
        'id_servicio_pertenece' => $_POST['id'],
        'id_paquete' => $servicio_actual->id_paquete,
        'principal' => 0,
        'pos' => $_POST['pos'],
        'folio_mostrar' => getFolioSubPaquete($_POST['id']) . '-' . $_POST['pos']
      ];
      //Insertar el servicio con su horario
      $this->db->insert('servicio_lavado', $servicio);
      $id_servicio_save = $this->db->insert_id();
      $this->db->where('id', $horario_lavador->id)->set('ocupado', 1)->update('horarios_lavadores');
    }
    $detalle_servicios_paquetes = [
      'id_servicio' => $id_servicio_save,
      'id_horario' => $horario_lavador->id,
      'id_usuario_guardo' => $this->session->userdata('id'),
      'id_paquete' => $_POST['id_paquete'],
      'created_at' => date('Y-m-d H:i:s'),
      'pos' => $_POST['pos'],
      'fecha' => $_POST['fecha'],
      'hora' => $_POST['hora'],
      'id_lavador' => $horario_lavador->id_lavador,
      'id_servicio_principal' => $_POST['id'],
      'id_servicio_realizar' => $_POST['id_servicio'],
      'folio' => getFolioSubPaquete($_POST['id']) . '-' . $_POST['pos']
    ];
    $this->db->insert('detalle_servicios_paquetes', $detalle_servicios_paquetes);
    echo json_encode(array('exito' => true, 'id_servicio_lavado' => $id_servicio_save, 'id_horario' => $horario_lavador->id));
    exit();
  }
  public function actualizarHorario()
  {
    $horario_lavador = $this->mp->getIdLavador();
    if (!$horario_lavador) {
      echo json_encode(array('exito' => false, 'mensaje' => 'El horario ya fue ocupado, elige otro'));
      exit();
    }
    $array_servicio = [
      "id_lavador" => $horario_lavador->id_lavador,
      "id_horario" => $horario_lavador->id,
      "id_servicio" => $_POST['id_servicio'],
      "fecha_programacion" => $horario_lavador->fecha . ' ' . $horario_lavador->hora,
      'pos' => $_POST['pos']
    ];
    //Actualizar el horario
    $this->db->where('id', $_POST['id_servicio_lavado'])->update('servicio_lavado', $array_servicio);
    $this->db->where('id', $horario_lavador->id)->set('ocupado', 1)->update('horarios_lavadores');
    $this->db->where('id', $_POST['id_horario'])->set('ocupado', 0)->update('horarios_lavadores');
    $id_servicio_save = $_POST['id_servicio_lavado'];
    //Actualizar el detalle
    $array_detalle = [
      'id_horario' => $horario_lavador->id,
      'updated_at' => date('Y-m-d H:i:s'),
      'id_usuario_actualizo' => $this->session->userdata('id'),
      'fecha' => $_POST['fecha'],
      'hora' => $_POST['hora'],
      'id_lavador' => $horario_lavador->id_lavador
    ];
    $this->db->where('id_servicio', $_POST['id_servicio_lavado'])->where('pos', $_POST['pos'])->update('detalle_servicios_paquetes', $array_detalle);
    echo json_encode(array('exito' => true, 'id_servicio_lavado' => $id_servicio_save, 'id_horario' => $horario_lavador->id));
  }
  public function actualizar_paquete_completo()
  {
    $this->db->where('id', $_POST['id'])->set('completo', $_POST['completo'])->update('servicio_lavado');
    $this->mp->enviar_correo($_POST['id']);
    echo 1;
  }
  public function enviar_correo()
  {
    $this->mp->enviar_correo(14);
  }
  public function validar_orden_fechas()
  {
    $data = $this->db->where('pos <', $_POST['pos'])
      ->where('id_servicio', $_POST['id_servicio'])
      ->where('fecha >=', $_POST['fecha'])
      ->get('detalle_servicios_paquetes')
      ->result();
    if (count($data) > 0) {
      echo -1;
      exit();
    } else {
      echo 1;
      exit();
    }
  }
}
