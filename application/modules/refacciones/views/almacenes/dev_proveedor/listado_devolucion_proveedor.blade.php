@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h2 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h2>
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Folio:</label>
                <input type="text" name="folio" id="folio" class="form-control"/>
                <div id="folio_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Proveedor:</label>
                <select class="form-control" id="proveedor_id" name="proveedor_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($cat_proveedor))
                    @foreach ($cat_proveedor as $proveedor)
                        <option value="{{ $proveedor->id}}"> {{$proveedor->proveedor_numero}} - {{ $proveedor->proveedor_nombre }}</option>
                        @endforeach
                    @endif
                </select>
                <div id="proveedor_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4 mt-4">
            <button type="button" id="btn-limpiar" onclick="limpiarFiltro()" class="btn btn-primary">
                <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
            </button>
            <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary">
                <i class="fa fa-search" aria-hidden="true"></i> Filtrar
            </button>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_devolucion_proveedor" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('included_js')
@include('main/scripts_dt')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script src="{{ base_url('js/refacciones/salidas/devoluciones/listadodevolucionproveedor.js') }}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_salidas").addClass("show");
            $("#refacciones_salidas").addClass("active");
            $("#refacciones_salidas_sub").addClass("show");
            $("#refacciones_salidas_sub").addClass("active");
            $("#dev_a_prov").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection