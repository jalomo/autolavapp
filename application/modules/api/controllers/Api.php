<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Api extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->model('proactivo_agencias/M_proactivo_agencias', 'mp', TRUE);
    $this->load->model('M_transiciones', 'mt', TRUE);
    $this->load->model('servicios/M_servicios', 'ms', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set('America/Mexico_City');
  }

  public function ver_perfil($id){

    header('Content-Type: application/json');
    $response = array();

    if ( (!empty($id) && isset($id)) ){
      $this->db->where('id', $id);
      $data = $this->db->get('usuarios')->row();
     
      $response['nombre'] = $data->nombre;
      $response['apellido_paterno'] = $data->apellido_paterno;
      $response['apellido_materno'] = $data->apellido_materno;
      $response['email'] = $data->email;
      $response['telefono'] = $data->telefono;
      $response['error'] = false;

      
    }else{
      $response['nombre'] = '';
      $response['apellido_paterno'] = '';
      $response['apellido_materno'] = '';
      $response['email'] = '';
      $response['telefono'] = '';
      $response['error'] = true;
    }

    echo json_encode($response);
  }

  public function servicios_usuario_activos($id_usuario)
  {
    $this->db->where('id_usuario', $id_usuario);
    $this->db->where('status', 1);
    $data = $this->db->get('servicio_lavado')->result();
    echo json_encode($data);
  }

  public function validar_cupon(){
    $cupon = $this->input->post('cupon');
    $id_usuario = $this->input->post('id_usuario');
    $data = array();
    if ( (!empty($cupon) && isset($cupon)) && (!empty($id_usuario) && isset($id_usuario)) ){
        $data['exito'] = true;
        $data['descuento'] = 10;
        $data['id_cupon'] = 10;
        $data['mensaje'] = "cupon valido";

    }else{
         $data['exito'] = false;
         $data['mensaje'] = "cupon invalido";
    }
    echo json_encode($data);

  }


  public function servicio_zona(){
    $latitud = $this->input->post('latitud');
    $longitud = $this->input->post('longitud');
    $data = array();
    if ( (!empty($latitud) && isset($longitud)) && (!empty($longitud) && isset($longitud)) ){
        $data['exito'] = true;
        $data['mensaje'] = "zona valida";

    }else{
         $data['exito'] = false;
         $data['mensaje'] = "zona invalida";
    }
    echo json_encode($data);
  }

  public function enviar_sms(){
    $celular = $this->input->post('telefono');
    
    $data = array();
    if ( (!empty($celular) && isset($celular)) ){
            $celular_if = strlen($celular); 
            if( $celular_if == 10 ){
                
              $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
              $codigo_mensaje =  substr(str_shuffle($permitted_chars), 0, 4);
              $mensaje_sms = "XEHOS: ".$codigo_mensaje;

              $celular = $celular; //celular 
              $mensaje = $mensaje_sms; //mensaje 160 caracteres
              $sucursal = "XEHOS"; // sucursal
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje");
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS,
                          "celular=".$celular."&mensaje=".$mensaje."&sucursal=".$sucursal." ");
              // Receive server response ...
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              $server_output = curl_exec($ch);
              curl_close ($ch);
             // var_dump($server_output);


              $data['exito'] = true;
              $data['mensaje'] = "mensaje enviado";
              $data['codigo'] = $codigo_mensaje;
            }else{
                $data['exito'] = false;
                $data['mensaje'] = "telefono debe ser 10 digitos";
            }
        }else{
            
            $data['exito'] = false;
            $data['mensaje'] = "telefono invalido";
       
        }
        echo json_encode($data);
    

  }

  public function registro()
  {
    //validar si ya existe el correo
    $existe_usuario = $this->ms->existeUsuario();
    if ($existe_usuario) {
      echo json_encode(array('exito' => false, 'mensaje' => 'El usuario ya existe'));
      return;
    }
    $this->ms->guardarUsuario();
    $usuario = $this->Mgeneral->get_row('usuario_Id', $this->db->insert_id(), 'usuarios');
    echo json_encode(array('exito' => true, 'usuario' => $usuario));
  }

  public function login()
  {
    $email = $this->input->post('telefono');
    $password = $this->input->post('password');
    $this->db->where('telefono', $email);
    $this->db->where('password', $password);
    $data = $this->db->get('usuarios')->row();
    $response = [];
    if (is_object($data)) {
      $response['error'] = 0;
      $response['id'] = $data->usuarioID;
      $response['mensaje'] = "";
    } else {
      $response['error'] = 1;
      $response['mensaje'] = "Usuario o contraseña incorrectos";
    }
    echo json_encode($response);
  }

  public function prueba_notificacion($id_usuario)
  {

    $token = $this->Mapi->get_row('usuarioID', $id_usuario, 'usuarios');
    $this->load->library('Firebase');
    $firebase = new Firebase();
    $res = array();
    $res['data']['mensaje'] = "mensaje";
    $res['data']['id_servicio'] = "";
    $res['data']['foto'] = "";


    $response4 = $firebase->send($token->token, $res);

    var_dump($response4);
  }

  public function servicios()
  {
    header('Content-Type: application/json');
    $res = $this->Mgeneral->get_table('servicios');
    echo json_encode($res,JSON_NUMERIC_CHECK);
  }

  public function get_puntos()
  {
    $id_usuario = $this->input->post('id_usuario');
    $res = $this->Mgeneral->get_row('usuarioID', $id_usuario, 'usuarios');
    $data = [];
    if (is_object($res)) {
      $puntos = $res->usuarioPuntos;
    } else {
      $puntos = 0;
    }

    echo json_encode(array('exito' => true, 'puntos' => $puntos),JSON_NUMERIC_CHECK);
  }

  // public function get_horarios()
  // {
  //   $fecha = $this->input->post("fecha");
  //   $res = $this->db->like('date', $fecha)->get('appointment')->result();
  //   echo json_encode($res);
  // }
  public function registro_auto()
  {
    $this->ms->guardarAuto(0);
    $auto = $this->Mgeneral->get_row('id', $this->db->insert_id(), 'autos');
    echo json_encode(array('exito' => true, 'auto' => $auto),JSON_NUMERIC_CHECK);
  }
  public function estados()
  {
    $res = $this->Mgeneral->get_table('estados');
    echo json_encode($res);
  }
  public function municipios($id_municipio)
  {
    $res = $this->ms->getMunicipiosByEstado($id_municipio);
    echo json_encode($res);
  }
  //Obtiene los colores
  public function colores()
  {
    $res = $this->Mgeneral->get_table('cat_colores');
    echo json_encode($res,JSON_NUMERIC_CHECK);
  }
  //Obtiene las marcas
  public function marcas()
  {
    $res = $this->Mgeneral->get_table('cat_marcas');
    echo json_encode($res,JSON_NUMERIC_CHECK);
  }
  //Obtiene los modelos
  public function modelos($idmarca)
  {
    header("Content-Type: application/json; charset=UTF-8");
    $res = $this->ms->getModelosByMarca($idmarca);
    
    $response = array();

    foreach($res as $row):
      $data = array();
      $data['id'] = intval($row->id);
      $data['modelo'] = $row->modelo;
      $data['id_marca'] = intval($row->id_marca);
      $data['id_tipo_auto'] = intval($row->id_tipo_auto);
      array_push($response,$data);
    endforeach;

   
    echo json_encode($response);
  }
  //crear tabla y CRUD
  public function tipo_auto()
  {
    $data = $this->db->get('cat_tipo_autos')->result();
    echo json_encode($data,JSON_NUMERIC_CHECK);
  }

  public function indica_ubicacion()
  {
    $data = [
      'id_usuario' => $this->input->post('id_usuario'),
      'numero_int' => $this->input->post('numero_int'),
      'numero_ext' => $this->input->post('numero_ext'),
      'colonia' => $this->input->post('colonia'),
      'id_estado' => $this->input->post('id_estado'),
      'id_municipio' => $this->input->post('id_municipio'),
      'latitud' => $this->input->post('latitud'),
      'longitud' => $this->input->post('longitud'),
      'created_at' => date('Y-m-d H:i:s')
    ];
    $this->Mgeneral->save_register('ubicacion_usuario', $data);

    $ubicacion = $this->Mgeneral->get_row('id', $this->db->insert_id(), 'ubicacion_usuario');
    echo json_encode(array('exito' => true, 'ubicacion' => $ubicacion),JSON_NUMERIC_CHECK);
  }
  // obtener los carros de un usuario
  public function getAutosByUsuario($idusuario)
  {
    $autos = $this->db->where('id_usuario', $idusuario)->get('autos')->result();
    echo json_encode($autos,JSON_NUMERIC_CHECK);
  }
  //Obtener los horarios disponibles del día
  public function getHorariosDisponibles()
  {
    $fecha = $this->input->post('fecha');
    //$isoDate = new DateTime($fecha);
    
    $horarios = $this->ms->getHorariosDisponibles($fecha);
    echo json_encode($horarios,JSON_NUMERIC_CHECK);
  }
  //Guardar todo el servicio
  public function saveService()
  {
    $horario_lavador = $this->ms->getIdLavador();
    if (count($horario_lavador) == 0) {
      echo json_encode(array('exito' => false, 'mensaje' => 'El horario ya está ocupado'),JSON_NUMERIC_CHECK);
      return;
    }
    $horario_lavador = $horario_lavador[0];
    $cupon = '';
    //Validar cupón si existe
    if (isset($_POST['cupon'])) {
      $validar_cupon = $this->ms->validarCupon($_POST['cupon']);
      if (!$validar_cupon) {
        echo json_encode(array('exito' => false, 'mensaje' => 'El cupón no es válido'),JSON_NUMERIC_CHECK);
        return;
      }
      $cupon = $validar_cupon->cupon;
      //Desactivar cupón
      $this->db->where('id', $validar_cupon->id)->set('activo', 0)->update('cupones');
    }
    //Guardar ubicacion 
    $this->ms->guardarUbicacion(0, $_POST['id_usuario']);
    $id_ubicacion = $this->db->insert_id();

    $servicio = [
      'id_usuario' => $_POST['id_usuario'],
      'id_auto' => $_POST['id_auto'],
      'id_lavador' => $horario_lavador->id_lavador,
      'id_servicio' => $_POST['id_servicio'],
      'puntos' => $_POST['puntos'],
      'comentarios' => $_POST['comentarios'],
      'calificacion' => $_POST['calificacion'],
      'created_at' => date('Y-m-d H:i:s'),
      'status' => 1,
      'latitud_lavador' => $_POST['latitud_lavador'],
      'longitud_lavador' => $_POST['longitud_lavador'],
      'id_horario' => $horario_lavador->id,
      'id_ubicacion' => $id_ubicacion,
      'nombre_cliente' => isset($_POST['nombre_cliente']) ? $_POST['nombre_cliente'] : '',
      'cupon' => $cupon,
      'origen' => 2
    ];
    $lavador = $this->db->select('lavadorNombre,lavadorFoto,lavadorEmail,qr,id_id')->where('lavadorId', $horario_lavador->id_lavador)->get('lavadores')->row();
    $ubicacion = $this->db->where('id', $id_ubicacion)->get('ubicacion_usuario')->row();
    $this->db->insert('servicio_lavado', $servicio);
    $this->db->where('id', $horario_lavador->id)->set('ocupado', 1)->update('horarios_lavadores');
    echo json_encode(array('exito' => true, 'servicio' => $servicio, 'horario' => $horario_lavador, 'lavador' => $lavador, 'ubicacion' => $ubicacion),JSON_NUMERIC_CHECK);
  }
  //saber si ya está ocupado un horario
  public function validarHorario($fecha, $hora, $id_lavador)
  {
    $q = $this->db->where('fecha', $fecha)
      ->where('hora', $hora)
      ->where('hora', $id_lavador)
      ->where('activo', 1)
      ->where('ocupado', 0)
      ->limit(1)
      ->get('horarios_lavadores')
      ->result();
    return $q->num_rows();
  }
  public function cambiarStatus()
  {
    if ($_POST['id_status_actual'] == $_POST['id_status_nuevo']) {
      echo json_encode((array('exito' => false, "mensaje" => "Los estatus no deben ser iguales")),JSON_NUMERIC_CHECK);
      return;
    }
    //validar servicio
    $q = $this->db->where('id', $_POST['id_servicio'])->limit(1)->get('servicio_lavado');
    if ($q->num_rows() == 0) {
      echo json_encode((array('exito' => false, "mensaje" => "El servicio no existe")),JSON_NUMERIC_CHECK);
      return;
    }
    $this->mt->insertTransicion();
    echo json_encode((array('exito' => true, "mensaje" => "Estatus cambiado correctamente")),JSON_NUMERIC_CHECK);
  }
  public function get_status_lavado()
  {
    $data = $this->db->where('activo', 1)->get('cat_estatus_lavado')->result();
    echo json_encode($data,JSON_NUMERIC_CHECK);
  }
  public function generar_registros_dia()
  {
    $newdate = strtotime('-7 days', strtotime(date('Y-m-d')));
    $date = date('Y-m-d', $newdate);
    $this->db->where('url !=', '');
    $agencias = $this->Mgeneral->get_table('catalogo_agencias');
    foreach ($agencias as $a => $agencia) {
      //validar si ya se generó
      if (!$this->mp->validarCPAgenciaFecha($date, $agencia->id)) {
        $info = json_decode($this->mp->getDataCPAgenciaFecha($date, $agencia->id));
        foreach ($info->data as $d => $registro) {
          $data_array = [
            'id_agencia' => $agencia->id,
            'id_cita' => $registro->id_cita,
            //'fecha_programacion' => date($registro->fecha_entrega_unidad),
            'fecha_programacion' => $date,
            'numero_interno' => $registro->numero_interno,
            'nombre_compania' => $registro->nombre_compania,
            'nombre_contacto_compania' => $registro->nombre_contacto_compania,
            'ap_contacto' => $registro->ap_contacto,
            'am_contacto' => $registro->am_contacto,
            'rfc' => $registro->rfc,
            'correo_compania' => $registro->correo_compania,
            'calle' => $registro->calle,
            'nointerior' => $registro->nointerior,
            'noexterior' => $registro->noexterior,
            'colonia' => $registro->colonia,
            'municipio' => $registro->municipio,
            'cp' => $registro->cp,
            'estado' => $registro->estado,
            'telefono_movil' => $registro->telefono_movil,
            'otro_telefono' => $registro->otro_telefono,
            'kilometraje' => $registro->km,
            'transmision' => $registro->transmision,
            'numero_cliente' => $registro->numero_cliente,
            'color' => $registro->color,
            'marca' => $registro->vehiculo_marca,
            'tipo_cliente' => $registro->tipo_cliente,
            'cilindros' => $registro->cilindros,
            'email' => $registro->email,
            'vehiculo_anio' => $registro->vehiculo_anio,
            'modelo' => $registro->vehiculo_modelo,
            'version' => $registro->vehiculo_version,
            'placas' => $registro->vehiculo_placas,
            'serie' => $registro->vehiculo_numero_serie,
            'nombre' => $registro->datos_nombres,
            'apellido_paterno' => $registro->datos_apellido_paterno,
            'apellido_materno' => $registro->datos_apellido_materno,
            'telefono' => $registro->datos_telefono,
            'servicio' => $registro->estatus_cita
          ];
          $this->db->insert('contacto_proactivo_agencias', $data_array);
        }
        $datos_proactivo = array(
          'fecha' => $date,
          'id_agencia' => $agencia->id,
          'idusuario' => $this->session->userdata('id'),
          'created_at' => date('Y-m-d H:i:s')
        );
        //Insertar que ya se hizo el del mes
        $this->db->insert('proactivo_dia_agencias', $datos_proactivo);
      }
    }
  }
}
