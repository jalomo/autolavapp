<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Modelo extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta(){
      $data['titulo'] = "Medidas" ;
      $data['titulo_dos'] = "Alta Medidas" ;
      $data['rows'] = $this->Mgeneral->get_table('servicios');
      $this->blade->render('modelo/alta', $data);
    }

    public function alta_guardar(){
     var_dump($_FILES['image']['name']);
      $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['image']['name']);
      $path_to_save = 'statics/logo/';
      if(!file_exists($path_to_save)){
        mkdir($path_to_save, 0777, true);
      }
      move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save.$name);
      $data['servicioImagen'] = $path_to_save.$name;
      $data['servicioNombre'] = $this->input->post('servicioNombre');
      $data['servicioPuntos'] = $this->input->post('servicioPuntos');
     $this->Mgeneral->save_register('servicios', $data);
    }

}
