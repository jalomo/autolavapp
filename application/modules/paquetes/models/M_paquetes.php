<?php
class M_paquetes extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function guardarUbicacion($id_servicio = 0)
    {
        //ubicación
        $ubicacion = [
            'numero_int' => $this->input->post('numero_int'),
            'numero_ext' => $this->input->post('numero_ext'),
            'colonia' => $this->input->post('colonia'),
            'id_estado' => $this->input->post('id_estado'),
            'id_municipio' => $this->input->post('id_municipio'),
            'id_sucursal' => $this->input->post('id_sucursal'),
            'latitud' => $this->input->post('latitud'),
            'longitud' => $this->input->post('longitud'),
            'calle' => $this->input->post('calle'),
            'created_at' => date('Y-m-d H:i:s')
        ];
        if ($id_servicio == 0) {
            $this->Mgeneral->save_register('ubicacion_servicio', $ubicacion);
        } else {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('id', $id_servicio)->update('ubicacion_servicio', $ubicacion);
        }
    }
    public function guardarAuto($id, $id_usuario = 0)
    {
        //auto
        $data['placas'] = $this->input->post('placas');
        $data['id_color'] = $this->input->post('id_color');
        $data['id_modelo'] = $this->input->post('id_modelo');
        $data['id_marca'] = $this->input->post('id_marca');
        $data['id_anio'] = $this->input->post('id_anio');
        $data['numero_serie'] = $this->input->post('numero_serie');
        $data['kilometraje'] = $this->input->post('kilometraje');
        $data['id_usuario'] = $id_usuario;
        $data['created_at'] = date('Y-m-d H:i:s');
        if ($id == 0) {
            $this->Mgeneral->save_register('autos', $data);
        } else {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('id', $id)->update('autos', $data);
        }
    }
    public function guardarDatosFacturación($id, $id_usuario)
    {
        //datos facturación
        $data['id_usuario'] = $id_usuario;
        $data['referencia'] = $this->input->post('referencia');
        $data['razon_social'] = $this->input->post('razon_social');
        $data['domicilio'] = $this->input->post('domicilio');
        $data['email_facturacion'] = $this->input->post('email_facturacion');
        $data['id_cfdi'] = $this->input->post('id_cfdi');
        $data['id_metodo_pago'] = $this->input->post('id_metodo_pago');
        $data['cp'] = $this->input->post('cp');
        $data['rfc'] = $this->input->post('rfc');
        $data['created_at'] = date('Y-m-d H:i:s');
        if ($id == 0) {
            $this->Mgeneral->save_register('datos_facturacion', $data);
        } else {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('id', $id)->update('datos_facturacion', $data);
        }
    }
    //guarda los lavadores
    public function guardarServicio()
    {
        $id = $this->input->post('id');
        $id_usuario = $this->input->post('id_usuario');
        $id_ubicacion = $this->input->post('id_ubicacion');
        $id_auto = $this->input->post('id_auto');
        $id_facturacion = $this->input->post('id_facturacion');

        //Validar el usuario
        if ($this->existeUsuario($id_usuario)) {
            echo -3;
            exit();
            return;
        }
        //Guardar usuario
        $this->guardarUsuario($id_usuario);
        if ($id_usuario == 0) {
            $id_usuario = $this->db->insert_id();
        }
        //guardar el auto
        $this->guardarAuto($id_auto, $id_usuario);
        if ($id_auto == 0) {
            $id_auto = $this->db->insert_id();
        }

        //Guardar ubicación
        $this->guardarUbicacion($id_ubicacion, $id_usuario);
        if ($id_ubicacion == 0) {
            $id_ubicacion = $this->db->insert_id();
        }
        //Si llega guardar la facturación
        if (isset($_POST['check_facturacion'])) {
            $this->guardarDatosFacturación($id_facturacion, $id_usuario);
            if ($id_facturacion == 0) {
                $id_facturacion = $this->db->insert_id();
            }
        }
        $servicio = [
            'id_usuario' => $id_usuario,
            'id_auto' => $id_auto,
            'puntos' => 1,
            'comentarios' => $_POST['comentarios'],
            'calificacion' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
            'latitud_lavador' => 1,
            'longitud_lavador' => 1,
            'id_ubicacion' => $id_ubicacion,
            'nombre_cliente' => '',
            'cupon' => '',
            'origen' => 1,
            'id_paquete' => $_POST['id_paquete'],
            'completo' => 0,
            'principal' => 1,
            'id_sucursal' => $_POST['id_sucursal_ubicacion'],
            'id_sucursal_creo' => $this->session->userdata('id_sucursal'),
            'id_tipo_pago' => 2,
            'subtotal' => getPrecioServicio($_POST['id_modelo'],$_POST['id_servicio']),
            'total' => getPrecioServicio($_POST['id_modelo'],$_POST['id_servicio']),
        ];
        if ($id == 0) {
            $servicio['id_usuario_creo'] = $this->session->userdata('id');
            $servicio['folio_mostrar'] = getFolioMostrar($_POST['id_sucursal'], false);
            $servicio['folio_control'] = getFolioControl();
            $this->db->insert('servicio_lavado', $servicio);
            $id_servicio_return = $this->db->insert_id();
        } else {
            $servicio['updated_at'] = date('Y-m-d H:i:s');
            $servicio['id_usuario_actualizo'] = $this->session->userdata('id');
            $this->db->where('id', $id)->update('servicio_lavado', $servicio);
            $id_servicio_return = $id;
        }
        echo $id_servicio_return;
        exit();
    }
    //Obtiene municipios por estado
    public function getMunicipiosByEstado($id_estado = '')
    {
        return $this->db->where('id_estado', $id_estado)->get('cat_municipios')->result();
    }
    //Obtiene sucursales por estado
    public function getSucursalesByEstado($id_estado = '')
    {
        return $this->db->where('id_estado', $id_estado)->get('sucursales')->result();
    }
    //Obtiene marcas por modelo
    public function getModelosByMarca($id_marca)
    {
        return $this->db->where('id_marca', $id_marca)->get('cat_modelos')->result();
    }
    //Obtener las fechas
    public function getFechasLavadores($fecha = '', $fecha_fin = '')
    {
        if ($fecha != '') {
            $this->db->where('fecha >=', $fecha);
        } else {
            $this->db->where('fecha >=', date('Y-m-d'));
        }
        if ($fecha_fin != '') {
            $this->db->where('fecha <=', $fecha_fin);
        }
        return $this->db
            ->select('DISTINCT(fecha) as fecha')
            ->order_by('fecha', 'asc')
            ->get('horarios_lavadores')
            ->result();
    }
    public function getFechasByLavador($id_lavador = '')
    {
        return $this->db->where('fecha >=', date('Y-m-d'))
            ->select('fecha')
            ->where('id_lavador', $id_lavador)
            ->order_by('fecha', 'asc')
            ->get('horarios_lavadores')
            ->result();
    }
    public function getFechasDisponiblesLavador($id_lavador)
    {
        return $this->db->where('fecha >=', date('Y-m-d'))
            ->where('id_lavador', $id_lavador)
            ->select('DISTINCT(fecha) as fecha')
            ->get('horarios_lavadores')
            ->result();
    }
    public function getIdLavador()
    {
        $q = $this->db
            ->where('fecha', $_POST['fecha'])
            ->where('hora', $_POST['hora'])
            ->where('id_sucursal', $_POST['id_sucursal_ubicacion'])
            ->where('activo', 1)
            ->where('ocupado', 0)
            ->get('horarios_lavadores');
        if ($q->num_rows() == 1) {
            return $q->row();
        } else {
            $id_lavador_siguiente = $this->getLavadorSiguiente();
            $array_lavadores = [];
            foreach($q->result() as $l => $lavador){
                $array_lavadores[] = $lavador->id_lavador;
            }
            //Puede ser que existen 3 lavadores con el horario por ej. 18:00 (4,5,6) pero el lavador siguiente es el 1 entonces debo validar
            if(!in_array($id_lavador_siguiente,$array_lavadores)){
                $id_lavador_siguiente = $array_lavadores[0];
            }
            $q = $this->db->where('fecha', $_POST['fecha'])
                ->where('hora', $_POST['hora'])
                ->where('activo', 1)
                ->where('ocupado', 0)
                ->where('id_lavador', $id_lavador_siguiente)
                ->get('horarios_lavadores');
            return $q->row();
        }
    }
    //obtiene los lavadores
    public function getLavadores()
    {
        return $this->db->where('id_sucursal', $_POST['id_sucursal_ubicacion'])
            ->where('activo', 1)
            ->get('lavadores')
            ->result();
    }
    public function guardarUsuario($id = 0)
    {
        $data['usuarioID'] = get_guid();
        $data['nombre'] = $this->input->post('nombre');
        $data['apellido_paterno'] = $this->input->post('apellido_paterno');
        $data['apellido_materno'] = $this->input->post('apellido_materno');
        $data['email'] = $this->input->post('email');
        $data['telefono'] = $this->input->post('telefono');
        $data['password'] = substr($this->input->post('telefono'), -5);
        $data['id_sucursal'] = $this->input->post('id_sucursal');
        $data['token'] = $this->input->post('token');
        if ($id == 0) {
            return $this->Mgeneral->save_register('usuarios', $data);
        } else {
            return $this->db->where('id', $id)->update('usuarios', $data);
        }
    }
    //Validar si existe el usuario
    public function existeUsuario($id = 0)
    {
        if ($id != 0) {
            $this->db->where('id !=', $id);
        }
        $existe_usuario = $this->Mgeneral->get_result('email', $_POST['email'], 'usuarios');
        if (count($existe_usuario) > 0) {
            return true;
        }
        return false;
    }
    public function getById($id, $tabla)
    {
        return $this->db->where('id', $id)->get($tabla)->row();
    }
    //Get nombre lavador 
    public function getLavadorById($id)
    {
        return $this->db->where('lavadorId', $id)->get('lavadores')->row();
    }
    public function getDatosFacturacionByIdUsuario($id_usuario = 0)
    {
        return $this->db->where('id_usuario', $id_usuario)->get('datos_facturacion')->row();
    }
    public function getAll()
    {
        if($this->session->userdata('id_rol')!=1){
            $this->db->where('id_sucursal',$this->session->userdata('id_sucursal'));
        }
        return $this->db->where('id_paquete is not null')->get('v_info_servicios')->result();
    }
    public function getPaquetes()
    {
        if($this->session->userdata('id_rol')!=1){
            $this->db->where('id_sucursal',$this->session->userdata('id_sucursal'));
        }
        return $this->db->where('id_paquete is not null')->where('completo', 0)->where('principal', 1)->get('v_info_paquetes')->result();
    }
    //Datos del usuario
    public function getDataUser($id = '')
    {
        return $this->db->where('u.id', $id)
            ->select('u.id as id_usuario,u.email,u.password,u.nombre,u.apellido_paterno,u.apellido_materno,u.telefono,u.id_sucursal,d.referencia,d.razon_social,d.domicilio,d.email_facturacion,d.id_cfdi,
                        d.id_forma_pago,d.id_metodo_pago,d.rfc,ub.numero_int,ub.numero_ext,ub.colonia,ub.id_estado,ub.id_municipio,ub.latitud,ub.longitud,ub.calle,ub.id as id_ubicacion,d.id as id_facturacion')
            ->join('datos_facturacion d', 'u.id = d.id_usuario', 'left')
            ->join('ubicacion_usuario ub', 'u.id = ub.id_usuario')
            ->get('usuarios u')
            ->row();
    }
    public function getVehiculosUser($id_usuario = '')
    {
        return $this->db->where('a.id_usuario', $id_usuario)
            ->join('cat_marcas cm', 'cm.id = a.id_marca')
            ->join('cat_modelos cmd', 'cmd.id = a.id_modelo')
            ->select('a.*,cm.marca,cmd.modelo')
            ->get('autos a')->result();
    }
    public function getAutoByPlacas($placas)
    {
        return $this->db->where('placas', $placas)->get('autos')->row();
    }
    public function getIdHorarioActual($id = '')
    {
        $q = $this->db->where('id', $id)->select('id_horario')->get('servicio_lavado');
        if ($q->num_rows() == 1) {
            return $q->row()->id_horario;
        } else {
            return '';
        }
    }
    //Información del horario
    public function getInfoHorario($id)
    {
        return $this->db->where('h.id', $id)->join('lavadores l', 'l.lavadorId=h.id_lavador')->get('horarios_lavadores h')->row();
    }
    //Obtener tipo de auto
    public function getTipoAuto($id_modelo = '')
    {
        $q = $this->db->where('id', $id_modelo)
            ->get('cat_modelos');
        if ($q->num_rows() == 1) {
            return $q->row()->id_tipo_auto;
        } else {
            return 0;
        }
    }
    //Precio paquete
    public function getPrecioPaquete($id_tipo_auto = '', $id_paquete = '')
    {
        $q = $this->db->where('id_tipo_auto', $id_tipo_auto)
            ->where('id_paquete', $id_paquete)
            ->limit(1)
            ->get('precios_paquetes');
        if ($q->num_rows() == 1) {
            return $q->row()->precio;
        } else {
            return 0;
        }
    }
    //Días paquete
    public function getDiasPaquete($id_paquete = '')
    {
        $q = $this->db->where('id', $id_paquete)
            ->get('paquetes');
        if ($q->num_rows() == 1) {
            return $q->row()->dias;
        } else {
            return 0;
        }
    }
    //Descripción del paquete
    public function descripcionPaquete($id_paquete = '')
    {
        return $this->db->where('id_paquete', $id_paquete)
            ->join('servicios s', 'p.id_servicio = s.servicioId')
            ->join('paquetes paq', 'p.id_paquete = paq.id')
            ->select('p.id,p.cantidad,s.servicioNombre,,paq.descripcion,s.servicioId')
            ->get('paquetes_lavado p')
            ->result();
    }
    //Información del paquete
    public function getDataPaquete($id_paquete = '')
    {
        return $this->db->where('id', $id_paquete)->get('paquetes')->row();
    }
    //Obtener lo guardado del horario y su servicio y pos
    public function getDetalleHorario($id_servicio = '', $pos = '', $principal = '')
    {
        //Principal es 1 cuando es el servicio principal
        if ($principal) {
            return $this->db->where('id_servicio', $id_servicio)->where('pos', $pos)->get('detalle_servicios_paquetes')->row();
        } else {
            $qs = $this->db->where('id_servicio_pertenece', $id_servicio)->where('pos', $pos)->get('servicio_lavado');
            if ($qs->num_rows() == 1) {
                $id_servicio_lavado = $qs->row()->id;
            } else {
                $id_servicio_lavado = 0;
            }
            return $this->db->where('id_servicio', $id_servicio_lavado)->where('pos', $pos)->get('detalle_servicios_paquetes')->row();
        }
        //Siempre va llegar el id del servicio principal y con la posición debo sacar que servicio es
    }
    public function enviar_correo($idservicio = '')
    {
        $datos_usuario = $this->db->where('s.id', $idservicio)
            ->join('usuarios u', 's.id_usuario = u.id')
            ->join('paquetes p', 's.id_paquete = p.id')
            ->select('u.nombre,u.apellido_paterno,u.apellido_materno,u.email,p.paquete,p.descripcion')
            ->get('servicio_lavado s')
            ->row();
        $datos_correo = [
            'nombre' => $datos_usuario->nombre,
            'apellido_paterno' => $datos_usuario->apellido_paterno,
            'apellido_materno' => $datos_usuario->apellido_materno,
            'paquete' => $datos_usuario->paquete,
            'descripcion_paquete' => $datos_usuario->descripcion,
        ];
        $horarios = $this->db->where('id_servicio_principal', $idservicio)
            ->join('lavadores l', 'l.lavadorId = d.id_lavador')
            ->join('servicios s', 's.servicioId=d.id_servicio_realizar')
            ->select('d.*,l.lavadorNombre,s.servicioNombre')
            ->order_by('fecha,hora', 'asc')
            ->get('detalle_servicios_paquetes d')
            ->result();
        $cuerpo = $this->blade->render('confirmacion_paquete', array('datos_correo' => $datos_correo, 'id_servicio' => $idservicio, 'asunto' => '¡Asignación de paquete!', 'horarios' => $horarios), true);
        enviar_correo($datos_usuario->email, "Confirmación de paquete!", $cuerpo, array());
    }
    //obtener el lavador que sigue
    public function getLavadorSiguiente()
    {
        $lavadores = $this->getLavadores();
        $q = $this->db->limit(1)->select('s.id_lavador')
            ->order_by('s.id', 'desc')
            ->where('id_lavador !=', '')
            ->where('s.id_ubicacion', $_POST['id_sucursal_ubicacion'])
            ->join('ubicacion_servicio u', 's.id_ubicacion = u.id')
            ->get('servicio_lavado s');
        if ($q->num_rows() == 1) {
            $id_lavador = $q->row()->id_lavador;
            //Validar si es el último lavador, regresar al primero
            if ($id_lavador == null) {
                return $lavadores[0]->lavadorId;
            }
            if ($id_lavador >= count($lavadores)) {
                return $lavadores[0]->lavadorId;
            } else {
                $q = $this->db->limit(1)->select('lavadorId')->order_by('lavadorId', 'asc')->where('activo', 1)->where('lavadorId >', $id_lavador)->get('lavadores');
                if ($q->num_rows() == 1) {
                    return $q->row()->lavadorId;
                } else {
                    return 1;
                }
            }
        } else {
            return 1;
        }
    }
    public function getFolioSubpaquete($idservicio)
    {
        $q = $this->db->where('id', $idservicio)->select('folio_mostrar')->get('servicio_lavado');
        if ($q->num_rows() == 1) {
            return $q->row()->folio_mostrar;
        } else {
            return '';
        }
    }
    //Funcion que devuelve si un usuario tiene paquetes pendientes por asignar
    public function existenPaquetesIncompletos()
    {
        $q = $this->db->where('completo', 0)->where('principal !=', 1)->where('id_paquete is not null')->where('id_usuario_creo', $this->session->userdata('id'))->get('servicio_lavado')->result();
        if (count($q) > 0) {
            return true;
        }
        return false;
    }
}
