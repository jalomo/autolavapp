@layout('layout')
@section('contenido')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maps.google.com/maps/api/js?libraries=drawing&key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM"></script>

<button onclick="goBack()" class="btn btn-info">Atras</button>

<style>
#vertices {
  height: 100px;
  width: 500px;
}</style>
<div id="map_canvas" style="width:500px; height:450px;"></div>




<textarea height="100" wid name="vertices" value="" id="vertices"  ></textarea>
<br/>
<button id="save" class="btn btn-primary">Guarda zona</button>

@endsection
@section('included_js')
    @include('main/scripts_dt')

    <script>
function goBack() {
  window.history.back();
}
var polygons = [];
var aux_coordenadas = "";
var map; // Global declaration of the map
			var iw = new google.maps.InfoWindow(); // Global declaration of the infowindow
			var lat_longs = new Array();
			var markers = new Array();
			var drawingManager;
			function initialize() {
				 var myLatlng = new google.maps.LatLng(20.6703146, -103.3597487);
				var myOptions = {
			  		zoom: 13,
					center: myLatlng,
			  		mapTypeId: google.maps.MapTypeId.ROADMAP}
				map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
				drawingManager = new google.maps.drawing.DrawingManager({
				drawingMode: google.maps.drawing.OverlayType.POLYGON,
  				drawingControl: true,
  				drawingControlOptions: {
  					position: google.maps.ControlPosition.TOP_CENTER,
					drawingModes: [google.maps.drawing.OverlayType.POLYGON]
				},
						polygonOptions: {
							editable: true
						}
			});
            drawingManager.setMap(map);
            
            const triangleCoords = [
                <?php if(is_array($poligono)):?>
                    <?php foreach($poligono as $poli):?>
                        { lat: <?php echo $poli->poligonoLatitud?>, lng: <?php echo $poli->poligonoLongitud?> },
                    <?php endforeach;?>
                <?php endif;?>
            ];
            // Construct the polygon.
            const bermudaTriangle = new google.maps.Polygon({
                paths: triangleCoords,
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0.35,
            });
            bermudaTriangle.setMap(map);


			
			google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
				var newShape = event.overlay;
                newShape.type = event.type;
                //polygons.push(newShape);
			});

            google.maps.event.addListener(drawingManager, "overlaycomplete", function(event){
                overlayClickListener(event.overlay);
                var values = event.overlay.getPath().getArray();
                for (var i = 0; i < values.length; i++){
                	console.log("lat:", values[i].lat());
                    console.log("lng:", values[i].lng());
                    polygons.push(values[i].lat()+","+values[i].lng());
                    //polygons.push(values[i].lng());
                    
                    
                }
                $('#vertices').val(event.overlay.getPath().getArray());
            });
        }
function overlayClickListener(overlay) {
    google.maps.event.addDomListener(overlay, "click", function(event){
    		console.log("Changing this guy!");
        $('#vertices').val(overlay.getPath().getArray());
    });
}
initialize();

$(function(){
    $('#save').click(function(){
    	
        /*vertices = "";
        for (var i=0; i<polygons.length; i++) {
            //vertices += "polygon "+i+"<br>";
            for (var j=0; j<polygons[i].getPath().getLength(); j++) {
                vertices += polygons[i].getPath().getAt(j).toUrlValue(6)+",";
            }
        }
        */

        value_json = $.ajax({
         type: "POST",
         url: '<?php echo base_url();?>index.php/zonas/guarda_zona/{{$id_ciudad}}',
         data:{poligono:polygons,idUser:$("#id_user").val()},
         async: true,
         dataType: "text",
          success: function(data){
            alert('poligono agregado');
            console.log(data);
            //location.reload();
            window.history.back();
          }
         }).responseText;
    });
});
</script>

    @endsection
