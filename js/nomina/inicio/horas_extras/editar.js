var _appsFunction = function () {

	this.addContenido = function(){
		var id = $('select[name=trabajador] option:selected').val();
		window.location.href = PATH + '/nomina/inicio/horas_extras/alta/'+id;
	},
	this.guardar = function () {

		var data_send =  $('form#general_form').serializeArray();
		data_send.push({ 'name': 'id','value':id });

		 $.ajax({
			 dataType: "json",
			 type: 'POST',
			 url: PATH + '/nomina/inicio/horas_extras/editar_guardar',
			 data: data_send,
			 success: function (response, status, xhr) {
 
				 if (response.status == 'success') {
					Swal.fire({
						 icon: 'success',
						 title: '',
						 text: response.message,
						 confirmButtonText: "Aceptar"
					 }).then((result) => {
						window.location.href = PATH + '/nomina/inicio/horas_extras/index/' + btoa(id_trabajador);
					 });
				 }
 
			 }
 
		 });
	},	

	this.get = function () {

		

		if(id_trabajador != false){
			$('select[name="trabajador"]').attr('attr-id',id_trabajador);
		}
		if(numSemana != false){
			$('select[name="fechas"]').attr('attr-id',numSemana);
		}

		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if($.trim(id).length > 0){
				$(this).find('option[value="'+id+'"]').attr("selected", "selected");
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				});
			}else{
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				}).val('').change();
			}                        
		});

	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	// Apps.init();
});
