<?php
class M_Inventario extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_table($table){
        $data = $this->db->get($table)->result();
        return $data;
    }

    public function get_table_limit($campo,$tabla){
        $result = $this->db->order_by($campo,"DESC")->limit(500)->get($tabla)->result();
        return $result;
    }

    public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->get($tabla)->result();
        return $result;
    }

    public function get_result_one($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->order_by("id","DESC")->limit(1)->get($tabla)->result();
        return $result;
    }

    public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        $result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
        return $result;
    }

  	public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function update_table_row($table,$data,$id_table,$id){
    		$result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
  	}

    public function recuperar_unidad($id_lavador='')
    {
        $q = $this->db->where('id_lavador',$id_lavador)->select('id')->get('unidades');
        if($q->num_rows()==1){
            $retorno = $q->row()->id;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function nombre_lavador($id_lavador='')
    {
        $q = $this->db->where('lavadorId',$id_lavador)->select('lavadorNombre')->get('lavadores');
        if($q->num_rows()==1){
            $retorno = $q->row()->lavadorNombre;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function buscar_inventario($id_lavador='')
    {
        $q = $this->db->where('id_lavador',$id_lavador)
            ->where('fecha_llenado',date("Y-m-d"))
            ->select('id')
            ->get('diag_unidades_lavadores');

        if($q->num_rows()==1){
            $retorno = $q->row()->id;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function nombre_usuario($id=''){
        $q = $this->db->where('adminId',$id)->select('adminNombre,adminUsername')->get('admin');
        if($q->num_rows()==1){
            $usuario = (($q->row()->adminNombre != "") ? $q->row()->adminNombre : $q->row()->adminUsername);
            $retorno = $usuario;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function listado_inventarios($fecha_previa)
    {
        $result = $this->db->where('diag.fecha_llenado >=',$fecha_previa)
            ->join('unidades AS uni','uni.id = diag.id_unidad','left')
            ->select('diag.lavador, diag.id_unidad, diag.fecha_alta, diag.supervisor, diag.fecha_revision, diag.firma_supervisor, diag.id, uni.placas, uni.n_serie, uni.unidad')
            ->order_by("diag.id","DESC")
            ->limit(500)
            ->get('diag_unidades_lavadores AS diag')
            ->result();
            //echo $this->db->last_query();die();
        return $result;
    }


}