<link rel="stylesheet" href="<?php echo base_url()?>statics/node_modules/fullcalendar/dist/fullcalendar.min.css"  type="text/css">
	<link rel="stylesheet" href="<?php echo base_url()?>statics/node_modules/fullcalendar/dist/fullcalendar.print.min.css"  type="text/css" media="print">
	

	<script type="text/javascript" src="<?php echo base_url()?>statics/node_modules/moment/min/moment.min.js"></script>
	<!--script type="text/javascript" src="<?php echo base_url()?>statics/node_modules/jquery/dist/jquery.min.js"></script-->
	<script type="text/javascript" src="<?php echo base_url()?>statics/node_modules/fullcalendar/dist/fullcalendar.min.js"></script>
	<!--script type="text/javascript" src="<?php echo base_url()?>statics/node_modules/bootstrap/dist/js/bootstrap.min.js"></script-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<style type="text/css">
		.fc-time{
   display : none;
}
.fc-title{
    font-size: .9em;
}
	</style>

	<script type="text/javascript">
	$(document).ready(function() {

		$('#datetimepicker3').datetimepicker({
                    format: 'hh:mm'
                });
		$('#datetimepicker4').datetimepicker({
                    format: 'hh:mm'
                });

	
		$('#calendar').fullCalendar({
      //hiddenDays: [ 0,4,5,6 ],
      dayRender: function(date, cell){
        
				
    },
      monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
      monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
      dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
      dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
			dayClick: function(date, jsEvent, view) {

      

				
				
				$('#fecha').val(date.format('YYYY-MM-DD HH:mm:ss'));
		       $('#myModal').modal('toggle');

		       jQuery('#save').click(function () {
			    	$.ajax({
			    		type : 'POST',
			    		data : {  date_appointment: $('#fecha').val(),
												id_lavador:$('#id_lavador').val(),hora_inicio:$('#hora_inicio').val(),hora_fin:$('#hora_fin').val()},
			    		url : "<?php echo base_url();?>index.php/lavadores/add/",
			    		beforeSend: function( xhr ) {
			    			var newEvent = {
				                title: $('#fecha').val(),
				                start: date.format('YYYY-MM-DD HH:mm:ss')
				            }
			    			$('#calendar').fullCalendar('renderEvent', newEvent);
			    		},
			    		success: function(result){
			    			location.reload();

			    		}
			    	})
			    });



			},
			header: {
				//left: 'prev,next today',
				left: 'prev,next',
				center: 'title',
				//right: 'month,agendaDay,listWeek'
			},
			eventClick: function(calEvent, jsEvent, view) {
				
	    	},
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: '<?php echo base_url();?>index.php/lavadores/ajaxevent' // get event list from JSON format,
			
		});
	});
	</script>


	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-8">
				<div id='calendar'></div>
			</div>
			
		</div>

	</div>





<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Horario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-12">
      			<input type="text" name="" id="id_lavador" value="<?php echo $id_lavador?>">
      			<!--div class="form-group">
                            <label for="cc-exp" class="control-label mb-1">Lavador</label>
                            <select name="productoMedida" id="id_lavador" class="form-control form-control-sm" >
                              <?php foreach($lavadores as $row):?>
                              <option value="<?php echo $row->lavadorId;?>"><?php echo $row->lavadorNombre?></option>
                              <?php endforeach;?>
                            </select>
                          </div-->
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-5">
      			<div class='input-group date' id='datetimepicker3'>
		             <input type='text' class="form-control" id="hora_inicio" />
		                <span class="input-group-addon">
		                       <span class="glyphicon glyphicon-time"></span>
		                 </span>
		        </div>
      		</div>
      		<div class="col-2">
      			A
      		</div>
      		<div class="col-5">
      			<div class='input-group date' id='datetimepicker4'>
		             <input type='text' class="form-control" id="hora_fin" />
		                <span class="input-group-addon">
		                       <span class="glyphicon glyphicon-time"></span>
		                 </span>
		        </div>
      		</div>
      	</div>
      	

        <input type="hidden" name="" id="fecha">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="save">Agregar</button>
      </div>
    </div>
  </div>
</div>