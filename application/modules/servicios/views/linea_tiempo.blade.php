@layout('layout')
@section('contenido')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Folio</th>
                                    <th>Servicio</th>
                                    <th>Lavador</th>
                                    <th>Estatus anterior</th>
                                    <th>Estatus nuevo</th>
                                    <th>Inicio</th>
                                    <th>Fin</th>
                                    <th>Minutos transcurridos</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($info as $i => $servicio)
                                    <tr>
                                        <td>{{ $servicio->folio_mostrar }}</td>
                                        <td>{{ $servicio->servicio }}</td>
                                        <td>{{ $servicio->lavador }}</td>
                                        <td>{{ $servicio->status_anterior }}</td>
                                        <td>{{ $servicio->status_nuevo }}</td>
                                        <td>{{ $servicio->inicio }}</td>
                                        <td>{{ $servicio->fin }}</td>
                                        <td>{{ $servicio->minutos_transcurridos }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')
@endsection
