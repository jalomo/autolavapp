<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Unidades extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    //$this->load->model('M_lavadores', 'ml', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url', 'astillero'));

    date_default_timezone_set('America/Mexico_City');

    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
  }

  public function alta()
  {
    $data['rendimientos'] = $this->Mgeneral->get_table('combustible_autolavapp.catalogo_rendimiento');
    $data['titulo'] = "Unidades";
    $data['titulo_dos'] = "Alta unidades";
    $data['sucursales'] = $this->db->get('sucursales')->result();
    $data['unidades'] = $this->db->get('unidades')->result();
    $data['lavadores'] = $this->db->get('lavadores')->result();
    $this->blade->render('unidades/alta', $data);
  }

  public function alta_guardar(){
    
    $data = $this->input->post("save");
    $data['fecha_creacion'] = date('Y-m-d H:i:s');
    $id_unidad = $this->Mgeneral->save_register('unidades', $data);

    
    $data_asignacion['id_lavador'] = $this->input->post("id_lavador");
    $data_asignacion['id_unidad'] = $id_unidad ;
    $data_asignacion['fecha_asignacion'] = date('Y-m-d H:i:s');
    $data_asignacion['status'] = 1;
    $this->Mgeneral->save_register('lavadores_unidades', $data_asignacion);
  }

  public function ver($id_unidad)
  {
    $data['rendimientos'] = $this->Mgeneral->get_table('combustible_autolavapp.catalogo_rendimiento');
    $data['titulo'] = "Unidades";
    $data['titulo_dos'] = "Alta unidades";
    $data['sucursales'] = $this->db->get('sucursales')->result();
    $data['unidad'] = $this->db->where('id',$id_unidad)->get('unidades')->row();
    //$data['estatus_asignacion'] = $this->db->where('id',$id_unidad)->get('unidades')->row();
    $data['lavadores'] = $this->db->get('lavadores')->result();
    $this->blade->render('unidades/ver', $data);
  }

  public function alta_editar($id_unidad){
    $data = $this->input->post("save");
    $data['fecha_edicion'] = date('Y-m-d H:i:s');
    $this->Mgeneral->update_table_row('unidades',$data,'id',$id_unidad);
  }

  public function inicidecias()
  {
    $data['titulo'] = "Unidades";
    $data['titulo_dos'] = "Alta unidades";
    $data['unidades'] = $this->db->get('unidades')->result();
    $data['incidencias'] = $this->db->get('incidencias')->result();
    $data['lavadores'] = $this->db->get('lavadores')->result();
    $this->blade->render('unidades/incidencias', $data);
  }

  public function alta_guardar_incidencia(){

    $name_documento = date('dmyHis') . '_' . str_replace(" ", "", $_FILES['documento']['name']);
    $path_to_save_do = 'statics/lavadores/';
    if (!file_exists($path_to_save_do)) {
      mkdir($path_to_save_do, 0777, true);
    }
    move_uploaded_file($_FILES['documento']['tmp_name'], $path_to_save_do . $name_documento);
    
    
    $data = $this->input->post("save");
    $data['imagen'] = $path_to_save_do . $name_documento;
    $data['fecha_creacion'] = date('Y-m-d H:i:s');
    $id_unidad = $this->Mgeneral->save_register('incidencias', $data);

    
  }
}