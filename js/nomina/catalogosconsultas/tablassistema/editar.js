var _appsFunction = function () {
    this.contentTable = {},
    this.labelsModal = {},
    this.init = function () {
        $("div#modalContenido").modal({
            show: false,
            keyboard: false,
            backdrop: 'static'
        });
    },

    this.addContenido = function(){
        $('form#modalContenidoForm input').val('');
        var domTiposCatalogo = $('input#id_TiposCatalogo');
        if(domTiposCatalogo.length){
            $('label[for=Valor_1]').html(domTiposCatalogo.attr('campo_1')+':');
            $('label[for=Valor_2]').html(domTiposCatalogo.attr('campo_2')+':');
            $('label[for=Valor_3]').html(domTiposCatalogo.attr('campo_3')+':');
        }
        $("div#modalContenido").modal('show');
    },

    this.editContenido = function($this){
        $('button#guardarEditButton').attr('data-id', $($this).attr('data-id') );
        $('form#modalContenidoEditForm input').val('');

        var domTiposCatalogo = $('input#id_TiposCatalogo');
        $('label[for=Valor_1]').html(domTiposCatalogo.attr('campo_1')+':');
        $('label[for=Valor_2]').html(domTiposCatalogo.attr('campo_2')+':');
        $('label[for=Valor_3]').html(domTiposCatalogo.attr('campo_3')+':');
        
        $('form#modalContenidoEditForm input[name=Valor_1]').val( $($this).attr('valor_1') );
        $('form#modalContenidoEditForm input[name=Valor_2]').val( $($this).attr('valor_2') );
        $('form#modalContenidoEditForm input[name=Valor_3]').val( $($this).attr('valor_3') );

        $("div#modalContenidoEdit").modal('show');
    },
    
    this.get = function () {
        $('table#listado').dataTable({
            "ajax": PATH + '/nomina/catalogosconsultas/ApiCatalogosSistema/listTablasSistema',
            columns: [{
                    'data': 'Clave'
                },
                {
                    'data': 'Descripcion'
                },
                {
                    'data': function (data) {
                        return "<a class='btn btn-success' href='" + PATH + '/nomina/catalogosconsultas/tablassistema/sublist?id=' + data.id + "'> Editar </a>";
                    }
                }
            ]
        })
    }

    this.guardarContenido = function () {
        var dataSend = $('form#modalContenidoForm').serializeArray();
        var domTiposCatalogo = $('input#id_TiposCatalogo');
        dataSend.push(
            {name: 'id_DeTablaSistema',value: null},
            {name: 'uuid_Temporal',value: uuid_temp},
            {name: 'Label_1',value: domTiposCatalogo.attr('campo_1')},
            {name: 'Label_2',value: domTiposCatalogo.attr('campo_2')},
            {name: 'Label_3',value: domTiposCatalogo.attr('campo_3')}
        );
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/detablassistematemp/post',
            data: dataSend,
            success: function (response, status, xhr) {
                if (response.status == 'success') {
                    var table = $('table#listado').DataTable();
                    table.ajax.reload(null, false);
                    $('div#modalContenido').modal('hide');
                }
                //     Swal.fire({
                //         icon: 'success',
                //         title: '',
                //         text: response.info,
                //         confirmButtonText: "Aceptar"
                //     }).then((result) => {
                //         window.location.href = PATH + '/nomina/catalogosconsultas/puestos/index?id=' + response.data.id;
                //     });
                // }
            }
        });
    },

    this.guardarEditarContenido = function ($this) {
        var dataSend = $('form#modalContenidoEditForm').serializeArray();
        var domTiposCatalogo = $('input#id_TiposCatalogo');
        dataSend.push(
            {name: 'id',value: $($this).attr('data-id')},
            {name: 'uuid_Temporal',value: uuid_temp},
            {name: 'Label_1',value: domTiposCatalogo.attr('campo_1')},
            {name: 'Label_2',value: domTiposCatalogo.attr('campo_2')},
            {name: 'Label_3',value: domTiposCatalogo.attr('campo_3')}
        );

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/detablassistematemp/put',
            data: dataSend,
            success: function (response, status, xhr) {
                if (response.status == 'success') {
                    var table = $('table#listado').DataTable();
                    table.ajax.reload(null, false);
                    $('div#modalContenidoEdit').modal('hide');
                }
            }
        });
    },

    this.guardar = function ($this) {
        var dataSend = $('form#formContent').serializeArray();
        dataSend.push(
            {name: 'Contenido',value: uuid_temp},
            {name: 'id',value: identity},
        );

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/tablassistema/edit',
            data: dataSend,
            success: function (response, status, xhr) {
                if (response.status == 'success') {
                    Swal.fire({
                        icon: 'success',
                        title: '',
                        text: response.message,
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        window.location.href = PATH + '/nomina/catalogosconsultas/tablassistema/index?id=' + response.data.id;
                    });
                }
            }
        });
    },

    this.delete = function ($this) {

        Swal.fire({
            icon: 'warning',
            text: '¿Esta seguro de eliminar el registro?',
            //text: response.info,
            showCancelButton: true,
            confirmButtonText: "Si",
            cancelButtonText: "No"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    dataType: "json",
                    type: 'post',
                    url: PATH + '/nomina/api/api/runner/detablassistematemp/delete',
                    data: {
                        'id': $($this).attr('data-id')
                    },
                    success: function (response, status, xhr) {
                        if (response.status == 'success') {
                            Swal.fire({
                                icon: 'success',
                                title: '',
                                text: response.message,
                                confirmButtonText: "Aceptar"
                            }).then((result) => {
                                var table = $('table#listado').DataTable();
                                table.ajax.reload(null, false);
                            });
                        }
                    }

                });
            }
        });
    },

    this.getLabelTable = function(){
        var domTiposCatalogo = $('input#id_TiposCatalogo');
        $('th#valor_1h').html( domTiposCatalogo.attr('campo_1') );
        $('th#valor_2h').html( domTiposCatalogo.attr('campo_2') );
        $('th#valor_3h').html( domTiposCatalogo.attr('campo_3') );

        $('th#valor_1f').html( domTiposCatalogo.attr('campo_1') );
        $('th#valor_2f').html( domTiposCatalogo.attr('campo_2') );
        $('th#valor_3f').html( domTiposCatalogo.attr('campo_3') );
    },

    this.initTable = function () {
        $('table#listado').dataTable({
            "ajax": {
                "url": PATH + '/nomina/api/api/runner/detablassistematemp/findAll',
                "type": 'POST',
                "data": {'uuid_Temporal':uuid_temp}
            },
            columns: [
                {'data': 'Valor_1'},
                {'data': 'Valor_2'},
                {'data': 'Valor_3'},
                {
                    defaultContent: '',
                    'render': function ( data, type, row ) {
                        return "<button type='button' valor_1='"+row.Valor_1+"' valor_2='"+row.Valor_2+"' valor_3='"+row.Valor_3+"' class='btn btn-success' onclick='Apps.editContenido(this);' data-id=" + row.id + " > Editar </button>";
                    },
                },
                {
                    'data': function (data) {
                        return "<button onclick='Apps.delete(this);' type='button' class='btn-borrar btn btn-danger' data-id=" + data.id + ">Borrar</button>";
                    }
                }
            ]
        })
    },

    this.render = function(){

        var dataSend = [];
        dataSend.push(
            {name: 'id',value: identity},
            {name: 'uuid_Temporal',value: uuid_temp}
        );

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/tablassistema/find',
            data: dataSend,
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    var content = {};
                    if(response.extends.TipoCatalogo.length){
                        response.extends.TipoCatalogo.forEach(element => {
                            if(response.data.id_TipoCatalogo == element.id){
                                content = {
                                    'id': element.id,
                                    'Nombre': element.Descripcion,
                                    'Campo_1': element.Campo_1,
                                    'Campo_2': element.Campo_2,
                                    'Campo_3': element.Campo_3,
                                };
                            }
                            
                        });
                    }

                    var datos_tpl = $('script#templateEdit').html();
                    var storange = {
                        Clave: response.data.Clave,
                        Descripcion: response.data.Descripcion,
                        id_TipoCatalogo: response.data.id_TipoCatalogo,
                        TipoCatalogoOption: 'disabled readonly',
                        TipoCatalogo: content
                    };
                    var renderedContent = Mustache.render(datos_tpl,storange);
                    $('form#formContent > div#contentDiv').html(renderedContent);

                    Apps.getLabelTable();
                    Apps.initTable();

                }
            }
        });
    }

};

var Apps;
$(function () {
    Apps = new _appsFunction();
    Apps.render();
    // Apps.init();
    // Apps.getLabelTable();
	
});



