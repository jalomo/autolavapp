<style type="text/css">
    .navbar {}

</style>
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img
                    src="<?php echo base_url(); ?>statics/tema/images/logo1.png"
                    alt="Logo"></a>
            <a class="navbar-brand hidden" href="./">
            </a>
        </div>
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#"> <i class="menu-icon "></i>Usuario: <?php echo
                        nombre_usuario($this->session->userdata('id')); ?> </a>
                </li>
                @if (PermisoAccion())
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Usuarios</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-list"></i><a
                                    href="<?php echo base_url(); ?>index.php/administradores/index">Lista
                                    de clientes</a></li>
                        </ul>
                    </li>
                @endif
                <!--li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-globe"></i>Ciudades</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-plus-circle"></i><a
                                href="<?php echo base_url(); ?>index.php/sucursales/alta">Alta
                                sucursales</a>
                        </li>
                    </ul>
                </li-->
                @if ($this->session->userdata('id_rol') == 1)
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-globe"></i>Zonas</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/sucursales/alta">Alta
                                    sucursales</a>
                            </li>
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/ciudades/alta">Alta
                                    Estados</a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if (PermisoModulo('servicios'))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-car"></i>Servicios</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-list"></i><a
                                    href="<?php echo base_url(); ?>index.php/servicios">Lista
                                    servicios</a>
                            </li>

                            @if ($this->session->userdata('id_rol') == 5)
                                <li><i class="menu-icon fa fa-plus-circle"></i><a
                                        href="<?php echo base_url(); ?>index.php/servicios/generar_servicio/{{ encrypt(0) }}/0/0/0/{{ encrypt($this->session->userdata('id_lavador')) }}">Generar
                                        servicio</a>
                                </li>
                            @else
                                <li><i class="menu-icon fa fa-plus-circle"></i><a
                                        href="<?php echo base_url(); ?>index.php/servicios/generar_servicio/{{ encrypt(0) }}">Generar
                                        servicio</a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if (PermisoAccion())
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-cube"></i>Paquetes</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/paquetes/generar_paquetes/{{ encrypt(0) }}">Generar
                                    paquete</a>
                            </li>
                            <li><i class="menu-icon fa fa-calendar-check-o"></i><a
                                    href="<?php echo base_url(); ?>index.php/paquetes/completar_paquetes">Completar
                                    paquetes</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Estadísticas</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sort-numeric-desc"></i><a
                                    href="<?php echo base_url(); ?>index.php/estadisticas/cantidad_servicios">Cantidad
                                    servicios</a>
                            </li>
                            <li><i class="menu-icon fa fa-money"></i><a
                                    href="<?php echo base_url(); ?>index.php/estadisticas/dinero_servicios">Dinero
                                    Servicios</a>
                            </li>
                            <li><i class="menu-icon fa fa-line-chart"></i><a
                                    href="<?php echo base_url(); ?>index.php/estadisticas/productividad_lavadores">Productividad
                                    lavadores</a>
                            </li>
                            <li><i class="menu-icon fa fa-line-chart"></i><a
                                    href="<?php echo base_url(); ?>index.php/estadisticas/productividad_lavadores_grafica">Productividad
                                    lavadores gráfica</a>
                            </li>
                            <li><i class="menu-icon fa fa-line-chart"></i><a
                                    href="<?php echo base_url(); ?>index.php/estadisticas/estadisticas_periodos">Periodos</a>
                            </li>
                            <li><i class="menu-icon fa fa-line-chart"></i><a
                                    href="<?php echo base_url(); ?>index.php/estadisticas/servicios_ciudad">Servicios
                                    por ciudad</a>
                            </li>
                            <li><i class="menu-icon fa fa-line-chart"></i><a
                                    href="<?php echo base_url(); ?>index.php/estadisticas/metodos_pago">Servicios
                                    por métodos pago</a>
                            </li>
                            <li><i class="menu-icon fa fa-line-chart"></i><a
                                    href="<?php echo base_url(); ?>index.php/estadisticas/cupones">Cupones
                                    efectivos por sucursal</a>
                            </li>

                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-lock"></i>Permisos</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/permisos/permisos_accion">Permisos
                                    por acción</a>
                            </li>
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/permisos/permisos_modulo">Permisos
                                    por módulo</a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if (PermisoAccion('proactivo'))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-list-alt"></i>Contacto proactivo</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/proactivo">Realizar
                                    contacto</a>
                            </li>
                            <li><i class="menu-icon fa fa-history"></i><a
                                    href="<?php echo base_url(); ?>index.php/proactivo/historial_proactivo">Historial</a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if (PermisoAccion('proactivo'))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-list-alt"></i>Proactivo por
                            agencias</a>
                        <ul class="sub-menu children dropdown-menu">
                            <!--<li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/proactivo_agencias/generar_cp_agencia">Generar
                                    contacto por mes</a>
                            </li>-->
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/proactivo_agencias">Realizar
                                    contacto</a>
                            </li>
                            <li><i class="menu-icon fa fa-history"></i><a
                                    href="<?php echo base_url(); ?>index.php/proactivo_agencias/historial_proactivo">Historial</a>
                            </li>
                            <li><i class="menu-icon fa fa-plus"></i><a
                                    href="<?php echo base_url(); ?>index.php/proactivo_agencias/citas">Agendar
                                    cita</a>
                            </li>
                            <!--<li><i class="menu-icon fa fa-plus"></i><a
                                    href="<?php echo base_url(); ?>index.php/proactivo_agencias/importar">Importar</a>
                            </li>-->
                        </ul>
                    </li>
                @endif
                @if (PermisoAccion())
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/contabilidad/index">
                            <i class="menu-icon fa fa-commenting-o"></i>Contablidad </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo base_url(); ?>index.php/contabilidad/combustible">
                            <i class="menu-icon fa fa-commenting-o"></i>Combustible </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo base_url(); ?>index.php/contabilidad/alta_combustible">
                            <i class="menu-icon fa fa-commenting-o"></i>Alta Combustible </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-archive"></i>Inventario Servicios</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li>
                                <i class="menu-icon fa fa-plus-circle"></i>
                                <a
                                    href="<?php echo base_url(); ?>index.php/inventario/index">
                                    Nuevo inventario
                                </a>
                            </li>
                            <li>
                                <i class="menu-icon fa fa-plus-circle"></i>
                                <a
                                    href="<?php echo base_url(); ?>index.php/inventario/listado">
                                    Inventarios
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                <!--li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-cubes"></i>Catálogo Servicios</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-fort-awesome"></i><a
                                href="<?php echo base_url(); ?>index.php/servicios/alta">Alta
                                servicios</a></li>
                    </ul>
                </li-->
                <!--li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"> <i class="menu-icon fa fa-map-marker"></i>Sucursales</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa fa-plus-circle"></i><a
                                href="<?php echo base_url(); ?>index.php/ciudades/alta">Alta
                                ciudad</a></li>
                    </ul>
                </li-->
                @if ($this->session->userdata('id_rol') == 1)
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-tint"></i>Catalogo Servicios</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/modelo/alta">Alta
                                </a></li>
                        </ul>
                    </li>
                @endif
                @if (PermisoAccion())
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-map"></i>Mapas</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a
                                    href="<?php echo base_url(); ?>index.php/mapa_servicios/index">Servicios
                                </a></li>
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a
                                    href="<?php echo base_url(); ?>index.php/mapa_servicios/lavadores">Lavadores
                                </a></li>
                        </ul>
                    </li>
                @endif
                @if (PermisoModulo('lavadores'))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Lavadores</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/lavadores/alta">Alta
                                    lavador</a></li>
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/inventario_lavador_n/alta">
                                    Lista Invetario</a></li>
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/lavadores/inventario/index">
                                    Inventarios</a></li>
                        </ul>
                    </li>
                @endif
                @if (PermisoModulo('unidades'))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-car"></i>Unidades</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/unidades/alta">Alta
                                    unidad</a></li>
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/unidades/inicidecias">Alta
                                    inicidecia</a></li>
                        </ul>
                    </li>
                @endif
                @if (PermisoModulo('clientes'))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Clientes</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-list"></i><a
                                    href="<?php echo base_url(); ?>index.php/usuarios/lista">Lista
                                    de clientes</a></li>
                        </ul>
                    </li>
                @endif
                @if (PermisoModulo('cupones'))
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-ticket"></i>Cupones</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/cupones/recomendadores/index">Recomendadores</a>
                            </li>
                            <li><i class="menu-icon fa fa-plus-circle"></i><a
                                    href="<?php echo base_url(); ?>index.php/cupones/cupones/index">Mis
                                    cupones</a></li>
                        </ul>
                    </li>
                @endif
                @if (PermisoAccion())
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/notificaciones/alta">
                            <i class="menu-icon fa fa-commenting-o"></i>notificacion </a>
                    </li>
                    <li>
                        <a
                            href="<?php echo base_url(); ?>index.php/servicios/tablero_lavadores">
                            <i class="menu-icon fa fa-table"></i>Tablero lavadores </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> <i class="menu-icon fa fa-comment"></i>Sistema</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-bell"></i><a
                                    href="<?php echo base_url(); ?>index.php/sistema/notificaciones">Notificaciones
                                </a></li>
                            <li><i class="menu-icon fa fa-users"></i><a
                                    href="<?php echo base_url(); ?>index.php/sistema/contactos">Contactos
                                </a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/servicios/menu_refacciones">
                            <i class="menu-icon fa fa-cogs"></i>Refacciones </a>
                    </li>
                @endif
                <li>
                    <a href="<?php echo base_url(); ?>index.php/login/cerrar_sesion"> <i
                            class="menu-icon fa fa-sign-out"></i>Cerrar sesión </a>
                </li>
            </ul>
        </div>
    </nav>
</aside>
