@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Poliza ventas</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="frm-productos">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo renderInputText("date", "fecha", "Fecha inicial", ''); ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo renderInputText("date", "fecha", "Fecha final", ''); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <table class=" table table-bordered" id="tabla_poliza">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Descripcion</th>
                                        <th>Cantidad</th>
                                        <th>Existencia</th>
                                        <th>Precio</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (!empty($producto))
                                    @foreach ($producto as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->descripcion }}</td>
                                        <td>{{ $item->cantidad }}</td>
                                        <td>{{ $item->existencia }}</td>
                                        <td>
                                            $ {{ porcentajeProducto($item->rel_precio->precio_publico,$item->precio_factura)}}
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="#"> <i class="fas fa-list-ul"></i> </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td>1</td>
                                        <td>Producto</td>
                                        <td>1</td>
                                        <td>si</td>
                                        <td>$200</td>
                                        <td>
                                            <a class="btn btn-primary" href="#"> <i class="fas fa-list-ul"></i> </a>
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Descripcion</th>
                                        <th>Cantidad</th>
                                        <th>Existencia</th>
                                        <th>Precio</th>
                                        <th>-</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('included_js')
<script>
    $(document).ready(function() {
        $("#tbl_salidas").dataTable({});
    });
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_salidas").addClass("show");
            $("#refacciones_salidas").addClass("active");
            $("#refacciones_salidas_sub").addClass("show");
            $("#refacciones_salidas_sub").addClass("active");
            $("#poliza_otras_salidas").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection