@layout('layout')
@section('contenido')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="ui-typography">
                <div class="row">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Ver  unidad</strong>
                        </div>
                        <div class="card-body">
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <form action="" method="post" novalidate="novalidate" id="alta_usuario">
                                    <div class="row">
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Unidad</label>
                                                    <input id="unidad" name="save[unidad]" type="text"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $unidad->unidad;?>"
                                                        placeholder="Unidad" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">No. economico</label>
                                                    <input id="economico" name="save[economico]" type="text"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $unidad->economico;?>"
                                                        placeholder="No. economico" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Presupuesto Mensual</label>
                                                    <input id="presupuesto" name="save[presupuesto]" type="text"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $unidad->presupuesto;?>"
                                                        placeholder="presupuesto" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Rendimiento</label>
                                                    <select class="form-control-sm form-control cc-exp" id="id_catalogo_rendimiento" name="save[id_catalogo_rendimiento]">
                                                        <?php if($unidad->id_catalogo_rendimiento == 0):?>
                                                            <option value="0" >Selecione un rendimiento</option>
                                                        <?php endif;?>
                                                        <?php foreach($rendimientos as $ren):?>
                                                        <?php if($unidad->id_catalogo_rendimiento ==  $ren->id):?>
                                                        <option value="<?php echo $ren->id ?>" selected><?php echo $ren->nombre." ".$ren->redimiento?> </option>
                                                        <?php else:?>
                                                            <option value="<?php echo $ren->id ?>" ><?php echo $ren->nombre." ".$ren->redimiento?> </option>
                                                        <?php endif;?>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Tipo</label>
                                                    <select class="form-control cc-exp form-control-sm" name="save[tipo]" id="tipo">
                                                        <?php if($unidad->tipo == 1):?>
                                                            <option value="1" selected>Moto</option>
                                                        <?php else:?>
                                                            <option value="1">Moto</option>
                                                        <?php endif;?>

                                                        <?php if($unidad->tipo == 2):?>
                                                            <option value="2" selected>Automovil</option>
                                                        <?php else:?>
                                                            <option value="2">Automovil</option>
                                                        <?php endif;?>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Marca</label>
                                                    <input id="marca" name="save[marca]" type="text"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $unidad->marca;?>"
                                                        placeholder="Marca" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Placas</label>
                                                    <input id="placas" name="save[placas]" type="text"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $unidad->placas;?>"
                                                        placeholder="Placas" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                           <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Color</label>
                                                    <input id="color" name="save[color]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Color" autocomplete="cc-exp" value="<?php echo $unidad->color;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Modelo</label>
                                                    <input id="modelo" name="save[modelo]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Modelo" autocomplete="cc-exp" value="<?php echo $unidad->modelo;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número de serie</label>
                                                    <input id="n_serie" name="save[n_serie]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Número serie" autocomplete="cc-exp" value="<?php echo $unidad->n_serie;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                        </div>

                                        <div class="row">
                                        <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">KM</label>
                                                    <input id="kilometros" name="save[kilometros]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="KM" autocomplete="cc-exp" value="<?php echo $unidad->kilometros;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Año</label>
                                                    <?php
                                                        $options = [
                                                                '2015'  => '2015',
                                                                '2016' => '2016',
                                                                '2017'  => '2017',
                                                                '2018' => '2018',
                                                                '2019' => '2019',
                                                                '2020' => '2020',
                                                                '2021' => '2021',
                                                        ];
                                                        
                                                        $js = 'class="form-control cc-exp form-control-sm" id="anio"';
                                                        echo form_dropdown('save[anio]', $options, $unidad->anio,$js);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Combustible</label>
                                                   
                                                    <?php
                                                        $options = [
                                                                '1'  => 'Gasolina',
                                                                '2' => 'Diésel',
                                                                '3'  => 'Híbrido',
                                                                '4' => 'Eléctrico',
                                                                
                                                        ];
                                                        
                                                        $js = 'class="form-control cc-exp form-control-sm" id="combustible"';
                                                        echo form_dropdown('save[combustible]', $options, $unidad->combustible,$js);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                           <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número de serie Motor</label>
                                                    <input id="n_serie_motor" name="save[n_serie_motor]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Número serie" autocomplete="cc-exp" value="<?php echo $unidad->n_serie_motor;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Puestos</label>
                                                    
                                                    <?php
                                                        $options = [
                                                                '1'  => '1',
                                                                '2' => '2',
                                                                '3'  => '3',
                                                                '4' => '4',
                                                                '5' => '5',
                                                                '6' => '6',
                                                                '7' => '7',
                                                                '8' => '8',
                                                                
                                                        ];
                                                        
                                                        $js = 'class="form-control cc-exp form-control-sm" id="puestos"';
                                                        echo form_dropdown('save[puestos]', $options, $unidad->puestos,$js);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                                                    
                                                    <?php $options_su = array();?>
                                                        @foreach ($sucursales as $s => $sucursal)
                                                            <?php $options_su[$sucursal->id] = $sucursal->sucursal;?>
                                                           
                                                        @endforeach
                                                    <?php
                                                        $options = [
                                                                '1'  => 'Activo',
                                                                '2' => 'Vencido',
                                                                
                                                        ];
                                                       
                                                       
                                                        
                                                        $js = 'class="form-control cc-exp form-control-sm" id="id_sucursal"';
                                                        echo form_dropdown('save[id_sucursal]', $options_su, $unidad->id_sucursal,$js);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                           <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Seguro</label>
                                                    
                                                    <?php
                                                        $options = [
                                                                '1'  => 'Activo',
                                                                '2' => 'Vencido',
                                                                
                                                        ];
                                                        
                                                        $js = 'class="form-control cc-exp form-control-sm" id="seguro"';
                                                        echo form_dropdown('save[seguro]', $options, $unidad->seguro,$js);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Aseguradora</label>
                                                    <input id="aseguradora" name="save[aseguradora]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Aseguradora" autocomplete="cc-exp" value="<?php echo $unidad->aseguradora;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número de poliza</label>
                                                    <input id="no_poliza" name="save[no_poliza]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Número de poliza" autocomplete="cc-exp" value="<?php echo $unidad->no_poliza;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                           <div class="col-2 col-xs-12">
                                           Vigencia poliza
                                           <input type="text" id="datepicker" name="save[vigencia_poliza]" id="vigrncia_pliza" class="form-control cc-exp form-control-sm" value="<?php echo $unidad->vigencia_poliza;?>">
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Cilindros</label>
                                                    <input id="cilindros" name="save[cilindros]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Cilindros" autocomplete="cc-exp" value="<?php echo $unidad->cilindros;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>



                                            <div class="col-2 col-xs-12">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Lavadores</label>
                                                    
                                                    <?php $options_su = array();?>
                                                        @foreach ($lavadores as $s => $lavador)
                                                            <?php $options_su[$lavador->lavadorId] = $lavador->lavadorNombre;?>
                                                           
                                                        @endforeach
                                                    <?php
                                                        
                                                       
                                                        
                                                        $js = 'class="form-control cc-exp form-control-sm" id="id_lavador"';
                                                        echo form_dropdown('save[id_lavador]', $options_su, $unidad->id_lavador,$js);
                                                    ?>
                                                </div>
                                            </div>


                                        </div>
                                        <div align="right">
                                            <button type="button" id="guardar" type="submit" class="btn btn-lg btn-info ">
                                                <i class="fa fa-edit fa-lg"></i>&nbsp;
                                                <span id="payment-button-amount">Editar</span>
                                                <span id="payment-button-sending" style="display:none;">Sending…</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                </div>
                <!--/.col-->
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
  
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
           
       // $( "#datepicker" ).datepicker();
        $( "#datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        
    });
            $("#cargando").hide();
            $('#guardar').on('click',function(event) {
                event.preventDefault();
                $("#enviar").hide();
                $("#cargando").show();
                var url_sis ="<?php echo base_url(); ?>index.php/unidades/alta_editar/<?php echo $unidad->id;?>";
                // Get form
                var form = $('#alta_usuario')[0];
                // Create an FormData object
                var data = new FormData(form);
                console.log(data);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url_sis,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        exito_redirect("DATOS GUARDADOS CON EXITO", "success",
                            "<?php echo base_url(); ?>index.php/unidades/alta"
                        );
                        $("#enviar").show();
                        $("#cargando").hide();

                    },
                    error: function(e) {
                        //$("#result").text(e.responseText);
                        console.log("ERROR : ", e);
                        //$("#btnSubmit").prop("disabled", false);
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                        $("#enviar").show();
                        $("#cargando").hide();

                    }
                });
            });
    </script>
@endsection