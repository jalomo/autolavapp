<?php
class M_Cupon extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->order_by("id", "DESC")->get($tabla)->result();
        return $result;
  	}

  	public function get_table($table){
		$data = $this->db->get($table)->result();
		return $data;
	}

  	public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function update_table_row($table,$data,$id_table,$id){
    		$result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
  	}

    public function get_username($id=''){
        $q = $this->db->where('adminId',$id)->select('adminNombre,adminUsername')->get('admin');
        if($q->num_rows()==1){
        	$usuario = (($q->row()->adminNombre != "") ? $q->row()->adminNombre : $q->row()->adminUsername);
            $retorno = $usuario;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function nombre_servicio($id=''){
        $q = $this->db->where('servicioId',$id)->select('servicioNombre')->get('servicios');
        if($q->num_rows()==1){
            $retorno = $q->row()->servicioNombre;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function recuperar_recomendador($id_cupon=''){
        $q = $this->db->where('cpo.id',$id_cupon)
            ->join('recomendadores AS reco','reco.id = cpo.id_recomendador')
            ->select('reco.nombre, reco.apellido_paterno, reco.apellido_materno')
            ->get('cupones AS cpo');

        if($q->num_rows()==1){
            $retorno = $q->row()->nombre." ".$q->row()->apellido_paterno." ".$q->row()->apellido_materno;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

}