@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <form id="frm-productos">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="producto_id_sat">Estado ventanilla:</label>
                            <select name="filtro_ventanillas" class="form-control" id="filtro_ventanillas">
                                <option value="">SELECCIONE ...</option>
                                <option value="0" >SIN SOLICITAR</option>
                                <option value="1">SOLICIATADAS</option>
                                <option value="3">RECIBIDAS</option>
                                <option value="2">ENTREGADAS</option>
                            </select>
                            <div id="filtro_ventanillas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <?php echo renderInputText("text", "busqueda_ventanilla", "Busqueda Gral.:", ''); ?>
                    </div>
                    <div class="col-md-3 mt-4">
                        <button class="btn btn-secondary" type="button" id="btn-busqueda"> 
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <table class=" table table-bordered table-responsive" id="tbl_busqueda">
                            <thead>
                                <tr>
                                    <th>NO. ORDEN</th>
                                    <th>SERIE</th>
                                    <th>VEHÍCULO</th>
                                    <th>PLACAS</th>
                                    <th>ASESOR</th>
                                    <th>TÉCNICO</th>
                                    <th>APRUEBA CLIENTE</th>
                                    <th>EDO. REFACCIONES</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>NO. ORDEN</th>
                                    <th>SERIE</th>
                                    <th>VEHÍCULO</th>
                                    <th>PLACAS</th>
                                    <th>ASESOR</th>
                                    <th>TÉCNICO</th>
                                    <th>APRUEBA CLIENTE</th>
                                    <th>EDO. REFACCIONES</th>
                                    <th>-</th>
                                    
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection


@section('included_js')
@include('main/scripts_dt')
<script>
    $(document).ready(function() {
        
        var tabla_presupuestos = $('#tbl_busqueda').DataTable({
            "ajax": {
                url:base_url + "refacciones/salidas/ajax_busqueda_presupuesto",
                type: 'POST',
                data: {
                    campo: function() { return $('#busqueda_ventanilla').val() },
                    filtro_ventanillas:function() { return $('#filtro_ventanillas').val() }
                }
            },
            columns: [
                {
                    'data': 'id_cita'
                },
                {
                    'data': 'serie'
                },
                {
                    'data': 'modelo'
                },
                {
                    'data': 'placas'
                },
                {
                    'data': 'asesor'
                },
                {
                    'data': 'tecnico'
                },
                {
                    'data': function(data){
                        return "<label style='font-weight: bolder;color:"+data.color+"'>"+data.acepta_cliente+"</label>";
                        
                    }
                },
                {
                    'data': function(data){
                        return data.estado_refacciones;
                    }
                },
                {
                    'data': function(data) {
                        // console.log(data.envia_ventanilla, data.envia_ventanilla == 0 ? "EDICION" )
                        if(data.envia_ventanilla == 0){
                            console.log("envia_ventanilla: ",data.envia_ventanilla,"EDICION")
                            return "<a class='btn-borrar btn btn-primary' href=" + base_url+"refacciones/salidas/detalleOtrasSalidas/"+ data.id_cita + ">Editar</button>";
                        }
                        if(data.envia_ventanilla == 1){
                            console.log("envia_ventanilla: ",data.envia_ventanilla, "libero ventanilla")
                            return "<a class='btn-borrar btn btn-primary' href=" + base_url+"refacciones/salidas/detalleOtrasSalidas/"+ data.id_cita + ">Ver</button>";
                        }
                        if(data.envia_ventanilla == 2){
                            console.log("envia_ventanilla: ",data.envia_ventanilla, "directo al asesor")
                            return "<a class='btn-borrar btn btn-primary' href=" + base_url+"refacciones/salidas/detalleOtrasSalidas/"+ data.id_cita + ">Sol. refacción</button>";
                        }
                        // return "<a class='btn-borrar btn btn-primary' href=" + base_url+"refacciones/salidas/detalleOtrasSalidas/"+ data.id_cita + ">Accion</button>";
                    }
                }
            ]
        });

        $("#btn-busqueda").on('click', function() {
            $.ajax({
                type: 'POST',
                url: base_url + "refacciones/salidas/ajax_busqueda_presupuesto",
                dataType: "json",
                data:{
                    campo:$('#busqueda_ventanilla').val(),
                    filtro_ventanillas:$('#filtro_ventanillas').val() 
                },
                beforeSend: function( xhr ) {
                    $.isLoading({ text: "Cargando..." });
                },
                success: function(response) {
                    tabla_presupuestos.ajax.reload();
                    $.isLoading( "hide" );
                }
            });
        });
    });
</script>
@endsection