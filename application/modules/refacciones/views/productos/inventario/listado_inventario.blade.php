@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ $bread_active}}</li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-4 mt-4">
            <button id="btn_nuevoinventario" class="btn btn-primary">
                <i class="fas fa-plus"></i> Nuevo inventario
            </button>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-12">
            <input type="hidden" name="usuario_id" id="usuario_id" value="{{ $usuario_id }}">
            <table class="table table-bordered" id="tbl_inventario" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Usuario realizó inventario</th>
                        <th>Fecha inventario </th>
                        <th>Justificación</th>
                        <th>Usuario actualizó</th>
                        <th>Estatus inventario</th>
                        <th> - </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Usuario realizó inventario</th>
                        <th>Fecha inventario </th>
                        <th>Justificación</th>
                        <th>Usuario actualizó</th>
                        <th>Estatus inventario</th>
                        <th> - </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('included_js')
@include('main/scripts_dt')
<script>
    let tabla_inventario = $('#tbl_inventario').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/productos/ajax_mainventario",
            type: 'GET'
        },
        columns: [{
                'data': 'id'
            },
            {
                'data': function(data) {
                    return `${data.nombre} ${data.apellido_materno} ${data.apellido_paterno}`
                }
            },
            {
                'data': (data) => utils.dateToLetras(data.created_at)
            },
            {
                'data': 'justificacion'
            },
            {
                'data': function(data) {
                    if (data.conciliacion_nombre) {
                        return `${data.conciliacion_nombre} ${data.conciliacion_apellido1} ${data.conciliacion_apellido2}`
                    } else {
                        return '';
                    }
                }
            },
            {
                'data': 'EstatusInventario'
            },
            {
                'data': function(data) {
                    let html = "";
                    if (data.estatus_inventario_id == 1) {
                        
                        html += "<button data-toggle='modal' data-target='#modalfinalizar' class='btn btn-primary btn-modal'  data-inventario_id=" + data.id + ">";
                        html += "</i><i class='fas fa-file-alt'></i></button> ";
                    }

                    html += " <a href=" + base_url + "refacciones/productos/inventario/" + data.id + " class='btn btn-primary ' >";
                    html += "<i class='fa fa-bars'></i></a>";
                    return html;
                }
            }

        ],
        "createdRow": function(row, data, dataIndex) {
            switch (data['estatus_inventario_id']) {
                case 1:
                    $(row).find('td:eq(5)').css('background-color', '#f6ffa4');
                    break;
                case 2:
                    $(row).find('td:eq(5)').css('background-color', '#8cdd8c');
                    break;
                case 3:
                    $(row).find('td:eq(5)').css('background-color', '#e37f7f');
                    break;
                default:
                    break;
            }
        }
    });


    $("#btn_nuevoinventario").on('click', function() {
        var band = false;
        ajax.get(`api/inventario`, {}, function(response, headers) {
            if (headers.status == 200) {
                $.each(response, function(index, value) {
                    console.log(value);
                    if (value.estatus_inventario_id == 1) {
                        band = true;
                        utils.displayWarningDialog("Existe un inventario en proceso. Favor de finalizarlo o cancelarlo", "error", function() {
                            return false;
                        });
                        return false;
                    }
                });
            }
        })

        ajax.post('api/inventario', {
            "estatus_inventario_id": 1, //ESTATUS EN PROCESO
            "usuario_registro": $("#usuario_id").val()
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                utils.displayWarningDialog("Creando inventario", "success", function(data) {
                    return window.location.href = base_url + 'refacciones/productos/inventario/' + response.id;
                })
            }
        });

    })

    $('#tbl_inventario').on('click', '.btn-modal', function() {
        $("#frm_inventario")[0].reset();
        let inventario_id = $(this).data('inventario_id');
        ajax.get(`api/inventario/${inventario_id}`, {}, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                console.log(response);
                $("#estatus_inventario_id").val(response.estatus_inventario_id);
                $("#usuario_registro").val(response.usuario_registro);
                $("#justificacion").val(response.justificacion);
                $("#inventario_id").val(response.id);
            }
        })
    });

    $('#btn-confirmar').on('click', function() {
        let inventario_id = document.getElementById('inventario_id').value;
        ajax.put(`api/inventario/${inventario_id}`, {
            'estatus_inventario_id': $("#estatus_inventario_id").val(),
            'usuario_registro': $("#usuario_registro").val(),
            'justificacion': $("#justificacion").val(),
            "usuario_actualizo": $("#usuario_id").val()
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                tabla_inventario.ajax.reload()

            }
        })
    });

    // 
</script>
@endsection


@section('modal')
<div class="modal fade" id="modalfinalizar" tabindex="-1" role="dialog" aria-labelledby="modalfinalizar" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Concesión de inventario</h3>
            </div>
            <div class="modal-body">
                <form id="frm_inventario" class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Estatus inventario</label>
                            <select class="form-control" id="estatus_inventario_id" name="estatus_inventario_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($estatus_inventario))
                                @foreach ($estatus_inventario as $estatus)
                                <option value="{{$estatus->id}}">{{$estatus->nombre}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Justificación</label>
                            <textarea class="form-control" name="justificacion" id="justificacion" rows="4" maxlength="1000"></textarea>
                            <div id="id_ubicacion_llaves_error" class="invalid-feedback"></div>
                        </div>
                        <input type="hidden" name="usuario_registro" id="usuario_registro">
                        <input type="hidden" name="inventario_id" id="inventario_id">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-confirmar" type="button" data-dismiss="modal" class="btn btn-primary">
                    <i class="fas fa-check-circle"></i> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection