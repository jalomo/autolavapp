var _appsFunction = function () {
	this.init = function () {
		update_select2('form#form_content select');
	}
	this.buscar = function(){
		var table = $('table#listado').DataTable();
		table.ajax.reload(null, false);
	},

	this.descargar = function(){
		var data_save = $('form#form_content').serializeArray();
		data_save.push({ name: "periodo", value: periodo });

		$.fileDownload(PATH+'/nomina/inicio/reportenomina/index_pdf',{
			httpMethod: "POST",
			data: data_save,
		}).done(function () {  }).fail(function () {  });
 
    	return false; //this is critical to stop the click event which will trigger a normal file download
	},

	this.check_percepcion_deduccion = function(){
		var pyd = $('input:radio[name=tipo_pyd]:checked').val();
		switch (pyd) {
			case '2':
				$('input[clave=D]').prop( "checked", false );
				$('input[clave=P]').prop( "checked", true );
				break;
			case '3':
				$('input[clave=P]').prop( "checked", false );
				$('input[clave=D]').prop( "checked", true );
				break;
			default:
				$('input.id_pyd').prop( "checked", true );
				break;
		}
	},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[0, 'asc']],
			ajax: {
				url: PATH + '/nomina/inicio/reportenomina/index_get',
				type: "POST",
				data: function(){
					
					var data_save = $('form#form_content').serializeArray();
					data_save.push({ name: "periodo", value: periodo });
					return data_save;
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Nombre del trabajador',
					render: function ( data, type, row, meta ) {
						var nombre = [row.Nombre, row.Apellido_1, row.Apellido_2];
						return nombre.join(' ');
					}
				},
				{
					title: 'Días trabajados',
					data: 'DiasPeriodo'
				},
				{
					title: 'Sueldo',
					data: 'Salario',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Otras percep.',
					data: 'OtrasPercepciones',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Total percep.',
					data: 'TotalPercepciones',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Neto pagado',
					data: 'NetoPagado',
					render: function ( data, type, row, meta ) {
						var calculado = row.TotalPercepciones - row.TotalDeducciones;
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format( (calculado>0)? calculado : 0 );
					}
				},
				{
					title: 'Total en especie',
					data: 'TotalEspecie',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Salario por hora',
					data: 'SalarioHora',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Total IMSS',
					data: 'TotalImss',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Horas por día',
					data: 'HorasDia'
				},
				{
					title: 'Total ISR',
					data: 'TotalIsr',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Subs. empleo',
					data: 'SubsidioEmpleo',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Otras deduc.',
					data: 'OtrasDeducciones',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Total deduc.',
					data: 'TotalDeducciones',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'T. efectivo',
					data: 'TotalEfectivo',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'N.S.S.',
					data: 'nss'
				},
				{
					title: 'RFC',
					data: 'rfc'
				},
				{
					title: 'CURP',
					data: 'curp'
				},
				{
					title: 'Sal. Diario',
					data: 'SalarioDia',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'S.D.I.',
					data: 'SDICapturado',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'D. Jornada',
					data: 'Jornada',
					render: function ( data, type, row, meta ) {
						return (data == 'si')? 'Completo' : 'Mixto';
					}
				},
				{
					title: 'F. alta',
					data: 'FechaAlta',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'Tipo salario',
					data: 'TipoPago'
				},
				{
					title: 'U.T. Laboradas',
					data: 'UTLaboradas'
				},
				
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});

		// var table = $('table#listado').DataTable();
		// $('table#listado').on( 'page.dt', function () {
			
		// 	var info = table.page.info();
		// 	//$('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
		// } );
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();	
	Apps.init();
	Apps.check_percepcion_deduccion();
	Apps.get();
});
