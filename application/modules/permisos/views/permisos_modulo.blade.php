@layout('layout')
<style>

</style>
@section('contenido')

<form action="" id="frm">
  <div class="container1">
    <h2>Permisos de usuarios por módulos</h2>

    <table class="table table-fixed">
      <thead>
        <tr>
          <th></th>
          <th>Usuarios</th>
          <th>Ciudades / Sucursales</th>
          <th>Servicios</th>
          <th>Paquetes</th>
          <th>Estadísticas</th>
          <th>Contacto proactivo</th>
          <th>Contabilidad</th>
          <th>Combustible</th>
          <th>Lavadores</th>
          <th>Unidades</th>
          <th>Clientes</th>
          <th>Cupones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($usuarios as $u => $usuario)
        <tr>
          <td>{{$usuario->adminId.'-'.$usuario->adminNombre}}</td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'administradores'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="administradores[{{$usuario->adminId}}]" value="administradores">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="administradores[{{$usuario->adminId}}]" value="Usuarios">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'sucursales'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="sucursales[{{$usuario->adminId}}]" value="sucursales">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="sucursales[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'servicios'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="servicios[{{$usuario->adminId}}]" value="servicios">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="servicios[{{$usuario->adminId}}]" value="Servicios">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'paquetes'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="paquetes[{{$usuario->adminId}}]" value="paquetes">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="paquetes[{{$usuario->adminId}}]" value="Paquetes">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'estadisticas',''))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="estadisticas[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="estadisticas[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'proactivo',''))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="proactivo[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="proactivo[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'contabilidad',''))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="contabilidad[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="contabilidad[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'combustible','combustible'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="combustible[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="combustible[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'lavadores'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="lavadores[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="lavadores[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'unidades'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="unidades[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="unidades[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'usuarios'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="usuarios[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="usuarios[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'cupones'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="cupones[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="cupones[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          
        </tr>
        @endforeach
      </tbody>
    </table>
  </form>
  <div class="row pull-right">
    <div class="col-sm-12">
      <button type="button" class="btn btn-success" id="guardar_permisos">Guardar Permisos</button>
    </div>
  </div>
  <br>
  <br>
</div>
@endsection

@section('included_js')
<script>
  var site_url = "{{site_url()}}";
  $("#guardar_permisos").on('click',function(){
    var url =site_url+"/permisos/updatePermisosModulo/0";
    ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
      if(result){
        ExitoCustom("Permisos actualizados correctamente",function(){
          window.location.reload();
        });
      }else{
        ErrorCustom("Error al actualizar los permisos",function(){
          window.location.reload();
        });
      }
    });
  });
</script>
@endsection
