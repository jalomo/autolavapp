let venta_realizada = 2;
function inicializaTabla() {
	var tabla_ventas = $('#tabla_ventas').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/ventas/busqueda-ventas?estatus_ventas_id=" + venta_realizada,
			type: 'GET',
		},
		columns: [{
				title: "#",
				data: 'id',
			},
			{
				title: "Folio",
				data: 'folio',
			},
			{
				title: "Cliente",
				render: function(data, type, row) {
					return row.numero_cliente + ' - ' + row.cliente;
				}
			},
			{
				title: "Total venta",
				data: 'venta_total',
            },
            {
				title: "Tipo de venta",
				render: function(data, type, row) {
					if (row.tipo_venta_id == 1){
						return 'Mostrador';
					} else {
						return 'Taller';
					}
				}
			},
			{
				title: "Estatus",
				data: 'estatus',
			},
			{
				title: "Fecha de venta",
				render: function(data, type, row) {
                    return obtenerFechaMostrar(row.created_at)

				}
            },
            {
                title: '-',
                render: function(data, type, row) {
                    return '<button title="Detalle venta" onclick="detalleVenta(this)" data-folio_id="' + row.folio_id + '" class="btn btn-default"><i class="fas fa-list"></i></button>';

				}
            }
		]
	});
}

function detalleVenta(_this){
    setTimeout(() => {
        var folio_id = $(_this).data('folio_id');
        window.location.href = PATH + '/refacciones/salidas/detalleVenta/' + folio_id;
    }, 200);
}

function obtenerFechaMostrar(fecha){
    const dia = 2, mes = 1, anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
		'estatus_ventas_id' : venta_realizada,
        'folio': $("#folio").val(),
        'cliente_id': $("#cliente_id option:selected").val(),
    });

    $('#tabla_ventas').DataTable().ajax.url(PATH_API + 'api/ventas/busqueda-ventas?' + params).load()
}

function limpiarFiltro() {
	$("#folio").val('');
	$("#cliente_id").val('');
	$("#cliente_id").trigger('change');
    var params = $.param({
		'estatus_ventas_id' : venta_realizada,
        'folio': '',
        'cliente_id': '',
    });

    $('#tabla_ventas').DataTable().ajax.url(PATH_API + 'api/ventas/busqueda-ventas?' + params).load()
}

this.inicializaTabla();
$("#cliente_id").select2();
