<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Panel extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Recomendadores', 'consulta', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

        if ($this->session->userdata('id_usuario')) {
        } else {
            redirect('recomendadores/login/');
        }
    }

    #----------------------------Datos generales del usuario
    public function index()
    {
        $data['titulo'] = "Panel Recomendadores";
        $data['titulo_dos'] = "Datos del Recomendador";

        if ($this->session->userdata('id_usuario')) {
            $data["id"] = $this->session->userdata('id_usuario');

            //Recuperamos los datos del recomendador
            $datos = $this->consulta->get_result('id', $data["id"], 'recomendadores');
            foreach ($datos as $row) {
                $data["nombre"] = $row->nombre;
                $data["ap_paterno"] = $row->apellido_paterno;
                $data["am_paterno"] = $row->apellido_materno;
                $data["celular"] = $row->celular;
                $data["email"] = $row->correo;
                $data["nUsuario"] = $row->usuario;

                $data["calle"] = $row->calle;
                $data["n_int"] = $row->num_interno;
                $data["n_ext"] = $row->num_externo;
                $data["colonia"] = $row->colonia;
                $data["municipio"] = $row->municipio;
                $data["edo"] = $row->estado;
                $data["rfc"] = $row->rfc;

                $data["reg_edo"] = $this->consulta->nombre_estado($row->id_estado_reg);
                $data["reg_sucursal"] = $this->consulta->nombre_sucursal($row->id_sucursal_reg);
                //$data["n_cupones"] = $row->n_cupones;
                
                //Recuperamos los datos bancarios
                $data["banco"] = $row->banco;
                $data["n_banco"] = $row->id_banco;
                $data["nombre_banco"] = $this->consulta->nombre_banco($row->id);
                $data["clave_sat"] = $this->consulta->clave_sat_banco($row->id);
                $data["sucursal"] = $row->sucural;
                $data["cuenta"] = $row->n_cuenta;
                $data["clave_bancaria"] = $row->clave_bancaria;
                $data["n_tarjeta"] = $row->n_tarjeta;
            }

            $data['cat_bancos'] = $this->consulta->get_table('cat_bancos');
        } 
        
        $this->blade->render('panel/principal',$data);
    }

    public function actualizar_recomendador()
    {
        $registro = date("Y-m-d H:i:s");

        $contenido["nombre"] = $_POST["nombre"];
        $contenido["apellido_paterno"] = $_POST["ap_paterno"];
        $contenido["apellido_materno"] = $_POST["am_paterno"];
        $contenido["celular"] = $_POST["celular"];
        $contenido["correo"] = $_POST["email"];
        $contenido["rfc"] = strtoupper($_POST["rfc"]);

        if (isset($_POST["calle"])) {
            $contenido["calle"] = $_POST["calle"];
        }

        if (isset($_POST["n_int"])) {
            $contenido["num_interno"] = $_POST["n_int"];
        }

        if (isset($_POST["n_ext"])) {
            $contenido["num_externo"] = $_POST["n_ext"];
        }

        if (isset($_POST["colonia"])) {
            $contenido["colonia"] = $_POST["colonia"];
        }

        if (isset($_POST["ciudad"])) {
            $contenido["municipio"] = $_POST["ciudad"];
        }

        if (isset($_POST["edo"])) {
            $contenido["estado"] = $_POST["edo"];
        }

        $contenido["usuario"] = $_POST["nUsuario"];

        $contenido["fecha_actualiza"] = $registro;

        $actualizar = $this->consulta->update_table_row('recomendadores', $contenido, 'id', $_POST["listado"]);
        if ($actualizar) {
            $respuesta = "OK";
        } else {
            $respuesta = "NO SE PUDO ACTUALIZAR EL FORMULARIO";
        }

        echo $respuesta;
    }

    #----------------------------Cupones asignados al usuario
    public function cupones()
    {
        $data['titulo'] = "Panel Recomendadores";
        $data['titulo_dos'] = "Mis Cupones";

        if ($this->session->userdata('id_usuario')) {
            $data["id"] = $this->session->userdata('id_usuario');

            //Recuperamos los cupones del recomendador
            $datos_cupones = $this->consulta->get_result('id_recomendador', $data["id"], 'cupones');
            foreach ($datos_cupones as $row) {
                $data["cupon"][] = $row->cupon;
                $data["descipcion"][] = $row->descripcion;
                $data["limite_uso"][] = $row->limite_uso;
                //$data["puntos"][] = $row->beneficio;
                $data["activo"][] = $row->activo;
                $data["caducidad"][] = $row->sin_caducidad;
                $data["fecha_caducidad"][] = $row->fecha_caducidad;
                $data["usos"][] = $this->consulta->usos_cupon($row->id);
                $data["ruta_cupo"][] = $this->encrypt($row->id);
            }
        }

        $this->blade->render('panel/lista_cupones', $data);
    }

    #----------------------------Datos bancarios del usuario
    public function datos_banco()
    {
        $data['titulo'] = "Panel Recomendadores";
        $data['titulo_dos'] = "Datos Bancarios";

        if ($this->session->userdata('id_usuario')) {
            $data["id"] = $this->session->userdata('id_usuario');

            //Recuperamos los datos del recomendador
            $datos = $this->consulta->get_result('id', $data["id"], 'recomendadores');
            foreach ($datos as $row) {
                $data["banco"] = $row->banco;
                $data["n_banco"] = $row->id_banco;
                $data["nombre_banco"] = $this->consulta->nombre_banco($row->id);
                $data["clave_sat"] = $this->consulta->clave_sat_banco($row->id);
                $data["sucursal"] = $row->sucural;
                $data["cuenta"] = $row->n_cuenta;
                $data["clave_bancaria"] = $row->clave_bancaria;
                $data["n_tarjeta"] = $row->n_tarjeta;
            }

            $data['cat_bancos'] = $this->consulta->get_table('cat_bancos');
        }

        $this->blade->render('panel/formulario_2', $data);
    }

    public function buscar_banco()
    {
        $clave = $_POST["clave_banco"];
        $respuesta = "";

        $datos = $this->consulta->get_result('clave_sat', $clave, 'cat_bancos');
        foreach ($datos as $row) {
            $id = $row->id;
            $respuesta .= $row->id . "=";
            $respuesta .= $row->razon_social . "=";
            $respuesta .= $row->clave_sat . "=";
        }

        if (isset($id)) {
            echo "OK=" . $respuesta;
        } else {
            echo "ERROR=";
        }
    }

    public function buscar_banco_nombre()
    {
        $banco = $_POST["banco"];
        $respuesta = "";

        $datos = $this->consulta->get_result('id', $banco, 'cat_bancos');
        foreach ($datos as $row) {
            $id = $row->id;
            $respuesta .= $row->nombre . "=";
            $respuesta .= $row->razon_social . "=";
            $respuesta .= $row->clave_sat . "=";
        }

        if (isset($id)) {
            echo "OK=" . $respuesta;
        } else {
            echo "ERROR=";
        }
    }

    public function actualizar_datos_banco()
    {
        $registro = date("Y-m-d H:i:s");

        if (isset($_POST["banco"])) {
            $contenido["id_banco"] = $_POST["n_banco"];
            if ($_POST["banco"] != "") {
                $contenido["banco"] = trim($_POST["banco"]);
            }
        }

        if (isset($_POST["sucursal"])) {
            $contenido["sucural"] = $_POST["sucursal"];
        }

        if (isset($_POST["cuenta"])) {
            $contenido["n_cuenta"] = $_POST["cuenta"];
        }

        if (isset($_POST["clave_bancaria"])) {
            $contenido["clave_bancaria"] = $_POST["clave_bancaria"];
        }

        if (isset($_POST["n_tarjeta"])) {
            $contenido["n_tarjeta"] = $_POST["n_tarjeta"];
        }

        $contenido["fecha_actualiza"] = $registro;

        $actualizar = $this->consulta->update_table_row('recomendadores', $contenido, 'id', $_POST["listado"]);
        if ($actualizar) {
            $respuesta = "OK";
        } else {
            $respuesta = "NO SE PUDO ACTUALIZAR EL FORMULARIO";
        }

        echo $respuesta;
    }

    #----------------------------Contactos del usuario (para envio de cupones)
    public function agenda()
    {
        $data['titulo'] = "Panel Recomendadores";
        $data['titulo_dos'] = "Agenda";

        if ($this->session->userdata('id_usuario')) {
            $data["id"] = $this->session->userdata('id_usuario');
            $data['mis_contatos'] = $this->consulta->get_result_field('id_usuario', $data["id"], "activo", "1", 'recomendadores_agenda');
        }

        $this->blade->render('panel/agenda', $data);
    }

    public function guardar_contacto()
    {
        $registro = date("Y-m-d H:i:s");

        if ($_POST["formulario"] == "1") {
            $contenido["id"] = NULL;
            $contenido["id_usuario"] = $this->session->userdata('id_usuario');
        }

        $contenido["contacto"] = $_POST["nombre"];
        $contenido["telefono"] = $_POST["telefono"];

        if (isset($_POST["notas"])) {
            $contenido["nota"] = $_POST["notas"];
        }

        if ($_POST["formulario"] == "1") {
            $contenido["activo"] = "1";
            $contenido["fecha_alta"] = $registro;
        }

        $contenido["fecha_actualiza"] = $registro;

        if ($_POST["formulario"] == "1") {
            $servicio = $this->consulta->save_register('recomendadores_agenda', $contenido);
            if ($servicio != 0) {
                $respuesta = "OK";
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO";
            }
        } else {
            $actualizar = $this->consulta->update_table_row('recomendadores_agenda', $contenido, 'id', $_POST["listado"]);
            if ($actualizar) {
                $respuesta = "OK";
            } else {
                $respuesta = "NO SE PUDO ACTUALIZAR EL FORMULARIO";
            }
        }

        echo $respuesta;
    }

    public function buscar_contacto()
    {
        $id_contacto = $_POST["id_contacto"];

        $datos = $this->consulta->get_result('id', $id_contacto, 'recomendadores_agenda');
        foreach ($datos as $row) {
            $id = $row->id;
            $respuesta .= $row->id . "=";
            $respuesta .= $row->contacto . "=";
            $respuesta .= $row->telefono . "=";
            $respuesta .= $row->nota . "=";
        }

        if (isset($id)) {
            echo "OK=" . $respuesta;
        } else {
            echo "ERROR=";
        }
    }

    public function eliminar_contacto()
    {
        $registro = date("Y-m-d H:i:s");
        $contenido["activo"] = "0";
        $contenido["fecha_actualiza"] = $registro;

        $actualizar = $this->consulta->update_table_row('recomendadores_agenda', $contenido, 'id', $_POST["id"]);

        echo "1";
    }

    #----------------------------Envio de cupones
    public function enviar_cupon($id_cupon = '')
    {
        $data['titulo'] = "Panel Recomendadores";
        $data['titulo_dos'] = "Envio de cupones";

        $cupon = $this->decrypt($id_cupon);

        if ($this->session->userdata('id_usuario')) {
            $data["id"] = $this->session->userdata('id_usuario');

            //Verificamos si los datos del usuario estan completos
            $data["datos_gral"] = $this->consulta->datos_generales($data["id"]);
            $data["datos_bco"] = $this->consulta->datos_banco($data["id"]);

            $datos_cupon = $this->consulta->get_result("id", $cupon, "cupones");
            foreach ($datos_cupon as $row) {
                $data["id_cupon"] = $row->id;
                $data["cupon"] = $row->cupon;
                $data["descripcion"] = $row->descripcion;
                $data["limite_uso"] = $row->limite_uso;

                $fecha_activacion = new DateTime($row->fecha_activacion . " 00:00:00");
                $fecha_caducidad = new DateTime($row->fecha_caducidad . " 00:00:00");
                $data["fecha_activacion"] = $fecha_activacion->format('d-m-Y');
                $data["fecha_caducidad"] = $fecha_caducidad->format('d-m-Y');

                $data["indefinido"] = $row->sin_caducidad;
                $data["descuento"] = $row->descuento;
                $data["beneficio"] = $row->beneficio;
            }

            $paquete = $this->consulta->get_result("id_cupon", $cupon, "cupon_servicio");
            foreach ($paquete as $row) {
                if ($row->activo == "1") {
                    $data["pqt"][] = $row->id_servicio;
                }
            }

            $data['servicios'] = $this->consulta->get_table_all('servicios');
            $data['mis_contatos'] = $this->consulta->get_result_field('id_usuario', $data["id"], "activo", "1", 'recomendadores_agenda');
        }

        $this->blade->render('panel/enviar_cupon', $data);
    }

    public function nvo_contacto_envio()
    {
        $registro = date("Y-m-d H:i:s");
        $id_recomendador = $this->session->userdata('id_usuario');

        $contenido["id"] = NULL;
        $contenido["id_usuario"] = $id_recomendador;

        $contenido["contacto"] = $_POST["nombre"];
        $contenido["telefono"] = $_POST["telefono"];

        if (isset($_POST["notas"])) {
            $contenido["nota"] = $_POST["notas"];
        }

        $contenido["activo"] = "1";
        $contenido["fecha_alta"] = $registro;

        $contenido["fecha_actualiza"] = $registro;

        $contacto = $this->consulta->save_register('recomendadores_agenda', $contenido);
        if ($contacto != 0) {
            $cupon = $_POST["clave2"];
            $id_cupon = $_POST["listado2"];
            $descuento = $_POST["descuento2"];

            //Creamos mensaje a enviar
            $texto = $this->consulta->nombre_usuario($id_recomendador);
            $texto .= ".Te invita a la experiencia Xehos AutoLavado. Cupon: " . $cupon;
            $texto .= ". Paga con el y recibe un dto. de " . $descuento;
            $texto .= ". Entra a: https://xehos.com";

            $this->sms_general_texto($_POST["telefono"], $texto);

            $contenido_2["id"] = NULL;
            $contenido_2["id_usuario"] = $id_recomendador;
            $contenido_2["id_cupon"] = $id_cupon;
            $contenido_2["telefono"] = $_POST["telefono"];
            $contenido_2["fecha_envio"] = $registro;

            $envio_cupon = $this->consulta->save_register('recomendadores_envio_cupones', $contenido_2);

            if ($envio_cupon != 0) {
                $respuesta = "OK";
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO 2";
            }
        } else {
            $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO";
        }

        echo $respuesta;
    }

    public function enviar_sms_cupon()
    {
        $respuesta = "";
        $id_recomendador = $this->session->userdata('id_usuario');

        //Recuperamos los telefonos de los contactos a enviar
        if (count($_POST["contactos"]) > 0) {
            $dato_contacto = [];
            for ($i = 0; $i < count($_POST["contactos"]); $i++) {
                $dato_contacto[] = $this->consulta->telefono_contacto($_POST["contactos"][$i]);
            }

            if (count($dato_contacto) > 0) {
                $id_cupon = $_POST["listado"];
                $cupon = $_POST["clave"];
                //$descripcion = $_POST["descripcion"];
                //$limite = $_POST["limite"];
                $descuento = $_POST["descuento"];
                //$fecha_caducidad = $_POST["fecha_caducidad"];
                //$servicios = $_POST["servicios"];

                //Creamos mensaje a enviar
                $texto = $this->consulta->nombre_usuario($id_recomendador);
                $texto .= ".Te invita a la experiencia Xehos AutoLavado. Cupon: " . $cupon;
                $texto .= ". Paga con el y recibe un desct. de " . $descuento;
                $texto .= ". Entra a: https://xehos.com";

                //Enviamos los mensajes de texto
                for ($i = 0; $i < count($dato_contacto); $i++) {
                    $this->sms_general_texto($dato_contacto[$i], $texto);
                }

                $registro = date("Y-m-d H:i:s");

                //Guardamos los telefonos alos que se enviaron
                for ($i = 0; $i < count($dato_contacto); $i++) {
                    $contenido = NULL;

                    $contenido["id"] = NULL;
                    $contenido["id_usuario"] = $id_recomendador;
                    $contenido["id_cupon"] = $id_cupon;
                    $contenido["telefono"] = $dato_contacto[$i];
                    $contenido["fecha_envio"] = $registro;

                    $envio_cupon = $this->consulta->save_register('recomendadores_envio_cupones', $contenido);
                    if ($envio_cupon != 0) {
                        $respuesta = "OK";
                    } else {
                        $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO";
                    }
                }
            } else {
                $respuesta = "OCURRIO UN PROBLEMA AL CARGAR LOS CONTACTOS";
            }
        } else {
            $respuesta = "NO SE ENCONTRARON TELEFONOS PARA ENVIAR CUPÓN";
        }

        echo $respuesta;
    }

    public function cupones_enviados()
    {
        $data['titulo'] = "Panel Recomendadores";
        $data['titulo_dos'] = "Mis cupones enviados";

        if ($this->session->userdata('id_usuario')) {
            $data["id"] = $this->session->userdata('id_usuario');

            $data["datos_cupon"] = $this->consulta->historico_envio($data["id"]);
        }

        $this->blade->render('panel/lista_cupones_enviados', $data);
    }

    #----------------------------Envio de cupones
    public function modificar_password()
    {
        $data['titulo'] = "Panel Recomendadores";
        $data['titulo_dos'] = "Modificar mi contraseña";

        if ($this->session->userdata('id_usuario')) {
            $data["id"] = $this->session->userdata('id_usuario');
        }

        $this->blade->render('panel/modificar_password', $data);
    }

    public function revisar_pass()
    {
        $pass = $_POST["pass"];
        $id_usuario = $this->session->userdata('id_usuario');

        $pass_db = $this->consulta->recuperar_password($id_usuario);

        if (md5($pass) === $pass_db) {
            echo "OK";
        } else {
            echo "ERROR";
        }
    }

    public function actualizar_passord()
    {
        $id_usuario = $this->session->userdata('id_usuario');

        $registro = date("Y-m-d H:i:s");

        $contenido["password"] = md5($_POST["pass_nvo"]);
        $contenido["resp_pass"] = $_POST["pass_nvo"];
        $contenido["fecha_actualiza"] = $registro;

        $actualizar = $this->consulta->update_table_row('recomendadores', $contenido, 'id', $id_usuario);

        echo "OK";
    }

    #----------------------------Bonificaciones cobradas

    public function bonificaciones()
    {
        $data['titulo'] = "Panel Recomendadores";
        $data['titulo_dos'] = "Mis bonificaciones cobradas";

        if ($this->session->userdata('id_usuario')) {
            $data["id"] = $this->session->userdata('id_usuario');

            $data["bonificaciones"] = $this->consulta->listado_bonificaciones($data["id"]);
        }

        $this->blade->render('panel/lista_bonificaciones', $data);
    }

    #----------------------------Encriptado de rutas y otras rutas
    function encrypt($data)
    {
        $id = (float)$data * CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "", $url_id);
        return $url;
        //return $data;
    }

    function decrypt($data)
    {
        $url_id = base64_decode($data);
        $id = (float)$url_id / CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    public function sms_general_texto($celular = '', $mensaje = '')
    {
        $sucursal = "XEHON";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje_xehos");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "celular=" . $celular . "&mensaje=" . $mensaje . "&sucursal=" . $sucursal . ""
        );
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close($ch);
    }

    public function lista_chat()
    {
        $data['titulo'] = "Panel";
        $data['titulo_dos'] = "Usuarios del sistema";
        $data['usuarios'] = $this->db->select('admin.*, ca_cargos.nombre as cargo')->where('telefono !=', null)->join('ca_cargos', 'admin.id_cargo = ca_cargos.id')->get('admin')->result();
        $data['datos']  = $this->db->where('id',$this->session->userdata('id_usuario'))->get('recomendadores')->row_array();
        //echo '<pre>'; print_r($data); exit();
        $this->blade->render('recomendadores/panel/listachat', $data);
    }

    public function  chat()
    {
      $data['titulo'] = "Panel";
      $data['titulo_dos'] = "Chat";
      $telefono1 = $this->input->get('telefono_cliente');
      $telefono2 = $this->input->get('telefono_destinatario');
  
      $data['telefono_remitente'] = $telefono1;
      $data['telefono_destinatario'] = $telefono2;
      $data['remitente'] = $this->getUserTelefono($telefono1);
      $data['destinatario'] = $this->getUserTelefono($telefono2);
      $data['template2'] = "recomendadores/layout";
  
      $this->blade->render('recomendadores/panel/chat_webView', $data);
    }

    public function getUserTelefono($telefono)
    {
        $usuarios_admin = $this->db->select('adminNombre as nombre, telefono')->where('telefono', $telefono)->get('admin')->result_array();
        $usuarios_cliente = $this->db->select('nombre, telefono')->where('telefono', $telefono)->get('usuarios')->result_array();
        $usuarios_recomendador = $this->db->select('nombre, celular as telefono')->where('celular', $telefono)->get('recomendadores')->result_array();
        $usuarios_lavadores = $this->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "Lavador" as tipo')->where('lavadorTelefono', $telefono)->get('lavadores')->result_array();
        $usuarios = array_merge($usuarios_admin, $usuarios_cliente, $usuarios_recomendador, $usuarios_lavadores);
        if (is_array($usuarios)){
          return current($usuarios)['nombre'];
        }
    }
}
