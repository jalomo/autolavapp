<input type="hidden" id="month" name="month" value="{{ $month }}">
<input type="hidden" id="year" name="year" value="{{ $year }}">
<input type="hidden" id="id_agencia_send" name="id_agencia_send" value="{{ $id_agencia }}">
<h1>Generar contacto proactivo mes y año</h1>
<div class="row">
    <div class="col-sm-12">
        <label for=""></label>
        <div style="margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">
            <strong>Solamente se puede generar una vez el contacto proactivo por mes y año</strong>
        </div>
    </div>
</div>
@if ($validar)
    <div class="row">
        <div class="col-sm-3">
            <input type="text" value="{{ $nameMes }}" class="
  form-control" readonly="">
        </div>
        <div class="col-sm-3">
            <input type="anio" value="{{ $year }}" class="
  form-control" readonly="">
        </div>
        <div class="col-sm-3">
            <button id="generar" class="btn btn-success">Generar contacto proactivo</button>
        </div>
    </div>
    <br>
@endif
<div class="row">
    <div class="col-sm-12">

    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-hover">
            <thead>
                <th>Mes</th>
                <th>Año</th>
                <th>Agencia</th>
            </thead>
            <tbody>
                @foreach ($registros as $r => $registro)
                    <tr>
                        <td>{{ $this->mp->getNameMes($registro->mes) }}</td>
                        <td>{{ $registro->anio }}</td>
                        <td>{{ $registro->agencia }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
