<table class="table table-bordered">
    <thead>
        <tr>
            <th>Estatus anterior</th>
            <th>Estatus nuevo</th>
            <th>Inicio</th>
            <th>Fin</th>
            <th>Minutos transcurrido</th>
        </tr>
    </thead>
    <tbody>
        <?php $total = 0; ?>
        @foreach ($info as $i => $data)
            <tr>
                <td colspan="5">
                    <h2>{{ $i }}</h2>
                </td>
            </tr>
            <?php $total = 0; ?>
            @foreach ($data as $d => $servicios)
                
                <?php
                $servicio_array = explode('/', $d);
                $tiempo_servicio = 0;
                ?>
                
                <tr>
                    <td colspan="5">
                        <h4># {{ $servicio_array[0] }} - {{ $servicio_array[1] }}</h4>
                    </td>
                </tr>
                @foreach ($servicios as $s => $servicio)
                    <?php  $backgroud = ''; ?>
                    @if($servicio->cancelado)
                        <?php $backgroud = 'red'; ?>
                    @endif
                    <tr style="background: {{$backgroud}}">
                        <td>{{ $servicio->status_anterior }}</td>
                        <td>{{ $servicio->status_nuevo }}</td>
                        <td>{{ $servicio->inicio }}</td>
                        <td>{{ $servicio->fin }}</td>
                        <td>{{ $servicio->minutos_transcurridos }}</td>
                    </tr>
                    <?php $tiempo_servicio = $tiempo_servicio + (int)$servicio->minutos_transcurridos; ?>
                @endforeach
                        <tr>
                        <td colspan="5">
                            <h4 class="text-right">Tiempo servicio: {{ $tiempo_servicio }} minutos</h4>
                        </td>
                        </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
