<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cupones extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Cupon', 'consulta', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        if($this->session->userdata('id')){}else{redirect('login/');}
    }


    public function index()
    {
    	$data['titulo'] = "Cupones";
        $data['titulo_dos'] = "Mis cupones";

        $data['listado'] = $this->consulta->get_table('cupones');
        $this->blade->render('listado_principal',$data);
    }

    public function nuevo_cupon()
    {
    	$data['titulo'] = "Cupones";
        $data['titulo_dos'] = "Alta";

        //Generamos una clave random para el codigo
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $clave = "";
        for($i=0; $i<=3; $i++ ){
            $clave .= substr($str, rand(0,strlen($str)-1) ,1 );
        }
        $clave .= date("is");

        $data['cupon'] = $clave;
        $data['usuarios'] = $this->consulta->get_result("activo","1","recomendadores");
        //Recuperamos los paquetes disponibles
        $data['servicios'] = $this->consulta->get_table('servicios');
        $this->blade->render('formulario',$data);
    }

    public function guardar_formulario()
    {
    	$registro = date("Y-m-d H:i:s");

        if ($_POST["envio_formulario"] == "1") {
            $contenido["id"] = NULL;
            $contenido["cupon"] = strtoupper($_POST["clave"]);
            $contenido["id_recomendador"] = $_POST["usuario"];
        	$contenido["usuario"] = $_POST["nombre_usuario"];
        }

        $contenido["descripcion"] = $_POST["descripcion"];
        $contenido["limite_uso"] = $_POST["limite"];
        $contenido["fecha_activacion"] = $_POST["fecha_activo"];
        $contenido["fecha_caducidad"] = $_POST["fecha_caducidad"];
        $contenido["sin_caducidad"] = ((isset($_POST["tiempo_iliminado"])) ? '1' : '0');
        $contenido["descuento"] = $_POST["descuento"];
        $contenido["beneficio"] = $_POST["beneficio"];
        $contenido["servicios"] = $this->txt_servicios();

        if ($_POST["envio_formulario"] == "1") {
            $contenido["id_usuario"] = $this->session->userdata('id');
        	$contenido["activo"] = "1";
            $contenido["fecha_alta"] = $registro;
        }
        
        $contenido["fecha_actualiza"] = $registro;

        if ($_POST["envio_formulario"] == "1") {
            $servicio = $this->consulta->save_register('cupones',$contenido);
            if ($servicio != 0) {
            	$paquetes = $this->guardar_paquetes($servicio);
                $respuesta = (($paquetes != 0) ? "OK" : "SE GUARDO EL CUPON PERO OCURRIO UN PROBLEMA CON LOS PAQUETES") ."_0";

            } else {
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_".$_POST["servicio_cupon"];
            }
            
        }else{
            $actualizar = $this->consulta->update_table_row('cupones',$contenido,'id',$_POST["servicio_cupon"]);
            if ($actualizar) {
                $paquetes = $this->guardar_paquetes($_POST["servicio_cupon"]);
                $respuesta = (($paquetes != 0) ? "OK" : "SE GUARDO EL CUPON PERO OCURRIO UN PROBLEMA CON LOS PAQUETES") ."_0";
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_".$_POST["servicio_cupon"];
            }
        }

        echo $respuesta;
    }

    public function guardar_paquetes($id_cupon = '')
    {
    	$registro = date("Y-m-d H:i:s");

    	if ($_POST["servicios"] == NULL) {
            $respuesta = 0;
        }else {
            $respuesta = 0;
            $paquetes = [];
            $paquetes_id = [];
            $paquetes_nvo = [];

            if (count($_POST["servicios"]) > 0){
                if ($_POST["envio_formulario"] == "1") {
                	for ($i=0; $i < count($_POST["servicios"]) ; $i++) {
                    	$paquetes_nvo[] = array(
                    		"id" => NULL,
                    		"id_cupon" => $id_cupon,
                    		"id_servicio" => $_POST["servicios"][$i],
                    		"activo" => 1,
                    		"fecha_alta" => $registro,
                    		"fecha_actualiza" => $registro
                    	);
                    }
                } else {
                	$servicios = [];
                	//Recuperamos los servicios actuales que aplica en el cupon
                	$query = $this->consulta->get_result("id_cupon",$id_cupon,"cupon_servicio");
			        foreach ($query as $row){
			            $servicios["id"][] = $row->id;
			            $servicios["servicio"][] = $row->id_servicio;
			            $servicios["activo"][] = $row->activo;
			        }

			        //Verificamos si se reactiva algun servicio o si se agrega uno nuevo
			        for ($i=0; $i < count($_POST["servicios"]) ; $i++) {
			        	if (in_array($_POST["servicios"][$i],$servicios["servicio"])) {
			        		$paquetes[] = array(
	                    		"activo" => 1,
	                    		"fecha_actualiza" => $registro
	                    	);
	                    	$paquetes_id[] = $servicios["id"][array_search($_POST["servicios"][$i],$servicios["servicio"])];
			        	} else {
			        		$paquetes_nvo[] = array(
	                    		"id" => NULL,
	                    		"id_cupon" => $id_cupon,
	                    		"id_servicio" => $_POST["servicios"][$i],
	                    		"activo" => 1,
	                    		"fecha_alta" => $registro,
	                    		"fecha_actualiza" => $registro
	                    	);
			        	}
			        }

			        //Verificamos si se desactivo algun servicio relacionado al cupon
			        for ($i=0; $i < count($servicios["servicio"]) ; $i++) {
			        	if (!in_array($servicios["servicio"][$i],$_POST["servicios"])) {
			        		$paquetes[] = array(
	                    		"activo" => 0,
	                    		"fecha_actualiza" => $registro
	                    	);
	                    	$paquetes_id[] = $servicios["id"][$i];
			        	} 
			        }
                }

                for ($i=0; $i < count($paquetes_nvo) ; $i++) { 
                	$paquete_nuevo = $this->consulta->save_register('cupon_servicio',$paquetes_nvo[$i]);
            		if ($paquete_nuevo != 0) {
            			$respuesta = 1;
            		}
                }

                for ($j=0; $j < count($paquetes) ; $j++) { 
                	$paquete = $this->consulta->update_table_row('cupon_servicio',$paquetes[$j],'id',$paquetes_id[$j]);
            		if ($paquete != 0) {
            			$respuesta = 1;
            		}
                }
            }else{
            	$respuesta = 0;
            }
        }

        return $respuesta;
    }

    public function txt_servicios()
    {
        $respuesta = "";
        if ($_POST["servicios"] != NULL) {
            if (count($_POST["servicios"]) > 0){
                for ($i=0; $i < count($_POST["servicios"]) ; $i++) {
                    $respuesta .= $this->consulta->nombre_servicio($_POST["servicios"][$i]).". ";
                }
            }else{
                $respuesta = "";
            }
        }else{
            $respuesta = "";
        }
        return $respuesta;
    }

    public function redireccionar($id='')
    {
    	$cupon = $this->encrypt($id);
    	echo $cupon;
    }

    public function editar_cupon($id_cupon='')
    {
    	$id = $this->decrypt($id_cupon);

    	$cupon = $this->consulta->get_result("id",$id,"cupones");
        foreach ($cupon as $row){
            $data["id"] = $row->id;
            $data["cupon"] = $row->cupon;
            $data["usuario"] = $row->id_recomendador;
            $data["nombre_usuario"] = $row->usuario;
            $data["descripcion"] = $row->descripcion;
            $data["limite_uso"] = $row->limite_uso;
            $data["fecha_activacion"] = $row->fecha_activacion;
            $data["fecha_caducidad"] = $row->fecha_caducidad;
            $data["indefinido"] = $row->sin_caducidad;
            $data["descuento"] = $row->descuento;
            $data["beneficio"] = $row->beneficio;
        }

        $paquete = $this->consulta->get_result("id_cupon",$id,"cupon_servicio");
        foreach ($paquete as $row){
        	if ($row->activo == "1") {
        		$data["pqt"][] = $row->id_servicio;
        	}
        }

        //$data['listado'] = $this->consulta->get_result('id_cupon',$data["id"],'cupon_historico');
        $data['usuarios'] = $this->consulta->get_result("activo","1","recomendadores");
		$data['servicios'] = $this->consulta->get_table('servicios');
		$this->blade->render('formulario',$data); 
    }

    public function reporte_recomendador($id_cupon='')
    {
        $id_cupon = $this->decrypt($id_cupon);

        $data["recomendador"] = $this->consulta->recuperar_recomendador($id_cupon);
        
        $cupon = $this->consulta->get_result("id",$id_cupon,"cupones");
        foreach ($cupon as $row){
            $data["id_cupon"] = $row->id;
            $data["cupon"] = $row->cupon;
            $data["id_reco"] = $row->id_recomendador;
            //$data["nombre_usuario"] = $row->usuario;
            $data["descripcion"] = $row->descripcion;
            $data["beneficio"] = $row->beneficio;
            $data["monto_pagado"] = $row->monto_pagado;
        }

        $data['listado'] = $this->consulta->get_result('id_cupon',$data["id_cupon"],'cupon_historico');

        $this->blade->render('reporte_cupon',$data); 
    }

    public function pagar_bonificaciones()
    {
        $monto = $_POST["monto_pago"];
        $id_cupon = $_POST["cupon"];
        $id_recomendador = $_POST["reco_user"];
        $id_usuario_paga = $this->session->userdata('id');
        $registro = date("Y-m-d H:i:s");

        $contenedor = array(
            "id" => NULL,
            "id_usuario_paga" => $id_usuario_paga,
            "id_cupon" => $id_cupon,
            "id_recomendador" => $id_recomendador,
            "monto_pagado" => $monto,
            "fecha_pago" => $registro
        );

        $historial = $this->consulta->save_register('recomendadores_pagos',$contenedor);
        if ($historial != 0) {
            //Actualizamos los montos finales
            $nvo_monto = $_POST["deposito"] + $monto;
            $monto_cupon = array(
                "monto_pagado" => $nvo_monto
            );

            $actualizar = $this->consulta->update_table_row('cupones',$monto_cupon,'id',$id_cupon);
            
            $respuesta = "OK_0";
        } else {
            $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_0";
        }

        echo $respuesta;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
}