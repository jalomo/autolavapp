<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Login extends MX_Controller
{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'companies', 'url'));
        $this->load->library('form_validation');
        date_default_timezone_set('America/Mexico_City');
    }

    public function index()
    {
        $this->load->view('login/index', '', FALSE);
    }

    public function crear_admin()
    {

        $this->load->view('login/registro_admin');
    }
    public function checkDataLogin()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if (isset($username) && isset($password) && !empty($password) && !empty($username)) {
            //   $pass = encrypt_password($username,
            //                            $this->config->item('encryption_key'),
            //                            $password);
            $pass = md5(sha1($password));
            $total = $this->Mgeneral->count_results_users($username, $pass);
            if ($total == 1) {
                echo "1";
            } else {
                echo "0";
            }
        } else {
            redirect('login');
        }
    }

    /*
  	*metodo para inicio de session
  	*/
    public function mainView()
    {
        $post = $this->input->post('Login');
        if (isset($post) && !empty($post)) {
            $pass = md5(sha1($post['adminPassword']));
            $dataUser = $this->Mgeneral->get_all_data_users_specific($post['adminUsername'], $pass);
            
            if($dataUser){
                $array_session = array(
                'id' => $dataUser->adminId, 
                'tipo' => $dataUser->tipo, 
                'id_rol' => $dataUser->idRol,
                'id_sucursal' => $dataUser->id_sucursal,
                'id_lavador' => $dataUser->id_lavador,
                'adminNombre' => $dataUser->adminNombre
                );
                $this->session->set_userdata($array_session);
                if($this->session->userdata('id_rol')==4){
                    redirect(site_url('servicios/generar_servicio/MA/0/0/0/'.encrypt($this->session->userdata('id_lavador'))));
                }else{
                    redirect('inicio');
                }
            }else{
                
            }
        } else {
        }
    }
    public function guarda_admin()
    {
        $post = $this->input->post('Registro');
        if ($post) {
            // $pass = encrypt_password($post['adminUsername'],
            //                          $this->config->item('encryption_key'),
            //                          $post['adminPassword']);
            $pass = md5(sha1($post['adminPassword']));
            $post['adminPassword'] = $pass;
            $post['adminStatus'] = 1;
            $post['adminFecha'] = date('Y-m-d');
            $id = $this->Mgeneral->save_admin($post);
            echo $id;
        } else {
        }
    }
    public function cerrar_sesion()
    {
        $this->session->unset_userdata('id');
        $this->session->sess_destroy();
        redirect('login');
    }
}
