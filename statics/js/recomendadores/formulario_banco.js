$("#envio_form_bco").on('click', function (e){
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#cargaIcono2").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar_banco();
    if(validaciones){
        $("input").attr('disabled',false);
        $("select").attr('disabled',false);

        var base = $("#sitio").val();
        var paqueteDatos = new FormData(document.getElementById('formulario_banco'));

        $.ajax({
            url: base+"recomendadores/panel/actualizar_datos_banco",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        //location.href = base+"index.php/recomendadores/panel/datos_banco";
                        location.href = base+"index.php/recomendadores/panel/index";
                    } else {
                        $("#formulario_error2").text(resp);
                    }

                }else{
                    $("#formulario_error2").text("ERROR");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono2").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono2").css("display","none");
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono2").css("display","none");
    }
});

function validar_banco() {
    var retorno = true;
    var campo_a = $("input[name='clave_bancaria']").val();
    if (campo_a == "") {
        var error_a = $("#clave_bancaria_error");
        error_a.empty();
        $("#clave_bancaria_error").css("display","inline-block");
        error_a.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_a = $("#clave_bancaria_error");
        error_a.empty();
    }

    var campo_b = $("#banco").val();
    if (campo_b == "") {
        var error_b = $("#banco_error");
        error_b.empty();
        $("#banco_error").css("display","inline-block");
        error_b.append('<label class="form-text text-danger">Campo incompleto</label>');
        retorno = false;
    }else{
        var error_b = $("#banco_error");
        error_b.empty();

        /*var clave = $("input[name='clave_bancaria']").val();
        var clave_co =$("#clave_bco").val();
        
        if (clave.substr(0,3) != clave_co) {
            $("#banco_error").css("display","inline-block");
            error_b.append('<label class="form-text text-danger">La clabe bancaria no corresponde al banco seleccionado</label>');
            retorno = false;
        }*/
    }

    var correo_2 = $("input[name='cuenta']").val();
    if (correo_2 == "") {
        var error_2 = $("#cuenta_error");
        $("#cuenta_error").css("display","inline-block");
        error_2.append('<label class="form-text text-danger">Formato invalido.</label>');
        retorno = false;
    }else{
        var error_2 = $("#cuenta_error");
        error_2.empty();
    }

    /*var campo_c = $("input[name='n_tarjeta']").val();
    if (campo_c == "") {
        var error_c = $("#n_tarjeta_error");
        error_c.empty();
        $("#n_tarjeta_error").css("display","inline-block");
        error_c.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_c = $("#n_tarjeta_error");
        error_c.empty();
    }*/

    return retorno;
}

function buscar_banco() {
    var clave = $("input[name='clave_bancaria']").val();
    if (clave.length > 16) {
        var base = $("#sitio").val();
        var url_sis = base+"recomendadores/panel/buscar_banco";
        var clave_banco = clave.substr(0,3);
        $.ajax({
            url: url_sis,
            method: 'post',
            data: {
                clave_banco : clave_banco
            },
            success:function(resp){
                //console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    var opciones = resp.split("=");
                    var error_c = $("#clave_bancaria_error");
                    error_c.empty();

                    if (opciones[0] == "OK") {
                        document.getElementById('banco').selectedIndex = opciones[1];
                        $("#nombre_banco").val(opciones[2]);
                        var nombre_banco = $("#banco option:selected").text();
                        $("input[name='banco']").val(nombre_banco);
                        $("input[name='cuenta']").val(clave.substr(6,17));
                        $("#clave_bco").val(opciones[3]);

                        $("#banco").attr("disabled",true);
                    } else {
                        $("#clave_bancaria_error").css("display","inline-block");
                        error_c.append('<label class="form-text text-danger">No se encontro banco asociado a la clabe proporcionada</label>');
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    } else {
        var error_c = $("#clave_bancaria_error");
        error_c.empty();
        $("#clave_bancaria_error").css("display","inline-block");
        error_c.append('<label class="form-text text-danger">Clabe incompleta</label>');
    }        
}

$('#banco').on('change', function() {
    var banco = $("#banco").val();
    if (banco != "") {
        var base = $("#sitio").val();
        var url_sis = base+"recomendadores/panel/buscar_banco_nombre";

        $.ajax({
            url: url_sis,
            method: 'post',
            data: {
                banco : banco
            },
            success:function(resp){
                //console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    var opciones = resp.split("=");
                    var error_c = $("#clave_bancaria_error");
                    error_c.empty();

                    if (opciones[0] == "OK") {
                        var clave = $("input[name='clave_bancaria']").val();
                        if (clave != "") {
                            if (clave.substr(0,3) != opciones[3]) {
                                $("#clave_bancaria_error").css("display","inline-block");
                                error_c.append('<label class="form-text text-danger">La clabe proporcionada no coincide con el banco seleccionado</label>');
                            }
                        }else{
                            $("input[name='banco']").val(opciones[1]);
                            $("#nombre_banco").val(opciones[2]);
                            $("#clave_bco").val(opciones[3]);
                        }
                    } else {
                        $("#clave_bancaria_error").css("display","inline-block");
                        error_c.append('<label class="form-text text-danger">No se encontro banco asociado a la clabe proporcionada</label>');
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    } else {
        var error_c = $("#clave_bancaria_error");
        error_c.empty();
        $("#clave_bancaria_error").css("display","inline-block");
        error_c.append('<label class="form-text text-danger">Clabe incompleta</label>');
    }     
});