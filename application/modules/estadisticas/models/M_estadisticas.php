<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_estadisticas extends CI_Model
{
    public function getCitas($origen)
    {
        if (isset($_POST['fecha_inicio']) && isset($_POST['fecha_fin'])) {
            $this->db->where('created_at >=', $_POST['fecha_inicio']);
            $this->db->where('created_at <=', $_POST['fecha_fin']);
        }
        return $this->db->select('count(id) as total')->where('origen', $origen)->get('servicio_lavado')->row()->total;
    }
    //Servicios por origen
    public function getServiciosByOrigen()
    {
        if (isset($_POST['fecha_inicio']) && isset($_POST['fecha_fin'])) {
            $this->db->where('date(created_at) >=', $_POST['fecha_inicio']);
            $this->db->where('date(created_at) <=', $_POST['fecha_fin']);
        }
        if ($this->input->post('origen')) {
            $this->db->where_in('s.origen', implode(',', $this->input->post('origen')), FALSE);
        }
        if ($this->input->post('confirmadas')) {
            $this->db->where_in('s.confirmado', implode(',', $this->input->post('confirmadas')), FALSE);
        }
        if ($this->input->post('cancelados')) {
            $this->db->where_in('s.cancelado', implode(',', $this->input->post('cancelados')), FALSE);
        }
        if ($this->session->userdata('id_rol') != 1) {
            $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
        } else {
            $this->db->where('id_sucursal', $_POST['id_sucursal']);
        }
        return $this->db->select('count(s.id) as total, o.origen ')
            ->join('cat_origenes o', 's.origen = o.id')
            ->group_by('origen')
            ->get('servicio_lavado s')
            ->result();
    }
    public function getDataServicios()
    {

        if ((isset($_POST['fecha_inicio']) && $_POST['fecha_inicio'] != '') && (isset($_POST['fecha_fin']) && $_POST['fecha_fin'] != '')) {
            $this->db->where('date(fecha_creacion_servicio) >=', $_POST['fecha_inicio']);
            $this->db->where('date(fecha_creacion_servicio) <=', $_POST['fecha_fin']);
        } else {
            $this->db->where('date(fecha_creacion_servicio) >=', date('Y-m-d'));
            $this->db->where('date(fecha_creacion_servicio) <=', date('Y-m-d'));
        }
        if (isset($_POST['id_lavador']) && $_POST['id_lavador'] != '') {
            $this->db->where('id_lavador', $_POST['id_lavador']);
        }
        if (isset($_POST['id_servicio']) && $_POST['id_servicio'] != '') {
            $this->db->where('id_servicio', $_POST['id_servicio']);
        }
        if ($this->input->post('origen')) {
            $this->db->where_in('id_origen', implode(',', $this->input->post('origen')), FALSE);
        }
        if ($this->input->post('confirmadas')) {
            $this->db->where_in('confirmado', implode(',', $this->input->post('confirmadas')), FALSE);
        }
        if ($this->input->post('cancelados')) {
            $this->db->where_in('cancelado', implode(',', $this->input->post('cancelados')), FALSE);
        }
        if ($this->session->userdata('id_rol') != 1) {
            $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
        } else {
            $this->db->where('id_sucursal', $_POST['id_sucursal']);
        }
        return $this->db->order_by('origen')->get('v_info_servicios')->result();
    }
    public function getProductividad()
    {
        if ((isset($_POST['fecha_inicio']) && $_POST['fecha_inicio'] != '') && (isset($_POST['fecha_fin']) && $_POST['fecha_fin'] != '')) {
            $this->db->where('date(fecha_creacion_servicio) >=', $_POST['fecha_inicio']);
            $this->db->where('date(fecha_creacion_servicio) <=', $_POST['fecha_fin']);
        } else {
            $this->db->where('date(fecha_creacion_servicio) >=', date('Y-m-d'));
            $this->db->where('date(fecha_creacion_servicio) <=', date('Y-m-d'));
        }
        if (isset($_POST['id_lavador']) && $_POST['id_lavador'] != '') {
            $this->db->where('id_lavador', $_POST['id_lavador']);
        }
        if (isset($_POST['id_servicio']) && $_POST['id_servicio'] != '') {
            $this->db->where('id_servicio', $_POST['id_servicio']);
        }
        if ($this->session->userdata('id_rol') != 1) {
            $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
        } else {
            $this->db->where('id_sucursal', $_POST['id_sucursal']);
        }
        return $this->db->order_by('id_transicion', 'asc')->get('v_kpi_lavador')->result();
    }
    //Obtener información por servicio de los estatus
    public function getDataTransicionesByEstatus($id_lavador = '', $id_servicio = '')
    {
        if ((isset($_POST['fecha_inicio']) && $_POST['fecha_inicio'] != '') && (isset($_POST['fecha_fin']) && $_POST['fecha_fin'] != '')) {
            $this->db->where('date(fecha_creacion_servicio) >=', $_POST['fecha_inicio']);
            $this->db->where('date(fecha_creacion_servicio) <=', $_POST['fecha_fin']);
        } else {
            $this->db->where('date(fecha_creacion_servicio) >=', date('Y-m-d'));
            $this->db->where('date(fecha_creacion_servicio) <=', date('Y-m-d'));
        }
        if ($id_lavador != '') {
            $this->db->where('id_lavador', $id_lavador);
        }
        if ($id_servicio != '') {
            $this->db->where('id_servicio', $id_servicio);
        }
        if ($this->session->userdata('id_rol') != 1) {
            $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
        } else {
            $this->db->where('id_sucursal', $_POST['id_sucursal']);
        }
        return $this->db->where('cancelado',0)->get('v_productividad_lavadores')->result();
    }
    public function getDatosPeriodos()
    {
        if ($this->input->post('anio') != '') {
            $anio = $this->input->post('anio');
        } else {
            $anio = date('Y');
        }
        if ($this->input->post('mes') != '') {
            $mes = $this->input->post('mes');
            $fecha_inicio = "01/" . $this->getMes($mes) . "/" . $anio;
            $fecha_fin = "31/" . $this->getMes($mes) . "/" . $anio;
        } else {
            $fecha_inicio = '01/01/' . $anio;
            $fecha_fin = '31/12/' . $anio;
        }
        if ($this->input->post('reagendado') != null) {
            $this->db->where('reagendado', 1);
        }
        if ($this->input->post('cancelado') != null) {
            $this->db->where('cancelado', 1);
        }
        if ($this->input->post('confirmado') != null) {
            $this->db->where('confirmado', 1);
        }
        if ($this->input->post('id_estatus_lavado') != '') {
            $this->db->where('id_estatus_lavado', $this->input->post('id_estatus_lavado'));
        }
        if ($this->input->post('id_lavador') != '') {
            $this->db->where('id_lavador', $this->input->post('id_lavador'));
        }
        if ($this->session->userdata('id_rol') != 1) {
            $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
        } else {
            $this->db->where('id_sucursal', $_POST['id_sucursal']);
        }
        return $this->db->where('fecha_programacion >=', date2sql($fecha_inicio))
            ->where('fecha_programacion <=', date2sql($fecha_fin))
            ->select('id,fecha_programacion as fecha')
            ->get('servicio_lavado')->result();
    }
    public function getMes($mes)
    {
        switch ($mes) {
            case 'ene.':
                return '01';
                break;
            case 'feb.':
                return '02';
                break;
            case 'mar.':
                return '03';
                break;
            case 'abr.':
                return '04';
                break;
            case 'may.':
                return '05';
                break;
            case 'jun.':
                return '06';
                break;
            case 'jul.':
                return '07';
                break;
            case 'ago.':
                return '08';
                break;
            case 'sep.':
                return '09';
                break;
            case 'oct.':
                return '10';
                break;
            case 'nov.':
                return '11';
                break;
            case 'dic.':
                return '12';
                break;
            default:
                break;
        }
    }
    public function getLavadoresBySucursal($id_sucursal = '')
    {
        return $this->db->where('id_sucursal', $id_sucursal)->get('lavadores')->result();
    }
    public function getCantidadServicios()
    {

        if ($this->session->userdata('id_rol') != 1) {
            $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
        } else {
            if ($_POST['id_sucursal'] != null) {
                $this->db->where('suc.id', $_POST['id_sucursal']);
            }
        }
        if (isset($_POST['id_tipo_pago']) && $_POST['id_tipo_pago'] != null) {
            $this->db->where('s.id_tipo_pago', $_POST['id_tipo_pago']);
        }
        return $this->db->select('MONTH(h.fecha) as mes, count(s.id) as total,suc.sucursal')
            ->join('horarios_lavadores h', 's.id_horario = h.id')
            ->join('sucursales suc', 's.id_sucursal=suc.id')
            ->where('YEAR(h.fecha)', $_POST['anio'])
            ->group_by('suc.sucursal,MONTH(h.fecha)')
            ->get('servicio_lavado s')
            ->result();
    }
    //Obtener información de seguro de lluvia
    public function getCantidadMetodosPago()
    {
        if ($this->session->userdata('id_rol') != 1) {
            $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
        } else {
            if ($_POST['id_sucursal'] != null) {
                $this->db->where('s.id_sucursal', $_POST['id_sucursal']);
            }
        }
        if (isset($_POST['id_tipo_pago']) && $_POST['id_tipo_pago'] != null) {
            $this->db->where('s.id_tipo_pago', $_POST['id_tipo_pago']);
        }
        return $this->db->select('MONTH(h.fecha) as mes, count(s.id) as total,cm.metodo_pago')
            ->join('horarios_lavadores h', 's.id_horario = h.id')
            ->join('cat_metodo_pago cm', 's.id_tipo_pago=cm.id')
            ->where('YEAR(h.fecha)', $_POST['anio'])
            ->group_by('cm.metodo_pago,MONTH(h.fecha)')
            ->get('servicio_lavado s')
            ->result();
    }
    public function cantidad_servicios_sucursal()
    {
        if ($_POST['id_sucursal'] != null) {
            $this->db->where('s.id_sucursal', $_POST['id_sucursal']);
        }
        return $this->db->select('MONTH(h.fecha) as mes, count(s.id) as total')
            ->join('horarios_lavadores h', 's.id_horario = h.id')
            ->where('YEAR(h.fecha)', $_POST['anio'])
            ->group_by('MONTH(h.fecha)')
            ->get('servicio_lavado s')
            ->result();
    }
}
