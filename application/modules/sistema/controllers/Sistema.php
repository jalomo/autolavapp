<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Sistema extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set('America/Mexico_City');

    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
  }

  public function  contactos()
  {
    $data['titulo'] = "Sistema";
    $data['titulo_dos'] = "mis contactos";
    $usuarios = $this->db->where('telefono !=', null)->get('usuarios')->result();
    $recomendadores = $this->db->where('celular !=', null)->get('recomendadores')->result();
    $lavadores = $this->db->where('lavadorTelefono !=', null)->get('lavadores')->result();
    $telefono = $this->session->userdata('telefono');
    $dataNotificacion = curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono, false, false);
    $notificaciones = json_decode($dataNotificacion)->data;

    $listadolavadores = [];
    foreach ($lavadores as $lavador) {
      $lavador->notificacion = false;
      foreach ($notificaciones as $notify) {
        if ($notify->status != 1) {
          if ($notify->celular2 == $lavador->lavadorTelefono) {
            $lavador->notificacion = true;
          }
        }
      }
      array_push($listadolavadores, $lavador);
    }

    $listadorecomendador = [];
    foreach ($recomendadores as $recomendador) {
      $recomendador->notificacion = false;
      foreach ($notificaciones as $notify) {
        if ($notify->status != 1) {
          if ($notify->celular2 == $recomendador->celular) {
            $recomendador->notificacion = true;
          }
        }
      }
      array_push($listadorecomendador, $recomendador);
    }

    $listadousuarios = [];
    foreach ($usuarios as $user) {
      $user->notificacion = false;
      foreach ($notificaciones as $notify) {
        if ($notify->status != 1) {
          if ($notify->celular2 == $user->telefono) {
            $user->notificacion = true;
          }
        }
      }
      array_push($listadousuarios, $user);
    }
    // echo '<pre>'; print_r($notificaciones); exit();
    $data['lavadores'] = $listadolavadores;
    $data['recomendadores'] = $listadorecomendador;
    $data['usuarios'] = $listadousuarios;
    $this->blade->render('sistema/contactos', $data);
  }

  public function  notificaciones()
  {
    $data['titulo'] = "Sistema";
    $data['titulo_dos'] = "mis contactos";
    $id_usuario = $this->session->userdata('id');
    $datos = $this->db->where('adminId', $id_usuario)->get('admin')->row_array();

    $data['telefono'] = $datos['telefono'];
    $this->blade->render('sistema/mis_notificaciones', $data);
  }

  public function  chat()
  {
    $data['titulo'] = "Sistema";
    $data['titulo_dos'] = "Chat";
    $id_usuario = $this->session->userdata('id');
    $datos = $this->db->where('adminId', $id_usuario)->get('admin')->row_array();
    $telefono1 = $datos['telefono'];
    $telefono2 = $this->input->get('telefono_destinatario');

    $data['telefono_remitente'] = $telefono1;
    $data['telefono_destinatario'] = $telefono2;
    $data['remitente'] = $this->getUserTelefono($telefono1);
    $data['destinatario'] = $this->getUserTelefono($telefono2);

    $data['telefono_panel'] = $datos['telefono'];
    $data['telefono_destinatario'] = $this->input->get('telefono_destinatario');
    $data['template2'] = "layout";
    $this->blade->render('sistema/chat_webView', $data);
  }

  public function getUserTelefono($telefono)
  {
    $usuarios_admin = $this->db->select('adminNombre as nombre, telefono')->where('telefono', $telefono)->get('admin')->result_array();
    $usuarios_cliente = $this->db->select('nombre, telefono')->where('telefono', $telefono)->get('usuarios')->result_array();
    $usuarios_recomendador = $this->db->select('nombre, celular as telefono')->where('celular', $telefono)->get('recomendadores')->result_array();
    $usuarios_lavadores = $this->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "Lavador" as tipo')->where('lavadorTelefono', $telefono)->get('lavadores')->result_array();
    $usuarios = array_merge($usuarios_admin, $usuarios_cliente, $usuarios_recomendador, $usuarios_lavadores);
    if (is_array($usuarios)) {
      return current($usuarios)['nombre'];
    }
  }
}
