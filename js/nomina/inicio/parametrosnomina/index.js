var _appsFunction = function () {
	this.init = function () {

		var change_input = {
			"NumeroNomina": {type: "number",max:"2",min:"0"},
			"DiasPago": {type: "number",step:"0.01",min:"0.00"},
			"DiasAnio": {type: "number",step:"0.01",min:"1",max:"366"},
			"SalarioMinimoSM": {type: "number",step:"0.01",min:"1"},
			"UMA": {type: "number",step:"0.01",min:"1"},
			"LimiteImpuestoLoc": {type: "number",step:"0.01",min:"1"},
			"LimiteVecesUMA": {type: "number",step:"0.01",min:"1"},
			"HorasTrabajo": {type: "number",step:"0.01",min:"1",max:"23:59"},
		};
		update_inputs(change_input);
		update_select2('form#general_form select');
		
	},
	this.guardar = function () {
	   var data_send =  $('form#general_form').serializeArray();
	   data_send.push({ 'name': 'id','value':identity });

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/inicio/parametrosnomina/guardar_general',
            data: data_send,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					});
				}

            }

        });
    }		
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
