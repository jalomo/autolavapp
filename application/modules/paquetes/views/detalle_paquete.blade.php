<div class="alert alert-info text-center" role="alert">
    Es necesario asignar todos los horarios para completar el registro
</div>
<h3 class="text-right">Precio: $ {{ number_format($precio, 2) }}</h3>
<div class="row">
    <div class="col-sm-4">
        <label for="">Nombre paquete: </label> <strong>{{ $nombre_paquete }}</strong>
    </div>
    <div class="col-sm-4">
        <label for="">Descripción paquete: </label> <strong>{{ $descripcion_paquete }}</strong>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Servicio</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php $posicion = 1; ?>
                @foreach ($detalle_paquete as $d => $detalle)

                    @for ($i = 0; $i < $detalle->cantidad; $i++)
                        <?php $info_detalle = $this->mp->getDetalleHorario($id_servicio_lavado,
                        $posicion, $posicion == 1 ? 1 : 0); ?>
                        @if ($info_detalle)
                            <tr>
                                <td>
                                    <input type="text" class="form-control" value="{{ $detalle->servicioNombre }}"
                                        readonly>
                                </td>
                                <td>
                                    <select data-id="{{ $posicion }}" name="fecha[{{ $posicion }}]"
                                        id="fecha_{{ $posicion }}" class="form-control js_select_hour">
                                        <option value="">-- Selecciona fecha --</option>
                                        @foreach ($fechas_lavadores as $f => $value)
                                            <?php $dia = dayOfweek(date('N', strtotime($value->fecha)));
                                            ?>
                                            <option value="{{ $value->fecha }}">
                                                {{ $dia . ' - ' . date_eng2esp_1($value->fecha) }}
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <?php $horarios =
                                    $this->mp->getHorariosByLavador($info_detalle->fecha, $info_detalle->id_lavador,
                                    $info_detalle->id_horario); ?>
                                    <select data-id="{{ $posicion }}" name="hora[{{ $posicion }}]" id="hora_{{ $posicion }}"
                                        class="form-control js_hora" data-id_horario="{{ $info_detalle->id_horario }}">
                                        <option value="">-- Selecciona el horario --</option>
                                        @foreach ($horarios as $h => $horario)
                                            <option
                                                {{ $info_detalle->hora == $horario->hora . ':00' ? 'selected' : '' }}
                                                value="{{ $horario->hora }}">{{ $horario->hora }}
                                            </option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <button id="button_{{ $posicion }}"
                                        data-id_servicio_lavado="{{ $info_detalle->id_servicio }}"
                                        data-id_horario="{{ $info_detalle->id_horario }}"
                                        data-id_servicio="{{ $detalle->servicioId }}" type="button"
                                        data-principal="{{ $posicion == 1 ? 1 : 0 }}" data-pos="{{ $posicion }}"
                                        class="btn btn-success js_guardar_servicio">Actualizar</button>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>
                                    <input type="text" class="form-control" value="{{ $detalle->servicioNombre }}"
                                        readonly>
                                </td>
                                <td>

                                    <select data-id="{{ $posicion }}" name="fecha[{{ $posicion }}]"
                                        id="fecha_{{ $posicion }}" class="form-control js_select_hour">
                                        <option value="">-- Selecciona fecha --</option>
                                        @foreach ($fechas_lavadores as $f => $value)
                                            <?php $dia = dayOfweek(date('N', strtotime($value->fecha)));
                                            ?>
                                            <option value="{{ $value->fecha }}">
                                                {{ $dia . ' - ' . date_eng2esp_1($value->fecha) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select data-id="{{ $posicion }}" name="hora[{{ $posicion }}]" id="hora_{{ $posicion }}"
                                        class="form-control js_hora" data-id_horario="0">
                                        <option value="">-- Selecciona el horario --</option>
                                    </select>
                                </td>
                                <td>
                                    <button id="button_{{ $posicion }}" data-id_servicio_lavado="0" data-id_horario="0"
                                        data-id_servicio="{{ $detalle->servicioId }}" type="button"
                                        data-principal="{{ $posicion == 1 ? 1 : 0 }}" data-pos="{{ $posicion }}"
                                        class="btn btn-success js_guardar_servicio">Guardar servicio</button>
                                </td>
                            </tr>
                        @endif
                        <?php $posicion++; ?>
                    @endfor
                @endforeach
            </tbody>
        </table>
    </div>
</div>
