<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_excel extends CI_Model{
	private $_batchImport;
	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Mexico_City');
	}
	  //Validar si ya existe el vin y la campaña
    public function validarCampania($vin='',$asc1=''){
      $q= $this->db->where('vin',$vin)->where('asc1',$asc1)->get('asc_campanias');
      if($q->num_rows()==1){
        return true;
      }else{
        return false;
      }
    }
    public function setBatchImport($batchImport) {
        $this->_batchImport = $batchImport;
    }
    // save data
    public function importData($path='',$random='') {
        //$this->deleteDataASC();
        $data = $this->_batchImport;

        if(count($data)>0){
          $this->db->insert_batch('asc_campanias', $data);
          $campanias = $this->db->where('random',$random)->get('asc_campanias')->result();
          foreach ($campanias as $key => $value) {
              $datos = array('tipo'=>$value->asc1,'vin'=>$value->vin);
              $this->db->insert('campanias',$datos);
          }
          if(file_exists($path)){
            unlink($path);
          }
        }else{
          echo 'El documento está vacío';
        }
    }
    // save data
    public function importDataUnidades($path='') {
        $data = $this->_batchImport;
        if(count($data)>0){
          $this->db->insert_batch('proactivo_unidades', $data);
          if(file_exists($path)){
            unlink($path);
          }
        }else{
          echo 'El documento está vacío';
        }
        
    }
    public function deleteDataASC(){
      //Solo borra las que no tienen comentarios
      $datos_id = $this->db->query('select c.id from asc_campanias c join historial_comentarios_campanias h  on h.id_campania = c.id')->result();
      $arr_id_eliminar = array();
      $cadena = '';
      foreach ($datos_id as $key => $value) {
         $cadena.= $value->id.',';
      }
      $cadena = substr($cadena, 0,-1);
      if($cadena!=''){
        $this->db->where_not_in('id',$cadena,false);
      }
       $this->db->where('asc2','')->where('asc3','')->where('asc4','')->delete('asc_campanias');
    }
    // get employee list
    public function employeeList() {
        $this->db->select(array('e.id', 'e.first_name', 'e.last_name', 'e.email', 'e.dob', 'e.contact_no'));
        $this->db->from('import as e');
        $query = $this->db->get();
        return $query->result_array();
    }
   public function exportarOrden(){
    ini_set('memory_limit', '-1');
    if($_POST['id_asesor']!=''){
      $this->db->where('c.asesor',$_POST['id_asesor']);
    }

    if($_POST['id_status_search']!=''){
      $this->db->where('o.id_status_intelisis',$_POST['id_status_search']);
    }
    if($_POST['id_situacion_intelisis_search']!=''){
      $this->db->where('o.id_situacion_intelisis',$_POST['id_situacion_intelisis_search']);
    }

    if($_POST['cita_previa']!=''){
      $this->db->where('c.cita_previa',$_POST['cita_previa']);
    }

    if(count($this->input->post('id_status_color'))>0){
      $this->db->where_in('c.id_status_color',implode(',',$this->input->post('id_status_color')),FALSE);
    } 

    if(count($this->input->post('id_status_tecnico'))>0){
      $this->db->where_in('c.id_status',implode(',',$this->input->post('id_status_tecnico')),FALSE);
    }

    $this->db->join('citas c','c.id_cita=o.id_cita');
    if($_POST['finicio']!='' || $_POST['ffin']!=''){
      $this->db->join('tecnicos_citas tc','c.id_cita=tc.id_cita');
      $this->db->where('tc.dia_completo',0);
    }
    $this->db->where('c.id_status !=',20);

    if($_POST['finicio']!=''){
      $this->db->where('tc.fecha >=',date2sql($_POST['finicio']));
    }
    if($_POST['ffin']!=''){
      $this->db->where('tc.fecha <=',date2sql($_POST['ffin']));
    }

    $this->db->select('o.*,c.*, csio.status as estatus_intelisis, csit.situacion as situacion_intelisis, cso.status as estatus_orden,cso.id as id_estatus_orden,cto.identificador,csc.status as status_tecnico,a.fecha as fecha_asesor,a.hora as hora_asesor, t.nombre as tecnico,e.nombre as estatus_cita');
    return $this->db->join('cat_status_intelisis_orden csio','o.id_status_intelisis = csio.id','left')
                              ->join('cat_situacion_intelisis csit','o.id_situacion_intelisis = csit.id','left')
                              ->join('cat_status_orden cso','cso.id = o.id_status','left')
                              ->join('cat_tipo_orden cto','o.id_tipo_orden=cto.id','left')
                              ->join('cat_status_citas_tecnicos csc','c.id_status=csc.id')
                              ->join('aux a','c.id_horario=a.id','left')
                              ->join('tecnicos t','c.id_tecnico=t.id','left')
                              ->join('estatus e','c.id_status_color=e.id')
                              ->order_by('o.id','desc')
                              ->where('o.cancelada',0)
                              ->get('ordenservicio o')
                              ->result();                  
    }
} 