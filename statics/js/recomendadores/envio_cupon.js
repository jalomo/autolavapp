$("#enviar_cupon").on('click', function (e){
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#cargaIcono").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        $("input").attr('disabled',false);

        var base = $("#sitio").val();
        var paqueteDatos = new FormData(document.getElementById('formulario_cupon'));

        $.ajax({
            url: base+"recomendadores/panel/enviar_sms_cupon", 
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        location.href = base+"index.php/recomendadores/panel/cupones";
                    } else {
                        $("#formulario_error").text(resp);
                    }
                }else{
                    $("#formulario_error").text("ERROR");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
    }
});

function validar() {
    var retorno = true;

    var check_contacto = $(".contacto:checked").length;

    if (check_contacto <= 0) {
        var error_8 = $("#contacto_error");
        error_8.empty();
        $("#contacto_error").css("display","inline-block");
        //error_8.append("<br>");
        error_8.append('<label class="form-text text-danger">Falta indicar al menos un contacto</label>');
        retorno = false;
    }else{
        var error_8 = $("#contacto_error");
        error_8.empty();
    }

    return retorno;
}


//###------------------------------------ Envio de nuevo contacto
$("#nvo_contacto").on('click', function (e){
    //Creamos visualmente el efecto de carga

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar_contacto();
    if(validaciones){
        $("input").attr('disabled',false);
        //$("select").attr('disabled',false);

        var base = $("#sitio").val();
        var paqueteDatos = new FormData(document.getElementById('formulario_contacto'));

        $.ajax({
            url: base+"recomendadores/panel/nvo_contacto_envio",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        location.reload();
                    } else {
                        $("#formulario_error").text(resp);
                    }

                }else{
                    $("#formulario_error").text("ERROR");
                }
            //Cierre de success
            },
              error:function(error){
                console.log(error);
            }
        });
    }else{
        console.log("Sin validar");
    }
});

function validar_contacto() {
    var retorno = true;
    var campo_a = $("input[name='nombre']").val();
    if (campo_a == "") {
        var error_a = $("#nombre_error");
        error_a.empty();
        $("#nombre_error").css("display","inline-block");
        error_a.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_a = $("#nombre_error");
        error_a.empty();
    }

    var correo_2 = $("input[name='telefono']").val();
    if (correo_2 == "") {
        var error_2 = $("#telefono_error");
        $("#telefono_error").css("display","inline-block");
        error_2.append('<label class="form-text text-danger">Formato invalido.</label>');
        retorno = false;
    }else{
        var error_2 = $("#telefono_error");
        error_2.empty();
    }

    return retorno;
}
