@layout('layout')
@section('included_css')
    <link href="statics/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <style>
        .fa {
            margin-right: 8px;
        }

    </style>
@endsection
@section('contenido')
    <h1 class="text-center">Proactivo</h1>
    <div class="row">
        <div class='col-sm-3'>
            <label for="">Selecciona la agencia</label>
            {{ $id_agencia }}
            <span class="error error_id_agencia"></span>
        </div>
        <div class='col-sm-3'>
            <label for="">Selecciona la fecha</label>
            <div class="form-group1">
                <div class='input-group date' id='datetimepicker1'>
                    <input id="fecha" name="fecha" type='text' class="form-control" value="{{ date('d/m/Y') }}" />
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </div>
            <span class="error_fecha"></span>
        </div>
        <div class="col-sm-2">
            <br>
            <button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="tabla-items"></div>
            <table id="tbl_proactivo" class="table table-hover table-striped">
                <thead>
                    <tr class="tr_principal">
                        <th>Acciones</th>
                        <th>ID</th>
                        <th>Agencia</th>
                        <th>ID cita</th>
                        <th>Fecha</th>
                        <th>Intentos</th>
                        <th>Kilometraje</th>
                        <th>Placas</th>
                        <th>Cliente</th>
                        <th>Email</th>
                        <th>Tel.</th>
                        <th>Otro tel.</th>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Serie</th>
                        <th>Servicio</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
@endsection

@section('included_js')
    @include('main/scripts_dt')
    <script src="statics/js/moment.js"></script>
    <script src="statics/js/bootstrap-datetimepicker.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var cliente = '';
        var idinfo = '';
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            daysOfWeekDisabled: [0],
            locale: 'es'
        });
        iniciarTabla()

        function iniciarTabla() {
            var tabla = $("#tbl_proactivo").dataTable({
                paging: true,
                bFilter: false,
                processing: true,
                responsive: true,
                serverSide: true,
                ajax: {
                    url: site_url + "/proactivo_agencias/getDatosProactivo",
                    type: 'POST',
                    //////////////////////////////////////////////////////////////// AQUI PUEDO AGREGAR MAS CAMPOS EN LA PETICION
                    data: function(data) {
                        data.fecha = $("#fecha").val();
                        data.id_agencia = $("#id_agencia").val();
                        data.buscar_campo = $("#buscar_campo").val();
                        empieza = data.start;
                        por_pagina = data.length;
                    }
                },
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar"
                },
            });
        }

        $("body").on("click", '.js_comentarios', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            cliente = $(this).data('cliente');
            id_agencia = $(this).data('id_agencia');
            var url = site_url + "/proactivo_agencias/comentarios/0";
            customModal(url, {
                "id": id,
                'fecha': $(this).data('fecha'),
                'id_agencia': id_agencia,
                "cliente": cliente
            }, "GET", "md", ingresarComentario, "", "Guardar", "Cancelar", "Ingresar comentario", "modal1");
        });
        $("body").on("click", '.js_historial', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            id_agencia = $(this).data('id_agencia');
            var url = site_url + "/proactivo_agencias/historial_comentarios/0";
            customModal(url, {
                "id": id,
                id_agencia:id_agencia
            }, "GET", "md", "", "", "", "Cerrar", "Historial de comentarios", "modal1");
        });

        function ingresarComentario() {
            var url = site_url + "/proactivo_agencias/comentarios";
            ajaxJson(url, $("#frm_comentarios").serialize(), "POST", "async", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(data, function(i, item) {
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    });
                } else {
                    if (result < 0) {
                        ErrorCustom('No se pudo guardar el comentario, por favor intenta de nuevo');
                    } else {
                        $(".close").trigger('click')
                        ExitoCustom("Comentario guardado con éxito", function() {
                            $("#buscar").trigger('click');
                        });

                    }
                }
            });
        }
        $("#buscar").on('click', function() {
            $(".error").empty();
            if($("#id_agencia").val()==''){
                $(".error_id_agencia").empty().append('Es necesario seleccionar la agencia').css('color', 'red')
                return;
            }
            if($("#fecha").val()==''){
                $(".error_fecha").empty().append('Es necesario seleccionar la fecha').css('color', 'red')
                return;
            }
            var tipo_busqueda = $("#tipo_busqueda").val();
            var finicio = $("#finicio").val();
            var ffin = $("#ffin").val();
            var table = $('#tbl_proactivo').DataTable();
            table.destroy();
            iniciarTabla();

        })
        $("body").on('click', '#enviar-correo', function() {
            var url = site_url + "/proactivo_agencias/correo_cp";
            ajaxJson(url, {}, "POST", "async", function(result) {
                ExitoCustom("Correo enviado correctamente");
            });
        });
        $("body").on("click", '.js_actualizar_asc', function(e) {
            e.preventDefault();
            campo = $(this).data('campo');
            id_input = $(this).data('idcampo');
            serie = $(this).data('serie');
            id = $(this).data('id');
            valor_input = $("#" + id_input).val();

            if (valor_input == '') {
                ErrorCustom("Es necesario ingresar el campo");
            } else {
                ConfirmCustom("¿Está seguro de actualizar el registro?", cambiarASC, "", "Confirmar", "Cancelar");

            }

        });

        function cambiarASC() {
            var url = site_url + "/proactivo_agencias/cambiarASC_Magic";

            ajaxJson(url, {
                "campo": campo,
                "valor": valor_input,
                "id": id
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('No se pudo actualizar el registro, por favor intenta de nuevo');
                } else {

                    ExitoCustom("Registro actualizado con éxito", function() {
                        $("#" + id_input).parent("td").empty().append(valor_input);
                    });

                }
            });

        }
        $("body").on("click", '.js_contactar', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            ConfirmCustom("¿Está seguro de cambiar el estatus para contactar al cliente?", contactarCliente, "",
                "Confirmar", "Cancelar");
        });

        function contactarCliente() {
            var url = site_url + "/citas/contactarCliente/0";
            ajaxJson(url, {
                "id": id
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('Error al actualizar, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Registro actualizado correctamente", function() {
                        $("#buscar").trigger('click');
                    });

                }
            });
        }
        const enviarCorreo = () => {
            var url = site_url + "/proactivo_agencias/correo_cp";
            ajaxJson(url, {
                "id": id,
                "email": email,
                "id_agencia": id_agencia,
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('Error al actualizar, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Correo enviado correctamente", function() {
                        $("#buscar").trigger('click');
                    });

                }
            });
        }
        $("body").on("click", '.js_correo', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            email = $(this).data('email');
            id_agencia = $(this).data('id_agencia');
            ConfirmCustom("¿Está seguro de enviar el correo al cliente?", enviarCorreo, "", "Confirmar",
                "Cancelar");
        });
        $("body").on("click", '.js_enviado', function(e) {
            e.preventDefault();
            ErrorCustom("El correo ya fue enviado");
        });

    </script>
@endsection
