@layout('layout')
@section('included_css')
    <link href="statics/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="statics/css/clockpicker.css" rel="stylesheet">
@endsection
@section('contenido')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <form action="" id="frm">
        @if ($this->session->userdata('id_rol') == 1)
            <div class="row">
                <div class="col-sm-6">
                    <label for="">Sucursal</label>
                    {{ $sucursales }}
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-3">
                <label for="">Lavadores</label>
                {{ $drop_lavadores }}
            </div>
            <div class="col-sm-3">
                <label>Estatus lavadores</label>
                <?php echo $drop_estatus; ?>
            </div>
            <div class="col-sm-2 form-group">
                <label>Año</label>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker_anio'>
                        <input type="text" name="anio" id="anio" class="form-control">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 form-group">
                <label>Mes</label>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker_mes'>
                        <input type="text" name="mes" id="mes" class="form-control">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 form-group">
                <button type="button" id="buscar" style="margin-top: 35px;" class="btn btn-success">Buscar</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label>Servicios reagendados</label>
                <input type="checkbox" name="reagendado" id="reagendada">
            </div>
            <div class="col-sm-3">
                <label>Servicios cancelados</label>
                <input type="checkbox" name="cancelado" id="cancelada">
            </div>
            <div class="col-sm-3">
                <label>Servicios confirmados</label>
                <input type="checkbox" name="confirmado" id="confirmada">
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-sm-12">
            <div id="div_busqueda">

            </div>

        </div>
    </div>
@endsection

@section('included_js')
    <script src="statics/js/moment.js"></script>
    <script src="statics/js/bootstrap-datetimepicker.js"></script>
    <script src="statics/js/lavadores.js"></script>
    <script type="text/javascript">
        var site_url = "{{ site_url() }}";
        const id_rol = "{{ $this->session->userdata('id_rol') }}";
        $(function() {
            $('#datetimepicker_anio').datetimepicker({
                format: 'YYYY',
                locale: 'es'
            });
            $('#datetimepicker_mes').datetimepicker({
                format: "MMM",
                viewMode: "months",
                locale: 'es'
            });
            $('#datetimepicker6').datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'es'
            });
            $('#datetimepicker7').datetimepicker({
                useCurrent: true, //Important! See issue #1075
                format: 'DD/MM/YYYY',
                locale: 'es'
            });
            $("#datetimepicker6").on("dp.change", function(e) {
                console.log($('#datetimepicker7').data("DateTimePicker").minDate(e.date));
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function(e) {
                console.log($('#datetimepicker6').data("DateTimePicker").maxDate(e.date));
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });
        $("#buscar").on('click', function() {
            buscar();
        });

        function buscar() {
            var url = site_url + "/estadisticas/getGraficaPeriodo";
            if ($("#tipo_busqueda").val() == '') {
                ErrorCustom('Es necesario ingresar el tipo de búsqueda');
            } else {
                if ($("#anio").val() == '') {
                    ErrorCustom('Es necesario ingresar el año');
                } else if (id_rol == 1 && $("#id_sucursal").val() == '') {
                    ErrorCustom('Es necesario seleccionar la sucursal');
                } else {
                    ajaxLoad(url, $("#frm").serialize(), "div_busqueda", "POST", function(result) {});
                }

            }
        }

    </script>
@endsection
