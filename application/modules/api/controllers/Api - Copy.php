<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Api extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set('America/Mexico_City');
  }

  public function registro()
  {
    $nombre = $this->input->post('nombre');
    $email = $this->input->post('email');
    $telefono = $this->input->post('telefono');
    $password = $this->input->post('password');
    $token = $this->input->post('token');
    $data['usuarioID'] = get_guid();
    $data['usuarioNombre'] = $nombre;
    $data['usuarioEmail'] = $email;
    $data['usuarioTelefono'] = $telefono;
    $data['usuarioPassword'] = $password;
    $data['token'] = $token;
    $this->Mgeneral->save_register('usuarios', $data);
    $response['error'] = 0;
    echo json_encode($response);
  }

  public function login()
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $this->db->where('usuarioEmail', $email);
    $this->db->where('usuarioPassword', $password);
    $data = $this->db->get('usuarios')->row();
    $response = "";
    if (is_object($data)) {
      $response['error'] = 0;
      $response['id'] = $data->usuarioID;
      $response['mensaje'] = "";
    } else {
      $response['error'] = 1;
      $response['mensaje'] = "Usuario o contraseña incorrectos";
    }
    echo json_encode($response);
  }

  public function prueba_notificacion($id_usuario)
  {

    $token = $this->Mapi->get_row('usuarioID', $id_usuario, 'usuarios');
    $this->load->library('Firebase');
    $firebase = new Firebase();
    $res = array();
    $res['data']['mensaje'] = "mensaje";
    $res['data']['id_servicio'] = "";
    $res['data']['foto'] = "";


    $response4 = $firebase->send($token->token, $res);

    var_dump($response4);
  }

  public function servicios()
  {
    $res = $this->Mgeneral->get_table('servicios');
    echo json_encode($res);
  }

  public function get_puntos()
  {
    $id_usuario = $this->input->post('id_usuario');
    $res = $this->Mgeneral->get_row('usuarioID', $id_usuario, 'usuarios');
    $data = "";
    if (is_object($res)) {
      $data['error'] = 0;
      $data['puntos'] = $res->usuarioPuntos;
    } else {
      $data['error'] = 1;
    }

    echo json_encode($data);
  }

  public function get_horarios()
  {
    $fecha = $this->input->post("fecha");
    $res = $this->db->like('date', $fecha)->get('appointment')->result();
    echo json_encode($res);
  }
  public function registro_auto()
  {

    $placas = $this->input->post("placas");
    $color = $this->input->post("color");
    $modelo = $this->input->post("modelo");
    $marca = $this->input->post("marca");
    $id_usuario = $this->input->post("id_usuario");

    $data['autoPlacas'] = $placas;
    $data['autoColor'] = $color;
    $data['autoModelo'] = $modelo;
    $data['autoMarca'] = $marca;
    $data['autoIdUsuario'] = $id_usuario;
    $data['autoFecha_creacion'] = time();
    $this->Mgeneral->save_register('autos', $data);

    $response['error'] = 0;
    $response['mensaje'] = "";
    echo json_encode($response);
  }
  public function estados()
  {
    $res = $this->Mgeneral->get_table('estados');
    echo json_encode($res);
  }
  //crear tabla y CRUD
  public function tipo_auto()
  {
    $id_servicio =  $this->input->post('id_servicio');
    $data = array();

    $aux['tipo'] = "Sedan";
    $aux['precio'] = "100";
    $aux['imagen'] = "https://www.proyectar.com.mx/i/cc_g1.jpg";
    array_push($data, $aux);

    $aux1['tipo'] = "SUV";
    $aux1['precio'] = "200";
    $aux1['imagen'] = "https://la-motorbit-media.s3.amazonaws.com/2015/12/marca-auto-chino-toyota-chery.jpg";
    array_push($data, $aux1);

    $aux3['tipo'] = "PICKUP";
    $aux3['precio'] = "300";
    $aux3['imagen'] = "https://http2.mlstatic.com/auto-coleccion-hot-wheels-nissan-skyline-2000-gt-r-fjx88-D_NQ_NP_706730-MLM27167540361_042018-O.webp";
    array_push($data, $aux3);

    echo json_encode($data);
  }

  public function indica_ubicacion()
  {
    $id_usuario = $this->input->post('id_usuario');
    $numero_int = $this->input->post('numero_int');
    $numero_ext = $this->input->post('numero_ext');
    $colonia = $this->input->post('colonia');
    $estado = $this->input->post('estado');
    $municipio = $this->input->post('municipio');
    $latitud = $this->input->post('latitud');
    $longitud = $this->input->post('longitud');
    $data['error'] = 0;
    $data['mensaje'] = "registro con exito";
    $data['id'] = "33";
    echo json_encode($data);
  }

  public function datos_vehiculo()
  {
    $id_usuario = $this->input->post('id_usuario');
    $marca = $this->input->post('marca');
    $modelo = $this->input->post('modelo');
    $placas = $this->input->post('placas');
    $color = $this->input->post('color');
    $data['error'] = 0;
    $data['mensaje'] = "registro con exito";
    $data['id'] = "33";
    echo json_encode($data);
  }
  //los horarios disponibles
  public function agenda()
  {
    $id_usuario = $this->input->post('id_usuario');
    $fecha = $this->input->post('fecha');

    $data = array();

    $aux['hora'] = "12:00";
    $aux['id_hora'] = "1";

    array_push($data, $aux);

    $aux1['hora'] = "13:00";
    $aux1['id_hora'] = "2";

    array_push($data, $aux1);

    $aux3['hora'] = "13:30";
    $aux3['id_hora'] = "3";

    array_push($data, $aux3);

    echo json_encode($data);
  }
}
