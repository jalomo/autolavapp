@layout('layout')
@section('contenido')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th>Fecha creación</th>
                                    <th>Folio</th>
                                    <th>Servicio</th>
                                    <th>Cliente</th>
                                    <th>Email cliente</th>
                                    <th>Teléfono</th>
                                    <th>Placas</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Año</th>
                                    <th>Color</th>
                                    <th>Lavador</th>
                                    <th>Fecha y hora servicio</th>
                                    <th>Estatus</th>
                                    <th>Puntaje</th>
                                    <th>Dirección</th>
                                    <th>Evidencia</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($info as $i => $servicio)
                                    <tr>
                                        <td>{{ $servicio->fecha_creacion_servicio }}</td>
                                        <td>{{ $servicio->folio_mostrar }}</td>
                                        <td>{{ $servicio->servicioNombre }}</td>
                                        <td>{{ $servicio->nombre . ' ' . $servicio->apellido_paterno . ' ' . $servicio->apellido_materno }}
                                        </td>
                                        <td>{{ $servicio->email }}</td>
                                        <td>{{ $servicio->telefono }}</td>
                                        <td>{{ $servicio->placas }}</td>
                                        <td>{{ $servicio->marca }}</td>
                                        <td>{{ $servicio->modelo }}</td>
                                        <td>{{ $servicio->anio }}</td>
                                        <td>{{ $servicio->color }}</td>
                                        <td>{{ $servicio->lavadorNombre }}</td>
                                        <th>{{ $servicio->fecha_cita . ' ' . $servicio->hora_cita }}</th>
                                        <th>{{$servicio->estatus_lavado}}</th>
                                        <th>{{($servicio->puntaje)?$servicio->puntaje:'-'}}</th>
                                        <td>{{ $servicio->estado . ', ' . $servicio->municipio . ', ' . $servicio->sucursal . ', ' . $servicio->calle . ' ' . $servicio->numero_ext . ' ' . $servicio->numero_int . ' ' . $servicio->colonia }}
                                        <td>
                                            @if ($servicio->evidencia != '')
                                                <a target="_blank"
                                                    href="{{ base_url('statics/evidencia/' . $servicio->evidencia) }}"><i
                                                        class="fa fa-file-image-o" aria-hidden="true"></i></a>
                                            @endif
                                        </td>
                                        </td>
                                        <td>
                                            @if (PermisoAccion('editar_servicio'))
                                                @if (!$servicio->id_paquete)
                                                    <a href="{{ site_url('servicios/generar_servicio/' . encrypt($servicio->id)) }}"
                                                        class="js_editar" aria-hidden="true" data-toggle="tooltip"
                                                        data-placement="top" title="Editar"> <i class="fa fa-edit"></i>
                                                    </a>
                                                @elseif($servicio->principal)
                                                    <a href="{{ site_url('servicios/generar_servicio/' . $servicio->id) }}"
                                                        class="js_editar" aria-hidden="true" data-toggle="tooltip"
                                                        data-placement="top" title="Editar"> <i class="fa fa-edit"></i>
                                                    </a>
                                                @endif
                                            @endif
                                            @if (PermisoAccion('cancelar_servicio'))
                                                <a style="cursor: pointer" data-id="{{ $servicio->id }}"
                                                    class="{{ $servicio->cancelado == 0 ? 'js_cancelar' : '' }}"
                                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                                    title="Cancelar cita">
                                                    <i style="{{ $servicio->cancelado == 1 ? 'color:red' : '' }}"
                                                        class="fa fa-ban"></i>
                                                </a>
                                            @endif
                                            @if ($servicio->confirmado)
                                                <a href="" data-id="{{ $servicio->id }}" class="js_confirmar" data-valor="0"
                                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                                    title="Confirmar"><i class="fa fa-check-square-o"></i></a>
                                            @else
                                                <a href="" data-id="{{ $servicio->id }}" class="js_confirmar" data-valor="1"
                                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                                    title="No confirmado"><i class="fa fa-square-o"></i></a>
                                            @endif
                                            @if (PermisoAccion('editar_fecha_servicio') && !$servicio->confirmado && !$servicio->cancelado)
                                                <a href="{{ site_url('servicios/editar_fecha_servicio/' . encrypt($servicio->id)) }}"
                                                    class="js_editar" aria-hidden="true" data-toggle="tooltip"
                                                    data-placement="top" title="Editar fecha/hora"> <i
                                                        class="fa fa-pencil"></i>
                                                </a>
                                            @endif
                                            <a href="{{ site_url('inventario/Inv_Exterior/generar_PDF/' . encrypt($servicio->id)) }}"
                                                target="_blank" aria-hidden="true" data-toggle="tooltip"
                                                data-placement="top" title="Orden"><i class="fa fa-file-pdf-o"
                                                    aria-hidden="true"></i></a>
                                            <a target="_blank"
                                                href="{{ site_url('servicios/linea_tiempo/' . encrypt($servicio->id)) }}"
                                                target="_blank" aria-hidden="true" data-toggle="tooltip"
                                                data-placement="top" title="Línea de tiempo"><i class="fa fa-clock-o"
                                                    aria-hidden="true"></i></a>
                                            <a target="_blank" href="#" target="_blank" aria-hidden="true"
                                                data-toggle="tooltip" data-placement="top" title="Evidencia"><i
                                                    class="fa fa-image js_evidencia" aria-hidden="true" data-id="{{ $servicio->id }}"></i>
                                            </a>
                                            <a href="{{ site_url('mapa_servicios/lavadores/?folio=' . $servicio->folio_mostrar) }}"
                                                 aria-hidden="true" data-toggle="tooltip"
                                                data-placement="top" title="mapa"><i class="fa fa-map-marker"
                                                    aria-hidden="true"></i></a>

                                            <a target="_blank" href="https://api.whatsapp.com/send?phone={{CONST_WS_CONTACTO}}&text=¡Estimado%20cliente!%20{{ $servicio->nombre . ' ' . $servicio->apellido_paterno . ' ' . $servicio->apellido_materno }}%20Bienvenido%20a%20XehosAutolavado%20Folio%20de%20orden%20de%20servicio%20{{$servicio->folio_mostrar}}">
                                                <i class="fa fa-whatsapp"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script>
        var site_url = "{{ site_url() }}";

        const callbackCancelarServicio = () => {
            var url = site_url + "/servicios/cancelar_servicio/";
            ajaxJson(url, {
                "id": id
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('El servicio no se pudo cancelar, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Servicio cancelado correctamente", function() {
                        $(aPos).find('i').css('color', 'red');
                        $(aPos).removeClass('js_cancelar');
                    });
                }
            });
        }
        const confirmar_rechazar = () => {
            var url = site_url + "/servicios/confirmar_rechazar/";
            ajaxJson(url, {
                "id": id,
                "valor": valor
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('Error de petición, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Registro actualizado correctamente.", function() {
                        $(".close").trigger("click");
                        aPosi = $(aPos).find('i');
                        if (valor == 1) {
                            //la está confirmando
                            $(aPos).data('valor', 0);
                            $(aPos).attr('title', 'Confirmar');
                            $(aPosi).removeClass('fa-square-o');
                            $(aPosi).addClass('fa-check-square-o');
                            $(aPos).attr('data-original-title', 'Confirmar');
                        } else {
                            //la está rechazando
                            $(aPos).data('valor', 1);
                            $(aPos).attr('title', 'No confirmada');
                            $(aPosi).removeClass('fa-check-square-o');
                            $(aPosi).addClass('fa-square-o');
                            $(aPos).attr('data-original-title', 'No confirmada');

                        }
                        $('[data-toggle="tooltip"]').tooltip()
                    });

                }
            });
        }
        $("body").on("click", '.js_cancelar', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            aPos = $(this);
            ConfirmCustom("¿Está seguro de cancelar el servicio?", callbackCancelarServicio, "", "Confirmar",
                "Cancelar");
        });
        $("body").on("click", '.js_confirmar', function(e) {
            e.preventDefault();
            //valor es 0 cuando va rechazar y 1 cuando lo va confirmar
            aPos = $(this);
            valor = $(this).data('valor');
            if (valor == 1) {
                mensaje = "¿Está seguro de confirmar el servicio?";
            } else {
                mensaje = "¿Está seguro de poner como no confirmado el servicio?";
            }
            id = $(this).data('id');
            ConfirmCustom(mensaje, confirmar_rechazar, "", "Confirmar", "Cancelar");
        });
        $("body").on('click', '.js_evidencia', function(e) {
            e.preventDefault();
            var url = "<?php echo base_url(); ?>index.php/servicios/evidencia";
            
            customModal(url, {
                    id_servicio: $(this).data('id')
                }, "POST", "lg", "", "", "", "Salir", "",
                "modal1");
        })

    </script>
@endsection
