<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Recomendadores', 'consulta', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

        if($this->session->userdata('id')){
            $this->session->unset_userdata('id');
            $this->session->sess_destroy();
            redirect('recomendadores/login');
        }else{
            //redirect('login/');
        }
    }

    public function index()
    {
    	$data['titulo'] = "Login";
        $data['titulo_dos'] = "Login Recomendadores";
        $this->blade->render('login',$data);
    }

    public function iniciar_sesion()
    {
        $usuario = $_POST["recordUsername"];
        $pass = $_POST["recordPassword"];

        $usuario_data = $this->consulta->get_result_field("usuario",$usuario,"activo","1","recomendadores");
        // echo '<pre>'; print_r($usuario_data); exit();
        foreach ($usuario_data as $row){
            $pass_db = $row->password;
            $usuario = $row->usuario;
            $id_usuario = $row->id;
            $telefono = $row->celular;
        }
  
        if (isset($pass_db)) {
            if (md5($pass) === $pass_db) { 
                $array_session = array(
                    'id_usuario'=>$id_usuario,
                    'usuario'=> $usuario,
                    'nombre' => $row->nombre." ".$row->apellido_paterno." ".$row->apellido_materno." ",
                    'telefono' => $telefono
                );
                
                $this->session->set_userdata($array_session);

                if($this->session->userdata('id_usuario')){
                    echo 'OK'; 
                }else{
                    echo "Reintentar";
                }
            } else { 
              echo 'Usuario o Contraseña incorrectos'; 
            } 
        } else {
            echo 'El usuario no existe o no ha sido activado'; 
        } 
    }

    public function registro()
    {
        $data['titulo'] = "Registro";
        $data['titulo_dos'] = "Registro Recomendadores";

        $data['cat_estados'] = $this->consulta->get_result('activo','1','estados');

        $this->blade->render('registro',$data);
    }

    public function usuario_disponible()
    {
        $usuario = $_POST["usuario"];

        $validar = $this->consulta->existe_nombre_usuario($usuario);

        if ($validar == "0") {
            echo "OK";
        } else {
            echo "NOMBRE DE USUARIO NO DISPONIBLE";
        }   
    } 

    public function registro_recomendador()
    {
        $registro = date("Y-m-d H:i:s");

        if ($_POST["formulario"] == "1") {
            $contenido["id"] = NULL;
        } 
        
        $contenido["nombre"] = $_POST["nombre"];
        $contenido["apellido_paterno"] = $_POST["ap_paterno"];
        $contenido["apellido_materno"] = $_POST["am_paterno"];
        $contenido["celular"] = $_POST["celular"];
        $contenido["correo"] = $_POST["email"];
        $contenido["password"] = md5($_POST["nPass"]);
        $contenido["resp_pass"] = $_POST["nPass"];
        $contenido["usuario"] = $_POST["nUsuario"];
        $contenido["n_cupones"] = 0;

        $contenido["id_estado_reg"] = $_POST["estados"];
        $contenido["id_sucursal_reg"] = $_POST["sucursal"];

        if ($_POST["formulario"] == "1") {
            $contenido["activo"] = "0";
            $contenido["fecha_alta"] = $registro;
        }
        
        $contenido["fecha_actualiza"] = $registro;

        if ($_POST["formulario"] == "1") {
            $servicio = $this->consulta->save_register('recomendadores',$contenido);
            if ($servicio != 0) {
                $respuesta = "OK";
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO";
            }
            
        }else{
            $actualizar = $this->consulta->update_table_row('recomendadores',$contenido,'id',$_POST["registro"]);
            if ($actualizar) {
                $respuesta = "OK";
            } else {
                $respuesta = "NO SE PUDO ACTUALIZAR EL FORMULARIO";
            }
        }

        echo $respuesta;
    }

    public function telefono_disponible()
    {
        $telefono = $_POST["telefono"];

        $check_admin = $this->consulta->telefono_en_uso("telefono",$telefono,"admin");
        $check_usuarios = $this->consulta->telefono_en_uso("telefono",$telefono,"usuarios");
        $check_lavadores = $this->consulta->telefono_en_uso("lavadorTelefono",$telefono,"lavadores");
        $check_recomendadores = $this->consulta->telefono_en_uso("celular",$telefono,"recomendadores");

        if (($check_admin == "1")||($check_usuarios == "1")||($check_lavadores == "1")||($check_recomendadores == "1")) {
            echo "ESTE TELÉFONO NO ESTA DISPONIBLE. INTENTAR CON OTRO";
        } else {
            echo "OK";
        }  
    }

    public function recuperar_sucursal()
    {
        $estado = $_POST["estado"];
        $cadena = "";
        $sucursales = $this->consulta->sucursales_estado($estado);
        
        foreach ($sucursales as $row) {
            $cadena .= $row->id."=";
            $cadena .= $row->sucursal."=";
            $cadena .= "|";
        }

        echo $cadena;

    }

    public function cerrar_sesion()
    {
        if($this->session->userdata('id_usuario')){
            $this->session->unset_userdata('id_usuario');
            $this->session->sess_destroy();
            redirect('recomendadores/login');
        }else{
            //redirect('login/');
        }
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
}