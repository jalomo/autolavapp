<table id="tbl_proactivo" class="table table-hover table-striped">
    <thead>
        <tr class="tr_principal">
            <th>Acciones</th>
            <th>Usuario</th>
            <th>Intentos</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Chasis</th>
            <th>Placas</th>
            <th>Cliente</th>
            <th>Tel.</th>
            <th>Fecha</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($proactivo as $p => $registro)
            <tr>
                <td>
                    <a href="" data-id="{{ $registro->id }}" class="js_historial" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Historial comentarios"><i
                            class="fa fa-info"></i></a>
                </td>
                <td>{{ $registro->usuario }}</td>
                <td>{{ $registro->intentos }}</td>
                <td>{{ $registro->marca }}</td>
                <td>{{ $registro->modelo }}</td>
                <td>{{ $registro->numero_serie }}</td>
                <td>{{ $registro->placas }}</td>
                <td>{{ $registro->nombre . ' ' . $registro->apellido_paterno . ' ' . $registro->apellido_materno }}
                </td>
                <td>{{ $registro->telefono }}</td>
                <td>{{ $registro->fecha_creacion_servicio }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
