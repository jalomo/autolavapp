<div class="row">
    <div class="col-sm-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Comentario</th>
                    <th>Evidencia</th>
                </tr>
            </thead>
            <tbody>
                @foreach($evidencia as $e => $ev)
                    <tr>
                        <td>{{$ev->titulo}}</td>
                        <td>{{$ev->comentarios}}</td>
                        <td><a target="_blank" href="{{$ev->url}}">Mostrar evidencia</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>