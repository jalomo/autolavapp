<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class ApiChatRecomendador extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set('America/Mexico_City');
  }


  public function getNotificaciones()
	{
		try {
			$telefono = $this->input->get('telefono');
      $usuarios_admin = $this->db->select('adminNombre as nombre, telefono')->where('telefono !=', null)->get('admin')->result_array();
      $usuarios_recomendador = $this->db->select('nombre, celular as telefono')->where('celular !=', null)->get('recomendadores')->result_array();

      $usuarios = array_merge($usuarios_admin, $usuarios_recomendador);
      $dataNotificacion = $this->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono, false, false);
			$notificaciones = json_decode($dataNotificacion)->data;
			$listado = [];
			foreach ($notificaciones as $notify) {
				$notify->usuario = null;
				foreach ($usuarios as $user) {
					if ($notify->celular2 == $user['telefono']) {
						$notify->usuario = ($user['nombre']);
					}
				}
				array_push($listado, $notify);
			}
      return print_r(json_encode($listado));
		}  catch (Exception $e) {
      return $e;
    }
	}

  public function getchat()
  {
    try {
      $telefono1 = $this->input->get('telefono1');
      $telefono2 = $this->input->get('telefono2');

      $usuarios_admin = $this->db->select('adminNombre as nombre, telefono')->where('telefono !=', null)->get('admin')->result_array();
      $usuarios_recomendador = $this->db->select('nombre, celular as telefono')->where('celular !=', null)->get('recomendadores')->result_array();

      $usuarios = array_merge($usuarios_admin, $usuarios_recomendador);
      $dataNotificacion = $this->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/notificaciones_busqueda', [
        'telefono' => $telefono1,
        'celular2' => $telefono2
      ], false);
      $notificaciones = json_decode($dataNotificacion)->data;

      $listado = [];
      foreach ($notificaciones as $notify) {
        if ($notify->eliminado == 0) {
          if ($notify->celular2 == $telefono2 || $notify->celular2 == $telefono1) {
            $notify->tipo = null;
            $notify->remitente = null;
            $notify->destinatario = null;
            foreach ($usuarios as $user) {
              if ($notify->celular2 == $user['telefono']) {
                $notify->destinatario = ($user['nombre']);
              }
              if ($notify->celular == $user['telefono']) {
                $notify->remitente = ($user['nombre']);
              }
            }
            array_push($listado, $notify);
          }
        }
      }
      $convArrayKardex = json_decode(json_encode($listado), true);

      usort($convArrayKardex, function ($b, $a) {
        return strcmp($b["fecha_creacion"], $a["fecha_creacion"]);
      });

      return print_r(json_encode($convArrayKardex));
    } catch (Exception $e) {
      return $e;
    }
  }
  public function apiNotificacion()
  {
    try {
      //echo '<pre>'; print_r($_GET); exit();
      $dataNotificacion = $this->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
        'celular' => $this->input->get('telefono'),
        'mensaje' => $this->eliminar_tildes($this->input->get('mensaje')),
        'sucursal' => 'M2137',
        'celular2' => $this->input->get('celular2'),
        'mensaje_respuesta' => $this->input->get('mensaje_respuesta')
      ], false);

      return print_r(json_encode($dataNotificacion));

    } catch (Exception $e) {
      return $e;
    }
  }

  private function curlPost($url = '', $data, $is_json_request = false)
  {
    $curl = curl_init();
    $final_url =  $url;
    $parametros = is_array($data) && !$is_json_request  ? http_build_query($data) : json_encode($data);

    curl_setopt_array($curl, array(
      CURLOPT_URL => $final_url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_HEADER => true,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $parametros,
    ));
    $body = curl_exec($curl);
    // extract header
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $header = substr($body, 0, $headerSize);
    $header = @$this->getHeaders($header);

    // extract body
    $body = substr($body, $headerSize);
    curl_close($curl);

    if ($httpcode == 400 && isset($header) && count($header) > 0) {
      return [
        'status_code' => $httpcode,
        'data' => $header['X-Message']
      ];
    }

    return $body;
  }
  public function curlGet($url = '', $data, $is_json_request = false)
  {
      $curl = curl_init();
      $final_url =  $url ;
      $parametros = is_array($data) && !$is_json_request  ? http_build_query($data) : json_encode($data);
      curl_setopt_array($curl, array(
          CURLOPT_URL => $final_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_HEADER => true,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => $parametros,
      ));
      $body = curl_exec($curl);
      // extract header
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

      $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
      $header = substr($body, 0, $headerSize);
      $header = @$this->getHeaders($header);

      // extract body
      $body = substr($body, $headerSize);
      curl_close($curl);

      if ($httpcode == 400 && isset($header) && count($header) > 0) {
          return [
              'status_code' => $httpcode,
              'data' => $header['X-Message']
          ];
      }

      return $body;
  }
  function getHeaders($respHeaders)
  {
    $headers = array();
    $headerText = substr($respHeaders, 0, strpos($respHeaders, "\r\n\r\n"));


    foreach (explode("\r\n", $headerText) as $i => $line) {
      if ($i === 0) {
        $headers['http_code'] = $line;
      } else {
        list($key, $value) = explode(': ', $line);
        if ($key == 'X-Message') {
          $headers[$key] = $value;
        }
      }
    }

    return $headers;
  }
  private function eliminar_tildes($cadena)
  {

    $cadena = ($cadena);

    $cadena = str_replace(
      array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
      array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
      $cadena
    );

    $cadena = str_replace(
      array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
      array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
      $cadena
    );

    $cadena = str_replace(
      array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
      array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
      $cadena
    );

    $cadena = str_replace(
      array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
      array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
      $cadena
    );

    $cadena = str_replace(
      array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
      array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
      $cadena
    );

    $cadena = str_replace(
      array('ñ', 'Ñ', 'ç', 'Ç'),
      array('n', 'N', 'c', 'C'),
      $cadena
    );

    return $cadena;
  }
}
