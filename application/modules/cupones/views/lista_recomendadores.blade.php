@layout('layout')
@section('contenido')
	<div class="row">
        <div class="col-sm-12" align="center">
        	<h5>Recomendadores</h5>
		</div>	
	</div>

	<br>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
			            <table id="bootstrap-data-table" class="table table-striped table-bordered">
			                <thead>
			                    <tr>
			                    	<th>#</th>
			                    	<th>Usuario</th>
			                    	<th>Nombre</th>
			                    	<th>Teléfono</th>
			                    	<th>Correo</th>
			                    	<th>Fecha Alta</th>
			                    	<th>Estatus</th>
			                    	<th>Acciones</th>
			                    </tr>
			                </thead>
			                <tbody id="cuerpo">
			                	@if ($listado != NULL)
									@foreach ($listado as $i => $usuario)
										<tr>
											<td style="vertical-align: middle;" align="center">
												<?= $i + 1;?>
											</td>
											<td style="vertical-align: middle;">
												<?= $usuario->usuario ?>
											</td>
											<td style="vertical-align: middle;">
												<?= $usuario->nombre." ".$usuario->apellido_paterno." ".$usuario->apellido_materno ?>
											</td>
											<td style="vertical-align: middle;">
												<?= $usuario->celular ?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?= $usuario->correo ?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?php 
													$fecha = new DateTime($usuario->fecha_alta);
		        									echo $fecha->format('d-m-Y');
												?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?php 
													if ($usuario->activo == "1") {
														echo "Activo";
													} else {
														echo "Inactivo";
													}
												?>
											</td>
											<td style="vertical-align: middle;" align="center">
												@if ($usuario->activo == "1")
													<a onclick="baja_usuario(<?= $usuario->id ?>)" class="btn btn-danger" style="color:white;font-size: 10px;">
														Desactivar
				                                    </a>
												@else
													<a onclick="alta_usuario(<?= $usuario->id ?>)" class="btn btn-success" style="color:white;font-size: 10px;">
														Activar
				                                    </a>
												@endif
											</td>
										</tr>
									@endforeach
			                	@else
			                		<tr>
			                			<td colspan="9" align="center">
			                				Sin cupones
			                			</td>
			                		</tr>
								@endif
			                </tbody>
			            </table>
		            </div>
		        </div>
		    </div>
		</div>
    </div><!-- .animated -->
@endsection
@section('included_js')
	@include('main/scripts_dt')
	<script>
		function baja_usuario(id_usuario) {
			var url_sis = "<?php echo base_url(); ?>index.php/cupones/recomendadores/baja_recomendador";
		    $.ajax({
		        url: url_sis,
		        method: 'post',
		        data: {
		            id_usuario : id_usuario
		        },
		        success:function(resp){
		            //console.log(resp);
		            if (resp.indexOf("handler           </p>")<1) {
		                if (resp == "OK") {
		                    location.reload();
		                } 
		            }
		        //Cierre de success
		        },
		        error:function(error){
		            console.log(error);
		        //Cierre del error
		        }
		    //Cierre del ajax
		    });     
		}

		function alta_usuario(id_usuario) {
			var url_sis = "<?php echo base_url(); ?>index.php/cupones/recomendadores/alta_recomendador";
		    $.ajax({
		        url: url_sis,
		        method: 'post',
		        data: {
		            id_usuario : id_usuario
		        },
		        success:function(resp){
		            //console.log(resp);
		            if (resp.indexOf("handler           </p>")<1) {
		                if (resp == "OK") {
		                    location.reload();
		                } 
		            }
		        //Cierre de success
		        },
		        error:function(error){
		            console.log(error);
		        //Cierre del error
		        }
		    //Cierre del ajax
		    });     
		}
    </script>
@endsection