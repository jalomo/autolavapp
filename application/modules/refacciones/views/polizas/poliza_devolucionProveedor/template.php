<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .contenedor {
            width: 100%;
        }

        .col-12 {
            width: 100%;
            padding: 3px;
        }

        .col-6 {
            float: left;
            width: 49%;
            padding: 3px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid #233a74;
            margin-bottom: 12px
        }
    </style>
</head>

<body>
    <div class="contenedor">
        <div class="col-12">
            <table class="table">
                <thead class="">
                    <tr>
                        <th style="width: 10%" scope="col">
                            Cantidad
                        </th>
                        <th scope="col">
                            Descripción
                        </th>
                        <th scope="col" style="width: 30%">
                            Fecha
                        </th>
                        <th scope="col" style="width: 20%">
                            Importe
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($ventas as $key => $item) { ?>

                        <?php $fecha = date('d/m/yy', strtotime($item->created_at)); ?>
                        <tr>
                            <td><?php echo $item->cantidad ?></td>
                            <td><?php echo $item->producto->descripcion ?></td>
                            <td>
                                <?php echo $fecha ?>
                            </td>
                            <td>
                                $ <?php echo porcentajeProducto($item->precio->precio_publico, $item->producto->precio_factura) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>