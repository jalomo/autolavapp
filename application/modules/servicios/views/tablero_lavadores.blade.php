@layout('layout')
<style>
    .error p {
        color: red
    }

</style>
@section('contenido')
    <div class="row form-group">
        <div class='col-sm-4'>
            <label for="">Selecciona la fecha</label>
            <input id="fecha" name="fecha" type='date' class="form-control" value="{{ date('Y-m-d') }}" />
            <span class="error_fecha"></span>
        </div>
        @if ($this->session->userdata('id_rol') == 1)
            <div class="col-sm-4">
                <label for="">Sucursal</label>
                {{ $sucursales }}
            </div>
        @endif
        <div class="col-sm-2">
            <br>
            <button id="buscar" style="margin-top: 5px;" class="btn btn-success">Buscar</button>
        </div>
    </div>
    <div class="row">
        <div id="div_tabla">
            <div class="col-sm-6">
                {{ $tabla1 }}
            </div>
            <div class="col-sm-6">
                {{ $tabla2 }}
            </div>
        </div>
    </div>
@endsection
@section('included_js')
    <script>
        var site_url = "{{ site_url() }}";
        const id_rol = "{{ $this->session->userdata('id_rol') }}";
        const buscar = () => {
            if (id_rol == 1 && $("#id_sucursal").val() == '') {
                ErrorCustom('Es necesario seleccionar la sucursal');
            } else {
                var url = site_url + "/servicios/tabla_horarios_lavadores";
                ajaxLoad(url, {
                    "fecha": $("#fecha").val(),
                    'id_sucursal_ubicacion' :  $("#id_sucursal").val(),
                }, "div_tabla", "POST", function() {});
            }
        }
        $("#buscar").on('click', buscar);

    </script>
@endsection
