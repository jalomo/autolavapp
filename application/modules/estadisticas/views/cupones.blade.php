@layout('layout')
@section('included_css')
    <link href="statics/css/bootstrap-datetimepicker.css" rel="stylesheet">
@endsection
@section('included_css')
    <style>
        .highcharts-figure,
        .highcharts-data-table table {
            min-width: 310px;
            max-width: 800px;
            margin: 1em auto;
        }

        #container {
            height: 400px;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

    </style>
    <link rel="stylesheet"
        href="<?php echo base_url(); ?>statics/css/bootstrap-multiselect.css">
@endsection
@section('contenido')
    <h2>{{ $titulo_dos }}</h2>
    <br>
    <form id="frm" action="" method="POST">
        <div class="row">
            @if ($this->session->userdata('id_rol') == 1)
                <div class="col-sm-4">
                    <label for="">Sucursal</label>
                    {{ $sucursales }}
                </div>
            @endif
            <div class="col-sm-2 form-group">
                <label>Año</label>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker_anio'>
                        <input type="text" name="anio" id="anio" class="form-control" value="{{ date('Y') }}">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-2" style="margin-top: 35px">
                <button type="button" id="buscar" class="btn btn-info">Buscar</button>
            </div>
        </div>

        <br>
    </form>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="table-servicios"></div>
        </div>
    </div>
    <div id="div_grafica"></div>

@endsection
@section('included_js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="statics/js/moment.js"></script>
    <script src="statics/js/bootstrap-datetimepicker.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        $('#datetimepicker_anio').datetimepicker({
            format: 'YYYY',
            locale: 'es'
        });
        const buscar = () => {
            if ($("#anio").val() != '') {
                var url = site_url + "/estadisticas/buscar_cupones";
                ajaxLoad(url, $("#frm").serialize(), "div_grafica", "POST");
            } else {
                ErrorCustom('Es necesario ingresar el año');
            }
        }
        $("#buscar").on('click', buscar);

    </script>

@endsection
