var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			ajax: {
				url: PATH + '/nomina/inicio/trabajador/historico_trayectoria_get?identity='+identity,
				type: "GET"
			},
			columns: [
				{
					title: 'Tipo de registro',
					data: 'Descripcion_HistTipoMov'
				},
				{
					title: 'Fecha',
					data: 'Fecha'
				},
				{
					title: 'Estatus',
					data: 'Descripcion_EstatusTrab'
				},
				{
					title: 'Puesto',
					data: 'Descripcion_Puesto'
				},
				{
					title: 'Departamento',
					data: 'Descripcion_Departamento'
				}
				
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
