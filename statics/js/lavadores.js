const getLavadores = () => {
	var url = site_url + "/estadisticas/getLavadoresBySucursal";
	$("#id_lavador").empty();
	$("#id_lavador").append("<option value=''>-- Selecciona --</option>");
	$("#id_lavador").attr("disabled", true);
	id_sucursal = $("#id_sucursal").val();
	if (id_sucursal != "") {
		ajaxJson(
			url,
			{
				id_sucursal: id_sucursal,
			},
			"POST",
			"",
			function (result) {
				if (result.length != 0) {
					$("#id_lavador").empty();
					$("#id_lavador").removeAttr("disabled");
					result = JSON.parse(result);
					$("#id_lavador").append("<option value=''>-- Selecciona --</option>");
					$.each(result, function (i, item) {
						$("#id_lavador").append(
							"<option value= '" +
								result[i].lavadorId +
								"'>" +
								result[i].lavadorNombre +
								"</option>"
						);
					});
				} else {
					$("#id_lavador").empty();
					$("#id_lavador").append(
						"<option value='0'>No se encontraron datos</option>"
					);
				}
			}
		);
	} else {
		$("#id_lavador").empty();
		$("#id_lavador").append("<option value=''>-- Selecciona --</option>");
		$("#id_lavador").attr("disabled", true);
	}
};
$("#id_sucursal").on("change", getLavadores);
