<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_lavadores extends CI_Model
{
  //obtener las citas de un lavador
  public function getCitasLavador($id = 0, $fecha = '')
  {
    if ($fecha != '') {
      $this->db->where('fecha', $fecha);
    }
    return $this->db->where('id_lavador', $id)->order_by('fecha', 'desc')->order_by('hora', 'asc')->get('horarios_lavadores')->result();
  }
  public function getNameLavador($id = 0)
  {
    $q = $this->db->where('lavadorId', $id)->select('lavadorNombre')->from('lavadores')->get();
    if ($q->num_rows() == 1) {
      return $q->row()->lavadorNombre;
    } else {
      return '';
    }
  }
  //guarda el horario
  public function guardarHorario()
  {
    $id_lavador = $this->input->post('id');
    $id_horario = $this->input->post('id_horario');
    $horario = $this->input->post('horario');
    $fecha_separada = explode('/', $horario);
    $datos = array(
      'hora' => $this->input->post('hora'),
      'id_lavador' => $id_lavador,
      'dia' => $fecha_separada[0],
      'mes' => $fecha_separada[1],
      'anio' => $fecha_separada[2],
      'fecha_creacion' => date('Y-m-d'),
      'fecha' => date2sql($horario)
    );
    $q = $this->db->where('hora', $datos['hora'])->where('dia', $fecha_separada[0])->where('mes', $datos['mes'])->where('anio', $datos['anio'])->where('id_lavador', $id_lavador)->get('horarios_lavadores');
    if ($q->num_rows() == 1) {
      echo -1;
      exit();
    }
    if ($id_horario == 0) {
      $exito = $this->db->insert('horarios_lavadores', $datos);
    } else {
      $exito = $this->db->where('id', $id_horario)->update('horarios_lavadores', $datos);
    }
    if ($exito) {
      echo 1;
      die();
    } else {
      echo 0;
      die();
    }
  }
  //guarda los operadores
  public function guardarOperador(){
    $id= $this->input->post('id');
    $datos = array('nombre' => $this->input->post('nombre'),
                   'telefono' => $this->input->post('telefono'),
                   'correo' => $this->input->post('correo'),
                   'rfc' => $this->input->post('rfc'),
                   'fecha_creacion' =>date('Y-m-d')
     );
     $q= $this->db->where('nombre',$datos['nombre'])->where('id !=',$id)->get('operadores');
     if($q->num_rows()==1){
       echo -1;exit();
     }
     if($id==0){
         $exito = $this->db->insert('operadores',$datos);
     }else{
         $exito = $this->db->where('id',$id)->update('operadores',$datos);
     
       if($exito){
         echo 1;die();
       } else{
         echo 0; die();
       }
   }
 }
}
