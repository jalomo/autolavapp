@layout('layout')
@section('contenido')
	<div class="row">
        <div class="col-sm-12" align="center">
        	<h5>Inventarios realizados</h5>
		</div>	
	</div>

	<br>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
			            <table <?php if ($listado != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered">
			                <thead>
			                    <tr>
			                    	<th>#</th>
			                    	<th>Lavador</th>
			                    	<th>Unidad</th>
			                    	<th>Placas</th>
			                    	<th>Fecha registro</th>
			                    	<th>Revisado por</th>
			                    	<th>Fecha revisión</th>
			                    	<th></th>
			                    </tr>
			                </thead>
			                <tbody id="cuerpo">
			                	@if ($listado != NULL)
									@foreach ($listado as $i => $registro)
										<tr>
											<td style="vertical-align: middle;" align="center">
												<?= $i + 1;?>
											</td>
											<td style="vertical-align: middle;">
												<?= $registro->lavador ?>
											</td>
											<td style="vertical-align: middle;">
												<?= $registro->unidad ?>
											</td>
											<td style="vertical-align: middle;">
												<?= $registro->placas ?>
											</td>
											<td style="vertical-align: middle;">
												<?php 
													$fecha = new DateTime($registro->fecha_alta);
		        									echo $fecha->format('d-m-Y');
		        									echo " ".$fecha->format('H:i');
												?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?= (($registro->supervisor != NULL) ? $registro->supervisor : "Sin revisar") ?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?php 
													if ($registro->firma_supervisor != "") {
														$fecha2 = new DateTime($registro->fecha_revision);
			        									echo $fecha2->format('d-m-Y');
			        									echo " ".$fecha2->format('H:i');
													} else {
														echo "Sin revisar";
													}
												?>
											</td>
											<td style="vertical-align: middle;" align="center">
												@if($registro->firma_supervisor != NULL)
													<a onclick="ver_datos(<?= $registro->id ?>)" class="btn btn-success" style="color:white;font-size: 12px!important;">
														Ver
				                                    </a>
												@else
													<a onclick="revisar_datos(<?= $registro->id ?>)" class="btn btn-info" style="color:white;font-size: 12px!important;">
														Revisar
				                                    </a>
												@endif
												
											</td>
										</tr>
									@endforeach
			                	@else
			                		<tr>
			                			<td colspan="7" align="center">
			                				Sin registros
			                			</td>
			                		</tr>
								@endif
			                </tbody>
			            </table>
		            </div>
		        </div>
		    </div>
		</div>
    </div><!-- .animated -->
@endsection
@section('included_js')
	@include('main/scripts_dt')

	
	<script>
		function ver_datos(indice) {
    		var url_sis = "<?php echo base_url(); ?>index.php/lavadores/inventario/redireccionar/"+indice;
	        $.ajax({
	            url: url_sis,
	            method: 'get',
	            data: {
	                
	            },
	            success:function(resp){
	              //console.log(resp);
	                if (resp.indexOf("handler			</p>")<1) {
	                    location.href = "<?php echo base_url(); ?>"+"index.php/lavadores/inventario/generar_revision/"+resp;
	                }
	            //Cierre de success
	            },
	            error:function(error){
	                console.log(error);
	            //Cierre del error
	            }
	        //Cierre del ajax
	        });
    	}

    	function revisar_datos(indice) {
    		var url_sis = "<?php echo base_url(); ?>index.php/lavadores/inventario/redireccionar/"+indice;
	        $.ajax({
	            url: url_sis,
	            method: 'get',
	            data: {
	                
	            },
	            success:function(resp){
	              //console.log(resp);
	                if (resp.indexOf("handler			</p>")<1) {
	                    location.href = "<?php echo base_url(); ?>"+"index.php/lavadores/inventario/generar_edicion/"+resp;
	                }
	            //Cierre de success
	            },
	            error:function(error){
	                console.log(error);
	            //Cierre del error
	            }
	        //Cierre del ajax
	        });
    	}
    </script>
@endsection