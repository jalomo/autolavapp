<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>

  <style type="text/css">

     .faces{

        border-style: solid;
        margin-bottom: 0.2in;
        width: 28.391141936224em;
        height: 42.803740285536em;

     }

     #credencial_front{
      background-image: url('<?php echo base_url();?>/gafete/front.jpeg');
      background-repeat: no-repeat;

     }

     #credencial_back{
      background-image: url('<?php echo base_url();?>/gafete/back.jpeg');
      background-repeat: no-repeat;

     }

     #wrapper{
        font-size: 0.7em;

        margin-top: 5.2em;
     }

     #wrapper > div{
       float: left;
     }

     #picture{margin-left: 1.6em;}

     #picture img{
       width: 7.9em;
       height: 9.2em;
     }

     #data{
      margin-left: 1em;
     }



     p{margin: 0em;}
     .lbl{margin:0em; font-weight: bold;}
     .section1{
       margin: 0 auto;
     }

     .sections1 p{
       text-align: center;
     }

     #credencial_back > div{
        float:  left;
     }

     #presidente{
       font-size: 0.7em;
       margin-top: 0em;
       margin-left: 0em;

     }

     #presidente p{
       text-align: center;
       font-weight: bold;
     }

     #folio{
     padding-left: 11em;
      margin-top: 14.8em;
      font-size: 0.7em;
     }


     #presidente{
      margin-left: 2em;
      margin-top: 11em;

     }

     .text{
       width: 200px;
       font-size: 0.7em;
       text-transform: uppercase;
     }

     .clean{ clear: both; }
     #txt_img{
      margin-top: 0px;
      margin-left:50px;
      font-size:18px;
      font-weight: bold;
      color:#0f713e;
     }

     #txt_nombre{
      margin-top: 80px;
      margin-left:30px;
      font-size:30px;
      font-weight: bold;
      color:#0f713e;
     }

     #txt_id{
      margin-top: 55px;
      margin-left:95px;
      font-size:18px;
      font-weight: bold;
      color:#0f713e;
     }
     #txt_qr{
      margin-top: 55px;
      margin-left:250px;
      font-size:18px;
      font-weight: bold;
      color:#0f713e;
     }

     #txt_id2{
      margin-top: 290px;
      margin-left:126px;
      font-size:18px;
      font-weight: bold;
      color:#0f713e;
     }
     #txt_qr2{
      margin-top: 55px;
      margin-left:250px;
      font-size:18px;
      font-weight: bold;
      color:#0f713e;
     }

     #tipo_sangre{
      margin-top: 505px;
      margin-left:150px;
      font-size:18px;
      font-weight: bold;
      color:#0f713e;
      position: absolute;
     }

     #fondo_img{
      margin-top: -38px;
      margin-left:18px;
      
      position: absolute;
     }

     #fecha_entrada{
      margin-top: 325px;
      margin-left:250px;
      font-size:15px;
      font-weight: bold;
      color:#0f713e;
      position: absolute;
     }

     #fecha_vencimiento{
      margin-top: 345px;
      margin-left:280px;
      font-size:15px;
      font-weight: bold;
      color:#0f713e;
      position: absolute;
     }

     

     </style>
</head>
<body>
  <div id="credencial_front" class="faces">
     <div id="wrapper">

        <div id="picture">
            
         </div>

       <div id="data">
         <!--div class="sections">
            <div><p class="lbl">Nombre</p></div>
            <div><p class="text"> </p></div>
          </div-->
          <div id="fondo_img"><img src="<?php echo base_url()?>gafete/fondo_img.png" width="240" height="240"/></div>
          <div id="txt_img"><img src="<?php echo base_url().$row->lavadorFoto;?>" width="158" height="158"/></div>
          
          <div id="txt_nombre"><?php echo $row->lavadorNombre;?></div>

          <div id="txt_id"><?php echo $row->id_id;?></div>
          <div id="txt_qr"><img src="<?php echo base_url().$row->qr;?>" width="165" height="165"/></div>

           <div class="sections">
            <div><p class="lbl"></p></div>
            <div><p class="text"></p></div>
           </div>

           <div class="sections1">
            <div><p class="lbl"></p></div>
            <!--div><p class="lbl"></p></div-->

           </div>


           <div class="sections ">
            <div><p ><span class="lbl "></span> <p class="text"></p></p></div>
           </div>

          <div class="sections ">
            <div><p ><span class="lbl "></span> </p></div>
           </div>

         </div>





      </div>

      

  <div class="clean"></div>

  </div>


  <div id="download">
    <a href="javascript:doPDF()">Descargar Frente </a>
  </div>


  <div id="credencial_back" class="faces">

  <div id="txt_id2"><?php echo $row->id_id;?></div>
  <div id="tipo_sangre"><?php echo $row->tipo_sangre;?></div>

  <div id="fecha_entrada"><?php echo $row->fecha_entrada;?></div>
  <div id="fecha_vencimiento"><?php echo $row->fecha_vencimiento;?></div>

  <div id="txt_qr2"><img src="<?php echo base_url().$row->qr;?>" width="165" height="165"/></div>
          <div  id="folio">
            <div>
               <p></p>

            </div>
           </div>

          <div  id="presidente">
            <div>
               <p ></p>
               <p></p>
            </div>
           </div>



  </div>

  <div id="download">
    <a href="javascript:doPDF()">Descargar en reverso </a>
  </div>


  <script src="<?php echo base_url();?>/gafete/jspdf.debug.js"></script>
  <script src="<?php echo base_url();?>/gafete/html2pdf.js"></script>
  <script type="text/javascript">
    function doPDF(){
        html2canvas(document.body, {
           onrendered: function (canvas) {
             var img = canvas.toDataURL("image/png");
             var doc = new jsPDF();
             doc.addImage(img,"JPEG",0,0);
             doc.save("credencial.pdf");
           }
        });
    }
    </script>
</body>
</html>

