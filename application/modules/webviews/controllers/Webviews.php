<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Webviews extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        

    }

    public function seminuevos(){
        $this->load->view('webviews/menu', '', FALSE);
    }

    public function menu(){
        $this->load->view('webviews/menu', '', FALSE);
    }

    public function factura(){
      $this->load->view('webviews/factura', '', FALSE);
    }


    public function generar_factura($id_factura){
      $factura_nueva = $this->Mgeneral->get_row('facturaID',$id_factura,'factura');
      //error_reporting(1);
      //ini_set('display_errors', 1);
      require $this->config->item('url_real').'cfdi/xml/lib/autoload.php';//'C:\xampp\htdocs\elastillero\cfdi\xml\lib\autoload.php';
      $cfdi = new Comprobante();
      // Preparar valores
      $moneda = 'MXN';
      $subtotal  = $this->Mgeneral->factura_subtotal($id_factura);//190.00;
      $iva       = $this->Mgeneral->factura_iva_total($id_factura);//30.40;
      //$descuento =   1.40;
      $total     = $this->Mgeneral->factura_subtotal($id_factura) + $this->Mgeneral->factura_iva_total($id_factura);
      $fecha     = time();
      // Establecer valores generales
      $cfdi->LugarExpedicion   = $factura_nueva->fatura_lugarExpedicion;//'12345';
      $cfdi->FormaPago         = $factura_nueva->factura_formaPago;//'27';
      $cfdi->MetodoPago        = $factura_nueva->factura_medotoPago;//'PUE';
      $cfdi->Folio             = $factura_nueva->factura_folio;//'24';
      $cfdi->Serie             = $factura_nueva->factura_serie;//'A';
      $cfdi->TipoDeComprobante = $factura_nueva->factura_tipoComprobante;//'I';
      $cfdi->TipoCambio        = 1;
      $cfdi->Moneda            = $moneda;
      $cfdi->setSubTotal($subtotal);
      $cfdi->setTotal($total);
      //$cfdi->setDescuento($descuento);
      $cfdi->setFecha($fecha);

      // Agregar emisor
      $cfdi->Emisor = Emisor::init(
          $factura_nueva->emisor_RFC,//'LAN7008173R5',                    // RFC
          $factura_nueva->emisor_regimenFiscal,//'622',                             // Régimen Fiscal
          $factura_nueva->emisor_nombre//'Emisor Ejemplo'                   // Nombre (opcional)
      );

      // Agregar receptor
      $cfdi->Receptor = Receptor::init(
          $factura_nueva->receptor_RFC,//'TCM970625MB1',                   // RFC
          $factura_nueva->receptor_uso_CFDI,//'I08',                             // Uso del CFDI
          $factura_nueva->receptor_nombre//'Receptor Ejemplo'                 // Nombre (opcional)
      );

      $conceptos_factura  = $this->Mgeneral->get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      foreach($conceptos_factura as $concepto_fac):
        // Preparar datos del concepto 1
        $concepto = Concepto::init(
            $concepto_fac->clave_sat,//'52141807',                        // clave producto SAT
            $concepto_fac->concepto_cantidad,//'2',                               // cantidad
            $concepto_fac->unidad_sat,//'P83',                             // clave unidad SAT
            $concepto_fac->concepto_nombre,//'Nombre del producto de ejemplo',
            $concepto_fac->concepto_precio,//95.00,                             // precio
            $concepto_fac->concepto_importe//190.00                             // importe
        );
        $concepto->NoIdentificacion = $concepto_fac->id_producto_servicio_interno;//'PR01'; // clave de producto interna
        $concepto->Unidad = $concepto_fac->unidad_sat;//'Servicio';       // unidad de medida interna
        //$concepto->Descuento = 0.0;

        // Agregar impuesto (traslado) al concepto 1
        $traslado = new ConceptoTraslado;
        $traslado->Impuesto = '002';          // IVA
        $traslado->TipoFactor = 'Tasa';
        $traslado->TasaOCuota = 0.16;
        $traslado->Base = $subtotal;
        $traslado->Importe = $iva;
        $concepto->agregarImpuesto($traslado);

        // Agregar concepto 1 a CFDI
        $cfdi->agregarConcepto($concepto);

        // Agregar más conceptos al CFDI
        // $concepto = Concepto::init(...);
        // ...
        // $cfdi->agregarConcepto($concepto);
     endforeach;


      // Mostrar XML del CFDI generado hasta el momento
      // header('Content-type: application/xml; charset=UTF-8');
      // echo $cfdi->obtenerXml();
      // die;

      // Cargar certificado que se utilizará para generar el sello del CFDI
      $cert = new UtilCertificado();

      // Si no se especifica la ruta manualmente, se intentará obtener automatícamente
      // UtilCertificado::establecerRutaOpenSSL();

      $ok = $cert->loadFiles(
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
          //dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
          ''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.cer',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.cer',
          ''.$this->config->item('url_real').'cfdi/xml/ejemplos/LAN7008173R5.key',//'C:\xampp\htdocs\elastillero\cfdi\xml\ejemplos\LAN7008173R5.key',
          '12345678a'
      );
      if(!$ok) {
          die('Ha ocurrido un error al cargar el certificado.');
      }

      $ok = $cfdi->sellar($cert);
      if(!$ok) {
          die('Ha ocurrido un error al sellar el CFDI.');
      }

      // Mostrar XML del CFDI con el sello
      //header('Content-type: application/xml; charset=UTF-8');
      //header('Content-Disposition: attachment; filename="xml/tu_archivo.xml');

      //echo $cfdi->obtenerXml();
      if($cfdi->obtenerXml()){
            //echo "XML almacenado correctamente en ".base_url()."statics/facturas/prefactura/$id_factura.xml";
            file_put_contents($this->config->item('url_real')."statics/facturas/prefactura/$id_factura.xml", $cfdi->obtenerXml());
            $data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
            $data_url_prefactura['xml_text'] = $cfdi->obtenerXml();
            $this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);
            $this->enviar_factura($id_factura);
      }else{
        $data_respone['error'] = 1;
        $data_respone['error_mensaje'] = "Ocurrio un error";
        $data_respone['factura'] = $id_factura;
        echo json_encode($data_respone);
        die;
      }
      //file_put_contents("C:/xampp/htdocs/elastillero/statics/facturas/prefactura/$id_factura.xml",$cfdi->obtenerXml());
      //$data_url_prefactura['url_prefactura'] = base_url()."statics/facturas/prefactura/$id_factura.xml";
      //$this->Mgeneral->update_table_row('factura',$data_url_prefactura,'facturaID',$id_factura);

      //$this->enviar_factura($id_factura);

      die;

      // Mostrar objeto que contiene los datos del CFDI
      print_r($cfdi);


      die;



      die('OK');
    }
}