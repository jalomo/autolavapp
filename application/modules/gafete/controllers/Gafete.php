<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Gafete extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         //if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function index($id_lavador){
        $data['row'] = $this->Mgeneral->get_row('id_id',$id_lavador,'lavadores');
        //$vista = $this->load->view('gafete/gafete', '', true);
        //$data['vista'] = $vista;
        $this->load->view('gafete/gafete', $data, false);
    }

    public function lavador($id_lavador){
        $data['row'] = $this->Mgeneral->get_row('id_id',$id_lavador,'lavadores');
        //$vista = $this->load->view('gafete/gafete', '', true);
        //$data['vista'] = $vista;
        $this->load->view('gafete/lavador', $data, false);
    }

   

   



}
