@layout('layout')
@section('contenido')
    <h2>{{ $titulo }}</h2>
    <br>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Paquete</th>
                                    <th>Cliente</th>
                                    <th>Email cliente</th>
                                    <th>Teléfono</th>
                                    <th>Placas</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Año</th>
                                    <th>Color</th>
                                    <th>Dirección</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($info as $i => $servicio)
                                    <tr>
                                        <td>{{ $servicio->id}}</td>
                                        <td>{{ $servicio->paquete}}</td>
                                        <td>{{ $servicio->nombre . ' ' . $servicio->apellido_paterno . ' ' . $servicio->apellido_materno }}
                                        </td>
                                        <td>{{ $servicio->email }}</td>
                                        <td>{{ $servicio->telefono }}</td>
                                        <td>{{ $servicio->placas }}</td>
                                        <td>{{ $servicio->marca }}</td>
                                        <td>{{ $servicio->modelo }}</td>
                                        <td>{{ $servicio->anio }}</td>
                                        <td>{{ $servicio->color }}</td>
                                        <td>{{ $servicio->estado . ', ' . $servicio->municipio . ', ' . $servicio->calle . ' ' . $servicio->numero_ext . ' ' . $servicio->numero_int . ' ' . $servicio->colonia }}
                                        </td>
                                        <td>
                                            @if (PermisoAccion('editar_servicio'))
                                                <a href="{{ site_url('paquetes/generar_paquetes/' . encrypt($servicio->id)) }}"
                                                    class="js_editar" aria-hidden="true" data-toggle="tooltip"
                                                    data-placement="top" title="Editar"> <i class="fa fa-edit"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script>
        var site_url = "{{ site_url() }}";

    </script>
@endsection
