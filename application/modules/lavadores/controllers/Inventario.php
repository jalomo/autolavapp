<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Inventario extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Inventario', 'consulta', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        if($this->session->userdata('id')){}else{redirect('login/');}
    }

    public function index()
    {
    	$data['titulo'] = "Inventario Lavadores";
        $data['titulo_dos'] = "Listado";

        $mes_actual = date("m");
        $fecha_busqueda = (($mes_actual <= 1) ? (date("Y")-1) : date("Y"))."-".(($mes_actual <= 1) ? "12" : ($mes_actual -1))."-".date("d");

        $data['listado'] = $this->consulta->listado_inventarios($fecha_busqueda); 
        
        $this->blade->render('inventario/listado',$data);
    }

    public function alta($id_lavador = '')
    {
        $data['titulo'] = "Inventario Lavadores";
        $data['titulo_dos'] = "Formulario";

        $data["lavador"] = $id_lavador;
        //Verificamos si no se ha llenado ningun diagnostico el dia de hoy por este lavador
        $validar = $this->consulta->buscar_inventario($id_lavador);

        if ($validar != "") {
            $redireccion = $this->encrypt($validar);
            redirect('lavadores/inventario/generar_revision/'.$redireccion, 'refresh');
        }else{
            $data["n_lavador"] = $this->consulta->nombre_lavador($id_lavador);

            $datos_unidad = $this->consulta->get_result_one('id_lavador',$id_lavador,'unidades');
            foreach ($datos_unidad as $row){
                $data["n_unidad"] = $row->id;
                $data["marca"] = $row->marca;
                $data["placas"] = $row->placas;
                $data["color"] = $row->color;
                $data["serie"] = $row->n_serie;
                $data["kilometros"] = $row->kilometros;
                $data["anio"] = $row->anio;
                $data["unidad"] = $row->unidad;
            }

            $data["checklist"] = $this->consulta->get_result("activo","1","inventario");
            
            $this->blade->render('inventario/formulario',$data);
        }  
    }

    public function guardar_formulario()
    {
        $registro = date("Y-m-d H:i:s");

        if ($_POST["envio_formulario"] == "1") {
            $contenido["id"] = NULL;
            $contenido["origen_diagnostico"] = "1";
            $contenido["id_lavador"] = $_POST["lavador"];
            $contenido["id_unidad"] = $_POST["servicio"];
        }

        if (isset($_POST["supervisor"])) {
            $contenido["id_supervisor"] = $_POST["supervisor"];
        }

        if ($_POST["envio_formulario"] == "1") {
            $contenido["fecha_llenado"] = date("Y-m-d");
        }

        $contenido["lavador"] = $_POST["n_lavador"];
        if ($_POST["envio_formulario"] == "1") {
            $contenido["firma_lavador"] = $this->base64ToImage($_POST["ruta_firma_lavador"],"firmas");
        }

        if (isset($_POST["supervisor"])) {
            $contenido["supervisor"] = $_POST["n_supervisor"];
            $contenido["firma_supervisor"] = $this->base64ToImage($_POST["ruta_firma_supervisor"],"firmas");
        }

        $contenido["marca_danos"] = $_POST["marca_danos"];
        $contenido["lavador"] = $_POST["n_lavador"];
        if ($_POST["envio_formulario"] == "1") {
            $contenido["diagrama"] = $this->base64ToImage($_POST["danosMarcas"],"diagramas");
        }
        
        $contenido["nivel_gasolina"] = $_POST["nivel_gasolina"];

        if (isset($_POST["notas_lavador"])) {
            $contenido["comentarios_lavador"] = $_POST["notas_lavador"];
        }

        if (isset($_POST["notas_supervisor"])) {
            $contenido["comentarios_supervisor"] = $_POST["notas_supervisor"];
        }

        $contenido["checklist_topicos"] = $this->validateCheck($_POST["topicos"],"");
        
        if ($_POST["envio_formulario"] == "1") {
            $contenido["fecha_alta"] = $registro;
        }

        if (isset($_POST["supervisor"])) {
            if ($contenido["firma_supervisor"] != "") {
                $contenido["fecha_revision"] = $registro;
            }
        }

        $contenido["fecha_actualiza"] = $registro;

        if ($_POST["envio_formulario"] == "1") {
            $diag = $this->consulta->save_register('diag_unidades_lavadores',$contenido);
            if ($diag != 0) {
                $archivos = $this->guardar_archivos($diag);
                $respuesta = "OK_".$diag;
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_".$_POST["inventario"];
            }
            
        }else{
            $actualizar = $this->consulta->update_table_row('diag_unidades_lavadores',$contenido,'id',$_POST["inventario"]);
            if ($actualizar) {
                $archivos = $this->guardar_archivos($_POST["inventario"]);
                $respuesta = "OK_".$_POST["inventario"];
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_".$_POST["inventario"];
            }
        }

        echo $respuesta;
    }

    public function guardar_archivos($diag='')
    {
        if(count($_FILES['archivo']['tmp_name']) > 0){
            //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
            for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
                $_FILES['tempFile']['name'] = $_FILES['archivo']['name'][$i];
                $_FILES['tempFile']['type'] = $_FILES['archivo']['type'][$i];
                $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'][$i];
                $_FILES['tempFile']['error'] = $_FILES['archivo']['error'][$i];
                $_FILES['tempFile']['size'] = $_FILES['archivo']['size'][$i];

                //Url donde se guardara la imagen
                $urlDoc = "videos";
                //Generamos un nombre random
                $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $urlNombre = date("YmdHi")."_";
                for($j=0; $j<=7; $j++ ){
                    $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
                }

                //Validamos que exista la carpeta destino   base_url()
                if(!file_exists($urlDoc)){
                    mkdir($urlDoc, 0647, true);
                }

                //Configuramos las propiedades permitidas para la imagen
                $config['upload_path'] = $urlDoc;
                $config['file_name'] = $urlNombre;
                $config['allowed_types'] = "*";

                //Cargamos la libreria y la inicializamos
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('tempFile')) {
                    $data['uploadError'] = $this->upload->display_errors();
                    // echo $this->upload->display_errors();
                    //$respuesta = "NO SE PUDIERON CARGAR TODOS LOS ARCHIVOS";
                    $respuesta = "";
                }else{
                    //Si se pudo cargar la imagen la guardamos
                    $fileData = $this->upload->data();
                    $ext = explode(".",$_FILES['tempFile']['name']);
                    $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];

                    $evidencia = array(
                        "id" => NULL,
                        "id_orden" => $diag,
                        "inventario" => "lavadores",
                        "repositorio" => "1",
                        "evidencia" => $path,
                        "activo" => 1,
                        "fecha_alta" => date("Y-m-d H:i:s")
                    );

                    $servicio = $this->consulta->save_register('evidencia_inventario',$evidencia);

                    $respuesta = "OK";
                }
            }
        }else{
            $respuesta = "OK";
        }

        return $respuesta;
    }

    public function validateCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >1){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i].",";
                }
            }
        }
        return $respuesta;
    }

    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Verificamos si se guarda la imagen o si se envio las url
        if (substr($imgBase64,0,8) != "statics/") {
            //Generamos un nombre random para la imagen
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_";
            for($i=0; $i<=7; $i++ ){
                $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            $urlDoc = 'statics/inventario/'.$direcctorio;

            $data = explode(',', $imgBase64);
            //Creamos la ruta donde se guardara la firma y su extensión
            if ($data[0] == "data:image/png;base64") {
                $base ='statics/inventario/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
                
            }elseif ($data[0] == "data:image/jpeg;base64") {
                $base ='statics/inventario/'.$direcctorio.'/'.$urlNombre.".jpeg";

                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            $base = $imgBase64;
        }

        return $base;
    }

    //Comprobamos que al final se genere el pdf
    public function generar_PDF($id_servicio = 0,$datos = NULL)
    {
        $datos["destinoVista"] = "PDF";
        $id_servicio = $this->decrypt($id_servicio);
        $this->recuperar_formulario($id_servicio,$datos);
    }

    //Comprobamos que al final se genera la vista de revision
    public function generar_revision($id_servicio = 0,$datos = NULL)
    {
        $datos["destinoVista"] = "Revisión";
        $id_servicio = $this->decrypt($id_servicio);
        $this->recuperar_formulario($id_servicio,$datos);
    }

    //Comprobamos que al final se genere el pdf
    public function generar_edicion($id_servicio = 0,$datos = NULL)
    {
        $datos["destinoVista"] = "Formulario";
        $id_servicio = $this->decrypt($id_servicio);
        $this->recuperar_formulario($id_servicio,$datos);
    }

    public function recuperar_formulario($id_servicio = '',$data = NULL)
    {
        //Datos del inventario
        $datos_unidad = $this->consulta->get_result_one('id',$id_servicio,'diag_unidades_lavadores');
        foreach ($datos_unidad as $row){
            $data["id"] = $row->id;
            $data["origen_repo"] = $row->origen_diagnostico;
            $data["n_unidad"] = $row->id_unidad;
            $data["lavador"] = $row->id_lavador;
            $data["supervisor"] = $row->id_supervisor;
            $data["n_lavador"] = $row->lavador;
            $data["n_supervisor"] = $row->supervisor;
            $data["marca_danos"] = $row->marca_danos;
            $data["nivel_gasolina"] = $row->nivel_gasolina;
            $data["notas_lavador"] = $row->comentarios_lavador;
            $data["notas_supervisor"] = $row->comentarios_supervisor;

            $data["cheking"] = explode(",",$row->checklist_topicos);

            if ($data["origen_repo"] == "1") {
                $firma_lavador = REPO_XEHOS.$row->firma_lavador;
                $firma_supervisor = (($row->firma_supervisor != "") ? REPO_XEHOS.$row->firma_supervisor : "");
                $danosMarcas = REPO_XEHOS.$row->diagrama;
            }else{
                $firma_lavador = REPO_XEHOS_OP.$row->firma_lavador;
                $firma_supervisor = (($row->firma_supervisor != "") ? REPO_XEHOS_OP.$row->firma_supervisor : "");
                $danosMarcas = REPO_XEHOS_OP.$row->diagrama;
            }

            $data["firma_lavador"] = (($this->url_exists($firma_lavador)) ? $firma_lavador : "") ;
            if ($firma_supervisor != "") {
                $data["firma_supervisor"] = (($this->url_exists($firma_supervisor)) ? $firma_supervisor : "") ;
            } else {
                $data["firma_supervisor"] = "";
            }
            
            $data["danosMarcas"] = (($this->url_exists($danosMarcas)) ? $danosMarcas : "") ;
        }

        if (isset($data["id"])) {
            //Datos generales de la unidad
            $datos_unidad = $this->consulta->get_result_one('id',$data["n_unidad"],'unidades');
            foreach ($datos_unidad as $row){
                $data["marca"] = $row->marca;
                $data["placas"] = $row->placas;
                $data["color"] = $row->color;
                $data["serie"] = $row->n_serie;
                $data["kilometros"] = $row->kilometros;
                $data["anio"] = $row->anio;
                $data["unidad"] = $row->unidad;
            }

            //Cargamos las evidencias  
            $evidencias = $this->consulta->get_result_field("id_orden",$data["id"],"inventario","lavadores","evidencia_inventario");
            foreach ($evidencias as $row){
                if ($row->activo == "1") {
                    $data["evidencias"][] = $row->evidencia;
                }
            }

            //Verificamos si ya se guardaron los datos de quien revisa
            if ($data["supervisor"] == NULL) {
                $id_usuario = $this->session->userdata('id');
                $data["supervisor"] = $id_usuario;
                $data["n_supervisor"] = $this->consulta->nombre_usuario($id_usuario);
            }
        }

        $data["checklist"] = $this->consulta->get_result("activo","1","inventario");

        if ($data["destinoVista"] == "PDF") {
            $this->blade->render('inventario/revision',$data);
        }elseif ($data["destinoVista"] == "Revisión") {
            $data["id_firma"] = $this->encrypt($data["id"]);
            $this->blade->render('inventario/revision',$data);
        }else{
            $this->blade->render('inventario/formulario',$data);
        }
    }

    public function url_exists($url) {
        $h = get_headers($url);
        $status = array();
        preg_match('/HTTP\/.* ([0-9]+) .*/', $h[0] , $status);
        return ($status[1] == 200);
    }

    public function guardar_diagrama()
    {
        $img_diagrama = $this->base64ToImage($_POST["diagrama"],"diagramas");
        echo "OK=".$img_diagrama;
    }

    public function redireccionar($id='')
    {
        $cupon = $this->encrypt($id);
        echo $cupon;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
}