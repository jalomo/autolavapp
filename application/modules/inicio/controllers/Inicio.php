<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Inicio extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Minicio', '', TRUE);
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        if($this->session->userdata('id')){}else{redirect('login/');}
    }
    public function test(){
      $this->load->view('inicio/test', '', FALSE);
    }

    public function guarda_test(){

      $this->form_validation->set_rules('uno', 'uno', 'required');
      $this->form_validation->set_rules('dos', 'dos', 'required');
      $this->form_validation->set_rules('tres', 'tres', 'required');
      $this->form_validation->set_rules('cuatro', 'cuatro', 'required');
      echo validate($this);
    }

    public function index2(){

      $head = $this->load->view('inicio/head', '', TRUE);
      $header_logo = $this->load->view('inicio/header_logo', '', TRUE);
      $menu = $this->load->view('inicio/menu', '', TRUE);
      $contenido = $this->load->view('inicio/contenido', '', TRUE);
      $this->load->view('home', array('head'=>$head,
                                      'header_logo'=>$header_logo,
                                      'menu'=>$menu,
                                      'contenido'=>$contenido,
                                          'included_js'=>array('')));

    }


    public function index(){
      $id_usuario = $this->session->userdata('id');
      $datos = $this->db->where('adminId', $id_usuario)->get('admin')->row_array();
      $this->session->set_userdata('telefono', $datos['telefono']);

      $this->blade->render('contenido');
    }


  }
