<!doctype html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <!--html class="no-js" lang=""--> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->config->item('TITULO');?></title>
        <meta name="description" content="Sufee Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/png" href="<?= base_url(); ?>statics/inventario/imgs/logos/xehos2.png"/>

        <!--<link rel="apple-touch-icon" href="apple-icon.png">
        <link rel="shortcut icon" href="favicon.ico">-->

        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/flag-icon.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/cs-skin-elastic.css">
        <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/scss/style.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

        <style type="text/css">
            .cargaIcono {
                -webkit-animation: rotation 1s infinite linear;
                font-size: 55px;
                color: darkblue;
                display: none;
            }
        </style>
        <!--EOD Styles-->
        <link rel="stylesheet" href="<?php echo base_url();?>statics/css/styleNETO.css">
        <script src="https://kit.fontawesome.com/fe47b5dbda.js" crossorigin="anonymous"></script>
    </head>

    <body class="login-image">
        <div class="sufee-login d-flex align-content-center flex-wrap">
            <div class="container">
                <div class="login-content">
                    <div class="login-form">
                        <form id="registro" method="post">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input type="text" class="form-control" name="nombre" required/>
                                <div id="nombre_error"></div>
                            </div>
                            
                            <div class="form-group">
                                <label>Apellido paterno:</label>
                                <input type="text" class="form-control" name="ap_paterno" />
                                <div id="ap_paterno_error"></div>
                            </div>

                            <div class="form-group">
                                <label>Apellido materno:</label>
                                <input type="text" class="form-control" name="am_paterno" />
                                <div id="am_paterno_error"></div>
                            </div>
                            
                            <div class="form-group">
                                <label>Teléfono:</label>
                                <input type="text" class="form-control" onblur="validar_telefono()" name="celular" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' />
                                <div id="celular_error"></div>
                            </div>

                            <div class="form-group">
                                <label>Correo Electrónico:</label>
                                <input type="text" class="form-control" name="email" value="notienecorreo@notiene.com" required/>
                                <div id="email_error"></div>
                            </div>

                            <div class="form-group">
                                <label>Estado:</label>
                                <select id="estados" name="estados" class="form-control"> 
                                    <option value=""> -- Seleccionar --</option>
                                    @foreach ($cat_estados as $s => $edo) 
                                        <option value="<?=$edo->id ?>"> 
                                            <?= $edo->estado  ?> 
                                        </option>
                                    @endforeach
                                </select>
                                <div id="estados_error"></div> 
                            </div>

                            <div class="form-group">
                                <label>Sucursal:</label>
                                <select id="sucursal" name="sucursal" class="form-control">
                                    <option value=''> -- Seleccione estado --</option>
                                    
                                </select>
                                <div id="sucursal_error"></div>
                            </div>

                            <div class="form-group">
                                <label>Usuario:</label>
                                <input type="text" class="form-control" onblur="validar_uso()" name="nUsuario" required/>
                                <div id="nUsuario_error"></div>
                            </div>

                            <div class="form-group">
                                <label>Contraseña:</label>
                                <input type="password" minlength="8" class="form-control" name="nPass" required/>
                                <span>*Minimo 8 caracteres </span>
                                <div id="nPass_error"></div>
                            </div>
                            <br>
                            <h5 style="color:white;" id="formulario_error"></h5>
                            <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>

                            <button type="submit" id="btnregistro" class="btn btn-success btn-flat m-b-30 m-t-30">Registrar</button>
                            <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                            <input type="hidden" name="formulario" value="1">
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <script src="<?php echo base_url();?>statics/tema/assets/js/vendor/jquery-2.1.4.min.js"></script>
        <!--script type="text/javascript" src="<?php echo base_url().'statics/js/libraries/jquery.js'; ?>"></script-->
        <!--script src="<?php echo base_url();?>statics/tema/assets/js/popper.min.js"></script>
        <script src="<?php echo base_url();?>statics/tema/assets/js/plugins.js"></script-->
        <!--script src="<?php echo base_url();?>statics/tema/assets/js/main.js"></script-->

        <script src="{{ base_url('statics/js/recomendadores/registro.js') }}"></script>
    </body>
</html>
