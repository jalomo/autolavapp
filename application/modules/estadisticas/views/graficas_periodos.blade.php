
<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>

<script>
    var periodo1 = [];
    var periodo2 = [];
    var periodo3 = [];
    var periodo4 = [];
    var nombre_grafica = "<?php echo $nombre_grafica ?>";

    <?php foreach ($p1 as $key => $value): ?>
     var valor = "<?php echo $value ?>";
     periodo1.push(parseInt(valor));
    <?php endforeach ?>


    <?php foreach ($p2 as $key => $value): ?>
     var valor2 = "<?php echo $value ?>";
     periodo2.push(parseInt(valor2));
    <?php endforeach ?>

     <?php foreach ($p3 as $key => $value): ?>
     var valor3 = "<?php echo $value ?>";
     periodo3.push(parseInt(valor3));
    <?php endforeach ?>


     <?php foreach ($p4 as $key => $value): ?>
     var valor4 = "<?php echo $value ?>";
     periodo4.push(parseInt(valor4));
    <?php endforeach ?>


    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Cantidad de  servicios '+nombre_grafica
    },
    xAxis: {
        categories: [
            'Ene',
            'Feb',
            'Mar',
            'Abr',
            'May',
            'Jun',
            'Jul',
            'Ago',
            'Sep',
            'Oct',
            'Nov',
            'Dic'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: '# de Servicios'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Periodo1',
        data: periodo1

    }, {
        name: 'Periodo2',
        data: periodo2

    }, {
        name: 'Periodo3',
        data: periodo3

    }, {
        name: 'Periodo4',
        data: periodo4

    }]
});
</script>