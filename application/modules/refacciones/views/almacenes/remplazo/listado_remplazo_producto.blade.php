@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ $bread_active}}</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>N. Identificacion</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($listado as $item)    
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->no_identificacion }}</td>
                                    <td>{{ $item->descripcion }}</td>
                                    <td>
                                        @if (isset($item->rel_precio->precio_publico))
                                            $ {{ porcentajeProducto($item->rel_precio->precio_publico,$item->precio_factura)}}
                                            @else
                                            $ {{$item->precio_factura}}
                                        @endif
                                    </td>
                                    
                                    <td>{{ $item->unidad }}</td>
                                    <td>
                                        <a class="btn btn-success" href="{{ site_url('refacciones/almacenes/productoRemplazo/'.$item->id) }}"> 
                                            <i class="fas fa-exchange-alt"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                           
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('included_js')
@include('main/scripts_dt')

<script>
    $(document).ready(function() {
        $('#tbl_productos').DataTable({})
    });

    $("#tbl_productos").on("click", ".btn-borrar", function() {
        var id = $(this).data('id')
        borrar(id)
    });

    function borrar(id) {
        utils.displayWarningDialog("Desea borrar el registro??", "warning", function(data) {
            if (data.value) {
                ajax.delete(`/api/productos/${id}`, null, function(response, headers) {
                    if (headers.status != 204) {
                        return utils.displayWarningDialog(headers.message)
                    }
                    location.reload(true)
                })

            }
        }, true)
    }

</script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_almacen").addClass("show");
            $("#refacciones_almacen").addClass("active");
            $("#refacciones_almacen_sub").addClass("show");
            $("#refacciones_almacen_sub").addClass("active");
            $("#almacen_remplazos").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection