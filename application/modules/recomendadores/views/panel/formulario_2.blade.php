@layout('recomendadores/layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12">
            <form id="formulario_banco" method="post" action="" autocomplete="on">
                <h3 style="color:darkblue;">
                    <a href="#" class="btn btn-menu-titulo">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                    </a>
                    <?php if(isset($titulo_dos)): ?>
                        <?php echo $titulo_dos; ?>
                    <?php endif; ?>
                </h3> 

                <hr>
                <div class="row">
                    <div class="col-sm-8">
                        <label for="">CLABE bancaria : </label>
                        <input type="text" class="form-control" name="clave_bancaria" maxlength="19" onblur="buscar_banco()" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($clave_bancaria)) echo $clave_bancaria; ?>">
                        <div id="clave_bancaria_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-5">
                        <label for="">Banco : </label>
                        <select id="banco" name="n_banco" class="form-control">
                            <option value=""> -- Seleccionar --</option>
                            @foreach ($cat_bancos as $s => $banco_cat) 
                                <option value="<?=$banco_cat->id ?>" <?= (($n_banco == $banco_cat->id) ? "selected" : "")?>> 
                                    <?= $banco_cat->nombre  ?> 
                                </option>
                            @endforeach
                        </select>
                        <input type="hidden" class="form-control" id="clave_bco" value="<?php if(isset($clave_sat)) echo $clave_sat; ?>">
                        <div id="banco_error"></div>
                    </div>
                    <div class="col-sm-7">
                        <label for="">Nombre / Razón Social : </label>
                        <input type="text" class="form-control" id="nombre_banco" value="<?php if(isset($nombre_banco)) echo $nombre_banco; ?>" disabled>
                        <input type="hidden" class="form-control" name="banco" value="<?php if(isset($banco)) echo $banco; ?>">
                        <div id="nombre_banco_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Sucursal : </label>
                        <input type="text" class="form-control" name="sucursal" value="<?php if(isset($sucursal)) echo $sucursal; ?>">
                        <div id="sucursal_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">N° de Cuenta : </label>
                        <input type="text" class="form-control" name="cuenta" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($cuenta)) echo $cuenta; ?>">
                        <div id="cuenta_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">N° de Tarjeta : </label>
                        <input type="text" class="form-control" name="n_tarjeta"  maxlength="18" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value="<?php if(isset($n_tarjeta)) echo $n_tarjeta; ?>">
                        <div id="n_tarjeta_error"></div>
                    </div>
                </div>

                <br>
                <div class="col-sm-12" align="center">
                    <br>
                    <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                    <br>
                    <h5 class="error" id="formulario_error"></h5>
                    
                    <input type="hidden" name="formulario" value="2">
                    <input type="button" class="btn btn-success" id="enviar_formulario" value="ACTUALIZAR">
                    
                    <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                    <input type="hidden" name="listado" value="<?php if(isset($id)) echo $id; else echo '0'; ?>">
                    <br>    
                </div>
            </form>            
        </div>
    </div>
@endsection
@section('included_js') 

    <script src="{{ base_url('statics/js/recomendadores/formulario_banco.js') }}"></script>

@endsection