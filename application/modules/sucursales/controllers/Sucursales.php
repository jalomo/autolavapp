<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Sucursales extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta(){
      $data['estado'] =  $this->db->get('estados')->result();
      $data['titulo'] = "Sucursal" ;
      $data['titulo_dos'] = "Alta Sucursal" ;
      $data['rows'] = $this->Mgeneral->get_table('sucursales');
      $this->blade->render('sucursales/alta', $data);
    }

    public function editar($id){
      $data['titulo'] = "Sucursal" ;
      $data['titulo_dos'] = "editar Sucursal" ;
      $data['row'] = $this->Mgeneral->get_row('id',$id,'sucursales');
      $this->blade->render('sucursales/editar', $data);
    }

    public function alta_guardar(){
      $data['sucursal'] = $this->input->post('sucursal');
      $data['nomenclatura'] = $this->input->post('nomenclatura');
      $data['id_estado'] = $this->input->post('id_estado');
      $data['activo'] = 1;
     $this->Mgeneral->save_register('sucursales', $data);
    }
    public function alta_editar($id){
      $data['sucursal'] = $this->input->post('sucursal');
      $data['nomenclatura'] = $this->input->post('nomenclatura');
      $data['activo'] = 1;
     $this->Mgeneral->update_table_row('sucursales',$data,'id',$id);
    }
    public function ejemplo_modal(){
      $data['parametro1'] = $_POST['parametro1'];
      $data['parametro2'] = $_POST['parametro2'];
      $this->blade->render('sucursales/ejemplo_modal', $data);
    }

}
