//Condicionamos las busquedas por fecha 
$('#fecha_activo').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();
    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_caducidad").attr('min',fecha_inicio);
});

$('#fecha_caducidad').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();
    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_activo").attr('max',fecha_fin);
});

$('#usuario').on('change', function() {
    var selected = $("#usuario option:selected").text();
    $("input[name='nombre_usuario']").val(selected);
});