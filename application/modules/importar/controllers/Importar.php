<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Importar extends MX_Controller
{
	private $filename = "import_data"; // 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Importar', '', TRUE);
		$this->load->library(array('session'));
		$this->load->helper(array('form', 'html', 'validation'));
		date_default_timezone_set('America/Mexico_City');
		if ($this->session->userdata('id')) {
		} else {
			redirect('login/');
		}
		if (!PermisoModulo('estadisticas')) {
			redirect(site_url('login'));
		}
	}
	public function index()
	{
		$data['info'] = $this->M_Importar->view();
		$this->blade->render('importar', $data);
	}
	public function jalisco_motors(){
		$this->subir_archivo(4);
	}
	public function subir_archivo($id_agencia=0)
	{
		$data = array(); 

		if (isset($_POST['preview'])) { 
			$upload = $this->M_Importar->upload_file($this->filename);
			if ($upload['result'] == "success") { 
				//print_r($_POST);die();
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); 
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
				$data['sheet'] = $sheet;
				$data['id_agencia'] = $_POST['id_agencia'];
				//debug_var($data);die();
			} else { 
				$data['upload_error'] = $upload['error']; 
			}
		}
		$data['id_agencia'] = $id_agencia;
		$this->blade->render('subir_archivo', $data);
	}
	public function import()
	{
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); 
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
		$data = array();
		$numrow = 1;
		foreach ($sheet as $row) {
			if ($numrow > 1) {
				array_push($data, array(
					'id_agencia' => $_POST['id_agencia_save'],
					'id_cita' => 0,
					'fecha_programacion' => $row['A'],
					'numero_interno' => $row['B'],
					'nombre_compania' => $row['C'],
					'nombre_contacto_compania' => $row['D'],
					'ap_contacto' => $row['E'],
					'am_contacto' => $row['F'],
					'rfc' => $row['G'],
					'correo_compania' => $row['H'],
					'calle' => $row['I'],
					'nointerior' => $row['J'],
					'noexterior' => $row['K'],
					'colonia' => $row['L'],
					'municipio' => $row['M'],
					'cp' => $row['N'],
					'estado' => $row['O'],
					'telefono_movil' => $row['P'],
					'otro_telefono' => $row['Q'],
					'kilometraje' => $row['R'],
					'transmision' => $row['S'],
					'numero_cliente' => $row['T'],
					'color' => $row['U'],
					'marca' => $row['V'],
					'tipo_cliente' => $row['W'],
					'cilindros' => $row['X'],
					'email' => $row['Y'],
					'vehiculo_anio' => $row['Z'],
					'modelo' => $row['AA'],
					'version' => $row['AB'],
					'placas' => $row['AC'],
					'serie' => $row['AD'],
					'nombre' => $row['AE'],
					'apellido_paterno' => $row['AF'],
					'apellido_materno' => $row['AG'],
					'telefono' => $row['AH'],
					'servicio' => $row['AI'],
					'intentos' => 0,
					'no_contactar' => 0,
					'nunca_contactar' => 0,
					'created_at' => date('Y-m-d H:i:s')
				));
			}
			$numrow++;
		}
		$this->M_Importar->insert_multiple($data);
		redirect("importar");
	}
}
