<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Servicios extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->model('M_servicios', 'ms');
    $this->load->library(array('table', 'email'));
    $this->load->helper(array('form', 'html', 'validation', 'url', 'astillero'));
    date_default_timezone_set('America/Mexico_City');
    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
    if (!PermisoModulo('servicios')) {
      redirect(site_url('login'));
    }
  }
  public function menu_refacciones(){
    $this->blade->render('menu_refacciones');
  }
  public function index()
  {
    $data['titulo'] = "Listado de Servicios";
    $data['titulo_dos'] = "Listado de Servicios";
    $data['info'] = $this->ms->getAll();
    $this->blade->render('servicios/listado_servicios', $data);
  }
  public function alta()
  {
    $data['titulo'] = "Medidas";
    $data['titulo_dos'] = "Alta Medidas";
    $data['rows'] = $this->db->join('sucursales s', 's.id=ser.id_sucursal')->select('ser.*,s.sucursal')->get('servicios ser')->result();
    $data['sucursales'] = $this->db->get('sucursales')->result();
    $this->blade->render('servicios/alta', $data);
  }

  public function alta_guardar()
  {
    $name = date('dmyHis') . '_' . str_replace(" ", "", $_FILES['image']['name']);
    $path_to_save = 'statics/logo/';
    if (!file_exists($path_to_save)) {
      mkdir($path_to_save, 0777, true);
    }
    move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save . $name);
    $data['servicioImagen'] = $path_to_save . $name;
    $data['servicioNombre'] = $this->input->post('servicioNombre');
    $data['servicioPuntos'] = $this->input->post('servicioPuntos');
    $data['id_sucursal'] = $this->input->post('id_sucursal');
    $this->Mgeneral->save_register('servicios', $data);
  }
  public function generar_servicio($id = 0, $proactivo = 0, $agencia = 0, $id_agencia = 0, $id_lavador = 0)
  {
    $id = decrypt($id);
    $id_lavador = decrypt($id_lavador);
    if ($id == 0) {
      $info = new Stdclass();
      $info_usuario = new Stdclass();
      $info_ubicacion = new Stdclass();
      $info_auto = new Stdclass();
      $info_horario = new Stdclass();
      $info_facturacion = new Stdclass();
      $data['mapa_latitud'] = '';
      $data['mapa_longitud'] = '';
      $data['id_usuario'] = 0;
      $data['id_ubicacion'] = 0;
      $data['id_auto'] = 0;
      $data['id_facturacion'] = 0;
      $data['id_horario'] = 0;
      $data['evidencia'] = '';
      $data['path_evidencia'] = '';
      $data['origen'] = ($agencia == 0) ? 1 : $this->ms->getDataCatAgencia($id_agencia)->origen;
      $data['url_agencia'] = ($agencia == 0) ? '' : $this->ms->getDataCatAgencia($id_agencia)->url;
    } else {
      $info = $this->ms->getById($id, 'servicio_lavado');
      $info_usuario = $this->ms->getById($info->id_usuario, 'usuarios');
      $info_ubicacion = $this->ms->getById($info->id_ubicacion, 'ubicacion_servicio');
      $data['mapa_latitud'] = $info_ubicacion->latitud;
      $data['mapa_longitud'] = $info_ubicacion->longitud;
      $info_auto = $this->ms->getById($info->id_auto, 'autos');
      $info_horario = $this->ms->getById($info->id_horario, 'horarios_lavadores');
      $info_facturacion = $this->ms->getDatosFacturacionByIdUsuario($info->id_usuario);
      $data['id_usuario'] = $info->id_usuario;
      $data['id_ubicacion'] = $info->id_ubicacion;
      $data['id_auto'] = $info->id_auto;
      $data['id_horario'] = $info->id_horario;
      if ($info_facturacion) {
        $data['id_facturacion'] = $info_facturacion->id;
      } else {
        $data['id_facturacion'] = 0;
      }
      $data['evidencia'] = $info->evidencia;
      $data['path_evidencia'] = $info->path_evidencia;
      $data['origen'] = $info->origen;
      $data['url_agencia'] = ($agencia == 0) ? '' : $this->ms->getDataCatAgencia($id_agencia)->url;
    }
    if ($this->input->post()) {
      //datos del vehículo
      $this->form_validation->set_rules('placas', 'placas', 'trim|required');
      $this->form_validation->set_rules('id_color', 'color', 'trim|required');
      $this->form_validation->set_rules('id_modelo', 'modelo', 'trim|required');
      $this->form_validation->set_rules('id_anio', 'año', 'trim|required');
      $this->form_validation->set_rules('id_marca', 'marca', 'trim|required');
      $this->form_validation->set_rules('numero_int', 'número interior', 'trim');
      $this->form_validation->set_rules('numero_ext', 'número exterior', 'trim|required');
      $this->form_validation->set_rules('colonia', 'colonia', 'trim|required');
      $this->form_validation->set_rules('id_estado', 'estado', 'trim|required');
      $this->form_validation->set_rules('id_sucursal', 'municipio', 'trim|required');
      $this->form_validation->set_rules('latitud', 'latitud', 'trim|required');
      $this->form_validation->set_rules('longitud', 'longitud', 'trim|required');
      $this->form_validation->set_rules('calle', 'calle', 'trim|required');
      $this->form_validation->set_rules('kilometraje', 'kilometraje', 'trim|numeric');
      $this->form_validation->set_rules('numero_serie', 'numero serie', 'trim|exact_length[17]');
      //servicio
      if ($_POST['id'] == 0) {
        $this->form_validation->set_rules('fecha', 'fecha', 'trim|required');
        $this->form_validation->set_rules('hora', 'hora', 'trim|required');
      }
      $this->form_validation->set_rules('id_servicio', 'servicio', 'trim|required');
      //usuario
      $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
      $this->form_validation->set_rules('apellido_paterno', 'apellido paterno', 'trim|required');
      $this->form_validation->set_rules('apellido_materno', 'apellido materno', 'trim|required');
      $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required');
      $this->form_validation->set_rules('telefono', 'telefono', 'trim|exact_length[10]|required|numeric');
      $this->form_validation->set_rules('id_sucursal', 'sucursal', 'trim|required');
      $this->form_validation->set_rules('id_municipio', 'municipio', 'trim|required');
      //Ubicación
      $this->form_validation->set_rules('id_sucursal_ubicacion', 'sucursal', 'trim|required');
      //facturacion
      if (isset($_POST['check_facturacion'])) {
        $this->form_validation->set_rules('referencia', 'referencia', 'trim|required');
        $this->form_validation->set_rules('razon_social', 'razon_social', 'trim|required');
        $this->form_validation->set_rules('domicilio', 'domicilio', 'trim|required');
        $this->form_validation->set_rules('email_facturacion', 'email', 'trim|valid_email|required');
        $this->form_validation->set_rules('id_cfdi', 'CFDI', 'trim|required');
        $this->form_validation->set_rules('id_metodo_pago', 'método de pago', 'trim|required');
        $this->form_validation->set_rules('rfc', 'R.F.C', 'trim|required');
        $this->form_validation->set_rules('cp', 'C.P.', 'trim|required|numeric');
      }

      if ($this->form_validation->run()) {
        $this->ms->guardarServicio();
      } else {
        $errors = array(
          //Datos del vehículo
          'placas' => form_error('placas'),
          'id_color' => form_error('id_color'),
          'id_modelo' => form_error('id_modelo'),
          'id_anio' => form_error('id_anio'),
          'id_marca' => form_error('id_marca'),
          'kilometraje' => form_error('kilometraje'),
          'numero_serie' => form_error('numero_serie'),
          //Ubicación
          'numero_int' => form_error('numero_int'),
          'numero_ext' => form_error('numero_ext'),
          'colonia' => form_error('colonia'),
          'id_estado' => form_error('id_estado'),
          'id_sucursal' => form_error('id_sucursal'),
          'latitud' => form_error('latitud'),
          'longitud' => form_error('longitud'),
          'calle' => form_error('calle'),
          //Servicio
          'fecha' => form_error('fecha'),
          'hora' => form_error('hora'),
          'id_servicio' => form_error('id_servicio'),
          //usuario
          'nombre' => form_error('nombre'),
          'apellido_paterno' => form_error('apellido_paterno'),
          'apellido_materno' => form_error('apellido_materno'),
          'email' => form_error('email'),
          'telefono' => form_error('telefono'),
          'id_sucursal' => form_error('id_sucursal'),
          'id_municipio' => form_error('id_municipio'),
          //'password' => form_error('password'),
          //facturación
          'referencia' => form_error('referencia'),
          'razon_social' => form_error('razon_social'),
          'domicilio' => form_error('domicilio'),
          'email_facturacion' => form_error('email_facturacion'),
          'id_cfdi' => form_error('id_cfdi'),
          'id_metodo_pago' => form_error('id_metodo_pago'),
          'rfc' => form_error('rfc'),
          'cp' => form_error('cp'),
          //ubicación
          'id_sucursal_ubicacion' => form_error('id_sucursal_ubicacion')
        );
        echo json_encode($errors);
        exit();
      }
    }
    //datos del vehículo
    $data['placas'] = form_input('placas', set_value('placas', exist_obj($info_auto, 'placas')), 'class="form-control" id="placas"');
    $data['kilometraje'] = form_input('kilometraje', set_value('kilometraje', exist_obj($info_auto, 'kilometraje')), 'class="form-control numeric" id="kilometraje" maxlength="10"');
    $data['numero_serie'] = form_input('numero_serie', set_value('numero_serie', exist_obj($info_auto, 'numero_serie')), 'class="form-control" id="numero_serie" maxlength="17"');
    $data['id_color'] = form_dropdown('id_color', array_combos($this->Mgeneral->get_table('cat_colores'), 'id', 'color', TRUE), set_value('id_color', exist_obj($info_auto, 'id_color')), 'class="form-control busqueda" id="id_color"');
    $data['id_anio'] = form_dropdown('id_anio', array_combos($this->Mgeneral->get_table('cat_anios'), 'id', 'anio', TRUE), set_value('id_anio', exist_obj($info_auto, 'id_anio')), 'class="form-control busqueda" id="id_anio"');

    if ($id == 0) {
      $lista_modelos = array();
    } else {
      $lista_modelos = $this->ms->getModelosByMarca(set_value('id_marca', exist_obj($info_auto, 'id_marca')));
    }
    $data['id_marca'] = form_dropdown('id_marca', array_combos($this->Mgeneral->get_table('cat_marcas'), 'id', 'marca', TRUE), set_value('id_marca', exist_obj($info_auto, 'id_marca')), 'class="form-control busqueda" id="id_marca"');
    $data['id_modelo'] = form_dropdown('id_modelo', array_combos($lista_modelos, 'id', 'modelo', TRUE), set_value('id_modelo', exist_obj($info_auto, 'id_modelo')), 'class="form-control busqueda" id="id_modelo"');
    //datos de ubicación
    $data['numero_int'] = form_input('numero_int', set_value('numero_int', exist_obj($info_ubicacion, 'numero_int')), 'class="form-control" id="numero_int"');
    $data['numero_ext'] = form_input('numero_ext', set_value('numero_ext', exist_obj($info_ubicacion, 'numero_ext')), 'class="form-control" id="numero_ext"');
    $data['colonia'] = form_input('colonia', set_value('colonia', exist_obj($info_ubicacion, 'colonia')), 'class="form-control" id="colonia"');
    $this->db->where('activo', 1);
    $data['id_estado'] = form_dropdown('id_estado', array_combos($this->Mgeneral->get_table('estados'), 'id', 'estado', TRUE), set_value('id_estado', exist_obj($info_ubicacion, 'id_estado')), 'class="form-control busqueda" id="id_estado"');
    $data['calle'] = form_input('calle', set_value('calle', exist_obj($info_ubicacion, 'calle')), 'class="form-control" id="calle"');
    if ($id == 0) {
      $lista_sucursales = array();
      $lista_municipios = array();
    } else {
      $lista_sucursales = $this->ms->getSucursalesByEstado(set_value('id_estado', exist_obj($info_ubicacion, 'id_estado')));
      $lista_municipios = $this->ms->getMunicipiosByEstado(set_value('id_estado', exist_obj($info_ubicacion, 'id_estado')));
    }

    $data['id_sucursal_ubicacion'] = form_dropdown('id_sucursal_ubicacion', array_combos($lista_sucursales, 'id', 'sucursal', TRUE), set_value('id_sucursal', exist_obj($info_ubicacion, 'id_sucursal')), 'class="form-control busqueda" id="id_sucursal_ubicacion"');
    $data['id_municipio'] = form_dropdown('id_municipio', array_combos($lista_municipios, 'id', 'municipio', TRUE), set_value('id_municipio', exist_obj($info_ubicacion, 'id_municipio')), 'class="form-control busqueda" id="id_municipio"');
    $data['latitud'] = form_input('latitud', set_value('latitud', exist_obj($info_ubicacion, 'latitud')), 'class="form-control" id="latitud" readonly');
    $data['longitud'] = form_input('longitud', set_value('longitud', exist_obj($info_ubicacion, 'longitud')), 'class="form-control" id="longitud" readonly');
    //Datos de la cita
    $data['fecha'] = form_dropdown('fecha', array_combos($this->ms->getFechasLavadores('', ''), 'fecha', 'fecha', TRUE), set_value('fecha', exist_obj($info_horario, 'fecha')), 'class="form-control" id="fecha"');
    if ($id == 0) {
      $lista_horas = array();
    } else {
      $lista_horas = $this->ms->getHorariosDisponibles(set_value('fecha', exist_obj($info_horario, 'fecha')), '', set_value('fecha', exist_obj($info_usuario, 'id_sucursal')));
    }
    $data['hora'] = form_dropdown('hora', array_combos($lista_horas, 'hora', 'hora', TRUE), set_value('hora', exist_obj($info, 'hora')), 'class="form-control" id="hora"');

    $data['comentarios'] = form_textarea('comentarios', set_value('comentarios', exist_obj($info, 'comentarios')), 'class="form-control" rows="2" id="comentarios"');
    $data['id_servicio'] = form_dropdown('id_servicio', array_combos($this->Mgeneral->get_table('servicios'), 'servicioId', 'servicioNombre', TRUE), set_value('id_servicio', exist_obj($info, 'id_servicio')), 'class="form-control busqueda" id="id_servicio"');
    //Datos del usuario
    $data['nombre'] = form_input('nombre', set_value('nombre', exist_obj($info_usuario, 'nombre')), 'class="form-control js_user" id="nombre"');
    $data['apellido_paterno'] = form_input('apellido_paterno', set_value('apellido_paterno', exist_obj($info_usuario, 'apellido_paterno')), 'class="form-control js_user" id="apellido_paterno"');
    $data['apellido_materno'] = form_input('apellido_materno', set_value('apellido_materno', exist_obj($info_usuario, 'apellido_materno')), 'class="form-control js_user" id="apellido_materno"');
    $data['email'] = form_input('email', set_value('email', exist_obj($info_usuario, 'email')), 'class="form-control js_user" id="email"');
    $data['telefono'] = form_input('telefono', set_value('telefono', exist_obj($info_usuario, 'telefono')), 'class="form-control js_user" id="telefono" maxlength="10');
    $data['password'] = form_input('password', set_value('password', exist_obj($info_usuario, 'password')), 'class="form-control js_user" id="password"');
    $data['id_sucursal'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), set_value('id_sucursal', exist_obj($info_usuario, 'id_sucursal')), 'class="form-control js_user busqueda" id="id_sucursal"');
    //Datos facturación
    $data['referencia'] = form_input('referencia', set_value('referencia', exist_obj($info_facturacion, 'referencia')), 'class="form-control" id="referencia"');
    $data['cp'] = form_input('cp', set_value('cp', exist_obj($info_facturacion, 'cp')), 'class="form-control" id="cp"');
    $data['razon_social'] = form_input('razon_social', set_value('razon_social', exist_obj($info_facturacion, 'razon_social')), 'class="form-control" id="razon_social"');
    $data['domicilio'] = form_input('domicilio', set_value('domicilio', exist_obj($info_facturacion, 'domicilio')), 'class="form-control" id="domicilio"');
    $data['email_facturacion'] = form_input('email_facturacion', set_value('email_facturacion', exist_obj($info_facturacion, 'email_facturacion')), 'class="form-control" id="email_facturacion"');
    $data['id_cfdi'] = form_dropdown('id_cfdi', array_combos($this->Mgeneral->get_table('cat_cfdi'), 'id', 'cfdi', TRUE), set_value('id_cfdi', exist_obj($info_facturacion, 'id_cfdi')), 'class="form-control busqueda" id="id_cfdi"');
    $data['rfc'] = form_input('rfc', set_value('rfc', exist_obj($info_facturacion, 'rfc')), 'class="form-control" id="rfc"');
    $data['id_metodo_pago'] = form_dropdown('id_metodo_pago', array_combos($this->Mgeneral->get_table('cat_metodo_pago'), 'id', 'metodo_pago', TRUE), set_value('id_metodo_pago', exist_obj($info_facturacion, 'id_metodo_pago')), 'class="form-control busqueda" id="id_metodo_pago"');
    $clientes = $this->db->select('id,concat(nombre," ",apellido_paterno," ",apellido_materno) as cliente')->get('usuarios')->result();
    $data['id_usuario_list'] = form_dropdown('id_usuario_list', array_combos_message($clientes, 'id', 'cliente', 'Cliente nuevo'), set_value('id_usuario_list', exist_obj($info_facturacion, 'id_usuario_list')), 'class="form-control busqueda" id="id_usuario_list"');

    if ($id_lavador != 0) {
      $lavador = $this->db->where('lavadorId', $id_lavador)->get('lavadores')->result();
      $data['drop_lavadores'] = form_dropdown('id_lavador', array_combos($lavador, 'lavadorId', 'lavadorNombre', TRUE), $id_lavador, 'class="form-control busqueda" id="id_lavador"');
    }

    $data['proactivo'] = $proactivo;
    $data['agencia'] = $agencia;
    $data['id'] = $id;
    $data['id_lavador'] = $id_lavador;
    $this->blade->render('servicios/nuevo', $data);
  }
  public function getAutoById()
  {
    $auto =  $this->ms->getById($_POST['id_auto'], 'autos');
    echo json_encode(array('auto' => $auto));
  }
  public function getMunicipios()
  {
    $municipios = $this->ms->getMunicipiosByEstado($_POST['id_estado']);
    echo json_encode($municipios);
  }
  public function getSucursales()
  {
    $sucursales = $this->ms->getSucursalesByEstado($_POST['id_estado']);
    echo json_encode($sucursales);
  }
  public function getModelos()
  {
    $modelos = $this->ms->getModelosByMarca($_POST['id_marca']);
    echo json_encode($modelos);
  }
  public function getFechasDisponiblesLavador()
  {
    $fechas = $this->ms->getFechasDisponiblesLavador($_POST['id_lavador']);
    echo json_encode($fechas);
  }
  public function getHorariosDisponibles()
  {
    $horarios = $this->ms->getHorariosDisponibles($_POST['fecha'], isset($_POST['id_lavador']) ? $_POST['id_lavador'] : '', $_POST['id_sucursal']);
    echo json_encode($horarios);
  }
  public function tablero_lavadores()
  {
    $datos['fecha'] = date('Y-m-d');
    $datos['tiempo_aumento'] = 60;
    $datos['valor'] = 11;
    $id_sucursal = 0;
    if ($this->session->userdata('id_rol') != 1) {
      $id_sucursal = $this->session->userdata('id_sucursal');
    }
    $datos['tabla1'] = $this->ms->tablero_lavadores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 0, $id_sucursal); // el último argumento es para saber si hace el cociente en base a 0 o a 1
    $datos['tabla2'] = $this->ms->tablero_lavadores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 1, $id_sucursal); // el último argumento es para saber si hace el cociente en base a 0 o a 1
    $datos['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $this->blade->render('tablero_lavadores', $datos);
  }
  public function tabla_horarios_lavadores()
  {
    $datos['fecha'] = $this->input->post('fecha');
    $datos['tiempo_aumento'] = 60;
    $datos['valor'] = 11;
    $id_sucursal = 0;
    if ($this->session->userdata('id_rol') != 1) {
      $id_sucursal = $this->session->userdata('id_sucursal');
    }
    $datos['tabla1'] = $this->ms->tablero_lavadores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 0, $id_sucursal);
    $datos['tabla2'] = $this->ms->tablero_lavadores($datos['fecha'], $datos['valor'], $datos['tiempo_aumento'], 1, $id_sucursal);
    $this->blade->render('tabla_horarios_lavadores', $datos);
  }
  public function getDatosUsuarios()
  {
    $datos_usuario = $this->ms->getDataUser($_POST['id_usuario']);
    $autos = $this->ms->getVehiculosUser($_POST['id_usuario']);
    echo json_encode(array('usuario' => $datos_usuario, 'autos' => $autos));
  }
  public function getVehiculos()
  {
    $data['autos'] = $this->ms->getVehiculosUser($_POST['id_usuario']);
    $this->blade->render('autos', $data);
  }
  //Obtener vehículo por placa
  public function getAutoByPlacas()
  {
    $auto = $this->ms->getAutoByPlacas($_POST['placas']);
    echo json_encode(array('auto' => $auto));
  }
  public function upload_evidencia($id = '', $formato = '')
  {
    $result = $this->ms->upload_evidencia('file', $formato);
    echo json_encode($result);
  }
  public function upload_delete_evidencia()
  {
    $directorio = $this->input->post('path_evidencia');
    if (file_exists($directorio)) {
      unlink($directorio);
      $result = array('isok' => true);
    } else {
      $result = array('isok' => true);
    }
    $this->db->where('path_evidencia', $directorio)->set('evidencia', '')->set('path_evidencia', '')->update('servicio_lavado');
    echo json_encode($result);
  }
  public function prueba_correo()
  {
    $datos_correo = [
      'nombre' => 'nombre prueba',
      'apellido_paterno' => 'ap prueba',
      'apellido_materno' => 'am prueba',
      'id_servicio' => 'id_servicio',
      'fecha_hora' => 'fecha_hora',
      'lavador' => 'lavador',
      'servicio' => 'servicio',
      'folio_mostrar' => 'folio'
    ];
    $cuerpo = $this->blade->render('confirmacion_cita', array('datos_cita' => $datos_correo, 'id_cita' => 1, 'asunto' => '¡Asignación de cita!'), true);
    // print_r($cuerpo);
    // die();
    enviar_correo('info@sohex.mx', "Confirmación de cita!", $cuerpo, array());
  }
  public function cancelar_servicio()
  {
    $idhorario = $this->ms->getIdHorarioActual($this->input->post('id'));
    $this->db->where('id', $this->input->post('id'))->set('cancelado', 1)->update('servicio_lavado');
    $this->db->where('id', $idhorario)->set('ocupado', 0)->update('horarios_lavadores');
    $historial = [
      'id_usuario' => $this->session->userdata('id'),
      'created_at' => date('Y-m-d H:i:s')
    ];
    $this->db->insert('historial_cancelaciones', $historial);
    echo 1;
    die();
  }
  //rechazar o confirmar cita
  public function confirmar_rechazar()
  {
    if ($this->db->where('id', $this->input->post('id'))->set('confirmado', $this->input->post('valor'))->update('servicio_lavado')) {
      $historial = [
        'id_usuario' => $this->session->userdata('id'),
        'created_at' => date('Y-m-d H:i:s'),
        'tipo' => ($_POST['valor']) ? 'Confirmada' : 'No confirmada'
      ];
      $this->db->insert('historial_confirmaciones', $historial);

      echo 1;
    } else {
      echo 0;
    }
  }
  public function editar_fecha_servicio($idservicio = 0)
  {
    $idservicio = decrypt($idservicio);
    if (!PermisoAccion('editar_fecha_servicio')) {
      redirect('login/');
    }
    $info = $this->ms->getById($idservicio, 'servicio_lavado');
    $info_auto = $this->ms->getById($info->id_auto, 'autos');
    $data['horario'] = $this->ms->getInfoHorario($info->id_horario);
    $horarios_lavador = $this->ms->getHorariosByLavador($data['horario']->fecha, $info->id_lavador, $data['horario']->id);


    $data['horario_actual'] = $info->id_horario;
    $data['id_servicio'] = $idservicio;
    $data['id_auto'] = $info->id_auto;
    $data['id_sucursal'] = $info->id_sucursal;
    $data['folio_mostrar'] = $info->folio_mostrar;
    $data['id_usuario'] = $info->id_usuario;
    $data['id_servicio_lavado'] = $info->id_servicio;
    $lavadores = $this->db->where('id_sucursal', $info->id_sucursal)->get('lavadores')->result();
    $data['kilometraje'] = form_input('kilometraje', set_value('kilometraje', exist_obj($info_auto, 'kilometraje')), 'class="form-control numeric" id="kilometraje" maxlength="10"');
    $data['placas'] = form_input('placas', set_value('placas', exist_obj($info_auto, 'placas')), 'class="form-control numeric" id="placas"');
    $data['numero_serie'] = form_input('numero_serie', set_value('numero_serie', exist_obj($info_auto, 'numero_serie')), 'class="form-control" id="numero_serie" maxlength="17"');
    $data['id_lavador'] = form_dropdown('id_lavador', array_combos($lavadores, 'lavadorId', 'lavadorNombre', TRUE), set_value('id_lavador', exist_obj($info, 'id_lavador')), 'class="form-control busqueda" id="id_lavador"');
    $data['fecha'] = form_dropdown('fecha', array_combos($this->ms->getFechasByLavadorEdit($info->id_lavador), 'fecha', 'fecha', TRUE), $data['horario']->fecha, 'class="form-control" id="fecha"');

    $data['id_horario'] = form_dropdown('id_horario', array_combos($horarios_lavador, 'id', 'hora', TRUE), set_value('id_horario', exist_obj($info, 'id_horario')), 'class="form-control busqueda" id="id_horario"');
    $this->blade->render('editar_fecha_servicio', $data);
  }
  public function editarHorario()
  {
    if ($_POST['fecha'] < date('Y-m-d')) {
      echo -3;
      exit();
    }

    $this->form_validation->set_rules('id_lavador', 'lavador', 'trim|required');
    $this->form_validation->set_rules('id_horario', 'horario', 'trim|required');
    $this->form_validation->set_rules('kilometraje', 'kilometraje', 'trim|numeric');
    $this->form_validation->set_rules('numero_serie', 'numero serie', 'trim|exact_length[17]');
    if ($this->form_validation->run()) {
      // Actualizar 
      $this->db->where('id', $_POST['horario_actual'])->set('ocupado', 0)->update('horarios_lavadores');
      $this->db->where('id', $_POST['id_horario'])->set('ocupado', 1)->update('horarios_lavadores');
      $this->db->where('id', $_POST['id_servicio'])->set('id_horario', $_POST['id_horario'])->set('id_lavador', $_POST['id_lavador'])->set('fecha_programacion', $_POST['fecha'])->update('servicio_lavado');
      $this->db->where('id', $_POST['id_auto'])->set('numero_serie', $_POST['numero_serie'])->set('kilometraje', $_POST['kilometraje'])->set('placas', $_POST['placas'])->update('autos');
      $historial = [
        'id_usuario' => $this->session->userdata('id'),
        'id_horario_anterior' => $_POST['horario_actual'],
        'id_horario_actual' => $_POST['id_horario'],
        'created_at' => date('Y-m-d H:i:s')
      ];
      $horario_lavador = $this->Mgeneral->get_row('id', $_POST['id_horario'], 'horarios_lavadores');
      $mensaje = 'SEHOX AUTOLAVADO, se asignó un nuevo servicio: ' . date_eng2esp_1($horario_lavador->fecha) . ' ' . $horario_lavador->hora . ' Folio:' . $_POST['folio_mostrar'];;
      $tel_lavador = $this->ms->getTelLavador($_POST['id_lavador']);
      sms_general($tel_lavador, $mensaje);
      //Enviar correo
      $info_usuario = $this->ms->getById($_POST['id_usuario'], 'usuarios');
      $datos_correo = [
        'nombre' => $info_usuario->nombre,
        'apellido_paterno' => $info_usuario->apellido_paterno,
        'apellido_materno' => $info_usuario->apellido_materno,
        'id_servicio' => $_POST['id_servicio_lavado'],
        'fecha_hora' => date_eng2esp_1($horario_lavador->fecha) . ' ' . $horario_lavador->hora,
        'lavador' => $this->ms->getLavadorById($horario_lavador->id_lavador)->lavadorNombre,
        'servicio' => $this->Mgeneral->get_row('servicioId', $_POST['id_servicio_lavado'], 'servicios')->servicioNombre,
        'folio_mostrar' => $_POST['folio_mostrar']
      ];
      $cuerpo = $this->blade->render('confirmacion_cita', array('datos_cita' => $datos_correo, 'id_servicio' => $datos_correo['id_servicio'], 'asunto' => '¡Asignación de cita!'), true);
      enviar_correo($info_usuario->email, "Reasignación de cita!", $cuerpo, array());

      $this->db->insert('historial_ediciones_horarios', $historial);
      echo 1;
      exit();
    } else {
      $errors = array(
        //Datos del vehículo
        'id_lavador' => form_error('id_lavador'),
        'id_horario' => form_error('id_horario'),
        'kilometraje' => form_error('kilometraje'),
        'numero_serie' => form_error('numero_serie')
      );
      echo json_encode($errors);
      exit();
    }
  }
  //Obtener los datos de la agencia
  public function getDataByAgencia()
  {
    $info = $this->ms->getDataAgencia($_POST['id_agencia']);
    echo json_encode(array('exito' => true, 'data' => $info));
  }
  //Obtener las fechas por sucursal
  public function getFechasBySucursal()
  {
    if (isset($_POST['id_lavador']) && $_POST['id_lavador'] != 0) {
      $fechas = $this->ms->getFechasByLavador($_POST['id_lavador']);
    } else {
      $fechas = $this->ms->getFechasLavadores('', $_POST['id_sucursal']);
    }
    echo json_encode($fechas);
  }
  public function linea_tiempo($id = '')
  {
    $id = decrypt($id);
    $data['info'] = $this->db->order_by('id_transicion', 'asc')->where('id', $id)->get('v_kpi_lavador')->result();
    $this->blade->render('linea_tiempo', $data);
  }
  public function evidencia($id_servicio = '')
  {
    $id_servicio = $_POST['id_servicio'];
    $this->load->library('curl');
    $request = $this->curl->curlPost('https://hexadms.com/xehos/index.php/inventario/api/recuperar_evidencia', [
      'id_servicio' => $id_servicio
    ], true);

    $request_decoded = json_decode($request);
    $data['evidencia'] = isset($request_decoded) && count($request_decoded) > 0 ? $request_decoded : [];
    $this->blade->render('detalle_evidencia', $data);
  }
}
