<?php
class M_Recomendadores extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->order_by($campo, "DESC")->get($tabla)->result();
        return $result;
  	}

  	public function get_table($table){
		$data = $this->db->get($table)->result();
		return $data;
	}

    public function get_table_all($table){
        $data = $this->db->get($table)->result();
        return $data;
    }

  	public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function update_table_row($table,$data,$id_table,$id){
    		$result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
  	}

    public function recuperar_password($id_usuario=''){
        $q = $this->db->where('id',$id_usuario)->select('password')->get('recomendadores');
        if($q->num_rows()==1){
            $retorno = $q->row()->password;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function existe_nombre_usuario($usuario){
        $result = $this->db->where('usuario',$usuario)->where('activo','1')->select('id')->order_by("id", "DESC")->get('recomendadores');
        if($result->num_rows()==1){
            $retorno = $result->row()->id;
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        $result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
        return $result;
    }

    public function usos_cupon($id_cupon='')
    {
        $q = $this->db->where('id_cupon',$id_cupon)->select('id')->get('cupon_historico');
        return $q->num_rows();
    }

    public function usos_cupon_usuario($id_cupon = '',$id_usuario = '')
    {
        $q = $this->db->where('id_cupon',$id_cupon)
        ->where('id_usuario',$id_usuario)
        ->select('id')
        ->limit(1)
        ->get('cupon_historico');

        if($q->num_rows()==1){
            $retorno = '1';
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function nombre_banco($id_banco=''){
        $q = $this->db->where('id',$id_banco)->select('razon_social')->get('cat_bancos');
        if($q->num_rows()==1){
            $retorno = $q->row()->razon_social;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function clave_sat_banco($id_banco=''){
        $q = $this->db->where('id',$id_banco)->select('clave_sat')->get('cat_bancos');
        if($q->num_rows()==1){
            $retorno = $q->row()->clave_sat;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function telefono_contacto($id_contacto=''){
        $q = $this->db->where('id',$id_contacto)->select('id,telefono')->get('recomendadores_agenda');
        if($q->num_rows()==1){
            $retorno = $q->row()->telefono;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function historico_envio($id_usuario='')
    {
        $result = $this->db->where('h_cupones.id_usuario',$id_usuario)
            ->join('cupones AS cupones','cupones.id = h_cupones.id_cupon')
            ->select('cupones.id AS id_cupon,cupones.cupon,cupones.servicios,cupones.descripcion,h_cupones.telefono,h_cupones.fecha_envio')
            ->order_by("h_cupones.id", "DESC")
            ->get('recomendadores_envio_cupones AS h_cupones')
            ->result();
        return $result;
    }

    public function nombre_servicio($id=''){
        $q = $this->db->where('servicioId',$id)->select('servicioNombre')->get('servicios');
        if($q->num_rows()==1){
            $retorno = $q->row()->servicioNombre;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function nombre_usuario($id=''){ 
        $q = $this->db->where('id',$id)->select('nombre')->get('recomendadores');
        if($q->num_rows()==1){
            $retorno = $q->row()->nombre;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function datos_generales($id_usuario = ''){
        $result = $this->db->where('id',$id_usuario)
            ->where('nombre !=','')
            ->where('apellido_paterno !=','')
            ->where('apellido_materno !=','')
            ->where('celular !=','')
            ->where('correo !=','')
            ->where('rfc !=','')
            ->where('calle !=','')
            ->where('colonia !=','')
            ->where('municipio !=','')
            ->where('estado !=','')
            ->select('id')
            ->get('recomendadores');

        if($result->num_rows()==1){
            $retorno = '1';
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function datos_banco($id_usuario = ''){
        $result = $this->db->where('id',$id_usuario)
            ->where('banco !=','')
            ->where('n_cuenta !=','')
            ->where('clave_bancaria !=','')
            ->select('id')
            ->get('recomendadores');

        if($result->num_rows()==1){
            $retorno = '1';
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function listado_bonificaciones($id_recomendador='')
    {
        $result = $this->db->where('reco_pago.id_recomendador',$id_recomendador)
            ->join('cupones AS cupones','cupones.id = reco_pago.id_cupon')
            ->select('reco_pago.monto_pagado, reco_pago.fecha_pago, cupones.cupon, cupones.beneficio')
            ->order_by("reco_pago.id", "DESC")
            ->get('recomendadores_pagos AS reco_pago')
            ->result();
        return $result;
    }

    public function telefono_en_uso($campo = "",$valor = "",$tabla = ""){
        $q = $this->db->where($campo,$valor)
            ->select($campo)
            ->get($tabla);

        if($q->num_rows()==1){
            $retorno = '1';
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function sucursales_estado($estado='')
    {
        $result = $this->db->where("id_estado",$estado)
            ->select("id,sucursal")
            ->get("sucursales")
            ->result();

        return $result;
    }

    public function nombre_estado($id = ""){
        $q = $this->db->where("id",$id)
            ->select("estado")
            ->get("estados");

        if($q->num_rows()==1){
            $retorno = $q->row()->estado;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function nombre_sucursal($id = ""){
        $q = $this->db->where("id",$id)
            ->select("sucursal")
            ->get("sucursales");

        if($q->num_rows()==1){
            $retorno = $q->row()->sucursal;
        }else{
            $retorno = '';
        }
        return $retorno;
    }


}