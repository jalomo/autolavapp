@layout('layout_correo')
@section('contenido')
    <tr style="border-collapse:collapse;">
        <td style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:left top;background-color:#FAFAFA;"
            bgcolor="#fafafa" align="left">
            <table width="100%" cellspacing="0" cellpadding="0"
                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                    <td width="540" valign="top" align="center" style="padding:0;Margin:0;">
                        <table width="100%" cellspacing="0" cellpadding="0" role="presentation"
                            style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr class="">
                                <td>
                                    <p>
                                        <strong>Estimado Cliente:</strong>
                                    </p>
                                    <p style="text-align: justify">
                                        Xehos AutoLavado<sup style="font-size:12px">&reg;</sup> Trabaja día a día para brindarle un mejor servicio a clientes
                                        distinguidos como usted.!
                                    </p>
                                    <p style="text-align: justify">
                                        Por lo que le recomendamos realizar, el <strong>SERVICIO DE LAVADO</strong>, el cual
                                        le permitirá disfrutar de un vehículo limpio y sanitizado para su salud.!
                                    </p>
                                    <p style="text-align: justify">
                                        Por tal motivo queremos ofrecerle y brindarle atención personalizada cumpliendo y
                                        dando seguimiento al servicio por parte nuestra.
                                    </p>
                                    <p style="text-align: justify">
                                        Si usted nos permite, un representante de Xehos AutoLavado®️ Se comunicara para
                                        agendarle una cita, y realizar el servicio de lavado en la comodidad de su Hogar u
                                        Oficina.!
                                    </p>
                                    <p style="text-align: justify">
                                        Sacando el máximo de provecho a su tiempo, en Xehos AutoLavado®️ Nos dedicamos a
                                        tener su vehículo impecable.
                                    </p>
                                    <p>
                                        <strong>Atención a Clientes</strong><br>
                                        <a target="_blank" href="https://www.xehos.com">www.xehos.com</a><br>
                                        <span>Tels.: <a href="tel:3338352058">33 3835 2058</a></span><br>
                                        <span>WhatsApp.: 33 2805 6406</span><br>
                                        <span>Lunes a Viernes de 09:00 a 18:00 Horas.</span><br>
                                        <span>Sábado 09:00 a 14:00 Horas</span>
                                    </p>
                                    <p>
                                        <strong>Horarios de Servicio</strong><br>
                                        <span>Lunes a Domingo de 08:00 a 18:00 Horas.</span><br>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@endsection
