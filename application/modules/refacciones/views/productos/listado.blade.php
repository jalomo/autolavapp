@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">Productos</h1>
    <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
    <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
    <li class="breadcrumb-item active">{{ ucwords($this->uri->segment(3)) }}</li>
    </ol>
    <div class="row mb-3">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
            <a class="btn btn-primary col-md-12" href="<?php echo base_url('refacciones/productos/crear') ?>"><i class="fa fa-plus"></i>&nbsp;Agregar</a>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('included_js')
<script src="{{ base_url('js/custom/jquery.dataTable.1.10.20.min.js') }}" crossorigin="anonymous"></script>
<script>
    var tabla_stock = $('#tbl_productos').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/productos/listadoStock",
			type: 'GET',
		},
		columns: [{
				title: "#",
				data: 'id',
			},
			{
				title: "No. identificación",
				data: 'no_identificacion',
			},
			{
				title: "Descripcion",
				data: 'descripcion',
			},
			{
				title: "Precio unitario",
				data: 'valor_unitario',
			},
			{
				title: "Unidad",
				data: 'unidad',
            },
			{
				title: "Ubicación",
				data: 'ubicacionProducto',
            },
            {
                title: "-",
                render: function (data, type, {id}) {
                    btn_ficha =  '<button  class="btn btn-primary" onclick="goTo(this)" data-producto_id="' + id + '"><i class="fas fa-list"></i> </button>';
                    btn_editar =  '<button  class="btn btn-warning" onclick="editar(this)" data-producto_id="' + id + '"><i class="fas fa-edit"></i> </button>';
                    return btn_ficha + ' ' + btn_editar;
                }
            }
		
			
		]
	});

    function goTo(_this) {
        window.location.href = PATH + 'refacciones/productos/ficha_tecnica/' + $(_this).data('producto_id');
    }

    function editar(_this) {
        window.location.href = PATH + 'refacciones/productos/editar/' + $(_this).data('producto_id');
    }

    function borrar(id) {
        utils.displayWarningDialog("Desea borrar el registro??", "warning", function(data) {
            if (data.value) {
                ajax.delete(`/api/productos/${id}`, null, function(response, headers) {
                    if (headers.status != 204) {
                        return utils.displayWarningDialog(headers.message)
                    }
                    location.reload(true)
                })

            }
        }, true)
    }
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_almacen").addClass("show");
            $("#refacciones_almacen").addClass("active");
            $("#refacciones_almacen_sub").addClass("show");
            $("#refacciones_almacen_sub").addClass("active");
            $("#almacen_productos").addClass("active");
            $("#M02").addClass("active");
        });
    </script>
@endsection