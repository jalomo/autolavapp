<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Recomendadores extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Cupon', 'consulta', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        if($this->session->userdata('id')){}else{redirect('login/');}
    }


    public function index()
    {
    	$data['titulo'] = "Cupones";
        $data['titulo_dos'] = "Recomendadores";

        $data['listado'] = $this->consulta->get_table('recomendadores');
        
        $this->blade->render('lista_recomendadores',$data);
    }

    public function baja_recomendador()
    {
        $registro = date("Y-m-d H:i:s");
        $contenido["activo"] = "0";
        $contenido["fecha_actualiza"] = $registro;

        $actualizar = $this->consulta->update_table_row('recomendadores',$contenido,'id',$_POST["id_usuario"]);

        echo "OK";
    }

    public function alta_recomendador()
    {
        $registro = date("Y-m-d H:i:s");
        $contenido["activo"] = "1";
        $contenido["fecha_actualiza"] = $registro;

        $actualizar = $this->consulta->update_table_row('recomendadores',$contenido,'id',$_POST["id_usuario"]);

        echo "OK";
    }


    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
}