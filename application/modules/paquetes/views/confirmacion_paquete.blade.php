@layout('layout_correo')
@section('contenido')
    <tr style="border-collapse:collapse;">
        <td style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:left top;background-color:#FAFAFA;"
            bgcolor="#fafafa" align="left">
            <table width="100%" cellspacing="0" cellpadding="0"
                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                    <td width="540" valign="top" align="center" style="padding:0;Margin:0;">
                        <table width="100%" cellspacing="0" cellpadding="0" role="presentation"
                            style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td align="left" style="padding:0;Margin:0;padding-bottom:10px;">
                                    <h4
                                        style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:16px;font-style:normal;font-weight:bold;color:#212121;">
                                        {{ $asunto }}
                                    </h4>
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse;">
                                <td align="left" style="padding:0;Margin:0;padding-bottom:5px;">
                                    <p
                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                                        Estimado
                                        <strong>{{ $datos_correo['nombre'] . ' ' . $datos_correo['apellido_paterno'] . ' ' . $datos_correo['apellido_materno'] }},</strong>
                                    </p>
                                    <p
                                        style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                                        Gracias por adquirir el paquete <strong>"{{ $datos_correo['paquete'] }}"</strong>, en
                                        el cual obtendrás:
                                    </p>
                                    <p>
                                        <strong>{{ $datos_correo['descripcion_paquete'] }}
                                    </p>
                                    <p>
                                        Planeación de los servicios de lavado:
                                    </p>
                                    @foreach ($horarios as $h => $horario)
                                        <hr>
                                        <div>
                                            <p
                                                style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                                                <strong>Folio: </strong> {{ $horario->folio }} <br>
                                                <strong>Fecha y hora: </strong>
                                                {{ date_eng2esp_1($horario->fecha) . ' ' . $horario->hora }}
                                                <br>
                                                <strong>Lavador: </strong> {{ $horario->lavadorNombre }} <br>
                                                <strong>Servicio: </strong> {{ $horario->servicioNombre }} <br>
                                            </p>
                                        </div>
                                    @endforeach
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@endsection
