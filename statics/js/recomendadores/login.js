$("#entrar").on('click', function (e){
    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        var base = $("#sitio").val();
        var paqueteDatos = new FormData(document.getElementById('form_login'));
        $.ajax({
            url: base+"recomendadores/login/iniciar_sesion",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        location.href = base+"index.php/recomendadores/panel/index";
                    } else {
                        $("#formulario_error").text(resp);
                    }

                }else{
                    $("#formulario_error").text(resp);
                }
            //Cierre de success
            },
              error:function(error){
                console.log(error);
            }
        });
    }else{

    }
});

function validar() {
    var retorno = true;
    var campo_a = $("input[name='recordUsername']").val();
    if (campo_a == "") {
        var error_a = $("#recordUsername_error");
        error_a.empty();
        $("#recordUsername_error").css("display","inline-block");
        error_a.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_a = $("#recordUsername_error");
        error_a.empty();
    }

    var campo_b = $("input[name='recordPassword']").val();
    if (campo_b == "") {
        var error_b = $("#recordPassword_error");
        error_b.empty();
        $("#recordPassword_error").css("display","inline-block");
        error_b.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_b = $("#recordPassword_error");
        error_b.empty();
    }

    return retorno;
}