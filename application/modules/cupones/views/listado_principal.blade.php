@layout('layout')
@section('contenido')
	<div class="row">
        <div class="col-sm-10" align="center">
        	<h5>Cupones</h5>
		</div>	
        <div class="col-sm-2">
        	<a class="btn btn-success" href="{{ site_url('cupones/nuevo_cupon/') }}" > 
                Nuevo Cupón
            </a>
		</div>	
	</div>

	<br>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
			            <table id="bootstrap-data-table" class="table table-striped table-bordered">
			                <thead>
			                    <tr>
			                    	<th>#</th>
			                    	<th>Cliente</th>
			                    	<th>Cupón</th>
			                    	<th>Descripción</th>
			                    	<th>Limite Usos</th>
			                    	<th>Fecha de Creación</th>
			                    	<th>Fecha de Expiración</th>
			                    	<th>Estatus</th>
			                    	<th colspan="2">Ver</th>
			                    </tr>
			                </thead>
			                <tbody id="cuerpo">
			                	@if ($listado != NULL)
									@foreach ($listado as $i => $cupon)
										<tr>
											<td style="vertical-align: middle;" align="center">
												<?= $i + 1;?>
											</td>
											<td style="vertical-align: middle;">
												<?= $cupon->usuario ?>
											</td>
											<td style="vertical-align: middle;">
												<?= $cupon->cupon ?>
											</td>
											<td style="vertical-align: middle;">
												<?= $cupon->descripcion ?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?= (($cupon->limite_uso != "0") ? $cupon->limite_uso : "Sin limite") ?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?php 
													$fecha = new DateTime($cupon->fecha_activacion.' 00:00:00');
		        									echo $fecha->format('d-m-Y');
												?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?php 
													if ($cupon->sin_caducidad == "1") {
														echo "Sin Vigencia";
													} else {
														$fecha2 = new DateTime($cupon->fecha_caducidad.' 00:00:00');
		        										echo $fecha2->format('d-m-Y');
													}
												?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?php 
													if ($cupon->activo == "1") {
														if ($cupon->sin_caducidad == "1") {
															echo "Activo";
														}elseif ($cupon->fecha_caducidad >= date("Y-m-d")) {
															echo "Activo";
														} else {
															echo "Caducado";
														}
														
													} else {
														echo "Inactivo";
													}
												?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<a onclick="revisar_datos(<?= $cupon->id ?>)" title="Editar"> 
													<i class="fa fa-edit"></i>
			                                    </a>
											</td>
											<td style="vertical-align: middle;" align="center">
												<a onclick="revisar_datos2(<?= $cupon->id ?>)" title="Reporte de Comisones"> 
													<i class="fa fa-list-ul" aria-hidden="true"></i>
			                                    </a>
											</td>
										</tr>
									@endforeach
			                	@else
			                		<tr>
			                			<td colspan="9" align="center">
			                				Sin cupones
			                			</td>
			                		</tr>
								@endif
			                </tbody>
			            </table>
		            </div>
		        </div>
		    </div>
		</div>
    </div><!-- .animated -->
@endsection
@section('included_js')
	@include('main/scripts_dt')
	<script>
    	function revisar_datos(indice) {
    		var url_sis = "<?php echo base_url(); ?>index.php/cupones/redireccionar/"+indice;
	        $.ajax({
	            url: url_sis,
	            method: 'get',
	            data: {
	                
	            },
	            success:function(resp){
	              //console.log(resp);
	                if (resp.indexOf("handler			</p>")<1) {
	                    location.href = "<?php echo base_url(); ?>"+"index.php/cupones/editar_cupon/"+resp;
	                }
	            //Cierre de success
	            },
	            error:function(error){
	                console.log(error);
	            //Cierre del error
	            }
	        //Cierre del ajax
	        });
    	}

    	function revisar_datos2(indice) {
    		var url_sis = "<?php echo base_url(); ?>index.php/cupones/redireccionar/"+indice;
	        $.ajax({
	            url: url_sis,
	            method: 'get',
	            data: {
	                
	            },
	            success:function(resp){
	              //console.log(resp);
	                if (resp.indexOf("handler			</p>")<1) {
	                    location.href = "<?php echo base_url(); ?>"+"index.php/cupones/reporte_recomendador/"+resp;
	                }
	            //Cierre de success
	            },
	            error:function(error){
	                console.log(error);
	            //Cierre del error
	            }
	        //Cierre del ajax
	        });
    	}

    </script>
@endsection