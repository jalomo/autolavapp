@layout('layout')
@section('contenido')
    <form action="" method="post" novalidate="novalidate" id="alta_usuario">
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="cc-exp" class="control-label mb-1">Servicio</label>
                    <input id="servicioNombre" name="servicioNombre" type="text" class="form-control cc-exp form-control-sm"
                        value="" placeholder="Servicio" autocomplete="cc-exp">
                    <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="cc-exp" class="control-label mb-1">Puntos</label>
                    <input id="servicioPuntos" name="servicioPuntos" type="text" class="form-control cc-exp form-control-sm"
                        value="" placeholder="Puntos" autocomplete="cc-exp">
                    <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                </div>
            </div>
            <div class="col-4">
                <div class="control-group">
                    <label class="control-label" for="basicinput">Imagen</label>
                    <div class="controls">
                        <input type="file" id="image" name="image" placeholder="" class="span8" required="">
                        <!--span class="help-inline">500px X 500px</span-->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                    <select name="id_sucursal" id="id_sucursal" class="form-control">
                        <option value="">-- Selecciona sucursal --</option>
                        @foreach ($sucursales as $s => $sucursal)
                            <option value="{{ $sucursal->id }}">{{ $sucursal->sucursal }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div align="right">
            <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                <i class="fa fa-edit fa-lg"></i>&nbsp;
                <span id="payment-button-amount">Guardar</span>
                <span id="payment-button-sending" style="display:none;">Sending…</span>
            </button>
        </div>
    </form>
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Imagen</th>
                                        <th>Nombre</th>
                                        <th>Puntos</th>
                                        <th>Sucursal</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (is_array($rows)): ?>
                                    <?php foreach ($rows as $row): ?>
                                    <tr>
                                        <td><img height="50" src="<?php
                                        echo base_url();
                                        echo $row->servicioImagen;
                                        ?>"></td>
                                        <td><?php echo $row->servicioNombre; ?></td>
                                        <td><?php echo $row->servicioPuntos; ?></td>
                                        <td><?php echo $row->sucursal; ?></td>
                                        <td></td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script>
        $(document).ready(function() {
            $("#cargando").hide();
            $('#alta_usuario').submit(function(event) {
                event.preventDefault();
                $("#enviar").hide();
                $("#cargando").show();
                var url_sis =
                    "<?php echo base_url(); ?>index.php/servicios/alta_guardar";
                // Get form
                var form = $('#alta_usuario')[0];
                // Create an FormData object
                var data = new FormData(form);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url_sis,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        //  $("#result").text(data);
                        console.log("SUCCESS : ", data);
                        //  $("#btnSubmit").prop("disabled", false);
                        exito_redirect("DATOS GUARDADOS CON EXITO", "success",
                            "<?php echo base_url(); ?>index.php/servicios/alta"
                        );
                        $("#enviar").show();
                        $("#cargando").hide();
                    },
                    error: function(e) {
                        console.log("ERROR : ", e);
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                        $("#enviar").show();
                        $("#cargando").hide();

                    }
                });
            });

        });

    </script>
@endsection
