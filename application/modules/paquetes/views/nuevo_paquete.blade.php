@layout('layout')
@section('included_css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
@endsection
<link href="{{ base_url('statics/css/jquery.fileupload.css') }}" rel="stylesheet">
<style>
    .error p {
        color: red
    }

    html,
    body {
        height: 100%;
        width: 100%;
    }

    #map {
        height: 100%;
        width: 100% margin: 0 15px 30px 130px;
        padding: 0 25px;
    }

    #tab-ubicacion .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 50px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 250px;
        height: 30px;
        margin-top: 20px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    #tab-ubicacion .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    .error {
        color: red;
    }

    .rojo {
        color: red;
    }

</style>
@section('contenido')
    @if (isset($paquetes_incompletos) && $paquetes_incompletos)
    <div class="alert alert-danger text-center" role="alert">
        Actualmente existen paquetes incompletos, es necesario completarlos para continuar con un nuevo registro
      </div>
    @else

        <form id="frm" method="post" novalidate="novalidate" id="nuevo-servicio">
            <input type="hidden" name="token" id="token" value="00001">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <input type="hidden" name="id_usuario" id="id_usuario" value="{{ $id_usuario }}">
            <input type="hidden" name="id_ubicacion" id="id_ubicacion" value="{{ $id_ubicacion }}">
            <input type="hidden" name="id_auto" id="id_auto" value="{{ $id_auto }}">
            <input type="hidden" name="id_horario" id="id_horario" value="{{ $id_horario }}">
            <input type="hidden" name="id_facturacion" id="id_facturacion" value="{{ $id_facturacion }}">
            <input type="hidden" name="id_tipo_auto" id="id_tipo_auto" value="{{ $id_tipo_auto }}">
            <input type="hidden" name="id_lavador_actual" id="id_lavador_actual" value="0">
            <h2>Datos del cliente</h2> <br>
            @if ($id == 0)
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Cliente</label>
                        {{ $id_usuario_list }}
                        <span class="error error_id_usuario_list"></span>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Nombre</label>
                    {{ $nombre }}
                    <span class="error error_nombre"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Apellido paterno</label>
                    {{ $apellido_paterno }}
                    <span class="error error_apellido_paterno"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Apellido materno</label>
                    {{ $apellido_materno }}
                    <span class="error error_apellido_materno"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Correo electrónico</label>
                    {{ $email }}
                    <span class="error error_email"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Teléfono</label>
                    {{ $telefono }}
                    <span class="error error_telefono"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Ciudad</label>
                    {{ $id_sucursal }}
                    <span class="error error_id_sucursal"></span>
                </div>
            </div>
            Datos de facturación <input type="checkbox" id="check_facturacion" name="check_facturacion">
            <br>
            <br>
            <div id="div_facturacion" class="d-none">
                <h2>Datos de facturación</h2>
                <div class="row">
                    <div class="col-sm-4">
                        <label class="control-label mb-1">Referencia</label>
                        {{ $referencia }}
                        <span class="error error_referencia"></span>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label mb-1">Razón social</label>
                        {{ $razon_social }}
                        <span class="error error_razon_social"></span>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label mb-1">Domicilio</label>
                        {{ $domicilio }}
                        <span class="error error_domicilio"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label class="control-label mb-1">Correo electrónico</label>
                        {{ $email_facturacion }}
                        <span class="error error_email_facturacion"></span>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label mb-1">CFDI</label>
                        {{ $id_cfdi }}
                        <span class="error error_id_cfdi"></span>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label mb-1">Método de pago</label>
                        {{ $id_metodo_pago }}
                        <span class="error error_id_metodo_pago"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label class="control-label mb-1">R.F.C</label>
                        {{ $rfc }}
                        <span class="error error_rfc"></span>
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label mb-1">C.P</label>
                        {{ $cp }}
                        <span class="error error_cp"></span>
                    </div>
                </div>
            </div>
            <h2>Registro de vehículo</h2><br>
            ¿Vehículo nuevo? <input type="checkbox" id="check_vehiculo" name="check_vehiculo">
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Placas</label>
                    {{ $placas }}
                    <span class="error error_placas"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Color</label>
                    {{ $id_color }}
                    <span class="error error_id_color"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Marca</label>
                    {{ $id_marca }}
                    <span class="error error_id_marca"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Modelo</label>
                    {{ $id_modelo }}
                    <span class="error error_id_modelo"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Año</label>
                    {{ $id_anio }}
                    <span class="error error_id_anio"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1"># Serie</label>
                    {{ $numero_serie }}
                    <span class="error error_numero_serie"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Kilometraje</label>
                    {{ $kilometraje }}
                    <span class="error error_kilometraje"></span>
                </div>
            </div>
            <h2>Ubicación</h2><br>
            <div class="row">
                <div class="col-sm-12">
                    <input id="pac-input" class="controls" type="text" placeholder="Encuentra tu ubicación">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Selecciona la ubicación del usuario (puedes escribir en el cuadro de texto o arrastrar el
                                icono de ubicación del mapa)</label>
                        </div>
                    </div>
                    <div style="width: 100%;height: 400px" id="map_subir"></div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Calle</label>
                    {{ $calle }}
                    <span class="error error_calle"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1"># Exterior</label>
                    {{ $numero_ext }}
                    <span class="error error_numero_ext"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1"># Interior</label>
                    {{ $numero_int }}
                    <span class="error error_numero_int"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Colonia</label>
                    {{ $colonia }}
                    <span class="error error_colonia"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Estado</label>
                    {{ $id_estado }}
                    <span class="error error_id_estado"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Ciudad</label>
                    {{ $id_sucursal_ubicacion }}
                    <span class="error error_id_sucursal_ubicacion"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Municipio</label>
                    {{ $id_municipio }}
                    <span class="error error_id_municipio"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Latitud</label>
                    {{ $latitud }}
                    <span class="error error_latitud"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Longitud</label>
                    {{ $longitud }}
                    <span class="error error_longitud"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Comentarios</label>
                    {{ $comentarios }}
                </div>
            </div>
            <h3>Selección de paquete</h3><br>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Paquete</label>
                    {{ $id_paquete }}
                    <span class="error error_id_paquete"></span>
                </div>
            </div>
            <div align="right">
                <button type="button" id="guardar" class="btn btn-lg btn-info ">
                    <i class="fa fa-edit fa-lg"></i>&nbsp;
                    <span>Guardar Servicio</span>
                </button>
            </div>
            <br>
            <div id="div_paquete"></div>
        </form>
    @endif
@endsection
@section('included_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var base_url = "{{ base_url() }}";
    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM&callback=initMap&libraries=places"
        async defer></script>
        <script src="statics/js/servicio.js"></script>
    <script>
        var id = "{{ $id }}";
        var id_facturacion = "{{ $id_facturacion }}";
        var latitud = parseFloat('{{ $mapa_latitud }}');
        var longitud = parseFloat('{{ $mapa_longitud }}');
        $(document).ready(function() {
            //$(".busqueda").select2();
            $("#cargando").hide();
            const guardar = () => {
                var url = site_url + '/paquetes/generar_paquetes/0';
                ajaxJson(url, $("#frm").serialize(), "POST", "", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        //Se recorre el json y se coloca el error en la div correspondiente
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    } else {
                        if (result > 0) {
                            ExitoCustom(
                                "Información guardada correctamente, ES NECESARIO ASIGNAR LOS HORARIOS DEL PAQUETE",
                                function() {
                                    window.location.href = site_url +
                                        '/paquetes/generar_paquetes/' + result;
                                });
                        } else if (result == -2) {
                            ErrorCustom('El horario ya fue ocupado, elige otro');
                        } else if (result == -3) {
                            ErrorCustom(
                                'El correo electrónico ya fue registrado, por favor elige otro');
                        }
                    }
                });
            }
            const getTipoAuto = () => {
                if ($("#id_modelo").val() != '') {
                    ajaxJson(site_url + "/paquetes/getTipoAuto", {
                        "id_modelo": $("#id_modelo").val()
                    }, "POST", "", function(result) {
                        $("#id_tipo_auto").val(result)
                    });
                }
            }
            const buscarPaquete = () => {
                if ($("#id_modelo").val() != '') {
                    var url = site_url + "/paquetes/detalle_paquete";
                    ajaxLoad(url, {
                        "id_tipo_auto": $("#id_tipo_auto").val(),
                        "id_paquete": $("#id_paquete").val(),
                        "id_servicio_lavado": $("#id").val(),
                    }, "div_paquete", "POST", function() {

                    });
                } else {
                    ErrorCustom('Es necesario seleccionar el modelo del vehículo');
                }
            }
            const validarHorasSeleccionadas = () => {
                var cantidad = $(".js_hora").length;
                var contador = 0;
                $.each($(".js_hora"), function(key, value) {
                    if ($(value).val() != '' && $(value).data('id_horario') != 0) {
                        contador++;
                    }
                });
                if (cantidad == contador) {
                    ajaxJson(site_url + "/paquetes/actualizar_paquete_completo", {
                        id: $("#id").val(),
                        'completo': 1
                    }, "POST", "", function(result) {
                        ExitoCustom("Ya fueron asignaron todos los horarios al paquete", function() {
                            window.location.href = site_url + '/servicios';
                        });
                    });
                    return true;
                }
                return false;
            }

            function validarHorasIguales(id, hora, fecha) {
                var contador = 0;
                $.each($(".js_hora"), function(key, value) {
                    if ($(value).data('id_horario') != 0 && $(value).data('id') != id && $("#fecha_" + $(
                            value).data('id')).val() == fecha) {
                        contador++;
                    }
                });
                if (contador > 0) {
                    return false;
                }
                return true;
            }

            function validarOrdenFechas(id_servicio, fecha, pos) {
                console.log(id_servicio, pos, fecha);
                if (pos > 1) {
                    var info = {
                        pos,
                        id_servicio,
                        fecha
                    }
                    ajaxJson(site_url + "/paquetes/validar_orden_fechas", info, "POST", "", function(
                        resultado) {
                        console.log("resultado", resultado)
                        if (resultado == -1) {
                            return false;
                        } else {
                            return true
                        }
                    });
                } else {
                    return true;
                }
            }
            $("#guardar").on('click', guardar);
            $("#id_modelo").on("change", getTipoAuto);
            $("body").on("click", '.js_guardar_servicio', function() {
                var url = site_url + '/paquetes/guardarHorario/0';
                var aPos = $(this);
                data = {
                    id: $("#id").val(),
                    id_servicio: $(this).data('id_servicio'),
                    id_servicio_lavado: $(this).data('id_servicio_lavado'),
                    id_horario: $(this).data('id_horario'),
                    pos: $(this).data('pos'),
                    principal: $(this).data('principal'),
                    fecha: $("#fecha_" + $(this).data('pos')).val(),
                    hora: $("#hora_" + $(this).data('pos')).val(),
                    id_paquete: $("#id_paquete").val(),
                    'id_sucursal_ubicacion' : $("#id_sucursal_ubicacion").val()
                }

                if (!data.fecha || !data.hora) {
                    ErrorCustom("Es necesario seleccionar la fecha y hora");
                } else {
                    url = site_url + "/paquetes/guardarHorario";
                    if (data.id_horario != 0) {
                        url = site_url + "/paquetes/actualizarHorario";
                    }
                    if (!validarHorasIguales($(aPos).data('pos'), data.hora, data.fecha)) {
                        ErrorCustom('Ya fue registrado el horario');
                    }
                    // else if (!validarOrdenFechas(data.id, data.fecha, data.pos)) {
                    //     ErrorCustom('La fecha a ingresar debe ser mayor a la anterior');
                    // } 
                    else {
                        ajaxJson(url, data, "POST", "", function(result) {
                            result = JSON.parse(result);
                            if (result.exito) {
                                ExitoCustom("Información guardada correctamente", function() {
                                    //$(aPos).prop('disabled', true);
                                    $(aPos).data('id_servicio_lavado', result
                                        .id_servicio_lavado)
                                    $(aPos).data('id_horario', result.id_horario)
                                    // $("#fecha_" + $(aPos).data('pos')).prop('disabled', true);
                                    // $("#hora_" + $(aPos).data('pos')).prop('disabled', true);
                                    $(aPos).text('Actualizar', true);
                                    $("#hora_" + $(aPos).data('pos')).data('id_horario',
                                        result
                                        .id_horario);
                                    validarHorasSeleccionadas();
                                });
                            } else {
                                ErrorCustom(result.mensaje);
                            }
                        });
                    }
                }
            });
            $("body").on("change", ".js_select_hour", function() {
                var url = site_url + "/servicios/getHorariosDisponibles";
                var id = $(this).data('id');
                $("#hora_" + id).empty();
                $("#hora_" + id).append("<option value=''>-- Selecciona --</option>");
                $("#hora_" + id).attr("disabled", true);
                fecha = $(this).val();
                if (fecha != '') {
                    ajaxJson(url, {
                        "fecha": fecha,
                        "id_sucursal": $("#id_sucursal_ubicacion").val()
                    }, "POST", "", function(result) {
                        if (result.length != 0) {
                            $("#hora_" + id).empty();
                            $("#hora_" + id).removeAttr("disabled");
                            result = JSON.parse(result);
                            $("#hora_" + id).append(
                                "<option value=''>-- Selecciona --</option>");
                            $.each(result, function(i, item) {
                                $("#hora_" + id).append("<option value= '" + result[
                                        i]
                                    .hora + "'>" +
                                    result[i].hora +
                                    "</option>");
                            });
                        } else {
                            $("#hora_" + id).empty();
                            $("#hora_" + id).append(
                                "<option value='0'>No se encontraron datos</option>");
                        }
                    });
                } else {
                    $("#hora_" + id).empty();
                    $("#hora_" + id).append("<option value=''>-- Selecciona --</option>");
                    $("#hora_" + id).attr("disabled", true);
                }
            });
            if (id != 0) {
                $("#id_paquete").on('change', buscarPaquete);
                $("#id_paquete").trigger('change');
            }
            $("body").on('click', '.select-auto', function(e) {
                e.preventDefault();
                ajaxJson(site_url + "/servicios/getAutoById", {
                    "id_auto": $(this).data('id')
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    if (result.auto.length != 0) {
                        $.each(result.auto, function(i, item) {
                            if (i != 'id') {
                                $("#" + i).val(item);
                                $("#" + i).prop('readonly', true);
                            }
                        });
                        $("#id_marca").trigger('change');
                        $("#id_modelo").val(result.auto.id_modelo)
                        $("#id_auto").val(result.auto.id)
                        $(".modalVehiculos").modal('hide')
                        $("#id_modelo").trigger('change')
                    }
                });

            });

            if (id != 0 && id_facturacion != 0) {
                $("#check_facturacion").prop('checked', true);
                $("#div_facturacion").removeClass('d-none');
            }
            $("#guardar-completo").on('click', function() {
                if (!validarHorasSeleccionadas()) {
                    ErrorCustom(
                        'Para completar el servicio es necesario asignar todas las horas del paquete');
                } else {
                    ExitoCustom('El paquete fue completado correctamente')
                }
            })

        });
        //mapa
        if (id == 0) {
            var zoom = 5;
        } else {
            zoom = 15;
        }

        function initMap() {
            map = new google.maps.Map(document.getElementById('map_subir'), {
                center: {
                    lat: latitud,
                    lng: longitud
                },
                zoom: zoom
            });
            infowindow = new google.maps.InfoWindow({
                map: map
            });
            input = (document.getElementById('pac-input'));
            var types = document.getElementById('type-selector');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);
            //inicializar el autocomplete
            autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);
            //---------------------------------
            // Try HTML5 geolocation.
            //si el id != 0 va poner la ubicación que trae
            if (id == 0) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        latLng = new google.maps.LatLng(position.coords.latitude, position.coords
                            .longitude);
                        inicializar_marcador(latLng);
                        infowindow.setPosition(pos);
                        infowindow.setContent('Ubicación encontrada');
                        map.setCenter(pos);
                    }, function() {
                        handleLocationError(true, infowindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    var pos = {
                        lat: 19.245657599999998,
                        lng: -103.76368649999999
                    };
                    latLng = new google.maps.LatLng(parseFloat(pos.lat), parseFloat(pos.lng));
                    inicializar_marcador(latLng);
                    infowindow.setPosition(pos);
                    infowindow.setContent('Ubicación encontrada');
                    map.setCenter(pos);

                    handleLocationError(false, infowindow, map.getCenter());

                }

            } else {
                latLng = new google.maps.LatLng(latitud, longitud);
                inicializar_marcador(latLng);
            }

        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            latLng = new google.maps.LatLng(latitud, longitud);
            inicializar_marcador(latLng);
            infowindow.setPosition(pos);
            infowindow.setContent(browserHasGeolocation ?
                'La geolocalización ha fallado.' :
                'Tu navegador no permite la geolocalización');
        }

        function inicializar_marcador(latLng = '') {
            marker = new google.maps.Marker({
                position: latLng,
                map: map,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });
            inicializar_autocomplete();
            openInfoWindow(marker);
            google.maps.event.addListener(marker, "dragend", function(event) {
                openInfoWindow(marker);
                //latitud
                //longitud
            }); //end addListener
        }

        function inicializar_autocomplete() {
            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Intenta de nuevo la búsqueda.");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17); // Why 17? Because it looks good.
                }
                marker.setIcon( /** @type {google.maps.Icon} */ ({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));

                //marker.setPosition(place.geometry.location);
                inicializar_marcador(place.geometry.location);
                marker.setVisible(true);
                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }
                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);
            });
        }

        function openInfoWindow(marker) {
            var markerLatLng = marker.getPosition();
            infowindow.setContent([
                'Haz click y arrastrame para actualizar la posición.'
            ].join(''));
            $("#latitud").val(markerLatLng.lat());
            $("#longitud").val(markerLatLng.lng());
            getAdress(markerLatLng.lat(), markerLatLng.lng())
            infowindow.open(map, marker);
        }

        function getAdress(lat, lng) {
            var url =
                `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM`;
            ajaxJson(url, {}, "GET", "", function(result) {
                if (result) {
                    result = result.results[0].address_components
                    $("#numero_ext").val(result[0].long_name);
                    $("#calle").val(result[1].long_name);
                    $("#colonia").val(result[2].long_name);
                }

            });
        }

    </script>
@endsection
