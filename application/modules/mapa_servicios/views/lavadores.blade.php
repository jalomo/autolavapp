
@layout('layout')
@section('contenido')





<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM&callback=initMap&libraries=&v=weekly"
      defer
    ></script>



<style>
#vertices {
  height: 100px;
  width: 500px;
}</style>
<div class="">
  <div class="row">
    <div class="col-3">
            <!--select class="form-control">
                <?php foreach($lavadores as $rows):?>
                    <option value="<?php echo $rows->lavadorId;?>"><?php echo $rows->lavadorNombre;?></option>
                <?php endforeach;?>
            
             </select-->

        <!--
        Inicio
        <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
          <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
          <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
          </div>
        </div>

        Fin
        <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
          <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
          <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
          </div>
        </div>
          
        <button >Buscar</button>
        -->
    </div>
  </div>
</div>


<div class="">
  <div class="row">
    <div class="col-3">
         

        <form action="<?php echo base_url()?>index.php/mapa_servicios/lavadores/" method="get">
          <div class="input-group date" id="" data-target-input="nearest">
          Folio:
          <br/>
            <input type="text" class="form-control " name="folio" value="<?php echo $folio;?>"/>
          </div>
            
          <button >Buscar</button>
          <br/><br/><br/>
        </form>
    </div>
  </div>
</div>


<div id="map" style="width:600px; height:600px;"></div>





@endsection
@section('included_js')
    @include('main/scripts_dt')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

  
    <script>

$( function() {
    $( "#datepicker" ).datepicker();

    $('#datetimepicker3').datetimepicker({
          pickDate: false
        });
  
  $('#datetimepicker4').datetimepicker({
          pickDate: false
        });
  });
// The following example creates complex markers to indicate beaches near
// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
// to the base of the flagpole.
/*function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 10,
    center: { lat: 20.6703146, lng: -103.3597487 },
  });
  setMarkers(map);
}
*/
function initMap() {
         map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: 20.5639985, lng: -103.4154582},
          mapTypeId: 'terrain'
        });

        var flightPlanCoordinates = [
        <?php $latitud_inicio = "";?>
        <?php $longitud_inicio = "";?>
        <?php $latitud_fin = "";?>
        <?php $longitud_fin = "";?>
        <?php $aux1 =1;?>
        <?php if(is_array($coordenadas)):?>
          <?php $ultimo = count($coordenadas);?>
          <?php foreach($coordenadas as $row):?>
            <?php if($aux1 == 1){$latitud_inicio =$row->latitud; $longitud_inicio = $row->longitud;}?>
            <?php if($aux1 == $ultimo){$latitud_fin = $row->latitud; $longitud_fin = $row->longitud;}?>
            {lat: <?php echo $row->latitud;?>, lng: <?php echo $row->longitud;?>},
            <?php $aux1++;?>
            <?php endforeach;?>
          
          ];
          
          var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
          });
        <?php endif;?>


        /*var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
        var beachMarker = new google.maps.Marker({
          position: {lat: <?php echo $latitud_inicio;?>, lng: <?php echo $longitud_inicio;?>},
          map: map
          //icon: image
        });
        */

        map.setZoom(13);      // This will trigger a zoom_changed on the map
        map.setCenter(new google.maps.LatLng(<?php echo $latitud_fin;?>,<?php echo $longitud_fin;?>));
        map.setMapTypeId(google.maps.MapTypeId.ROADMAP);


       


        
        flightPath.setMap(map);
      }

// Data for the markers consisting of a name, a LatLng and a zIndex for the
// order in which these markers should display on top of each other.


function setMarkers(map) {
  // Adds markers to the map.
  // Marker sizes are expressed as a Size of X,Y where the origin of the image
  // (0,0) is located in the top left of the image.
  // Origins, anchor positions and coordinates of the marker increase in the X
  // direction to the right and in the Y direction down.
  const image = {
    url:
      "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
    // This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(20, 32),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(0, 32),
  };
  // Shapes define the clickable region of the icon. The type defines an HTML
  // <area> element 'poly' which traces out a polygon as a series of X,Y points.
  // The final coordinate closes the poly by connecting to the first coordinate.
  const shape = {
    coords: [1, 1, 1, 20, 18, 20, 18, 1],
    type: "poly",
  };
/*
  for (let i = 0; i < beaches.length; i++) {
    const beach = beaches[i];
    new google.maps.Marker({
      position: { lat: beach[1], lng: beach[2] },
      map,
      icon: image,
      shape: shape,
      title: beach[0],
      zIndex: beach[3],
    });
  }*/
}
</script>

    @endsection
