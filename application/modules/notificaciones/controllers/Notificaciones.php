<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Notificaciones extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta(){
      $data['titulo'] = "Notificaciones" ;
      $data['titulo_dos'] = "Envío de notificaciones" ;
      $data['rows'] = $this->db->where('id_sucursal',1)->get('notificaciones')->result();
      $this->blade->render('notificaciones/alta', $data);
    }

    public function alta_guardar(){
      $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['image']['name']);
      $path_to_save = 'statics/notificaciones/';
      if(!file_exists($path_to_save)){
        mkdir($path_to_save, 0777, true);
      }
      move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save.$name);
      $data['imagen'] = $path_to_save.$name;
      $data['titulo'] = $this->input->post('titulo');
      $data['texto'] = $this->input->post('texto');
      $data['id_sucursal'] = 1;
     $this->Mgeneral->save_register('notificaciones', $data);
    }

}
