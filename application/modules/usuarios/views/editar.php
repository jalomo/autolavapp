<script>

$(document).ready(function(){
$("#cargando").hide();
$('#alta_usuario').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();
  var url_sis ="<?php echo base_url()?>index.php/usuarios/add_puntos/<?php echo $row->usuarioID?>";

  // Get form
        var form = $('#alta_usuario')[0];

    // Create an FormData object
        var data = new FormData(form);

  $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: url_sis,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
              //  $("#result").text(data);
                console.log("SUCCESS : ", data);
              //  $("#btnSubmit").prop("disabled", false);
                exito_redirect("DATOS GUARDADOS CON EXITO","success","<?php echo base_url()?>index.php/usuarios/lista");
                $("#enviar").show();
                $("#cargando").hide();

            },
            error: function (e) {
                //$("#result").text(e.responseText);
                console.log("ERROR : ", e);
                //$("#btnSubmit").prop("disabled", false);
                exito("<h3>ERROR intente de nuevo<h3/> <br/>"+aux,"danger");
                $("#enviar").show();
                $("#cargando").hide();

            }
        });
});

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Alta de servicios</strong>
        </div>
        <div class="card-body">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="" method="post" novalidate="novalidate" id="alta_usuario">



                      <div class="row">
                          <div class="col-3">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Nombre:</label>
                                  <label><?php echo $row->usuarioNombre?></label>
                              </div>
                          </div>
                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Email:</label>
                                  <label><?php echo $row->usuarioEmail?></label>
                              </div>
                          </div>

                          <div class="col-2">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Puntos</label>
                                  <input id="usuarioPuntos" name="usuarioPuntos" type="text" class="form-control cc-exp form-control-sm" value="<?php echo $row->usuarioPuntos;?>" placeholder="Puntos" autocomplete="cc-exp">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          
                      </div>





                      <div align="right">
                          <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                              <i class="fa fa-edit fa-lg"></i>&nbsp;
                              <span id="payment-button-amount">Guardar</span>
                              <span id="payment-button-sending" style="display:none;">Sending…</span>
                          </button>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->