<header id="header" class="header">
    <div class="header-menu">
        <div class="col-sm-7">
            <a style="background:#434a5c" id="menuToggle" class="menutoggle pull-left noti-header"><i class="fa fa fa-tasks"></i></a>
            <div class="header-left">
                @if($this->session->userdata('id_rol')!=4)
                <div class="dropdown for-notification">
                    <a href="<?php echo base_url('sistema/notificaciones') ?>" class="btn btn-secondary dropdown-toggle" id="notification">
                        <i class="fa fa-bell fa-2x noti-header"></i>
                        <span id="total_notify" class="count bg-danger">-</span>
                    </a>

                </div>
                <div class="dropdown for-message">
                    <a href="<?php echo base_url('sistema/contactos') ?>" class="btn btn-secondary dropdown-toggle" id="message">
                        <i class="fa fa-users fa-2x noti-header"></i>
                    </a>
                </div>
                @endif
            </div>
        </div>
        <div class="col-sm-5">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user fa-2x noti-header"></i>
                    <!-- <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar"> -->
                </a>
                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>

                    <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>

                    <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>

                    <a class="nav-link" href="#"><i class="fa fa-power -off"></i>Logout</a>
                </div>
            </div>
        </div>
    </div>
</header>