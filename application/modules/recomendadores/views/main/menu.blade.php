<style>
	#total_notify {
		border-radius: 40px;
		font-size: 18px;
		padding-left: 5px;
		padding-right: 5px;
		margin-top: -10px !important;
		position: absolute;
	}
</style>
<div class="menu-back">
	<div class="menuborder">
		<div class=" menu-items">
			<div>
				<a href="<?= base_url() . 'index.php/recomendadores/panel/index' ?>" class="btn btn-menu-cabecera" title="DATOS PERSONALES">
					<i class="fa fa-user-o" aria-hidden="true"></i>
				</a>

				<!--<a href="<?= base_url() . 'index.php/recomendadores/panel/datos_banco' ?>" class="btn btn-menu-cabecera" title="DATOS BANCARIOS">
		        <i class="fa fa-credit-card" aria-hidden="true"></i>
		    </a>-->

				<a href="<?= base_url() . 'index.php/recomendadores/panel/cupones' ?>" class="btn btn-menu-cabecera" title="MIS CUPONES">
					<i class="fa fa-ticket" aria-hidden="true"></i>
				</a>

				<!--<a href="<?= base_url() . 'index.php/recomendadores/panel/agenda' ?>" class="btn btn-menu-cabecera" title="MI AGENDA">
		        <i class="fa fa-address-book-o" aria-hidden="true"></i>
		    </a>-->

				<a href="<?= base_url() . 'index.php/recomendadores/panel/cupones_enviados' ?>" class="btn btn-menu-cabecera" title="MIS CUPONES ENVIADOS">
					<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
				</a>

				<!--<a href="<?= base_url() . 'index.php/recomendadores/panel/modificar_password' ?>" class="btn btn-menu-cabecera" title="MODIFICAR CONTRASEÑA">
		        <i class="fa fa-unlock-alt" aria-hidden="true"></i>
		    </a>-->

				<!--<a href="<?= base_url() . 'index.php/recomendadores/panel/bonificaciones' ?>" class="btn btn-menu-cabecera" title="MIS BONIFICACIONES">
		        <i class="fa fa-calculator" aria-hidden="true"></i>
		    </a>-->

				<a href="<?= base_url() . 'index.php/recomendadores/panel/lista_chat' ?>" class="btn btn-menu-cabecera" title="CHAT">
					<i class="fa fa-comments-o" aria-hidden="true"></i>
					<span id="total_notify" class="count bg-danger">-</span>

				</a>

				<a href="#" class="btn btn-menu-cabecera" title="VIDEOTUTORIAL">
					<i class="fa fa-play" aria-hidden="true"></i>
				</a>

				<a href="<?= base_url() . 'index.php/recomendadores/login/cerrar_sesion' ?>" class="btn btn-menu-cabecera" title="CERRAR SESIÓN">
					<i class="fa fa-arrow-right" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div>
	<!--<div class="col-sm-1"></div>-->
</div>