<table class="table table-bordered">
    <thead>
        <tr>
            <th>Fecha Servicio</th>
            <th>Lavador</th>
            <th>Servicio</th>
            <th>Cliente</th>
            <th>Placas</th>
            <th>Modelo</th>
            <th>Marca</th>
            <th>Precio</th>
            <th>Origen</th>
            <th>Confirmada</th>
            <th>Cancelado</th>
        </tr>
    </thead>
    <tbody>
        <?php $total = 0; ?>
        @foreach ($info as $i => $data)
            <tr>
                <td>{{ $data->fecha_creacion_servicio }}</td>
                <td>{{ $data->lavadorNombre }}</td>
                <td>{{ $data->servicioNombre }}</td>
                <td>{{ $data->nombre . ' ' . $data->apellido_paterno . ' ' . $data->apellido_materno }}</td>
                <td>{{ $data->placas }}</td>
                <td>{{ $data->modelo }}</td>
                <td>{{ $data->marca }}</td>
                <td>${{ number_format($data->precio, 2) }}</td>
                <td>{{$data->origen}}</td>
                <td>{{($data->confirmado)?'Si':'No'}}</td>
                <td>{{($data->cancelado)?'Si':'No'}}</td>
            </tr>
        @endforeach
    </tbody>
</table>