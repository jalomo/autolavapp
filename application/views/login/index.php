<!doctype html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<!--html class="no-js" lang=""-->
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Auto LavApp</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="https://hexadms.com/xehos/statics/inventario/imgs/logos/xehos2.png" />
    <!--<link rel="apple-touch-icon" href="apple-icon.png">
        <link rel="shortcut icon" href="favicon.ico">-->

    <link rel="stylesheet" href="https://hexadms.com/xehos/statics/tema/assets/css/normalize.css">
    <link rel="stylesheet" href="https://hexadms.com/xehos/statics/tema/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://hexadms.com/xehos/statics/tema/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://hexadms.com/xehos/statics/tema/assets/css/themify-icons.css">
    <link rel="stylesheet" href="https://hexadms.com/xehos/statics/tema/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://hexadms.com/xehos/statics/tema/assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="https://hexadms.com/xehos/statics/tema/assets/scss/style.css">

    <style type="text/css">
        .cargaIcono {
            -webkit-animation: rotation 1s infinite linear;
            font-size: 55px;
            color: darkblue;
            display: none;
        }

        .btn-logo {
            margin-left: 10px;
        }
    </style>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <!--EOD Styles-->
    <link rel="stylesheet" href="https://hexadms.com/xehos/statics/css/styleNETO.css">
    <script src="https://kit.fontawesome.com/fe47b5dbda.js" crossorigin="anonymous"></script>
</head>

<body class="login-image">
    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo" style="background: #fff">
                    <div id="errorMessageLogin" style="color: #FF0000; display: none"></div>
                    <div id="errorLoginData" style="color: #FF0000; display: none"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <img src="https://hexadms.com/xehos/statics/img/logohorizontal.png" class="logo" style="height: 4cm;">
                    </div>
                </div>
                <br>
                <div class="login-form">
                    <?php echo anchor('login/checkDataLogin', '', array('id' => 'checkValues', 'style' => 'display: none')); ?>
                    <form action="<?php echo base_url() ?>index.php/login/mainView" onsubmit="return login()" id="form_login" method="post">
                        <div class="form-group">
                            <label>Usuario</label>
                            <input type="text" class="form-control" placeholder="Usuario" id="loginAstillero" name="Login[adminUsername]" required />
                            <div id="recordUsername_error"></div>
                        </div>

                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" placeholder="Contraseña" id="passAstillero" name="Login[adminPassword]" required />
                            <div id="recordPassword_error"></div>
                        </div>

                        <h3 id="error_login" style="color:red;background:white;border-radius:10px"></h3>
                        <br>
                        <h5 style="color:white;" id="formulario_error"></h5>
                        <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                        <button type="submit" id="entrar" class="btn btn-success btn-flat m-b-30 m-t-30">Entrar</button>
                    </form>

                    <hr>
                </div>

                <br>
                <div class="row socialmedia">
                    <div class="col-sm-12" align="center">
                        <a href="https://www.facebook.com/Xehos-Autolavado-112141223862934" class="btn-logo" target="_blank">
                            <i class="fab fa-facebook"></i>
                        </a>

                        <a href="https://www.instagram.com/xehosautolavado/" class="btn-logo" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>

                        <a href="https://twitter.com/xehosautolavado" class="btn-logo" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>

                        <a href="https://www.youtube.com/channel/UCqmz6-WM_Ud8luM8ibmHZpA" class="btn-logo" target="_blank">
                            <i class="fab fa-youtube"></i>
                        </a>

                        <a href="https://api.whatsapp.com/send?phone=+523328056406&text=¡Hola!%20Quiero%20solicitar%20un%20servicio" class="btn-logo" target="_blank">
                            <i class="fab fa-whatsapp"></i>
                        </a>
                    </div>
                </div>

                <br>
                <br>
            </div>
        </div>
    </div>
    <script src="https://hexadms.com/xehos/statics/tema/assets/js/vendor/jquery-2.1.4.min.js"></script>

    <script>
        function login() {
            var band = 0;
            if ($("#loginAstillero").val() == '') {
                $("#loginAstillero").css("border", "1px solid #FF0000");
                band++;
            } else {
                $("#loginastillero").css("border", "1px solid #ADA9A5");
            }

            if ($("#passAstillero").val() == '') {
                $("#passAstillero").css("border", "1px solid #FF0000");
                band++;
            } else {
                $("#passAstillero").css("border", "1px solid #ADA9A5");
            }

            if ($("#loginAstillero").val() != '' && $("#passAstillero").val() != '') {
                values = $.ajax({
                    url: $("#checkValues").attr("href"),
                    type: "POST",
                    data: {
                        username: $("#loginAstillero").val(),
                        password: $("#passAstillero").val()
                    },
                    async: false
                }).responseText;
                if (values == 0 || values == '0') {
                    // $("#errorLoginData").text("Por favor, verifique el usuario/password.").show();
                    // $("#loginAstillero").css("border", "1px solid #FF0000");
                    // $("#passAstillero").css("border", "1px solid #FF0000");
                    band++;
                } else {
                    $("#errorLoginData").hide();
                    $("#loginAstillero").css("border", "1px solid #ADA9A5");
                    $("#passAstillero").css("border", "1px solid #ADA9A5");
                }
            }

            if (band != 0) {
                $("#error_login").text("Usuario o contraseña inválidos").show();
                return false;
            } else {
                $("#errorMessageLogin").hide();
                return true;
            }
        }
    </script>
</body>

</html>