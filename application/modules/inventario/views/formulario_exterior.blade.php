<!doctype html>
<html class="no-js" lang="">
    <base href="{{ base_url() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->config->item('TITULO'); ?></title>

        <meta name="description" content="Sufee Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--<link rel="apple-touch-icon" href="apple-icon.png">
        <link rel="shortcut icon" href="favicon.ico">-->

        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/flag-icon.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/cs-skin-elastic.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/lib/datatable/dataTables.bootstrap.min.css">
        <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/scss/style.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/css/isloading.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/css/sweetalert2.min.css">

        <style>
            td a i {
                font-size: 20px !important;
            }
            .error p{
                color:red !important;
            }

        </style>

        <script src="<?php echo base_url(); ?>statics/tema/assets/js/vendor/jquery-2.1.4.min.js"></script>
        <!--script src="<?php echo base_url(); ?>statics/tema/assets/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/datatables.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/jszip.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/pdfmake.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/vfs_fonts.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/buttons.html5.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/buttons.print.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/buttons.colVis.min.js"></script>
        <script src="<?php echo base_url(); ?>statics/tema/assets/js/lib/data-table/datatables-init.js"></script-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

        <!--<script type="text/javascript">
            $(document).ready(function() {
                $('#menuToggle').on('click', function(event) {
                    $('body').toggleClass('open');
                    $("#left-panel").toggle();
                });

                $("button").addClass("btn btn-info");

            });

        </script>-->
        <style>
            label {
                font-family: 'Roboto', sans-serif;
                font-size: 12px;
            }

            input {
                font-family: 'Roboto', sans-serif !important;
                font-size: 12px !important;
            }

            table {
                font-family: 'Roboto', sans-serif !important;
                font-size: 12px !important;
            }

            select {
                font-family: 'Roboto', sans-serif !important;
                font-size: 12px !important;
            }

            button {
                font-family: 'Roboto', sans-serif !important;
                font-size: 12px !important;
            }

            strong {
                font-family: 'Roboto', sans-serif !important;
                font-size: 12px !important;
            }

            .right-panel {
                background: transparent !important;
            }

            a {
                font-family: 'Poppins', sans-serif !important;
                font-size: 16px !important;
            }
        </style>
        
        <style>
            body {
                padding: 20px !important;
            }

            header {
                background-color: #eaeafd !important;
            }

            .for-notification {
                color: red;
            }

            .navbar {
                padding-top: 50px !important;
                background: transparent !important;
            }

            .noti-header {
                color: #434a5c
            }

            aside {
                background-image: url('statics/img/Patron.png') !important;
                background-size: cover !important;
                background-color: #7B95FF !important;
                border-radius: 20px !important;
                background-size: cover;
            }
            .navbar .navbar-nav li > a:hover, .navbar .navbar-nav li > a:hover .menu-icon{
                background-color: transparent !important;
                padding-left:1px;
                color:white
            }
            .navbar .navbar-nav li > a{
                color:white !important;
            }
            .navbar .navbar-nav li.menu-item-has-children .sub-menu{
                background: transparent !important;
            }
            .sub-menu .menu-icon{
                color:white !important
            }
            .card{
                border-radius: 20px;
            }
        </style>


        <!-- inventario -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>statics/css/css_inventario.css">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--> 
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">

        <style type="text/css">
            #cargaIcono {
                /*-webkit-animation: rotation 1s infinite linear;*/
                font-size: 55px !important;
                color: darkblue;
                display: none;
            }
        </style>
    </head>

    <body>
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <form id="formulario" method="post" action="" autocomplete="on" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="titulo_pdf" align="center" style="margin-left: 15px; margin-right: 15px;">
                                                <h5>DIAGNÓSTICO Y POSIBLES CONSECUENCIAS</h5>

                                                <br>
                                            </div>

                                            <div class="panel-body cuadro_pdf">
                                                <div class="col-sm-12 table-responsive" style="width: 100%;" align="center">
                                                    <table  class="table table-bordered" style="background-color: white;">
                                                        <tr>
                                                            <td style="font-size: 14px;width: 30%;">
                                                                No. Orden / Placas:
                                                            </td>
                                                            <td style="font-size: 14px;">
                                                                <input type="text" class="form-control" name="orden" value="<?php if(isset($folio_info)) echo $folio_info; ?>" onblur="revisar_datos()" style="width: 5cm;">
                                                                <label for="" id="errorConsultaServer" class="error"></label>
                                                                <div id="orden_error"></div>
                                                            </td>
                                                            <td align="center">
                                                                <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                Si&nbsp;&nbsp;
                                                                <input type="radio" class="danos" style="transform: scale(1.5);" value="1" name="danos" <?php if(isset($marca_danos)){ if($marca_danos == "1") echo "checked";}else { echo 'checked'; }?>>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                No&nbsp;&nbsp;
                                                                <input type="radio" class="danos" style="transform: scale(1.5);" value="0" name="danos" <?php if(isset($marca_danos)) if($marca_danos == "0") echo "checked";?>>

                                                                <input type="hidden" name="marca_danos" value="<?php if(isset($marca_danos)) echo $marca_danos; else echo '1'?>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <input type="radio" style="transform: scale(1.5);" id="golpes" name="marcasRadio" checked value="hit">
                                                                &nbsp;&nbsp;
                                                                Golpes
                                                                &nbsp;&nbsp;
                                                                <input type="radio" style="transform: scale(1.5);" id="roto" name="marcasRadio" value="broken">
                                                                &nbsp;&nbsp;
                                                                Roto / Estrellado
                                                                &nbsp;&nbsp;
                                                                <input type="radio" style="transform: scale(1.5);" id="rayones" name="marcasRadio" value="scratch">
                                                                &nbsp;&nbsp;
                                                                Rayones
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="3">
                                                                <img src="<?php if(isset($img_diagrama)) { echo base_url().(($img_diagrama != "") ? $img_diagrama : 'statics/inventario/imgs/car_1.jpg'); } else{ echo base_url().'statics/inventario/imgs/car_1.jpg'; } ?>" id="img_diagrama" style="width:420px; height:500px;" <?php if(!isset($img_diagrama)) echo 'hidden'; ?>>

                                                                <canvas id="canvas_3" width="420" height="500" <?php if(isset($img_diagrama)) echo 'hidden'; ?>>
                                                                    Su navegador no soporta canvas.
                                                                </canvas>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'statics/inventario/imgs/car_1.jpg'; ?>">
                                                    <input type="hidden" name="danosMarcas" id="danosMarcas" value="<?php if(isset($img_diagrama)) echo $img_diagrama; ?>">
                                                </div>

                                                <div class="col-sm-12 table-responsive" align="center">
                                                    <br>
                                                    <a href="<?php if(isset($img_diagrama)) echo base_url().$img_diagrama; else echo '#'; ?>" class="btn btn-default" id="btn-download" download="Diagrama_Diagnostico">
                                                        <i class="fa fa-download" aria-hidden="true"></i>
                                                        Descargar diagrama
                                                    </a>

                                                    <button type="button" class="btn btn-primary" id="resetoeDiagrama" style="margin-left: 20px;" <?php if(isset($img_diagrama)) echo 'hidden'; ?> >
                                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                                        Restaurar diagrama
                                                    </button>
                                                </div>

                                                
                                                <div class="col-sm-12 table-responsive">
                                                    <br><br>
                                                    <table class="table table-bordered" style="min-width: 100%;background-color: white;border-right: 10px;">
                                                        <thead class="titulo_pdf">
                                                            <tr>
                                                                <td align="center" class="td_tablas">
                                                                    Interiores
                                                                    <div id="interiores_error"></div>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    Si
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    No
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    (No cuenta)&nbsp;
                                                                    NC
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Llavero.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck interiores" name="interiores[]" value="Llavero.Si" <?php if(isset($interiores)) if(in_array("Llavero.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck interiores" name="interiores[]" value="Llavero.No" <?php if(isset($interiores)) if(in_array("Llavero.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck interiores" name="interiores[]" value="Llavero.NC" <?php if(isset($interiores)) if(in_array("Llavero.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Seguro rines.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="interiores[]" value="SeguroRines.Si" <?php if(isset($interiores)) if(in_array("SeguroRines.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="interiores[]" value="SeguroRines.No" <?php if(isset($interiores)) if(in_array("SeguroRines.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="interiores[]" value="SeguroRines.NC" <?php if(isset($interiores)) if(in_array("SeguroRines.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">Indicadores de falla Activados:</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <table style="width:100%;">
                                                                        <tr>
                                                                            <td align="center" class="td_tablas">
                                                                                <img src="<?php echo base_url();?>statics/inventario/imgs/icono_5.png" style="width:1.2cm;">
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <img src="<?php echo base_url();?>statics/inventario/imgs/icono_2.png" style="width:1.2cm;">
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <img src="<?php echo base_url();?>statics/inventario/imgs/icono_7.png" style="width:1.2cm;">
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <img src="<?php echo base_url();?>statics/inventario/imgs/icono_1.png" style="width:1.2cm;">
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <img src="<?php echo base_url();?>statics/inventario/imgs/icono_6.png" style="width:1.2cm;">
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <img src="<?php echo base_url();?>statics/inventario/imgs/icono_4.png" style="width:1.2cm;">
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <img src="<?php echo base_url();?>statics/inventario/imgs/icono_3.png" style="width:1.2cm;">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" class="td_tablas">
                                                                                <input type="checkbox" class="IndicadorGasolinaCheck interiores" style="margin-right: 6px; transform: scale(1.5);" name="interiores[]" value="IndicadorGasolina.Si" <?php if(isset($interiores)) if(in_array("IndicadorGasolina.Si",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" class="IndicadorGasolinaCheck interiores" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);"  name="interiores[]" value="IndicadorGasolina.No" <?php if(isset($interiores)) if(in_array("IndicadorGasolina.No",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" class="IndicadorGasolinaCheck interiores" style="margin-left: 6px; transform: scale(1.5);" name="interiores[]" value="IndicadorGasolina.NC" <?php if(isset($interiores)) if(in_array("IndicadorGasolina.NC",$interiores)) echo "checked";?>>
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck interiores" name="interiores[]" value="IndicadorMantenimento.Si" <?php if(isset($interiores)) if(in_array("IndicadorMantenimento.Si",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck interiores" name="interiores[]" value="IndicadorMantenimento.No" <?php if(isset($interiores)) if(in_array("IndicadorMantenimento.No",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);"  class="IndicadorMantenimentoCheck interiores" name="interiores[]" value="IndicadorMantenimento.NC" <?php if(isset($interiores)) if(in_array("IndicadorMantenimento.NC",$interiores)) echo "checked";?>>
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="SistemaABSCheck interiores" name="interiores[]" value="SistemaABS.Si" <?php if(isset($interiores)) if(in_array("SistemaABS.Si",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="SistemaABSCheck interiores" name="interiores[]" value="SistemaABS.No" <?php if(isset($interiores)) if(in_array("SistemaABS.No",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);"  class="SistemaABSCheck interiores" name="interiores[]" value="SistemaABS.NC" <?php if(isset($interiores)) if(in_array("SistemaABS.NC",$interiores)) echo "checked";?>>
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck interiores" name="interiores[]" value="IndicadorFrenos.Si" <?php if(isset($interiores)) if(in_array("IndicadorFrenos.Si",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck interiores" name="interiores[]" value="IndicadorFrenos.No" <?php if(isset($interiores)) if(in_array("IndicadorFrenos.No",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);"  class="IndicadorFrenosCheck interiores" name="interiores[]" value="IndicadorFrenos.NC" <?php if(isset($interiores)) if(in_array("IndicadorFrenos.NC",$interiores)) echo "checked";?>>
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck interiores" name="interiores[]" value="IndicadorBolsaAire.Si" <?php if(isset($interiores)) if(in_array("IndicadorBolsaAire.Si",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck interiores" name="interiores[]" value="IndicadorBolsaAire.No" <?php if(isset($interiores)) if(in_array("IndicadorBolsaAire.No",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);"  class="IndicadorBolsaAireCheck interiores" name="interiores[]" value="IndicadorBolsaAire.NC" <?php if(isset($interiores)) if(in_array("IndicadorBolsaAire.NC",$interiores)) echo "checked";?>>
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck interiores" name="interiores[]" value="IndicadorTPMS.Si" <?php if(isset($interiores)) if(in_array("IndicadorTPMS.Si",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck interiores" name="interiores[]" value="IndicadorTPMS.No" <?php if(isset($interiores)) if(in_array("IndicadorTPMS.No",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck interiores" name="interiores[]" value="IndicadorTPMS.NC" <?php if(isset($interiores)) if(in_array("IndicadorTPMS.NC",$interiores)) echo "checked";?>>
                                                                            </td>
                                                                            <td align="center" class="td_tablas">
                                                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck interiores" name="interiores[]" value="IndicadorBateria.Si" <?php if(isset($interiores)) if(in_array("IndicadorBateria.Si",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck interiores" name="interiores[]" value="IndicadorBateria.No" <?php if(isset($interiores)) if(in_array("IndicadorBateria.No",$interiores)) echo "checked";?>>
                                                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);"  class="IndicadorBateriaCheck interiores" name="interiores[]" value="IndicadorBateria.NC" <?php if(isset($interiores)) if(in_array("IndicadorBateria.NC",$interiores)) echo "checked";?>>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Indicador de falla activados.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="interiores2[]" value="IndicadorDeFalla.Si" <?php if(isset($interiores)) if(in_array("IndicadorDeFalla.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="interiores2[]" value="IndicadorDeFalla.No" <?php if(isset($interiores)) if(in_array("IndicadorDeFalla.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="interiores2[]" value="IndicadorDeFalla.NC" <?php if(isset($interiores)) if(in_array("IndicadorDeFalla.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Rociadores y Limpiaparabrisas.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="interiores2[]" value="Rociadores.Si" <?php if(isset($interiores)) if(in_array("Rociadores.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="interiores2[]" value="Rociadores.No" <?php if(isset($interiores)) if(in_array("Rociadores.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="interiores2[]" value="Rociadores.NC" <?php if(isset($interiores)) if(in_array("Rociadores.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Claxon.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="interiores2[]" value="Claxon.Si" <?php if(isset($interiores)) if(in_array("Claxon.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="interiores2[]" value="Claxon.No" <?php if(isset($interiores)) if(in_array("Claxon.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="interiores2[]" value="Claxon.NC" <?php if(isset($interiores)) if(in_array("Claxon.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    Luces
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Delanteras.
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="interiores2[]" value="LucesDelanteras.Si" <?php if(isset($interiores)) if(in_array("LucesDelanteras.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="interiores2[]" value="LucesDelanteras.No" <?php if(isset($interiores)) if(in_array("LucesDelanteras.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="interiores2[]" value="LucesDelanteras.NC" <?php if(isset($interiores)) if(in_array("LucesDelanteras.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Traseras.
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="interiores2[]" value="LucesTraseras.Si" <?php if(isset($interiores)) if(in_array("LucesTraseras.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="interiores2[]" value="LucesTraseras.No" <?php if(isset($interiores)) if(in_array("LucesTraseras.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="interiores2[]" value="LucesTraseras.NC" <?php if(isset($interiores)) if(in_array("LucesTraseras.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                    Stop.
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="interiores2[]" value="LucesStop.Si" <?php if(isset($interiores)) if(in_array("LucesStop.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="interiores2[]" value="LucesStop.No" <?php if(isset($interiores)) if(in_array("LucesStop.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="interiores2[]" value="LucesStop.NC" <?php if(isset($interiores)) if(in_array("LucesStop.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Radio / Caratulas.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="interiores2[]" value="Caratulas.Si" <?php if(isset($interiores)) if(in_array("Caratulas.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="interiores2[]" value="Caratulas.No" <?php if(isset($interiores)) if(in_array("Caratulas.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="interiores2[]" value="Caratulas.NC" <?php if(isset($interiores)) if(in_array("Caratulas.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Pantallas.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck interiores" name="interiores2[]" value="Pantallas.Si" <?php if(isset($interiores)) if(in_array("Pantallas.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck interiores" name="interiores2[]" value="Pantallas.No" <?php if(isset($interiores)) if(in_array("Pantallas.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck interiores" name="interiores[]" value="Pantallas.NC" <?php if(isset($interiores)) if(in_array("Pantallas.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>A/C.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="AACheck interiores" name="interiores2[]" value="AA.Si" <?php if(isset($interiores)) if(in_array("AA.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="AACheck interiores" name="interiores2[]" value="AA.No" <?php if(isset($interiores)) if(in_array("AA.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="AACheck interiores" name="interiores2[]" value="AA.NC" <?php if(isset($interiores)) if(in_array("AA.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Encendedor.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="interiores3[]" value="Encendedor.Si" <?php if(isset($interiores)) if(in_array("Encendedor.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="interiores3[]" value="Encendedor.No" <?php if(isset($interiores)) if(in_array("Encendedor.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="interiores3[]" value="Encendedor.NC" <?php if(isset($interiores)) if(in_array("Encendedor.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Vidrios.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck interiores" name="interiores3[]" value="Vidrios.Si" <?php if(isset($interiores)) if(in_array("Vidrios.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck interiores" name="interiores3[]" value="Vidrios.No" <?php if(isset($interiores)) if(in_array("Vidrios.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck interiores" name="interiores3[]" value="Vidrios.NC" <?php if(isset($interiores)) if(in_array("Vidrios.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Espejos.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck interiores" name="interiores3[]" value="Espejos.Si" <?php if(isset($interiores)) if(in_array("Espejos.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck interiores" name="interiores3[]" value="Espejos.No" <?php if(isset($interiores)) if(in_array("Espejos.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck interiores" name="interiores3[]" value="Espejos.NC" <?php if(isset($interiores)) if(in_array("Espejos.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Seguros eléctricos.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="interiores3[]" value="SegurosEléctricos.Si" <?php if(isset($interiores)) if(in_array("SegurosEléctricos.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="interiores3[]" value="SegurosEléctricos.No" <?php if(isset($interiores)) if(in_array("SegurosEléctricos.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="interiores3[]" value="SegurosEléctricos.NC" <?php if(isset($interiores)) if(in_array("SegurosEléctricos.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Disco compacto.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="CDCheck interiores" name="interiores3[]" value="CD.Si" <?php if(isset($interiores)) if(in_array("CD.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="CDCheck interiores" name="interiores3[]" value="CD.No" <?php if(isset($interiores)) if(in_array("CD.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="CDCheck interiores" name="interiores3[]" value="CD.NC" <?php if(isset($interiores)) if(in_array("CD.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Asientos y vestiduras.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="interiores3[]" value="Vestiduras.Si" <?php if(isset($interiores)) if(in_array("Vestiduras.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="interiores3[]" value="Vestiduras.No" <?php if(isset($interiores)) if(in_array("Vestiduras.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="interiores3[]" value="Vestiduras.NC" <?php if(isset($interiores)) if(in_array("Vestiduras.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tapetes.</td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck interiores" name="interiores3[]" value="Tapetes.Si" <?php if(isset($interiores)) if(in_array("Tapetes.Si",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck interiores" name="interiores3[]" value="Tapetes.No" <?php if(isset($interiores)) if(in_array("Tapetes.No",$interiores)) echo "checked";?>>
                                                                </td>
                                                                <td align="center" class="td_tablas">
                                                                    <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck interiores" name="interiores3[]" value="Tapetes.NC" <?php if(isset($interiores)) if(in_array("Tapetes.NC",$interiores)) echo "checked";?>>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="col-sm-12 table-responsive">
                                                    <div class="titulo_pdf" align="center" style="margin-left: 15px; margin-right: 15px;">
                                                        <h5>Cajuela / Exteriores / Documentación</h5>
                                                    </div>

                                                    <div class="panel-body" style="background-color: white;">
                                                        <div class="col-sm-4">
                                                            <table class="table table-bordered table-responsive" style="width: 100%;">
                                                                <thead class="titulo_pdf">
                                                                    <tr>
                                                                        <td>
                                                                            Cajuela
                                                                            <div id="cajuela_error"></div>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            Si
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            No
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            No cuenta<br>
                                                                            (NC)
                                                                        </td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Herramienta.</td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.Si" <?php if(isset($cajuela)) if(in_array("Herramienta.Si",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.No" <?php if(isset($cajuela)) if(in_array("Herramienta.No",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.NC" <?php if(isset($cajuela)) if(in_array("Herramienta.NC",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Gato / Llave.</td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.Si" <?php if(isset($cajuela)) if(in_array("eLlave.Si",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.No" <?php if(isset($cajuela)) if(in_array("eLlave.No",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.NC" <?php if(isset($cajuela)) if(in_array("eLlave.NC",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Reflejantes.</td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.Si" <?php if(isset($cajuela)) if(in_array("Reflejantes.Si",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.No" <?php if(isset($cajuela)) if(in_array("Reflejantes.No",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.NC" <?php if(isset($cajuela)) if(in_array("Reflejantes.NC",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Cables.</td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cajuela[]" value="Cables.Si" <?php if(isset($cajuela)) if(in_array("Cables.Si",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cajuela[]" value="Cables.No" <?php if(isset($cajuela)) if(in_array("Cables.No",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cajuela[]" value="Cables.NC" <?php if(isset($cajuela)) if(in_array("Cables.NC",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Extintor.</td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.Si" <?php if(isset($cajuela)) if(in_array("Extintor.Si",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.No" <?php if(isset($cajuela)) if(in_array("Extintor.No",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.NC" <?php if(isset($cajuela)) if(in_array("Extintor.NC",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Llanta Refacción.</td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.Si" <?php if(isset($cajuela)) if(in_array("LlantaRefaccion.Si",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.No" <?php if(isset($cajuela)) if(in_array("LlantaRefaccion.No",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                        <td align="center" class="td_tablas">
                                                                            <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.NC" <?php if(isset($cajuela)) if(in_array("LlantaRefaccion.NC",$cajuela)) echo "checked";?>>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="col-sm-8">
                                                            <div class="row">
                                                                <div class="col-sm-6 table-responsive  table-responsive" style="width: 100%;">
                                                                    <table class="table table-bordered">
                                                                        <thead class="titulo_pdf">
                                                                            <tr>
                                                                                <td>
                                                                                    Exteriores
                                                                                    <div id="exteriores_error"></div>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    Si
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    No
                                                                                </td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Tapones rueda.</td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="TaponesRuedaCheck exteriores" name="exteriores[]" value="TaponesRueda.Si" <?php if(isset($exteriores)) if(in_array("TaponesRueda.Si",$exteriores)) echo "checked";?>>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="TaponesRuedaCheck exteriores" name="exteriores[]" value="TaponesRueda.No" <?php if(isset($exteriores)) if(in_array("TaponesRueda.No",$exteriores)) echo "checked";?>>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Gomas de limpiadores.</td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="GotasCheck exteriores" name="exteriores[]" value="Gomas.Si" <?php if(isset($exteriores)) if(in_array("Gomas.Si",$exteriores)) echo "checked";?>>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="GotasCheck exteriores" name="exteriores[]" value="Gomas.No" <?php if(isset($exteriores)) if(in_array("Gomas.No",$exteriores)) echo "checked";?>>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Antena.</td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="AntenaCheck exteriores" name="exteriores[]" value="Antena.Si" <?php if(isset($exteriores)) if(in_array("Antena.Si",$exteriores)) echo "checked";?>>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="AntenaCheck exteriores" name="exteriores[]" value="Antena.No" <?php if(isset($exteriores)) if(in_array("Antena.No",$exteriores)) echo "checked";?>>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Tapón de gasolina.</td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="TaponGasolinaCheck exteriores" name="exteriores[]" value="TaponGasolina.Si" <?php if(isset($exteriores)) if(in_array("TaponGasolina.Si",$exteriores)) echo "checked";?>>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="TaponGasolinaCheck exteriores" name="exteriores[]" value="TaponGasolina.No" <?php if(isset($exteriores)) if(in_array("TaponGasolina.No",$exteriores)) echo "checked";?>>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                                <div class="col-sm-6 table-responsive  table-responsive" style="width: 100%;">
                                                                    <table class="table table-bordered">
                                                                        <thead class="titulo_pdf">
                                                                            <tr>
                                                                                <td>
                                                                                    Documentación
                                                                                    <div id="documentacion_error"></div>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    Si
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    No
                                                                                </td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Póliza garantía/Manual prop.</td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="PolizaGarantiaCheck documentacion" name="documentacion[]" value="PolizaGarantia.Si" <?php if(isset($documentacion)) if(in_array("PolizaGarantia.Si",$documentacion)) echo "checked";?>>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="PolizaGarantiaCheck documentacion" name="documentacion[]" value="PolizaGarantia.No" <?php if(isset($documentacion)) if(in_array("PolizaGarantia.No",$documentacion)) echo "checked";?>>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Seguro de Rines.</td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesDocCheck documentacion" name="documentacion[]" value="SeguroRinesDoc.Si" <?php if(isset($documentacion)) if(in_array("SeguroRinesDoc.Si",$documentacion)) echo "checked";?>>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesDocCheck documentacion" name="documentacion[]" value="SeguroRinesDoc.No" <?php if(isset($documentacion)) if(in_array("SeguroRinesDoc.No",$documentacion)) echo "checked";?>>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Certificado verificación.</td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="cVerificacionCheck documentacion" name="documentacion[]" value="cVerificacion.Si" <?php if(isset($documentacion)) if(in_array("cVerificacion.Si",$documentacion)) echo "checked";?>>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="cVerificacionCheck documentacion" name="documentacion[]" value="cVerificacion.No" <?php if(isset($documentacion)) if(in_array("cVerificacion.No",$documentacion)) echo "checked";?>>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Tarjeta de circulación.</td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="tCirculacionCheck documentacion" name="documentacion[]" value="tCirculacion.Si" <?php if(isset($documentacion)) if(in_array("tCirculacion.Si",$documentacion)) echo "checked";?>>
                                                                                </td>
                                                                                <td align="center" class="td_tablas">
                                                                                    <input type="checkbox" style="transform: scale(1.5);" class="tCirculacionCheck documentacion" name="documentacion[]" value="tCirculacion.No" <?php if(isset($documentacion)) if(in_array("tCirculacion.No",$documentacion)) echo "checked";?>>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-sm-12" align="center">
                                                                    <table class="table table-bordered">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td colspan="2" align="center">
                                                                                    <h5>Nivel de Gasolina:</h5>
                                                                                    <img src="<?php echo base_url();?>statics/inventario/imgs/gasolina_a.png" style="width:100px;" id="nivel_gasolina_img">
                                                                                    <br>
                                                                                    <div id="slider-2"style="width:200px;"></div>
                                                                                    <input type="hidden" name="nivel_gasolina" style="width:1.5cm;" value="<?php if(isset($nivel_gasolina)) echo $nivel_gasolina; else echo "0"; ?>">
                                                                                    <br>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>¿Deja artículos personales?</td>
                                                                                <td>
                                                                                    (Si&nbsp;&nbsp;
                                                                                    <input type="radio" class="articulos" style="transform: scale(1.5);" value="1" name="articulos" <?php if(isset($articulos_vehiculo)) if($articulos_vehiculo == "1") echo "checked" ?>>&nbsp;&nbsp;
                                                                                    No&nbsp;&nbsp;
                                                                                    <input type="radio" class="articulos" style="transform: scale(1.5);" value="0" name="articulos" <?php if(isset($articulos_vehiculo)) {if($articulos_vehiculo == "0") echo "checked";}else { echo "checked"; } ?>>
                                                                                    &nbsp;&nbsp;)
                                                                                    <br>
                                                                                    <input type="hidden" name="articulos_vehiculo" value="0">
                                                                                    <div id="articulos_error"></div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>¿Cuales?</td>
                                                                                <td>
                                                                                    <input type="text" class="form-control" value="<?php if(isset($articulos)) echo $articulos; ?>" name="articulos" style="width: 100%;">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>¿Desea reportar algo más?</td>
                                                                                <td>
                                                                                    <textarea name="reporte_articulos" class="form-control" rows="2"><?php if(isset($reporte_articulos)) echo $reporte_articulos; ?></textarea>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 table-responsive" style="margin-top: 30px;">
                                                    <div class="titulo_pdf" align="center">
                                                        <h4>IMPORTANTE</h4>
                                                    </div>

                                                    <div class="panel-body" style="background-color: white;">
                                                        <p align="justify" style="text-align: justify;">
                                                            Manifiesto bajo protesta que soy el dueño del vehículo, o tengo orden y/o autorización de él para utilizarlo y ordenar éste servicio, en su representación, autorizando en su nombre y en el propio la realización de los trabajos descritos, así como el uso de materiales e insumos necesarios para efectuarlos, comprometiéndome en su nombre y en el propio a pagar el importe del servicio; también manifiesto mi conformidad con el inventario realizado y que consta en el ejemplar del presente que recibo; en cualquier caso que las partes acuerden que el vehículo vaya a ser recogido o entregado por personal del PROVEEDOR en el domicilio del CONSUMIDOR, ello solo será mediante previo acuerdo u orden del CONSUMIDOR por escrito en la orden de servicio digital vía App. O habiendo pasado los filtros de seguridad, para el debido resguardo del vehículo y debidamente aceptada por el PROVEEDOR, con un costo de 
                                                            $ <input type="text" class="input_field" name="costo_diagnostico" style="width:3cm;" value="<?php if(isset($costo_diagnostico)) echo $costo_diagnostico; else echo 'Por definir'; ?>">
                                                            (MONEDA NACIONAL); será en todo caso obligación del personal del PROVEEDOR identificarse plenamente ante el CONSUMIDOR como tal. 
                                                            <br>
                                                            Finalmente manifiesto mi conformidad con los términos y condiciones previstas en el contrato inscrito en el presente, el cual manifiesto que he leído, obligándome en lo personal y en nombre del propietario del automóvil en los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día 
                                                            <input type="text" class="input_field" name="dia_diagnostico" style="width:1cm;" value="<?php if(isset($dia_diagnostico)) echo $dia_diagnostico; else echo date('d'); ?>">
                                                            de
                                                            <input type="text" class="input_field" name="mes_diagnostico" style="width:3cm;" value="<?php if(isset($mes_diagnostico)) echo $mes_diagnostico; else echo $mes ?>">
                                                            de
                                                            <input type="text" class="input_field" name="anio_diagnostico" style="width:2cm;" value="<?php if(isset($anio_diagnostico)) echo $anio_diagnostico; else echo date("Y"); ?>"> .
                                                        </p>
                                                    </div>

                                                    <div class="col-sm-12" style="background-color: white;" align="right">
                                                        <h5>Evidencias</h5>
                                                        <input type="file" class="btn btn-default" name="archivo[]" value="" multiple>
                                                        <br>
                                                        
                                                        <?php if (isset($evidencias)): ?>
                                                            <?php for ($x = 0 ; $x < count($evidencias) ; $x++): ?>
                                                                <a href="<?php echo base_url().$evidencias[$x]; ?>" class="btn btn-success" target="_blank">Archivo (<?php echo $x+1; ?>)</a>
                                                            <?php endfor ?>
                                                        <?php endif ?>
                                                    </div>

                                                    <div class="col-sm-12" style="background-color: white;">
                                                        <div class="col-sm-6" align="center">
                                                            <img class="marcoImg" src="<?php if(isset($firma_asesor)) { echo base_url().(($firma_asesor != "") ? $firma_asesor : 'statics/inventario/imgs/fondo_bco.jpeg'); } else{ echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; } ?>" id="firma_asesor_Img" style="width: 2.5cm;" >

                                                            <input type="hidden" id="ruta_firma_asesor" name="ruta_firma_asesor" value="<?php if(isset($firma_asesor)) echo $firma_asesor;?>">

                                                            <br>
                                                            <input type="text" onblur='llenadoAsesor()'  class="input_field" name="nombre_asesor" style="width:100%;" value="<?php if(isset($nombre_asesor)) echo $nombre_asesor;?>" <?php if(isset($id)) echo 'disabled'?>>

                                                            <div id="ruta_firma_asesor_error"></div>
                                                            <div id="nombre_asesor_error"></div>
                                                            <br>
                                                            Nombre y firma del operador de servicio.
                                                        </div>

                                                        <div class="col-sm-6" align="center">
                                                            <img class="marcoImg" src="<?php if(isset($firma_cliente)) { echo base_url().(($firma_cliente != "") ? $firma_cliente : 'statics/inventario/imgs/fondo_bco.jpeg'); } else{ echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; } ?>" id="firma_cliente_Img" style="width: 2.5cm;" >

                                                            <input type="hidden" id="ruta_firma_cliente" name="ruta_firma_cliente" value="<?php if(isset($firma_cliente)) echo $firma_cliente;?>">

                                                            <br>
                                                            <input type="text" onblur='llenadoCliente()' class="input_field" name="nombre_cliente" style="width:100%;" value="<?php if(isset($nombre_cliente)) echo $nombre_cliente;?>" <?php if(isset($id)) echo 'disabled'?>>

                                                            <div id="ruta_firma_cliente_error"></div>
                                                            <div id="nombre_cliente_error"></div>
                                                            <br>
                                                            Firma del consumidor
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12" style="background-color: white;">
                                                        <div class="col-sm-6" align="center">
                                                            <?php if (!isset($id)): ?>
                                                                <a class="cuadroFirma btn btn-primary" data-value="Asesor_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                                            <?php endif ?>
                                                        </div>

                                                        <div class="col-sm-6" align="center">
                                                            <?php if (!isset($id)): ?>
                                                                <a class="cuadroFirma btn btn-primary" data-value="Cliente_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                                            <?php endif ?>
                                                        </div>

                                                        <br><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br><br>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="titulo_pdf" align="center">
                                                <h5>CONTRATO DE ADHESIÓN</h5>
                                            </div>

                                            <div class="panel-body cuadro_pdf">
                                                <div class="col-sm-12" style="background-color: white;">
                                                    <br>
                                                    <table>
                                                        <tr>
                                                            <td style="width: 60%;" rowspan="3">
                                                                <p align="justify" style="text-align: justify;font-size: 13px;">
                                                                    SISTEMAS OPERATIVOS HEXADECIMAL, S.A. DE C.V.  <br>
                                                                    AV. IGNACIO SANDOVAL 1939. LOC. 6. PLAZA MILAN.  <br>
                                                                    COL. PASEO DE LA CANTERA. C.P. 28017, COLIMA, COLIMA <br>
                                                                    RFC: SOH190520AR4.   Tels. 33 3835 2043 / 33 3835 2058 <br>
                                                                    Email: info@xehos.com (Quejas y Reclamaciones) 
                                                                </p>
                                                            </td>
                                                            <td style="width: 25%;padding-right: 15px;font-size: 13px;" align="right">
                                                                Folio No.
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control" name="folio" style="width:100%;" value="<?php if(isset($folio)) echo $folio;?>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%;padding-right: 15px;font-size: 13px;" align="right">
                                                                Fecha:
                                                            </td>
                                                            <td>
                                                                <input type="date" class="form-control" name="fecha_contrato" style="width:100%;padding: 0px;" value="<?php if(isset($fecha_contrato)) echo $fecha_contrato; else echo date('Y-m-d');?>">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%;padding-right: 15px;font-size: 13px;" align="right">
                                                                Hora:
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control" name="hora_contrato" style="width:100%;padding: 0px;" value="<?php if(isset($hora_contrato)) echo $hora_contrato; else echo date('H:i:s');?>">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                                    <br><br>
                                                    <div class="col-sm-12">
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            CONTRATO DE PRESTACIÓN DE SERVICIO DE AUTOLAVADO Y DETALLADO  DE VEHÍCULOS QUE CELEBRAN POR UNA PARTE
                                                            <input type="text" class="input_field" name="empresa_contrato" style="width:9cm;" value="<?php if(isset($empresa_contrato)) echo $empresa_contrato; else echo 'SISTEMAS OPERATIVOS HEXADECIMAL , S.A. DE C.V.';?>">
                                                            , AUTORIZADO 
                                                            <input type="text" class="input_field" name="autoriza_contrato" style="width:5cm;" value="<?php if(isset($autoriza_contrato)) echo $autoriza_contrato; else echo 'XEHOS AUTOLAVADO';?>"> . 
                                                            Y POR LA OTRA EL CONSUMIDOR CUYO NOMBRE SE SEÑALA EN EL INICIO DEL PRESENTE, A QUIENES EN LO SUCESIVO Y PARA EFECTO DEL PRESENTE CONTRATO SE LE DENOMINARA “EL PROVEEDOR” Y “EL CONSUMIDOR”, RESPECTIVAMENTE. 
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DECLARACIONES
                                                        </h5>

                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            Las partes denominadas CONSUMIDOR y PROVEEDOR, se reconocen las personalidades con las cuales se ostentan y que se encuentran especificadas en este documento (orden de servicio y contrato), por lo cual, están dispuestas a sujetarse a las condiciones que se establecen en las siguientes: 
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            CLÁUSULAS
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>1.- </samp> Objeto: El PROVEEDOR realizará todo el  Lavado. Limpieza. Detallado y/o Semi-Detallados, solicitados por el CONSUMIDOR que suscribe el presente contrato, a las que se someterá el vehículo que al inicio se detalla, para obtener las condiciones de limpieza de acuerdo a lo solicitado por el CONSUMIDOR y que serán realizadas a cargo y por cuenta del CONSUMIDOR. EL PROVEEDOR no condicionará en modo alguno la prestación de servicios de Lavado. Limpieza. Detallado y/o Semi-Detallados del vehículo, a la adquisición o renta de otros productos o servicios en el mismo servicio, predeterminada. El precio total de los servicios contratados se establece en el presupuesto que forma parte del presente y se describe en su inicio; dicho precio será pagado por el CONSUMIDOR antes de darle el servicio al vehículo;  Lavado. Detallado y/o Semi-Detallados, todo pago efectuado por el CONSUMIDOR deberá efectuarse en el instrumento App. Ios y Android, descargables de tiendas oficiales. O en la terminal punto de venta, portada por el operador que prestara el servicio del PROVEEDOR solo de forma digital o tarjetas debito y/o tarjeta de crédito  y en Moneda Nacional o extranjera al tipo de cambio vigente al día de pago, NO SE RECIBEN PAGOS EN EFECTIVO. Solo mediante tarjeta de crédito o transferencia bancaria efectuada con anterioridad a realizar el servicio al vehículo. 
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DE LAS CONDICIONES GENERALES:
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>2.- </samp> Las partes están de acuerdo en que las condiciones generales en las que se encuentra el automóvil de acuerdo con el inventario visual al momento de su recepción, son las que se definen en la orden de servicio-presupuesto que se encuentra al inicio del presente contrato. El CONSUMIDOR tendrá la obligación de aceptar dicho inventario con firma fotostática, misma que será de forma digital, con la que da aceptación y en conformidad con lo expuesto 
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DEL PRESUPUESTO
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>3.- </samp> El PROVEEDOR se obliga ante el CONSUMIDOR a respetar el presupuesto contenido en el presente contrato. 
                                                            <br>
                                                            <samp>4.- </samp> El CONSUMIDOR se obliga pagar a el PROVEEDOR por el servicio solicitado para su automóvil la cantidad de 
                                                            $   <input type="text" class="input_field" name="cantidad_contrato" style="width:3cm;" value="<?php if(isset($cantidad_contrato)) echo $cantidad_contrato; else echo 'Por definir';?>"> . (MONEDA NACIONAL), 
                                                            siempre y cuando no sea un vehículo distinto a lo solicitado en el servicio, en caso contrario será modificada la orden de servicio y se realizaran ajustes en costos y/o presupuestos por prestación de servicio. 
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DE LA PRESTACIÓN DE SERVICIOS
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>5.- </samp> Las partes convienen en que la fecha de aceptación del presupuesto es la que se indica en el inicio de este contrato.
                                                            <br>
                                                            <samp>6.- </samp> El PROVEEDOR se obliga a realizar el servicio de  Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado, en hora previamente establecido en el presupuesto. El PROVEEDOR se obliga a emplear productos adecuados y afines para los servicios contratados salvo que el CONSUMIDOR ordene o autorice expresamente y por escrito se utilicen otros o las provea, de conformidad con lo establecido en el artículo 60 de la Ley Federal de Protección al Consumidor, caso en el cual el Lavado. Detallado y/o Semi-Detallados del vehículo tendrá garantía por lluvias, en lo que se relacione a ese servicio, durante las primeras 24 horas continuas a su servicio. Siendo este un Lavado Express solo en el exterior del vehículo.
                                                            <br>
                                                            <samp>7.- </samp> El PROVEEDOR no podrá usar el vehículo para fines propios o de terceros. El PROVEEDOR se hace responsable por los daños causados al vehículo, como consecuencia del servicio efectuados por parte de su personal. Cuando el CONSUMIDOR, solicite que él o un representante suyo sea quien conduzca el automóvil para poder realizar el servicio, el riesgo, será por su cuenta.
                                                            <br>
                                                            <samp>8.- </samp> El PROVEEDOR se hace responsable por las posibles, daños o pérdidas parciales o totales imputables a él o a sus subalternos que sufra el vehículo, el equipo y los aditamentos adicionales que el CONSUMIDOR le haya notificado que existen en el momento de la recepción del mismo, o que se causen a terceros, mientras el vehículo se encuentre bajo su resguardo, salvo los ocasionados por desperfectos mecánicos o a resultas de piezas gastadas o sentidas, por incendio motivado por deficiencia eléctrica, o fallas en el sistema de combustible, y no causadas por el PROVEEDOR; para tal efecto el PROVEEDOR cuenta con un seguro suficiente para cubrir dichas eventualidades, bajo póliza expedida por compañía de seguros autorizada al efecto. El PROVEEDOR no se hace responsable por la pérdida de objetos dejados en el interior del automóvil, aun con la cajuela cerrada, salvo que estos le hayan sido notificados y puestos bajo su resguardo al momento de la recepción del automóvil. El PROVEEDOR tampoco se hace responsable por daños causados por fuerza mayor o caso fortuito, ni por la situación legal del automóvil cuando éste previamente haya sido robado o se hubiere utilizado en la comisión de algún ilícito; lo anterior, salvo que alguna de éstas cuestiones resultara legalmente imputable al PROVEEDOR; así mismo, el CONSUMIDOR, libera al PROVEEDOR de cualquier responsabilidad que surja o pueda surgir con relación al origen, posesión o cualquier otro derecho inherente al vehículo o a partes y componentes del mismo, obligándose en lo personal y en nombre del propietario, a responder de su procedencia. 
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DEL PRECIO Y FORMAS DE PAGO
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>9.- </samp> El CONSUMIDOR acepta haber tenido a su disposición los precios de la tarifa de mano de obra, Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado. El PROVEEDOR; los incrementos que resulten durante el servicio, por costos no previsibles y/o incrementos que resulten al momento de la ejecución del servicio por ser un vehículo distinto al cual se le realizara el servicio, deberán ser autorizados por el CONSUMIDOR, antes de realizar el servicio, y teniendo que pagar la s diferencias correspondientes, equivalentes al automóvil, El tiempo que, en su caso, que transcurra para cumplir esta condición, modificará la hora de entrega, en la misma proporción. Todas las quejas y sugerencias serán atendidas en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio.
                                                            <br>
                                                            <samp>10.- </samp> Será obligación del PROVEEDOR expedir la factura correspondiente por los servicios y productos que preste o enajene; el importe total del servicio, así como el precio por concepto del servicio, quedará especificado en ella, conforme a la ley.
                                                            <br>
                                                            <samp>11.- </samp> El CONSUMIDOR se obliga a pagar de contado al PROVEEDOR (conforme a lo establecido en la cláusula 1), en los medios dispuestos ; DIGITALES APP. WEB, de éste y POR ADELANTADO AL SERVICO CONTRATADO. A realizar al  automóvil, el importe del servicio, de conformidad con el presupuesto elaborado para tal efecto.
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DE LA ENTREGA.
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>12.- </samp> El PROVEEDOR se obliga a hacer la entrega del automóvil LIMPIO, en la hora establecida en este contrato, pudiendo ampliarse dicho plazo en caso fortuito o de fuerza mayor, en cuyo caso será obligación del PROVEEDOR dar aviso previo al CONSUMIDOR de la causa y del tiempo que se ampliará el plazo de entrega. Que en ese caso no podrá ser más de 30 Min. 
                                                            <br>
                                                            <samp>13.- </samp> La basura o residuos, derivados del servicio de lavado, quedarán a disposición del CONSUMIDOR al momento de hacerle entrega del automóvil.
                                                            <br>
                                                            <samp>14.- </samp> El CONSUMIDOR se obliga a hacer el pago, antes de realizarse el servicio. Y al momento que reciba el automóvil con el servicio de Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado  realizado , se entenderá que esto fue a completa satisfacción del CONSUMIDOR en lo que respecta a sus condiciones generales de acuerdo al inventario visual, mismas que fueron descritos en la orden de servicio, así como también en cuanto al Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado efectuada, sin afectar sus derechos a ejercer la garantía.
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DE LA RESCISIÓN Y PENAS CONVENCIONALES
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>15.- </samp> Es causa de rescisión del presente contrato: A.- Que EL PROVEEDOR incumpla en la hora y lugar de entrega del vehículo por causas propias, caso en el cual el CONSUMIDOR notificará por escrito del incumplimiento al PROVEEDOR, y éste hará entrega inmediata del vehículo debidamente con su servicio conforme y presupuesto establecido, descontando del precio pactado para la prestación del servicio, la suma equivalente al 2% (dos por ciento) del total del servicio, que se pacta como pena convencional; B.- Que el CONSUMIDOR incumpla con el pago del servicio ordenado, en el término previsto en la cláusula 19, caso en el cual el PROVEEDOR le notificará por escrito su incumplimiento, y podrá optar por exigir la recisión o cumplimiento forzoso de la obligación, cobrando la misma pena pactada del 2% al CONSUMIDOR, por la mora.
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DE LAS GARANTÍAS
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>16.- </samp> <u>Las reparaciones a que se refiere el presupuesto aceptado por el CONSUMIDOR y éste contrato están garantizadas por 24 horas naturales contados a partir de la hora de la entrega del vehículo ya Lavado</u>. Detallado y/o Semi-Detallados, según sea el servicio contratado, Si el vehículo es intervenido por un tercero, el PROVEEDOR no será responsable y la garantía quedará sin efecto. Las reclamaciones por garantía o solicitud de la misma  se harán vía App. Correo y/o telefónica  del PROVEEDOR, para lo cual el CONSUMIDOR pueda optar por su derecho, el servicio efectuado por el PROVEEDOR en cumplimiento a la garantía del servicio serán sin cargo alguno para el CONSUMIDOR. Esta garantía cubre cualquier lluvia o llovizna  por causas imputables al mismo y solo será válida siempre y cuando el automóvil se haya utilizado en condiciones de uso normal, se hayan observado en su uso las indicaciones de manejo y servicio que se le hubiera dado. En todo caso, el PROVEEDOR será corresponsable y solidario con los terceros del cumplimiento o incumplimiento de las garantías por ellos otorgadas en lo que se relacione al servicios a éste realizados, siempre que hayan sido contratados ante el CONSUMIDOR.
                                                            <br>
                                                            <samp>17.- </samp> Toda reclamación dentro del término de garantía, deberá ser realizada ante el PROVEEDOR que efectuó el servicio y en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio del presente contrato. En caso de que sea necesario hacer válida la garantía en un domicilio diverso al del PROVEEDOR, los gastos por ello deberán ser cubiertos por éste, siempre y cuando la garantía proceda, dichos gastos sean indispensables para tal fin, y sea igualmente indispensable realizar el servicio al vehículo en domicilio diverso al del PROVEEDOR, pero en dado caso, si el automóvil esta fuera de la entidad donde se localiza el PROVEEDOR, éste inmediatamente después de que tenga conocimiento, podrá indicar al CONSUMIDOR en donde se encuentra una filial de la marca más cercana, para hacer efectiva la garantía por conducto de ésta, si procede, debiendo acreditar con la factura correspondiente al servicio efectuado, con el objeto, de no hacer ningún cargo por ello al CONSUMIDOR. Con el objeto de dar cumplimiento a las obligaciones que ésta cláusula le impone, El PROVEEDOR señala como teléfonos de atención al CONSUMIDOR los que aparecen en éste contrato. Para los efectos de la atención y resolución de quejas y reclamaciones, estas deberán ser presentadas dentro de días y horas hábiles, que son los detallados en la parte superior del presente, en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio, al  PROVEEDOR, detallando en forma expresa la causa o motivo de la reclamación; la Gerencia de Servicio procederá a analizar la queja y resolverá lo conducente dentro de un lapso de tres días hábiles, procediendo al servicio del vehículo o manifestando las causas de improcedencia de la reclamación, en forma escrita. 
                                                        </p>

                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DE LA INFORMACIÓN Y PUBLICIDAD
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>18.- </samp> El PROVEEDOR se obliga a observar en todo momento lo dispuesto por los capítulos III y IV de la Ley Federal de Protección al Consumidor, en cuanto a la información, publicidad, promociones y ofertas.
                                                            <br>
                                                            <samp>19.- </samp> El CONSUMIDOR  (&nbsp;Si&nbsp;&nbsp;
                                                            <input type="radio" style="transform: scale(1.5);" value="1" name="publicidad_contrato" <?php if(isset($publicidad_contrato)){ if($publicidad_contrato == "1") echo "checked";}else { echo 'checked'; }?>>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            No&nbsp;&nbsp;
                                                            <input type="radio" style="transform: scale(1.5);" value="0" name="publicidad_contrato" <?php if(isset($publicidad_contrato)) if($publicidad_contrato == "0") echo "checked";?>> &nbsp;)
                                                            acepta que el PROVEEDOR ceda o transmita a tercero, con fines mercadotécnicos o publicitarios, la información proporcionada por él con motivo del presente contrato, y  &nbsp;Si&nbsp;&nbsp;
                                                            <input type="radio" style="transform: scale(1.5);" value="1" name="envio_publicidad" <?php if(isset($envio_publicidad)){ if($envio_publicidad == "1") echo "checked";}else { echo 'checked'; }?>>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            No&nbsp;&nbsp;
                                                            <input type="radio" style="transform: scale(1.5);" value="0" name="envio_publicidad" <?php if(isset($envio_publicidad)) if($envio_publicidad == "0") echo "checked";?>> &nbsp;)
                                                            acepta que el PROVEEDOR le envíe publicidad sobre bienes y servicios, firmando en éste espacio. 
                                                        </p>
                                                    </div>

                                                    <div class="col-sm-12" align="center">
                                                        <br>
                                                        <img class="marcoImg" src="<?php if(isset($firma_cliente_c1)) { echo base_url().(($firma_cliente_c1 != "") ? $firma_cliente_c1 : 'statics/inventario/imgs/fondo_bco.jpeg'); } else{ echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; } ?>" id="firma_cliente_c1" style="width: 2.5cm;">

                                                        <input type="hidden" id="ruta_firma_cliente_c1" name="ruta_firma_cliente_c1" value="<?php if(isset($firma_cliente_c1)) echo $firma_cliente_c1;?>">

                                                        <h6 id="h_cliente" style="text-align: center;font-weight: bold;">
                                                            <?php if(isset($cnombre_cliente)) echo $cnombre_cliente;?>
                                                        </h6>

                                                        <?php if (!isset($id)): ?>
                                                            <a class="cuadroFirma btn btn-primary" data-value="CCliente1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                                        <?php endif ?>
                                                        <div id="ruta_firma_cliente_c1_error"></div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            DE LA INFORMACIÓN Y PUBLICIDAD
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            <samp>20.- </samp> .- La Procuraduría Federal del Consumidor, es competente para conocer y resolver en la vía administrativa de cualquier controversia que se suscite sobre la interpretación o cumplimiento del presente contrato, por lo que las partes están de acuerdo en someterse a ella en términos de ley, para resolver sobre la interpretación o cumplimiento de los términos del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-174-SCFI-2007, Prácticas Comerciales- Elementos de Información para la Prestación de Servicios en General y cualquier otra disposición aplicable. Sin perjuicio de lo anterior en caso de persistir la inconformidad, las partes se someten a la jurisdicción de los tribunales competentes del domicilio del PROVEEDOR, renunciando en forma expresa a cualquier otra jurisdicción o al fuero que pudiera corresponderles en razón de sus domicilios presente o futuros o por cualquier otra razón. 
                                                        </p>
                                                    </div>

                                                    <div class="col-sm-12" style="background-color: white;">
                                                        <div class="col-sm-6" align="center">
                                                            <img class="marcoImg" src="<?php if(isset($ccfirma_asesor)) { echo base_url().(($ccfirma_asesor != "") ? $ccfirma_asesor : 'statics/inventario/imgs/fondo_bco.jpeg'); } else{ echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; } ?>" id="ccfirma_asesor_Img" style="width: 2.5cm;">

                                                            <input type="hidden" id="ruta_ccfirma_asesor" name="ruta_ccfirma_asesor" value="<?php if(isset($ccfirma_asesor)) echo $ccfirma_asesor;?>">

                                                            <br>
                                                            <input type="text" class="input_field" name="cnombre_asesor" style="width:100%;" value="<?php if(isset($cnombre_asesor)) echo $cnombre_asesor;?>" <?php if(isset($id)) echo 'disabled'?>>

                                                            <div id="ruta_ccfirma_asesor_error"></div>
                                                            <br>
                                                            <div id="cnombre_asesor_error"></div>
                                                            <h6 style="text-align: center;">EL PROVEEDOR (NOMBRE Y FIRMA). <br>
                                                            OPERADOR </h6>

                                                        </div>

                                                        <div class="col-sm-6" align="center">
                                                            <img class="marcoImg" src="<?php if(isset($ccfirma_cliente)) { echo base_url().(($ccfirma_cliente != "") ? $ccfirma_cliente : 'statics/inventario/imgs/fondo_bco.jpeg'); } else{ echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; } ?>" id="ccfirma_cliente_Img" style="width: 2.5cm;">

                                                            <input type="hidden" id="ruta_ccfirma_cliente" name="ruta_ccfirma_cliente" value="<?php if(isset($ccfirma_cliente)) echo $ccfirma_cliente;?>">

                                                            <br>
                                                            <input type="text"  class="input_field" name="cnombre_cliente" style="width:100%;" value="<?php if(isset($cnombre_cliente)) echo $cnombre_cliente;?>" <?php if(isset($id)) echo 'disabled'?>>

                                                            <div id="ruta_firma_ccliente_error"></div>
                                                            <br>
                                                            <div id="cnombre_cliente_error"></div>
                                                            <h6 style="text-align: center;">EL CONSUMIDOR (NOMBRE Y FIRMA) <br>
                                                            CLIENTE  </h6>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12" style="background-color: white;">
                                                        <div class="col-sm-6" align="center">
                                                            <?php if (!isset($id)): ?>
                                                                <a class="cuadroFirma btn btn-primary" data-value="CAsesor1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                                            <?php endif ?>
                                                        </div>

                                                        <div class="col-sm-6" align="center">
                                                            <?php if (!isset($id)): ?>
                                                                <a class="cuadroFirma btn btn-primary" data-value="CCliente2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                                            <?php endif ?>
                                                        </div>

                                                        <br><br>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <br>
                                                        <h5 align="center" style="font-weight:bold;">
                                                            AVISO DE PRIVACIDAD 
                                                        </h5>
                                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                                            EL Aviso de Privacidad Integral podrá ser consultado en nuestra página en internet www.xehos.com/privacidad/, o lo puede solicitar al correo electrónico info@xehos.com, u obtener personalmente en el Área de Atención a la Privacidad.
                                                            <br>
                                                            (&nbsp;Si&nbsp;&nbsp;
                                                            <input type="radio" style="transform: scale(1.5);" value="1" name="privacidad_contrato" <?php if(isset($privacidad_contrato)){ if($privacidad_contrato == "1") echo "checked";}else { echo 'checked'; }?> <?php if(isset($id)) echo 'disabled'?>>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            No&nbsp;&nbsp;
                                                            <input type="radio" style="transform: scale(1.5);" value="0" name="privacidad_contrato" <?php if(isset($privacidad_contrato)) if($privacidad_contrato == "0") echo "checked";?> <?php if(isset($id)) echo 'disabled'?>> &nbsp;)
                                                            Al marcar el recuadro precedente y remitir mis datos, otorgo mi consentimiento para que mis datos personales sean tratados conforme a lo señalado en el Aviso de Privacidad.
                                                            <br>
                                                            Este contrato fue registrado ante la Procuraduría Federal del Consumidor. Registro público de contratos de adhesión y aprobado e inscrito con el número 
                                                            <input type="text" class="input_field" name="registro_publico" style="width:3cm;" value="<?php if(isset($registro_publico)) echo $registro_publico; else echo 'XX-XXXX'; ?>" <?php if(isset($id)) echo 'disabled'?> >, 
                                                            Expediente No. 
                                                            <input type="text" class="input_field" name="expediente_publico" style="width:5cm;" value="<?php if(isset($expediente_publico)) echo $expediente_publico; else echo 'XXX.XX.X.X-XXXXX-XXXX'; ?>" <?php if(isset($id)) echo 'disabled'?> >, 
                                                            de fecha 
                                                            <input type="text" class="input_field" name="registro_publico_dia" style="width:1cm;" value="<?php if(isset($registro_publico_dia)) echo $registro_publico_dia; else echo '01'; ?>" <?php if(isset($id)) echo 'disabled'?> > de 
                                                            <input type="text" class="input_field" name="registro_publico_mes" style="width:4cm;" value="<?php if(isset($registro_publico_mes)) echo $registro_publico_mes; else echo 'Octubre'; ?>" <?php if(isset($id)) echo 'disabled'?> > de 
                                                            <input type="text" class="input_field" name="registro_publico_anio" style="width:1.5cm;" value="<?php if(isset($registro_publico_anio)) echo $registro_publico_anio; else echo '2020'; ?>" <?php if(isset($id)) echo 'disabled'?> >. 
                                                            <br>
                                                            Cualquier variación del presente contrato en perjuicio de EL CONSUMIDOR, frente al contrato de adhesión registrado, se tendrá por no puesta. 
                                                        </p>
                                                        <h6 style="font-weight:bold;">
                                                            “DESEO ADHERIRME AL CONTRATO TIPO DE SERVICIO DE AUTOLAVADO DETALLADO Y SEMI DETALLO  DE VEHICULOS XEHOS AUTOLAVADO DE PROFECO” 
                                                        </h6>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12" style="background-color: white;" align="center">
                                                    <br>
                                                    <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                                                    <br>
                                                    <h5 class="error" id="formulario_error"></h5>
                                                    <?php if (isset($id)): ?>
                                                        <input type="hidden" name="envio_formulario" value="2">
                                                        <input type="button" class="btn btn-success" name="enviar_formulario" id="enviarOrdenForm" value="ACTUALIZAR">
                                                    <?php else: ?>
                                                        <input type="hidden" name="envio_formulario" value="1">
                                                        <input type="button" class="btn btn-success" name="enviar_formulario" id="enviarOrdenForm" value="GUARDAR">
                                                    <?php endif ?>
                                                    <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                                                    <input type="hidden" name="id_infop" value="<?php if(isset($id_orden)) echo $id_orden; else echo '0'; ?>">
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal para la firma del quien elaboro el diagnóstico-->
        <div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index: 30000;">
            <div class="modal-dialog modal-lg" role="document" style="z-index: 30001;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="firmaDigitalLabel"></h5>
                    </div>
                    <div class="modal-body" align="center">
                        <div class="signatureparent_cont_1">
                            <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                                Su navegador no soporta canvas
                            </canvas>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="" id="destinoFirma" value="">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                        <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                        <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ base_url('statics/js/jquery.min.js') }}"></script>
        <script src="{{ base_url('statics/js/bootbox.min.js') }}"></script>
        <!--<script src="{{ base_url('statics/js/general.js') }}"></script>-->
        <script src="{{ base_url('statics/js/isloading.js') }}"></script>
        <script src="{{ base_url('statics/js/utils.js') }}"></script>
        <script src="{{ base_url('statics/js/sweetalert2.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/popper.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/plugins.js') }}"></script>

        <!-- ---   --- -- --- -->
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/datatables.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/jszip.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/pdfmake.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/vfs_fonts.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/buttons.html5.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/buttons.print.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/buttons.colVis.min.js') }}"></script>
        <script src="{{ base_url('statics/tema/assets/js/lib/data-table/datatables-init.js') }}"></script>

        <!-- ---   --- -- --- -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>

        <script src="{{ base_url('statics/js/inventario/interacciones_check.js') }}"></script>
        <script src="{{ base_url('statics/js/inventario/envio_form_ext.js') }}"></script>
        <script src="{{ base_url('statics/js/inventario/diagrama_danos.js') }}"></script>
        <script src="{{ base_url('statics/js/inventario/firma.js') }}"></script>

        <script>
            function revisar_datos() {
                var base = $("#sitio").val();
                var servicio = $("input[name='orden']").val();

                if (servicio != "") {
                    $("#cargaIcono").css("display","inline-block");
                    $.ajax({
                        url: base+"inventario/Inv_Exterior/recuperar_datos",
                        method: 'post',
                        data: {
                            orden: servicio,
                        },
                        success:function(resp){ 
                          console.log(resp);
                            if (resp.indexOf("handler           </p>")<1) { 
                                var respuesta = resp.split("=");

                                if (respuesta[0] == "EDITA") {
                                    //Redireccionamos al formulario de alta
                                    location.href = base+"index.php/inventario/Inv_Exterior/generar_Revision/"+respuesta[1];
                                }else{
                                    if (respuesta[1] == "") {
                                        $("#errorConsultaServer").text("No se encontro ningun servicio abierto con ese folio");
                                        $("input").attr("disabled",true);
                                        $("input[name='orden']").attr("disabled",false);
                                    } else {
                                        $("#errorConsultaServer").text("");
                                        $("input").attr("disabled",false);
                                        
                                        $("input[name='id_infop']").val(respuesta[1]);
                                        //$("input[name='orden']").val(respuesta[1]);
                                        //$("input[name='folio']").val(respuesta[1]);

                                        $("input[name='nombre_asesor']").val(respuesta[2]);
                                        $("input[name='cnombre_asesor']").val(respuesta[2]);

                                        $("input[name='nombre_cliente']").val(respuesta[3]);
                                        $("#h_cliente").text(respuesta[3]);
                                        $("input[name='cnombre_cliente']").val(respuesta[3]);

                                        $("input[name='costo_diagnostico']").val(respuesta[4]);
                                        $("input[name='cantidad_contrato']").val(respuesta[4]);

                                        $("input[name='orden']").val(respuesta[5]);
                                        $("input[name='folio']").val(respuesta[5]);
                                    }
                                }
                            }
                            $("#cargaIcono").css("display","none");
                        //Cierre de success
                        },
                        error:function(error){
                            console.log(error);
                            $("#cargaIcono").css("display","none");
                        //Cierre del error
                        }
                    //Cierre del ajax
                    });
                }else{
                    $("#errorConsultaServer").text("No se indco el No. de orden");
                }
            }
        </script>
    </body>                          	
	
</html>