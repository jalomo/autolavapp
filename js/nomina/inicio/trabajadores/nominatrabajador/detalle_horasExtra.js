var _appsFunction = function () {
	this.init = function () {},

	this.get = function () {
		
		$('table#table_content').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "desc" ]],
			ajax: {
				url: PATH + '/nomina/inicio/horas_extras/index_get',
				type: "POST",
				data: function(){
					return {
						id: id
					};
				}
			},
			columns: [
				{
					title: 'Fecha',
					data: 'Fecha',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'Horas',
					data: 'Horas'
				}
			],
			initComplete: function(settings, data) {
				$('table#table_content').append(
					$('<tfoot/>').append( $("table#table_content thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
	
});
