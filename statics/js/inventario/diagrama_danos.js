//Cargamos la imagen para daños
var canvas_3 = document.getElementById('canvas_3');
var ctx_3 = canvas_3.getContext("2d");
var background = new Image();

if ($("#danosMarcas").val() == "") {
  background.src = $("#direccionFondo").val();
} else {
  background.src = $("#danosMarcas").val();
}

background.onload = function(){
  ctx_3.drawImage(background,0,0,420,500);
}

$("#canvas_3").click(function(e){
  getPosition(e);
});

// var pointSize = 3;
function getPosition(event){
  var rect = canvas_3.getBoundingClientRect();
  var x = event.clientX - rect.left;
  var y = event.clientY - rect.top;
  //Recuperamos el tipo de marca que se pondra
  var marca = $("input[name='marcasRadio']:checked").val();
  switch(marca)
  {
      case 'hit':
          draw(ctx_3, x, y, 8);
      break;
      case 'broken':
          drawX(ctx_3,x, y, 3);
      break;
      case 'scratch':
          drawZ(ctx_3,x, y, 5);
      break;
      default:
      break;
  }
  // drawCoordinates(x,y);
}

//Funciones de dibujo para diagrama de daños
function draw(ctx,x,y,size) {
  ctx.fillStyle = "#ffffff"
  ctx.strokeStyle = "#ff0000"
  ctx.beginPath()
  ctx.arc(x, y, size, 0, Math.PI*2, true)
  ctx.closePath()
  ctx.fill()
  ctx.stroke()
}

function drawX(ctx,x,y,size){
   ctx.beginPath();
   ctx.strokeStyle = "#00ff00";
   ctx.moveTo(x - 15, y - 15);
   ctx.lineTo(x + 15, y + 15);

   ctx.moveTo(x + 15, y - 15);
   ctx.lineTo(x - 15, y + 15);
   ctx.stroke();
}

function drawZ(ctx,x,y,size){
   // ctx.font = "15px Arial";
   ctx.fillStyle = "#0000ff";
   ctx.font = "20px Arial";
   ctx.fillText("#", x, y);
}

//Orden de servicio descarga de imagenes
if ($("input[name='envio_formulario']").val() == "1") {
  var button = document.getElementById('btn-download');
  button.addEventListener('click', function (e) {
      var dataURL = canvas_3.toDataURL('image/png');
      button.href = dataURL;
  });
}

function draw(ctx,x,y,size) {
  ctx.fillStyle = "#ffffff"
  ctx.strokeStyle = "#ff0000"
  ctx.beginPath()
  ctx.arc(x, y, size, 0, Math.PI*2, true)
  ctx.closePath()
  ctx.fill()
  ctx.stroke()
}

//Presionamos el boton para resetear la imagen (diagrama)
$("#resetoeDiagrama").on('click',function(){
    background.src = $("#direccionFondo").val();
}); 


/*********************************Medidor de gasolina *****************************************/
//Inicializar progress
var base_img = $("#sitio").val();
$("#slider-2").css('background', '#ffffff');
$("#slider-2").css('width', '200px');

if ($("input[name='nivel_gasolina']").val() == 0) {
  $("#slider-2").css('background', '#ffffff');
}else if(($("input[name='nivel_gasolina']").val() >= 1) && ($("input[name='nivel_gasolina']").val() < 4)){
  $('#slider-2').css('background','#ff0000');
}else if (($("input[name='nivel_gasolina']").val() >= 4) && ($("input[name='nivel_gasolina']").val() < 7)) {
  $('#slider-2').css('background','#ffb400');
}else if (($("input[name='nivel_gasolina']").val() >= 7) && ($("input[name='nivel_gasolina']").val() < 11)) {
  $("#slider-2").css('background', '#ffff00');
}else if($("input[name='nivel_gasolina']").val() >= 11){
  $("#slider-2").css('background', '#00ff00');
}

if ($("input[name='nivel_gasolina']").val() == "0") {
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_a.png');
}else if ($("input[name='nivel_gasolina']").val() == "1") {
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_b.png');
}else if($("input[name='nivel_gasolina']").val() == "2"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_c.png');
}else if($("input[name='nivel_gasolina']").val() == "3"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_d.png');
}else if($("input[name='nivel_gasolina']").val() == "4"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_e.png');
}else if($("input[name='nivel_gasolina']").val() == "5"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_f.png');
}else if($("input[name='nivel_gasolina']").val() == "6"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_g.png');
}else if($("input[name='nivel_gasolina']").val() == "7"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_h.png');
}else if($("input[name='nivel_gasolina']").val() == "8"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_i.png');
}else if($("input[name='nivel_gasolina']").val() == "9"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_j.png');
}else if($("input[name='nivel_gasolina']").val() == "10"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_k.png');
}else if($("input[name='nivel_gasolina']").val() == "11"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_l.png');
}else if($("input[name='nivel_gasolina']").val() == "12"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_m.png');
}else if($("input[name='nivel_gasolina']").val() == "13"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_n.png');
}else if($("input[name='nivel_gasolina']").val() == "14"){
    $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_o.png');
}


$("#slider-2").slider({
    range:false,
    min: 0,
    max: 14,
    //values: [ 35, 200 ],
    value:$("input[name='nivel_gasolina']").val(),
    slide: function(event, ui) {

        //console.log(ui.value);
        $("input[name='nivel_gasolina']").val(ui.value);
        var nivel = $("input[name='nivel_gasolina']").val();

        if((nivel >= 0) && (nivel < 4)){
          $('#slider-2').css('background','#ff0000');
        }else if ((nivel >= 4) && (nivel < 7)) {
          $('#slider-2').css('background','#ffb400');
        }else if ((nivel >= 7) && (nivel < 11)) {
          $("#slider-2").css('background', '#ffff00');
        }else if(nivel >= 11){
          $("#slider-2").css('background', '#00ff00');
        }

        if (nivel == 0) {
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_a.png');
        }else if (nivel == 1) {
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_b.png');
        }else if(nivel == 2){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_c.png');
        }else if(nivel == 3){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_d.png');
        }else if(nivel == 4){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_e.png');
        }else if(nivel == 5){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_f.png');
        }else if(nivel == 6){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_g.png');
        }else if(nivel == 7){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_h.png');
        }else if(nivel == 8){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_i.png');
        }else if(nivel == 9){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_j.png');
        }else if(nivel == 10){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_k.png');
        }else if(nivel == 11){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_l.png');
        }else if(nivel == 12){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_m.png');
        }else if(nivel == 13){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_n.png');
        }else if(nivel == 14){
            $("#nivel_gasolina_img").attr('src', base_img+'statics/inventario/imgs/gasolina_o.png');
        }
    }
});