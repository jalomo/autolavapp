<table class="table table-bordered">
    <thead>
        <tr>
            <th>Fecha Servicio</th>
            <th>Lavador</th>
            <th>Servicio</th>
            <th>Cliente</th>
            <th>Placas</th>
            <th>Modelo</th>
            <th>Marca</th>
            <th>Sucursal</th>
            <th>Precio</th>
        </tr>
    </thead>
    <tbody>
        <?php $total = 0; ?>
        @foreach ($info as $i => $data)
            <tr>
                <td>{{ $data->fecha_creacion_servicio }}</td>
                <td>{{ $data->lavadorNombre }}</td>
                <td>{{ $data->servicioNombre }}</td>
                <td>{{ $data->nombre . ' ' . $data->apellido_paterno . ' ' . $data->apellido_materno }}</td>
                <td>{{ $data->placas }}</td>
                <td>{{ $data->modelo }}</td>
                <td>{{ $data->marca }}</td>
                <td>{{ $data->sucursal }}</td>
                <td>${{ number_format($data->precio, 2) }}</td>
                <?php $total = $total + $data->precio; ?>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <td colspan="9">
            <h4 class="text-right">Total: ${{ number_format($total, 2) }}</h4>
        </td>
    </tfoot>
</table>