@layout('layout')
@section('contenido')
<style>
    .submenu{
        font-size: 15px;
        font-weight: bold;
    }
</style>
<h1>Refacciones</h1>
<br>
<div class="row">
    <div class="col-sm-4">
        <h3>Movimientos</h3>
        <hr>
        <span class="submenu">Almacén</span>
        <div class="col-sm-12">
            <ul>
                <li>
                    <a target="_blank" href="{{base_url('refacciones/productos/listado')}}">Listado productos</a>
                </li>
                <li>
                    <a target="_blank" href="{{base_url('refacciones/productos/stock')}}">Stock productos</a>
                </li>
                <li>
                    <a target="_blank" href="{{base_url('refacciones/almacenes/generarTraspaso')}}">Traspaso almacén</a>
                </li>
                <li>
                    <a target="_blank" href="{{base_url('refacciones/almacenes/listadoremplazos')}}">Reemplazos</a>
                </li>
                <li>
                    <a target="_blank" href="{{base_url('refacciones/productos/pedidoSugerido')}}">Pedido sugerido</a>
                </li>
                <li>
                    <a target="_blank" href="{{base_url('refacciones/productos/seleccionaInventario')}}">Inventario</a>
                </li>
                <li>
                    <a target="_blank" href="{{base_url('refacciones/productos/crearPedido')}}">Pedido piezas</a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <span class="submenu">Salidas</span>
        <div class="col-sm-12">
            <ul>
                <li> <a target="_blank" href="{{base_url('refacciones/salidas/listadoVentas')}}">Listado de ventas</a></li>
                <li> <a target="_blank" href="{{base_url('refacciones/salidas/ventasMostrador')}}">Ventas mostrador</a></li>
                <li> <a target="_blank" href="{{base_url('refacciones/salidas/devolucion')}}">Dev proveedores</a></li>
                <li> <a target="_blank" href="{{base_url('refacciones/salidas/listadoDevoluciones')}}">Dev realizadas</a></li>
                <li> <a target="_blank" href="{{base_url('refacciones/polizas?tipo_poliza=1/#')}}">Poliza ventas</a></li>
                <li> <a target="_blank" href="{{base_url('rrefacciones/polizas?tipo_poliza=2/#')}}">Poliza dev. prod</a></li>
                <li> <a target="_blank" href="{{base_url('refacciones/polizas/#')}}">Poliza otras salidas</a></li>
            </ul>
        </div>
    </div>
    <div class="col-sm-4">
        <h3>Facturas</h3>
        <hr>
        <span class="submenu">Listado</span>
        <div class="col-sm-12">
            <ul>
                <li> <a target="_blank" href="{{base_url('refacciones/factura/listado')}}">Listado</a></li>
            </ul>
        </div>
    </div>
    <div class="col-sm-4">
    <h3>Consultas</h3>
    <hr>
    <span class="submenu">Req mostrador</span>
    <div class="col-sm-12">
        <ul>
            <li> <a target="_blank" href="{{base_url('refacciones/kardex/#')}}">Kardex</a></li>
            <li> <a target="_blank" href="{{base_url('refacciones/Productos/busquedaFoliosTipoCuenTa')}}">Búsqueda por folios</a></li>
        </ul>
    </div>
    <div class="clearfix"></div>
    <span class="submenu">Compras</span>
    <div class="col-sm-12">
        <ul>
            <li> <a target="_blank" href="{{base_url('refacciones/entradas/listadoEntradas')}}">Listado de compras</a></li>
            <li> <a target="_blank" href="{{base_url('refacciones/entradas/autorizarpedido')}}">Autorizar pedidos</a></li>
        </ul>
    </div>
    <div class="clearfix"></div>
    <span class="submenu">Polizas</span>
    <div class="col-sm-12">
        <ul>
            <li> <a target="_blank" href="{{base_url('refacciones/polizas?tipo_poliza=1/#')}}">Consultar</a></li>
        </ul>
    </div>
</div>
</div>
@endsection