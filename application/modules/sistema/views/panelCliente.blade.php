@layout('layoutuser')
@section('contenido')

<div class="container-fluid panel-body">
	<h1 class="mt-4">Panel del Cliente</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item ">{{ ucwords($titulo) }}</li>
		<li class="breadcrumb-item">{{ ($titulo_dos) }}</li>
	</ol>
	@if(isset($datos) && $datos['telefono'])

	<div class="row">
		<div class="col-md-3">
			<h2>Mis datos</h2>
			{{ $datos['nombre']}} {{ $datos['apellido_paterno']}} {{ $datos['apellido_materno']}} <br />
			{{ $datos['telefono'] }} <br />
			{{ $datos['email']}}
			<input type="hidden" value="{{ $datos['telefono']}}" id="telefono_remitente" />
		</div>
		<div class="col-md-9">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#operadores">Operadores</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#lavadores">Lavadores</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="operadores" class="container tab-pane active"><br>
					<h3>Listado de Operadores</h3>
					<div class="row">
						<?php
						if (isset($usuarios) && $usuarios) {
							foreach ($usuarios as $usuario) {  ?>
								<div class="col-md-4 mt-3">
									<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:160px">
										<div style="padding:10px" class="">
											<i class="fa fa-user-circle fa-3x"></i>
											<h4>{{ ucwords($usuario->adminNombre) }}</h4>
											<p>
												<b> {{ $usuario->cargo }}</b>
											</p>
											<button class="btn btn-default btn-lg" style="background-color:#fff; color:#323232; width:100%;" title="Comenzar chat" onclick="gotoChat(this)" data-telefono="{{ $usuario->telefono }}"><i style="color:#22ae22" class="fa fa-comment fa-2x"></i> <span style="font-size:16px">Comenzar chat</span></button>
										</div>
									</div>
								</div>
						<?php
							}
						} ?>
					</div>
				</div>
				<div id="lavadores" class="container tab-pane"><br>
					<h3>Listado de Lavadores</h3>
					<div class="row">
						<?php
						if (isset($lavadores) && $lavadores) {
							foreach ($lavadores as $lavador) {  ?>
								<div class="col-md-4 mt-3">
									<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:160px">
										<div style="padding:10px" class="">
											<i class="fa fa-user-circle fa-3x"></i>
											<h4>{{ $lavador->lavadorNombre}}</h4>
											<button class="btn btn-default btn-lg mt-4" style="background-color:#fff; color:#323232; width:100%;" title="Comenzar chat" onclick="gotoChat(this)" data-telefono="{{ $lavador->lavadorTelefono }}"><i style="color:#22ae22" class="fa fa-comment fa-2x"></i> <span style="font-size:16px">Comenzar chat</span></button>
										</div>
									</div>
								</div>

						<?php
							}
						} else {
							echo '<h4 class="mt-5 mb-5 text-center">Sin lavadores asignados para el servicio</h4>';

						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	@else
	<h3 class="mt-5 mb-5 text-center">El cliente no esta registrado en el sistema</h3>
	@endif
</div>
@endsection
<script type="text/javascript">
	var site_url = "{{site_url()}}";

	function gotoChat(_this) {
		window.location.href = site_url + '/sistema/panelCliente/chat?telefono_remitente=' + document.getElementById("telefono_remitente").value + '&telefono_destinatario=' + $(_this).data('telefono')
	}
</script>