function inicializaTabla() {
	var tabla_stock = $('#tabla_stock').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/productos/listadoStock",
			type: 'GET',
		},
		columns: [{
				title: "#",
				data: 'id',
			},
			{
				title: "No. identificación",
				data: 'no_identificacion',
			},
			{
				title: "Descripcion",
				data: 'descripcion',
			},
			{
				title: "Precio unitario",
				data: 'valor_unitario',
			},
			{
				title: "Unidad",
				data: 'unidad',
			},
			{
				title: "Cantidad actual",
				data: 'cantidad_actual',
			},
			{
				title: "Almacen primario",
				data: 'cantidad_almacen_primario'
			},
			{
				title: "Almacen secundario",
				data: 'cantidad_almacen_secundario'
			}
			
		]
	});
}

this.inicializaTabla();

