@layout('layout')

@section('included_css')

@endsection

    <style>
        .error div {
            color: red
        }

        input[type="checkbox"] {
            transform: scale(1.5);
        }

        input[type="number"]::-webkit-outer-spin-button, 
        input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type="number"] {
            -moz-appearance: textfield;
        }

        #cargaIcono {
            /*-webkit-animation: rotation 1s infinite linear;*/
            font-size: 55px !important;
            color: darkblue;
            display: none;
        }

    </style>

@section('contenido')    
    <div class="row">
        <div class="col-sm-12">
            <h3> 
                Cupón: 
                <span style="color:darkblue;"><?= ((isset($cupon)) ? $cupon : "") ?></span>
            </h3>
            <br>
            <h6>
                Descripción del cupón:
                <span style="color:darkblue;"><?= ((isset($descripcion)) ? $descripcion : "") ?></span>
            </h6>

            <hr>
            <h4> 
                Recomendador: 
                <span style="color:darkblue;"><?= ((isset($recomendador)) ? $recomendador : "") ?></span>
            </h4>
            <h4> 
                Bonificación por uso: 
                <span style="color:darkblue;">$<?= ((isset($beneficio)) ? number_format($beneficio,2) : "0.00") ?></span>
            </h4>
        </div>
    </div>
    
    <br><hr>
    <div class="row">
        <div class="col-sm-12">
            <h5 style="text-align: center;"> Historial de usos</h5>
            <br>
            <table <?php if ($listado != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Cliente</th>
                        <th>Telefono</th>
                        <th>Fecha de Uso</th>
                        <th>Bonificación</th>
                    </tr>
                </thead>
                <tbody id="cuerpo">
                    <?php $sub_total = 0; ?>
                    @if ($listado != NULL)
                        @foreach ($listado as $i => $cupon)
                            <tr>
                                <td style="vertical-align: middle;" align="center">
                                    <?= $i + 1;?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= $cupon->usuario ?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= $cupon->telefono ?>
                                </td>
                                <td style="vertical-align: middle;" align="center">
                                    <?php 
                                        $fecha2 = new DateTime($cupon->fecha_uso);
                                        echo $fecha2->format('d-m-Y')." ";
                                        echo $fecha2->format('H:i');
                                    ?>
                                </td>
                                <td style="vertical-align: middle;" align="center">
                                    <?= number_format($beneficio,2) ?>
                                    <?php $sub_total += $beneficio; ?>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" align="center">
                                Sin registros
                            </td>
                        </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td style="vertical-align: middle;" align="right" colspan="4">
                            Subtotal:
                        </td>
                        <td style="vertical-align: middle;" align="center">
                            <?= "$ ".number_format($sub_total,2) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;" align="right" colspan="4">
                            Pagado:
                        </td>
                        <td style="vertical-align: middle;" align="center">
                            <?php $pagado = ((isset($monto_pagado)) ? $monto_pagado : 0) ?>
                            <?= "$ ".number_format($pagado,2) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;" align="right" colspan="4">
                            Total:
                        </td>
                        <td style="vertical-align: middle;" align="center">
                            <?php $a_pagar = $sub_total - $pagado; ?>
                            <?= "$ ".number_format($a_pagar,2) ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <br><br>
    
    <form id="formulario_reporte" method="post" action="">
        <div class="row">
            <div class="col-sm-12" align="center">
                <br>
                <i id="cargaIcono" class="fa fa-spinner fa-spin"></i>
                <br>
                <h5 class="error" id="formulario_error"></h5>

                <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                <input type="hidden" name="cupon" value="<?= ((isset($id_cupon)) ? $id_cupon : "0") ?>">
                <input type="hidden" name="reco_user" value="<?= ((isset($id_reco)) ? $id_reco : "0") ?>">
                <input type="hidden" name="monto_pago" value="<?= ((isset($a_pagar)) ? $a_pagar : "0") ?>">  
                <input type="hidden" name="deposito" value="<?= ((isset($monto_pagado)) ? $monto_pagado : "0") ?>">     
                <br>    
                <input type="hidden" name="envio_formulario" value="1">
                <input type="button" class="btn btn-success" id="pagar_cupon" value="PAGAR" <?= (($a_pagar < 1) ? "disabled" : "") ?> >
            </div>
        </div>
    </form>

@endsection
@section('included_js')
    @include('main/scripts_dt')

    <script>
        $("#pagar_cupon").on('click', function (e){
            $("#cargaIcono").css("display","inline-block");

            // Evitamos que salte el enlace.
            e.preventDefault(); 
            $('input').attr('disabled',false);
            
            var paqueteDatos = new FormData(document.getElementById('formulario_reporte'));
            var orden = $("input[name='orden']").val();
            var base = $("#sitio").val();

            $.ajax({
                url: base+"index.php/cupones/cupones/pagar_bonificaciones",
                type: 'post',
                contentType: false,
                data: paqueteDatos,
                processData: false,
                cache: false,
                success:function(resp){
                    console.log(resp);
                    if (resp.indexOf("handler         </p>")<1) {
                        var respuesta = resp.split("_");
                        if (respuesta[0] == "OK") {
                            location.href = base+"index.php/cupones/index";
                        } else {
                            $("#formulario_error").text(respuesta[0]);
                        }

                    }else{
                        $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                    }
                    $("#cargaIcono").css("display","none");
                //Cierre de success
                },
                  error:function(error){
                    console.log(error);
                    $("#cargaIcono").css("display","none");
                    $("#formulario_error").text("ERROR AL ENVIAR");
                }
            });
        });
    </script>
@endsection