<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Api_Cupones extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Recomendadores', 'consulta', TRUE);
        //$this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
        //if($this->session->userdata('id')){}else{redirect('login/');}
    }


    public function index()
    {
    	echo "hola";
    }

    public function validar_cupon() 
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $cupon = strtoupper($_POST["cupon"]);
        $id_usuario = $_POST["id_usuario"];
        $contenedor = [];

        //consultamos el cupon
        $datos_cupon = $this->consulta->get_result("cupon",$cupon,"cupones");
        foreach ($datos_cupon as $row){
            $id_cupon = $row->id;
            $limite_uso = $row->limite_uso;
            $fecha_caducidad = $row->fecha_caducidad;
            
            $indefinido = $row->sin_caducidad;
            $descuento = $row->descuento;
        }

        //Validamos que exista el cupon
        if (isset($id_cupon)) {
            $usos = $this->consulta->usos_cupon($id_cupon);
            $uso_cupon = $this->consulta->usos_cupon_usuario($id_cupon,$id_usuario);

            //Verificamos si el usuario no ha usado previamente el cupon
            if ($uso_cupon == "0") {
                //Si el campo "sin fecha de caducidad" fue activado
                if ($indefinido == "1") {
                    //Si el cupon fue marcado como "sin limite de uso"
                    if ($limite_uso == "0") {
                        //Cupon valido
                        $contenedor = array(
                            "id_cupon" => $id_cupon,
                            "descuento" => $descuento,
                            "error" => ""
                        );

                    //De lo contrario, validamos los usos limite del cupon
                    }else{
                        //Si los usos del cupon no han excedido el limite
                        if ($limite_uso >= $usos) {
                            //Cupon valido
                            $contenedor = array(
                                "id_cupon" => $id_cupon,
                                "descuento" => $descuento,
                                "error" => ""
                            );

                        //De lo cntrario, si se supoero el limite de usos el cupon ya no se puede usar
                        } else {
                            //Cupon invalido
                            $contenedor = array(
                                "id_cupon" => $id_cupon,
                                "descuento" => $descuento,
                                "error" => "Cupon agotado"
                            );
                        }
                    }

                //De lo contrario, validamos si el cupon no ha caducado
                }else{
                    //Si la fecha de vencimiento , no se ha cumplido
                    if ($fecha_caducidad >= date("Y-m-d")) {
                        if ($limite_uso == "0") {
                            //Cupon valido
                            $contenedor = array(
                                "id_cupon" => $id_cupon,
                                "descuento" => $descuento,
                                "error" => ""
                            );

                        //De lo contrario, validamos los usos limite del cupon
                        }else{
                            //Si los usos del cupon no han excedido el limite
                            if ($limite_uso >= $usos) {
                                //Cupon valido
                                $contenedor = array(
                                    "id_cupon" => $id_cupon,
                                    "descuento" => $descuento,
                                    "error" => ""
                                );

                            //De lo cntrario, si se supoero el limite de usos el cupon ya no se puede usar
                            } else {
                                //Cupon invalido
                                $contenedor = array(
                                    "id_cupon" => $id_cupon,
                                    "descuento" => $descuento,
                                    "error" => "Cupon agotado"
                                );
                            }
                        }
                    
                    //De lo contrario, el cupon esta caducado
                    } else {
                        //Cupon invalido
                        $contenedor = array(
                            "id_cupon" => $id_cupon,
                            "descuento" => $descuento,
                            "error" => "Cupon caducado"
                        );
                    }
                    
                }
            //De lo contrario, el cupon ya no se puede usar nuevamente por el mismo usuario
            } else {
                //Cupon invalido
                $contenedor = array(
                    "id_cupon" => NULL,
                    "descuento" => 0,
                    "error" => "Este cupon ya ha sido usado por el usuario."
                );
            }
        }else{
            //Cupon invalido
            $contenedor = array(
                "id_cupon" => NULL,
                "descuento" => 0,
                "error" => "No se encontro el cupon."
            );
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    public function usar_cupon()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $cupon = $_POST["id_cupon"];
        $id_cliente = $_POST["id_cliente"];
        $registro = date("Y-m-d H:i:s"); 

        $usuario = $this->consulta->get_result("id",$id_cliente,"usuarios");
        foreach ($usuario as $row){
            $cliente = $row->nombre;
            $telefono = $row->telefono;
        }

        $result = [];

        if (isset($cliente)) {
            $contenedor = array(
                "id" => NULL,
                "id_cupon" => $cupon,
                "id_usuario" => $id_cliente,
                "usuario" => $cliente,
                "telefono" => $telefono,
                "fecha_uso" => $registro
            );

            $historial = $this->consulta->save_register('cupon_historico',$contenedor);

            if ($historial != 0) {
                $result = array(
                    "error" => "ok"
                );
            } else {
                $result = array(
                    "error" => "No se guardo el movimiento"
                );
            }
            
        }else{
            $result = array(
                "error" => "No se encontro el usuario"
            );
        }

        $respuesta = json_encode($result);
        echo $respuesta;
    }

}