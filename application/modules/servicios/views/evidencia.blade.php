<div id="divfileupload" style="{{(($evidencia != '') ? 'display:none;' : '')}}">
	<span class="btn btn-success fileinput-button btn-xs" title="Agregar evidencia">
	    <i class="fa fa-plus"></i>
    <span>Agregar evidencia</span>
	    <!-- The file input field used as target for the file upload widget -->
	    <input id="fileupload_evidencia" type="file" name="file[]" data-url="{{ site_url('servicios/upload_evidencia/').$id }}" multiple class="fileupload_evidencia btn-success">
	</span>

	<div id="progress_evidencia" class="progress_evidencia" style="display:none;">
	    <div class="progress-bar_evidencia progress-bar-azul"></div>
	</div>
</div>

<div id="files_evidencia" class="files_evidencia">
<?php if($evidencia != ''){ ?>
	<?php echo anchor(base_url('statics/evidencia/'.$evidencia),'<i class="fa fa-save-file"></i> Descargar','title="'.$evidencia.'" class="btn btn-info btn-xs" target="_blank"') ?>
	<?php echo anchor('servicios/upload_delete_evidencia/'.$evidencia,'<i class="fa fa-trash"></i>','data-id="" data-pdf="'.$evidencia.'" title="Eliminar'.$evidencia.'" class="btn btn-xs eliminar_evidencia text-centernteright" target="_blank"') ?>

<?php } ?>
</div>


