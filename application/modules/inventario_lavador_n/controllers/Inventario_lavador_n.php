<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Inventario_lavador_n extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta(){
      $data['titulo'] = "Inventario lavador" ;
      $data['titulo_dos'] = "Alta " ;
      $data['rows'] = $this->Mgeneral->get_table('inventario');
      $this->blade->render('inventario_lavador_n/alta', $data);
    }

    
    public function alta_guardar(){
        $data['nombre'] = $this->input->post('nombre');
        
        $data['activo'] = 1;
       $this->Mgeneral->save_register('inventario', $data);
      }


      public function editar($id){
        $data['titulo'] = "Sucursal" ;
        $data['titulo_dos'] = "editar Sucursal" ;
        $data['row'] = $this->Mgeneral->get_row('id',$id,'inventario');
        $this->blade->render('inventario_lavador_n/editar', $data);
      }

      public function alta_editar($id){
        $data['nombre'] = $this->input->post('nombre');
        
        $data['activo'] = 1;
        
       $this->Mgeneral->update_table_row('inventario',$data,'id',$id);
      }

    
}