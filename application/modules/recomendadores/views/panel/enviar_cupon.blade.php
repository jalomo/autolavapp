@layout('recomendadores/layout')

@section('included_css')

@endsection

@section('contenido')
<div class="row">
    <div class="col-sm-12">
        <form id="formulario_cupon" method="post" action="" autocomplete="on" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="color:darkblue;">
                        <a href="#" class="btn btn-menu-titulo">
                            <i class="fa fa-ticket" aria-hidden="true"></i>
                        </a>
                        <?php if(isset($titulo_dos)): ?>
                            <?php echo $titulo_dos; ?>
                        <?php endif; ?>
                    </h3>
                </div>
            </div>
            
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Clave : </label>&nbsp;
                    <input type="text" class="form-control" name="clave" value="<?php if(isset($cupon)) echo $cupon; ?>" style="width:100%;text-transform: uppercase;" disabled>
                    <div id="cupon_error"></div>
                </div>
                <div class="col-sm-8">
                    <label for="">Descripción : </label>&nbsp;
                    <input type="text" class="form-control" name="descripcion" value="<?php if(isset($descripcion)) echo $descripcion; else echo 'Cliente recomendador'; ?>" style="width:100%;" disabled>
                    <div id="descripcion_error"></div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Limite de usos : </label>
                    <input type="text" class="form-control" name="limite" value="<?php if(isset($limite_uso)) echo (($limite_uso == '0') ? 'Sin limite de uso' : $limite_uso); ?>" style="width:100%;" disabled>
                    <div id="limite_error"></div>
                </div>
                <div class="col-sm-4">
                    <label for="">Descuento (%) : </label>
                    <input type="text" class="form-control" name="descuento" value="<?php if(isset($descuento)) echo $descuento; else echo '0'; ?>" style="width:100%;" disabled>
                    <div id="descuento_error"></div>
                </div>
                <div class="col-sm-4">
                    <label for="">Bonificación por uso : </label>
                    <input type="text" class="form-control" name="beneficio" value="<?php if(isset($beneficio)) echo $beneficio; else echo '0'; ?>" style="width:100%;" disabled>
                    <div id="beneficio_error"></div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha de activación : </label>
                    <input type="text" class="form-control" name="fecha_activo" id="fecha_activo" value="<?php if(isset($fecha_activacion)) echo $fecha_activacion; ?>" style="width:100%;" disabled>
                    <div id="fecha_activo_error"></div>
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha de Caducidad : </label>
                    <input type="text" class="form-control" name="fecha_caducidad" id="fecha_caducidad" value="<?php if(isset($fecha_caducidad)) echo (($indefinido == '1') ? 'Sin caducidad' : $fecha_caducidad); ?>" style="width:100%;" disabled>
                    <div id="fecha_caducidad_error"></div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-3">
                    <label for="">Servicios en aplica: </label>
                </div>
                <div class="col-sm-9">
                    <table>
                        <?php 
                            $indice = 1; 
                            $servicios_txt = "";
                        ?>
                        @if(isset($pqt))
                            @foreach ($servicios as $serv)
                                @if(in_array($serv->servicioId, $pqt))
                                    <tr>
                                        <td style="vertical-align: middle;" align="center">
                                            <?= $indice++; ?>.-
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <?= $serv->servicioNombre ?>
                                        </td>
                                    </tr>
                                    <?php $servicios_txt .= $serv->servicioNombre.","; ?>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td style="vertical-align: middle;">
                                    &nbsp;&nbsp; <label for="">Sin datos</label>   
                                </td>
                            </tr>
                        @endif                            
                    </table>
                    <input type="hidden" name="servicios" value="<?= $servicios_txt ?>">
                    <div id="servicios_error"></div>
                </div>
            </div>

            <br>
            <h5>Enviar cupón a:</h5>
            <hr>

            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table style="width: 100%;">
                        <?php $renglon = 1 ; ?>
                        <tr>
                            @foreach ($mis_contatos as $contact)
                                <td style="width: 10%;height: 30px;">
                                    <input type="checkbox" class="form-control contacto" name="contactos[]" value="{{ $contact->id}}">
                                </td>
                                <td style="vertical-align: middle;">
                                    {{ $contact->contacto}} ({{ $contact->telefono}})
                                </td>
                                <td style="width: 10%;">
                                    <br>
                                </td>

                                @if($renglon %2 == 0)
                                    </tr>
                                    <tr>
                                @endif
                                <?php $renglon++; ?>
                            @endforeach   
                        </tr>                     
                    </table>
                    <div id="contacto_error"></div>
                </div>
            </div>

            <div class="col-sm-12" align="center">
                <br>
                <i id="cargaIcono" class="fa fa-spinner fa-spin"></i>
                <br>
                <h5 class="error" id="formulario_error"></h5>

                <input type="hidden" name="envio_formulario" value="0">
                <input type="button" class="btn btn-success" name="enviar" id="enviar_cupon" value="ENVIAR CUPÓN">

                <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                <input type="hidden" name="listado" value="<?php if(isset($id_cupon)) echo $id_cupon; else echo '0'; ?>">
                <input type="hidden" id="datos_1" value="<?php if(isset($datos_gral)) echo $datos_gral; else echo '0'; ?>">
                <input type="hidden" id="datos_2" value="<?php if(isset($datos_bco)) echo $datos_bco; else echo '0'; ?>">
                <br>    
            </div>
        </form>
    </div>
</div>    

<br>
<br>
<h5>Enviar cupón a otros contactos:</h5>
<hr>
<form id="formulario_contacto" method="post" action="" autocomplete="on">
    <div class="row">
        <div class="col-sm-5">
            <label for="">Nombre : </label>
            <input type="text" class="form-control" name="nombre" value="<?php if(isset($nombre)) echo $nombre; ?>">
            <div id="nombre_error"></div>
        </div>
        <div class="col-sm-3">
            <label for="">Telefono : </label>
            <input type="text" class="form-control" name="telefono" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($telefono)) echo $telefono; ?>">
            <div id="telefono_error"></div>
        </div>
        <div class="col-sm-4" align="center" style="margin-top: 26px;">
            <input type="hidden" name="clave2" value="<?php if(isset($cupon)) echo $cupon; ?>">
            <input type="hidden" name="listado2" value="<?php if(isset($id_cupon)) echo $id_cupon; else echo '0'; ?>">
            <input type="hidden" name="descuento2" value="<?php if(isset($descuento)) echo $descuento; ?>">
            <input type="button" class="btn btn-info" id="nvo_contacto" value="GUARDAR Y ENVIAR">
        </div>
    </div>
</form>

<br><br>
@endsection
@section('included_js')
    <script src="{{ base_url('statics/js/recomendadores/envio_cupon.js') }}"></script>

    <script>
        $(document).ready(function() {
            validar_datos();
        });


        const callbackDatosPerfil = () => {
            var base = $("#sitio").val();
            location.href = base+"index.php/recomendadores/panel/index";
        }

        const callbackDatosBanco = () => {
            var base = $("#sitio").val();
            location.href = base+"index.php/recomendadores/panel/datos_banco";
        }

        function validar_datos() {
            var datos_1 = $("#datos_1").val();
            var datos_2 = $("#datos_2").val();

            console.log(datos_1);
            console.log(datos_2);
            if ((datos_1 != "1")||(datos_2 != "1")) {
                if (datos_1 != "1") {
                    ConfirmCustom("La información general del usuario esta incompleta", callbackDatosPerfil, callbackDatosPerfil, "OK",
                    "Cerrar");
                }else{
                    ConfirmCustom("La información bancaria del usuario esta incompleta", callbackDatosBanco, callbackDatosBanco, "OK",
                    "Cerrar");
                }
            }
        }
    </script>
@endsection