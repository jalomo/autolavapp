@layout('recomendadores/layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-9 table-responsive">
            <h3 style="color:darkblue;">
                <a href="#" class="btn btn-menu-titulo">
                    <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                </a>
                <?php if(isset($titulo_dos)): ?>
                    <?php echo $titulo_dos; ?>
                <?php endif; ?>
            </h3>
        </div>
        <div class="col-sm-3" align="right">
            <a href="<?= base_url().'index.php/recomendadores/panel/bonificaciones' ?>" class="btn btn-warning">
                MIS BONIFICACIONES
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 table-responsive">        
            <br>
            <table <?php if ($datos_cupon != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Cupon</th>
                        <th>Descripción</th>
                        <th>Servicios que aplica</th>
                        <th>Teléfono</th>
                        <th>Fecha de envio</th>
                    </tr>
                </thead>
                <tbody id="cuerpo">
                    @if ($datos_cupon != NULL)
                        @foreach ($datos_cupon as $i => $registro)
                            <tr>
                                <td style="vertical-align: middle;" align="center">
                                    <?= $i + 1;?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= $registro->cupon ?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= $registro->descripcion ?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= str_replace(".","<br>",$registro->servicios) ?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= $registro->telefono ?>
                                </td>
                                <td style="vertical-align: middle;" align="center">
                                    <?php 
                                        $fecha = new DateTime($registro->fecha_envio);
                                        echo $fecha->format('d-m-Y');
                                        echo " ".$fecha->format('H:i');
                                    ?>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" align="center">
                                Sin cupones
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
            
    <br>
@endsection
@section('included_js')
    @include('main/scripts_dt')
@endsection