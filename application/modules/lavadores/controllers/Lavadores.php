<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Lavadores extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->model('M_lavadores', 'ml', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url', 'astillero'));

    date_default_timezone_set('America/Mexico_City');

    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
    if (!PermisoModulo('lavadores')) {
      redirect(site_url('login'));
    }
  }

  public function alta()
  {
    $data['titulo'] = "Lavadores";
    $data['titulo_dos'] = "Alta lavadores";
    if ($this->session->userdata('id_rol') != 1) {
      $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
    }
    $data['rows'] = $this->db->join('sucursales s', 's.id=l.id_sucursal')->select('l.*,s.sucursal')->get('lavadores l')->result();
    $data['sucursales'] = $this->db->get('sucursales')->result();
    $this->blade->render('lavadores/alta', $data);
  }

  public function editar($id_lavador)
  {
    $data['lavador'] = $this->Mgeneral->get_row('lavadorId', $id_lavador, 'lavadores');
    $data['titulo'] = "Lavadores";
    $data['titulo_dos'] = "Alta lavadores";
    $data['rows'] = $this->Mgeneral->get_table('lavadores');
    $data['rows'] = $this->db->join('sucursales s', 's.id=l.id_sucursal')->select('l.*,s.sucursal')->get('lavadores l')->result();
    $data['sucursales'] = $this->db->get('sucursales')->result();
    $this->blade->render('lavadores/editar', $data);
  }

  public function alta_guardar()
  {
    var_dump($_FILES['image']['name']);
    $name = date('dmyHis') . '_' . str_replace(" ", "", $_FILES['image']['name']);
    $path_to_save = 'statics/lavadores/';
    if (!file_exists($path_to_save)) {
      mkdir($path_to_save, 0777, true);
    }
    move_uploaded_file($_FILES['image']['tmp_name'], $path_to_save . $name);

    print_r($_FILES);


    $name_documento = date('dmyHis') . '_' . str_replace(" ", "", $_FILES['documento']['name']);
    $path_to_save_do = 'statics/lavadores/';
    if (!file_exists($path_to_save_do)) {
      mkdir($path_to_save_do, 0777, true);
    }
    move_uploaded_file($_FILES['documento']['tmp_name'], $path_to_save_do . $name_documento);
    $data['url_documento'] = $path_to_save_do . $name_documento;


    $data['lavadorFoto'] = $path_to_save . $name;
    $data['lavadorTelefono'] = $this->input->post('lavadorTelefono');
    $data['lavadorNombre'] = $this->input->post('lavadorNombre');
    $data['lavadorEmail'] = $this->input->post('lavadorEmail');
    $data['lavadorPassword'] = $this->input->post('lavadorPassword');
    if ($this->session->userdata('id_rol') != 1) {
      $data['id_sucursal'] = $this->session->userdata('id_sucursal');
    } else {
      $data['id_sucursal'] = $_POST['id_sucursal'];
    }
    $data['tipo_sangre'] = $this->input->post('tipo_sangre');
    $data['fecha_entrada'] = $this->input->post('fecha_entrada');
    $data['numero_lavador'] = 0;//$this->input->post('numero_lavador');
    $data['fecha_vencimiento'] = $this->input->post('fecha_vencimiento');

    $data['numero_licencia'] = $this->input->post('numero_licencia');
    $data['fecha_expedicion'] = $this->input->post('fecha_expedicion');
    $data['fecha_expiracion'] = $this->input->post('fecha_expiracion');
    $id_id = get_guid();

    $url_qr = base_url() . 'assets/images/qr/' . $id_id;
    $imagen_qr = $this->Mgeneral->qr($url_qr, $id_id);

    $data['qr'] = $imagen_qr;
    $data['id_id'] = $id_id;


    //GUARDA EN LA TABLA ADMIN
    if ($this->session->userdata('id_rol') != 1) {
      $post['id_sucursal'] = $this->session->userdata('id_sucursal');
    } else {
      $post['id_sucursal'] = $_POST['id_sucursal'];
    }
    $post['adminPassword'] = md5(sha1($post['adminPassword']));
    $post['adminStatus'] = 1;
    $post['adminFecha'] = date('Y-m-d');
    //$post['id_sucursal'] =  $this->input->post('id_sucursal');
    $post['id_cargo'] =  $this->input->post('id_cargo');
    $post['idRol'] =  4;
    $post['adminEmail'] =  $this->input->post('lavadorEmail');
    $post['telefono'] =  $this->input->post('lavadorTelefono');
    $post['adminUsername'] =  $this->input->post('lavadorTelefono');
    $id = $this->Mgeneral->save_admin($post);


    $this->Mgeneral->save_register('lavadores', $data);
  }

  public function editar_guardar($id_lavador)
  {
    if (!empty($_FILES['image']['name'])) {

      $name = date('dmyHis') . '_' . str_replace(" ", "", $_FILES['image']['name']);
      $path_to_save = 'statics/lavadores/';
      if (!file_exists($path_to_save)) {
        mkdir($path_to_save, 0777, true);
      }
      move_uploaded_file($_FILES['image']['tmp_name'], '/var/www/web/xehos/' . $path_to_save . $name);
      $data['lavadorFoto'] = $path_to_save . $name;

      print_r($_FILES);
    }



    if (!empty($_FILES['documento']['name'])) {
      $name_documento = date('dmyHis') . '_' . str_replace(" ", "", $_FILES['documento']['name']);
      $path_to_save_do = 'statics/lavadores/';
      if (!file_exists($path_to_save_do)) {
        mkdir($path_to_save_do, 0777, true);
      }
      move_uploaded_file($_FILES['documento']['tmp_name'], '/var/www/web/xehos/' . $path_to_save_do . $name_documento);
      $data['url_documento'] = $path_to_save_do . $name_documento;
    }




    $data['lavadorNombre'] = $this->input->post('lavadorNombre');
    $data['lavadorEmail'] = $this->input->post('lavadorEmail');
    $data['lavadorTelefono'] = $this->input->post('lavadorTelefono');
    $data['lavadorPassword'] = $this->input->post('lavadorPassword');
    $data['id_sucursal'] = $this->input->post('id_sucursal');
    $data['tipo_sangre'] = $this->input->post('tipo_sangre');
    $data['fecha_entrada'] = $this->input->post('fecha_entrada');
    $data['fecha_vencimiento'] = $this->input->post('fecha_vencimiento');

    $data['numero_licencia'] = $this->input->post('numero_licencia');
    $data['fecha_expedicion'] = $this->input->post('fecha_expedicion');
    $data['fecha_expiracion'] = $this->input->post('fecha_expiracion');

    $info_lavador = $this->Mgeneral->get_row('lavadorId', $id_lavador, 'lavadores');
    $id_id = $info_lavador->id_id; //get_guid();

    $url_qr = base_url() . 'assets/images/qr/' . $id_id;
    $imagen_qr = $this->Mgeneral->qr($url_qr, $id_id);

    $data['qr'] = $imagen_qr;
    //$data['id_id'] = $id_id;

    $this->Mgeneral->update_table_row('lavadores', $data, 'lavadorId', $id_lavador);
    //$this->Mgeneral->save_register('lavadores', $data);
  }


  public function calendario($id_lavador)
  {
    $rows['id_lavador'] = $id_lavador;
    $titulo['titulo'] = "Lavador:" . nombre_lavador($id_lavador);
    $titulo['titulo_dos'] = "Alta lavadores";
    $rows['lavadores'] = $this->Mgeneral->get_table('lavadores');
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = ""; //$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('lavadores/calendario', $rows, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array(
      'header' => $header,
      'menu' => $menu,
      'header_dos' => $header_dos,
      'titulo' => $titulo,
      'contenido' => $contenido,
      'footer' => $footer,
      'included_js' => array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js',
        'statics/bootstrap4/js/bootstrap.min.js'
      )
    ));
  }

  public function add()
  {
    if (isset($_POST['date_appointment'])) { // if form filled
      // insert $_POST form to array
      $data_insert = array(
        'author' => nombre_lavador($_POST['id_lavador']),
        'title' => $_POST['hora_inicio'] . "-" . $_POST['hora_fin'],
        'date' => $_POST['date_appointment'],
        'id_lavador' => $_POST['id_lavador'],
        'nombre_lavador' => nombre_lavador($_POST['id_lavador']),
        'hora_inicio' => $_POST['hora_inicio'],
        'hora_fin' => $_POST['hora_fin'],
        'borderColor' => "#000",
        'backgroundColor' => "#000",
        'textColor' => "#fff",

      );
      // create new appointment from($data_insert)
      $res = $this->db->insert('appointment', $data_insert);
      echo "success"; //if success will return string "success"
    } else {
      echo "error"; //if error will return string "error"
    }
  }

  public function ajaxevent()
  {
    //header('Access-Control-Allow-Origin: *');
    // if user not login, redirect to login(homepage)
    $result = $this->db->get('appointment')->result_array();
    print_r(json_encode($result));
  }
  public function lista_horarios($id = '')
  {
    $data['fecha'] = date('Y-m-d');
    $citas = $this->ml->getCitasLavador($id, $data['fecha']);
    $citas_ordenadas = array();
    foreach ($citas as $c => $value) {
      $citas_ordenadas[$value->fecha][] = $value;
    }
    $this->blade->set_data(array('citas' => $citas_ordenadas, 'id' => $id, 'nombre_operador' => $this->ml->getNameLavador($id), 'fecha' => $data['fecha']))->render('lista_horarios');
  }
  function agregar_motivo($id = 0)
  {
    if ($this->input->post()) {
      $this->form_validation->set_rules('motivo', 'motivo', 'trim|required');
      if ($this->form_validation->run()) {
        $this->ml->guardarOperador();
      } else {
        $errors = array(
          'motivo' => form_error('motivo'),
        );
        echo json_encode($errors);
        exit();
      }
    }
    if ($id == 0) {
      $info = new Stdclass();
    } else {
      $info = $this->mc->getOperadorId($id);
      $info = $info[0];
    }
    $data['input_id'] = $id;
    $data['input_motivo'] = form_input('motivo', set_value('motivo', exist_obj($info, 'motivo')), 'class="form-control" rows="5" id="motivo" ');
    $this->blade->render('motivo_desactivar', $data);
  }
  public function desactivar_horarios_asesores()
  {
    foreach ($_POST['seleccion'] as $key => $value) {
      $this->db->where('id', $key)->set("activo", 0)->set("motivo", $this->input->post('motivo_desactivar'))->update('horarios_lavadores');
    }
    echo 1;
    exit();
  }
  public function tabla_horarios()
  {
    $citas = $this->ml->getCitasLavador($this->input->post('id'), date2sql($this->input->post('fecha')));
    $citas_ordenadas = array();
    foreach ($citas as $c => $value) {
      $citas_ordenadas[$value->fecha][] = $value;
    }
    $this->blade->set_data(array('citas' => $citas_ordenadas))->render('tbl_horarios');
  }
  public function agregar_horario($id = 0)
  {
    $this->form_validation->set_rules('horario', 'horario', 'trim|required');
    $this->form_validation->set_rules('hora', 'hora', 'trim|required');
    if ($this->form_validation->run()) {
      $this->ml->guardarHorario();
    } else {
      $errors = array(
        'horario' => form_error('horario'),
        'hora' => form_error('hora'),
      );
      echo json_encode($errors);
      exit();
    }
  }
  //activa o desactiva el horario
  public function cambiar_status()
  {
    if ($this->db->where('id', $this->input->post('id'))->set("activo", $this->input->post('valor'))->set("motivo", $this->input->post('motivo'))->update($this->input->post('tabla'))) {
      echo 1;
    } else {
      echo 0;
    }
  }
}
