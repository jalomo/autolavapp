<h2>Lista de vehículos</h2>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Placas</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($autos as $a => $auto)
            <tr>
                <th>{{ $auto->placas }}</th>
                <th>{{ $auto->marca }}</th>
                <th>{{ $auto->modelo }}</th>
                <th>
                    <a href="" class="select-auto" data-id="{{ $auto->id }}">Selecciona</a>
                </th>
            </tr>
        @endforeach
    </tbody>
</table>
