@layout('layout')
@section('contenido')
    <table id="bootstrap-data-table" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Telefono</th>
                <th>Fecha creacion</th>

                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            <?php if (is_array($rows)): ?>
            <?php foreach ($rows as $row): ?>
            <tr>
                <td><?php echo $row->nombre . ' ' . $row->apellido_paterno . ' ' . $row->apellido_materno;
                    ?></td>
                <td><?php echo $row->email; ?></td>
                <td><?php echo $row->telefono; ?></td>
                <td><?php echo $row->created_at; ?></td>

                <td>
                    @if (PermisoAccion('editar_usuario'))
                        <a href="" data-id="{{ $row->id }}" class="js_editar" aria-hidden="true" data-toggle="tooltip"
                            data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                    @endif
                </td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
@endsection
@section('included_js')
    <script>
        var site_url = "{{ site_url() }}";
        const callbackGuardar = () => {
            var url = site_url + "/usuarios/editar_usuario";
            ajaxJson(url, $("#frm").serialize(), "POST", "", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(data, function(i, item) {
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    });
                } else {
                    if (result == -1) {
                        ErrorCustom('El correo ya fue registrado, por favor intenta con otro');
                    } else if (result == -2) {
                        ErrorCustom('El teléfono ya fue registrado, por favor intenta con otro');
                    } else if (result == 0) {
                        ErrorCustom('No se pudo guardar el usuario, por favor intenta de nuevo');
                    } else {
                        ExitoCustom("Guardado correctamente", function() {
                            window.location.reload();
                        });
                    }
                }
            });
        }
        $(".js_editar").on('click', function(e) {
            e.preventDefault();
            id = $(this).data('id')
            var url = site_url + "/usuarios/editar_usuario/" + id;
            customModal(url, {}, "GET", "lg", callbackGuardar, "", "Guardar", "Cancelar", "Editar usuario",
                "modal1");
        });

    </script>
@endsection
