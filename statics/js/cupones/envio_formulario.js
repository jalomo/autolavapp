$("#enviar_formulario").on('click', function (e){
    console.log("Se envia formulario_cupon");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#formulario_cupon").css("background-color","#ddd");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        $('input').attr('disabled',false);
        
    	var paqueteDatos = new FormData(document.getElementById('formulario_cupon'));
        var orden = $("input[name='orden']").val();
    	var base = $("#sitio").val();

        $.ajax({
            url: base+"cupones/guardar_formulario",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("_");
                    if (respuesta[0] == "OK") {
                        location.href = base+"index.php/cupones/index/";
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $("#formulario_cupon").css("background-color","white");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $("#formulario_cupon").css("background-color","white");
                $("#formulario_error").text("ERROR AL ENVIAR");
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
        //Regresamos los colores de la tabla a la normalidad
        $("#formulario_cupon").css("background-color","white");
    }
});

function validar() {
    var retorno = true;
    var tipo_formulario = $("input[name='envio_formulario']").val();

    if (tipo_formulario == "1") {
        var campo_1 = $("input[name='clave']").val();
        if (campo_1 == "") {
            var error_1 = $("#cupon_error");
            error_1.empty();
            $("#cupon_error").css("display","inline-block");
            //error_1.append("<br>");
            error_1.append('<label class="form-text text-danger">Campo requerido</label>');
            retorno = false;
        }else{
            var error_1 = $("#cupon_error");
            error_1.empty();
        }

        var campo_a = $("#usuario").val();
        if (campo_a == "") {
            var error_a = $("#usuario_error");
            error_a.empty();
            $("#usuario_error").css("display","inline-block");
            //error_a.append("<br>");
            error_a.append('<label class="form-text text-danger">Campo requerido</label>');
            retorno = false;
        }else{
            var error_a = $("#usuario_error");
            error_1.empty();
        }
    }

    var campo_2 = $("input[name='descripcion']").val();
    if (campo_2 == "") {
        var error_2 = $("#descripcion_error");
        error_2.empty();
        $("#descripcion_error").css("display","inline-block");
        //error_2.append("<br>");
        error_2.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_2 = $("#descripcion_error");
        error_2.empty();
    }

    var campo_3 = $("input[name='nombre_usuario']").val();
    if (campo_3 == "") {
        var error_3 = $("#nombre_usuario_error");
        error_3.empty();
        $("#nombre_usuario_error").css("display","inline-block");
        //error_3.append("<br>");
        error_3.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_3 = $("#nombre_usuario_error");
        error_3.empty();
    }

    var campo_4 = $("input[name='limite']").val();
    if (campo_4 == "") {
        var error_4 = $("#limite_error");
        error_4.empty();
        $("#limite_error").css("display","inline-block");
        //error_4.append("<br>");
        error_4.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_4 = $("#limite_error");
        error_4.empty();
    }

    var campo_5 = $("input[name='descuento']").val();
    if (campo_5 == "") {
        var error_5 = $("#descuento_error");
        error_5.empty();
        $("#descuento_error").css("display","inline-block");
        //error_5.append("<br>");
        error_5.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_5 = $("#descuento_error");
        error_5.empty();
    }

    var campo_6 = $("input[name='fecha_activo']").val();
    if (campo_6 == "") {
        var error_6 = $("#fecha_activo_error");
        error_6.empty();
        $("#fecha_activo_error").css("display","inline-block");
        //error_6.append("<br>");
        error_6.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_6 = $("#fecha_activo_error");
        error_6.empty();
    }

    var campo_7 = $("input[name='fecha_caducidad']").val();
    if (campo_7 == "") {
        var error_7 = $("#fecha_caducidad_error");
        error_7.empty();
        $("#fecha_caducidad_error").css("display","inline-block");
        //error_7.append("<br>");
        error_7.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_7 = $("#fecha_caducidad_error");
        error_7.empty();
    }

    var campo_9 = $("input[name='beneficio']").val();
    if (campo_9 == "") {
        var error_9 = $("#beneficio_error");
        error_9.empty();
        $("#beneficio_error").css("display","inline-block");
        //error_9.append("<br>");
        error_9.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_9 = $("#beneficio_error");
        error_9.empty();
    }

    var check_inidcadores = $(".servicio:checked").length;

    console.log("servicios = "+check_inidcadores);
    if (check_inidcadores <= 0) {
        var error_8 = $("#servicios_error");
        error_8.empty();
        $("#servicios_error").css("display","inline-block");
        //error_8.append("<br>");
        error_8.append('<label class="form-text text-danger">Falta indicar al menos un servicio</label>');
        retorno = false;
    }else{
        var error_8 = $("#servicios_error");
        error_8.empty();
    }

    return retorno;
}