@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <hr>
    <div class="row">
        <div class="col-md-12 text-right">
            <a target="blank" href="{{ base_url('refacciones/polizas/generarPolizaDevolucionProveedorPDF?fecha_inicio='.$fecha_inicio.'&fecha_fin='.$fecha_fin) }}" class="btn btn-primary">
                <i class="fas fa-file-pdf" aria-hidden="true"></i> &nbsp;Generar PDF
            </a>
        </div>
        
        <div class="col-md-12 mt-2 text-right">
            <a href="{{ base_url('refacciones/polizas/') }}" class="btn btn-primary">
                <i class="fas fa-list" aria-hidden="true"></i> Regresar
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h4>HORA: {{ date('H:i') }}</h4>
        </div>
        <div class="col-md-4 text-center">
            <h4>DMS FORD</h4>
            <div>REFACCIONES</div>
        </div>
        <div class="col-md-4 text-right">
            <h4>FECHA: {{ date('d/m/yy') }}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div>
                Fecha del: <span id="fecha_inicio_label">{{ $fecha_inicio }}</span> al <span id="fecha_fin_label"> {{ $fecha_fin }}</span>
            </div>
        </div>
        <div class="col-md-4 text-center"> </div>
        <div class="col-md-4 text-right"> </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="listadoPolizaProveedor"></table>
        </div>
    </div>
    <h3>Resultados totales</h3>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Total Devolución</th>
                        <th scope="col">Total Cantidad</th>
                        <th scope="col">Periodo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="totalCompras"></td>
                        <td id="totalCantidad"></td>
                        <td><span id=""><?php echo obtenerFechaEnLetra($fecha_inicio); ?></span> AL <span id=""> <?php echo obtenerFechaEnLetra($fecha_fin); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <input type="hidden" id="fecha_inicio" value="{{ $fecha_inicio }}" />
    <input type="hidden" id="fecha_fin" value="{{ $fecha_fin }}" />
</div>
@endsection
@section('included_js')
<script src="{{ base_url('js/refacciones/polizas/poliza_devolucion_proveedor/index.js') }}"></script>
@endsection
