@layout('layout')
@section('contenido')
<?php echo form_open('administradores/guarda_admin'); ?>
<div class="row">
    <div class="col-md-5">
        <h3 class="mb-3">Registro de usuario</h3>
        <div class="form-group">
            <span>
                <?php echo form_label('Nombre: ', 'adminNombre'); ?>
            </span>
            <span>
                <?php echo form_input(array(
                    'id' => '',
                    'class' => 'form-control cc-exp fform-control-sm',
                    'name' => 'Registro[adminNombre]',
                    'value' => ''
                )); ?>
            </span>
        </div>
        <div class="form-group">
            <span>
                <?php echo form_label('Nombre de Usuario: ', 'nombreUsurio'); ?>
            </span>
            <span>
                <?php echo form_input(array(
                    'id' => '',
                    'class' => 'form-control cc-exp fform-control-sm',
                    'name' => 'Registro[adminUsername]',
                    'value' => ''
                )); ?>
            </span>
        </div>
        <div class="form-group">
            <span>
                <?php echo form_label('Password: ', 'password'); ?>
            </span>
            <span>
                <?php echo form_password(array(
                    'id' => '',
                    'class' => 'form-control cc-exp fform-control-sm',
                    'name' => 'Registro[adminPassword]',
                    'value' => ''
                )); ?>
            </span>
        </div>
        <div class="form-group">
            <span>
                <?php echo form_label('Teléfono: ', 'telefono'); ?>
            </span>
            <span>
                <?php echo form_input(array(
                    'id' => '',
                    'class' => 'form-control cc-exp fform-control-sm',
                    'name' => 'Registro[telefono]',
                    'maxlength' => '10',
                    'type' => 'number',
                    'value' => ''
                )); ?>
            </span>
        </div>
        <div class="form-group">
            <span>
                <?php echo form_label('Sucursales: ', 'Sucursales'); ?>
            </span>
            <span>
                <select id="id_sucursal" name="id_sucursal" class="form-control">
                    <option value=""></option>
                    <?php foreach ($sucursales as $sucu) : ?>
                        <option value="<?php echo $sucu->id; ?>"><?php echo $sucu->sucursal; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>
        <div class="form-group">
            <span>
                <?php echo form_label('Cargos: ', 'Cargos'); ?>
            </span>
            <span>
                <select id="id_cargo" name="id_cargo" class="form-control">
                    <option value=""></option>
                    <?php foreach ($cargos as $cargo) : ?>
                        <option value="<?php echo $cargo->id; ?>"><?php echo $cargo->nombre; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>
        <div class="form-group">
            <span>
                <?php echo form_label('Tipo usuario: ', 'Cargos'); ?>
            </span>
            <span>
                <select id="idRol" name="idRol" class="form-control">
                    <option value=""></option>
                    <?php foreach ($tipo_usuario as $tipo_user) : ?>
                        <option value="<?php echo $tipo_user->id; ?>"><?php echo $tipo_user->rol; ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>
        <div class="form-group mt-3 text-right">
            <?php echo form_submit(array(
                'id' => '',
                'class' => 'btn btn-lg btn-info ',
                'value' => 'Guardar'
            )); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
    <div class="col-md-12">
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Fecha creación</th>
                                    <th>Usuario</th>
                                    <th>Teléfono</th>
                                    <th>Contraseña</th>
                                    <th>Sucursal</th>
                                    <th>Cargo</th>
                                    <th>Opciones</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php if (is_array($usuarios)) : ?>
                                    <?php foreach ($usuarios as $row) : ?>
                                        <tr>
                                            <td><?php echo $row->adminFecha; ?></td>
                                            <td><?php echo $row->adminUsername; ?></td>
                                            <td><?php echo $row->telefono; ?></td>
                                            <td><?php echo "*******"; ?></td>
                                            <td><?php echo nombre_sucursal_n($row->id_sucursal); ?></td>
                                            <td><?php echo nombre_cargo_n($row->id_cargo); ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>index.php/administradores/editar/<?php echo $row->adminId; ?>">
                                                    <button type="button" class="btn btn-warning " id="">Editar</button>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('included_js')
@include('main/scripts_dt')

@endsection