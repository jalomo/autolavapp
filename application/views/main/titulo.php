<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
.page-title{
  font-family: 'Roboto', sans-serif;
}
.breadcrumbs{
    background: #ecf0f1 !important;
}
</style>
<div class="breadcrumbs" >
    <div class="col-sm-4">
        <div class="page-header float-left" style="background:#ecf0f1; ">
            <div class="page-title">
                <h1><?php echo $titulo;?></h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8" >
        <div class="page-header float-right" style="background:#ecf0f1; ">
            <div class="page-title">
                <ol class="breadcrumb text-right" style="background:#ecf0f1; ">
                    <li><a href="#"><?php echo $titulo_dos;?></a></li>
                    <!--li><a href="#">UI Elements</a></li>
                    <li class="active">Typography</li-->
                </ol>
            </div>
        </div>
    </div>
</div>
