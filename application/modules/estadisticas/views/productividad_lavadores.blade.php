@layout('layout')
@section('included_css')
@endsection
@section('contenido')
    <h2>{{ $titulo_dos }}</h2>
    <br>
    <form id="frm" method="POST">
        @if ($this->session->userdata('id_rol') == 1)
            <div class="row">
                <div class="col-sm-6">
                    <label for="">Sucursal</label>
                    {{ $sucursales }}
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-6">
                <label for="">Servicio</label>
                {{ $id_servicio }}
            </div>
            <div class="col-sm-6">
                <label for="">Lavador</label>
                {{ $id_lavador }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label for="">Fecha inicio</label>
                <input type="date" id="fecha_inicio" name="fecha_inicio" class="form-control" value="{{ date('Y-m-d') }}">
            </div>
            <div class="col-sm-6">
                <label for="">Fecha fin</label>
                <input type="date" id="fecha_fin" name="fecha_fin" class="form-control" value="{{ date('Y-m-d') }}">
            </div>
        </div>
    </form>
    <br>
    <div class="row text-right">
        <div class="col-sm-10 pull-right">
            <button id="buscar" class="btn btn-info">Buscar</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="div_estadisticas">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Estatus anterior</th>
                            <th>Estatus nuevo</th>
                            <th>Inicio</th>
                            <th>Fin</th>
                            <th>Minutos transcurrido</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach ($info as $i => $data)
                            <tr>
                                <td colspan="5">
                                    <h2>{{ $i }}</h2>
                                </td>
                            </tr>
                            <?php $total = 0; ?>
                            @foreach ($data as $d => $servicios)
                                <?php
                                $servicio_array = explode('-', $d);
                                $tiempo_servicio = 0;
                                ?>
                                <tr>
                                    <td colspan="5">
                                        <h4>{{ $servicio_array[1] }}</h4>
                                    </td>
                                </tr>
                                @foreach ($servicios as $s => $servicio)
                                    <tr>
                                        <td>{{ $servicio->status_anterior }}</td>
                                        <td>{{ $servicio->status_nuevo }}</td>
                                        <td>{{ $servicio->inicio }}</td>
                                        <td>{{ $servicio->fin }}</td>
                                        <td>{{ $servicio->minutos_transcurridos }}</td>
                                    </tr>
                                    <? $tiempo_servicio = $tiempo_servicio + (int)$servicio->minutos_transcurridos; ?>
                                        @endforeach
                                            <tr>
                                            <td colspan="5">
                                                <h4 class="text-right">Tiempo servicio: {{ $tiempo_servicio }} minutos</h4>
                                            </td>
                                            </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


@endsection
@section('included_js')
                @include('main/scripts_dt')
                <script src="statics/js/lavadores.js"></script>
                <script>
                    var site_url = "{{ site_url() }}";
                    const id_rol = "{{ $this->session->userdata('id_rol') }}";
                    const buscar = () => {
                        if (($("#fecha_inicio").val() == '' && $("#fecha_fin").val() == '') || ($("#fecha_inicio").val() != '' && $(
                                "#fecha_fin").val() != '')) {
                            if (id_rol == 1 && $("#id_sucursal").val() == '') {
                                ErrorCustom('Es necesario seleccionar la sucursal');
                            } else {
                                var url = site_url + "/estadisticas/buscar_productividad_dinero";
                                ajaxLoad(url, $("#frm").serialize(), "div_estadisticas", "POST", function() {

                                });
                            }
                        } else {
                            ErrorCustom('Es necesario ingresar ambas fechas');
                        }
                    }
                    $("#buscar").on('click', buscar);
                    $(".js_exportar").on('click', function() {
                        var action = $(this).data('action');
                        $('#frm').attr('action', action);
                        $('#frm').submit();
                    });

                </script>

@endsection
