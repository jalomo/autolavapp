@layout('layout')
@section('contenido')
<div class="row text-right">
    <div class="col-sm-12">
        <a href="formato_unidades_nuevas.xlsx">Exportar formato</a>
    </div>
</div>
<?php
$output = '';
$output .= form_open_multipart('excel/save_unidades_nuevas');
$output .= '<div class="row">';
$output .= '<div class="col-lg-12 col-sm-12"><div class="form-group">';
$output .= form_label('Importar Unidades nuevas', 'image');
$data = array(
    'name' => 'userfile',
    'id' => 'userfile',
    'class' => 'form-control filestyle',
    'value' => '',
    'data-icon' => 'false'
);
$output .= form_upload($data);
$output .= '</div> <span style="color:red;">*Sólo archivos(.xls or .xlsx) como archivo</span></div>';
$output .= '<div class="col-lg-12 col-sm-12"><div class="form-group text-right">';
$data = array(
    'name' => 'importfile',
    'id' => 'importfile-id',
    'class' => 'btn btn-primary',
    'value' => 'Importar',
);
$output .= form_submit($data, 'Import Data');
$output .= '</div>
                        </div></div>';
$output .= form_close();
echo $output;
?>

<div class="row">
    <div class="col-sm-8">
        <label for=""></label>
        <div style="margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">
            <strong>Descargar el archivo, llenarlo con la información solicitada y volver a importarlo, EL FORMATO DEL CAMPO FECHA ES YYYY-MM-DD </strong>.
        </div>
    </div>
</div>
@endsection