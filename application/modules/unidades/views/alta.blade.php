@layout('layout')
@section('contenido')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="ui-typography">
                <div class="row">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Alta de unidad</strong>
                        </div>
                        <div class="card-body">
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <form action="" method="post" novalidate="novalidate" id="alta_usuario">
                                        <div class="row">
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Unidad</label>
                                                    <input id="unidad" name="save[unidad]" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="Unidad" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">No. economico</label>
                                                    <input id="economico" name="save[economico]" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="No. economico" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Presupuesto Mensual</label>
                                                    <input id="presupuesto" name="save[presupuesto]" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="presupuesto" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Rendimiento</label>
                                                    <select class="form-control-sm form-control cc-exp" id="id_catalogo_rendimiento" name="save[id_catalogo_rendimiento]">
                                                        <?php if($row->id_catalogo_rendimiento == 0):?>
                                                            <option value="0" >Selecione un rendimiento</option>
                                                        <?php endif;?>
                                                        <?php foreach($rendimientos as $ren):?>
                                                        <?php if($row->id_catalogo_rendimiento ==  $ren->id):?>
                                                        <option value="<?php echo $ren->id ?>" selected><?php echo $ren->nombre." ".$ren->redimiento?> </option>
                                                        <?php else:?>
                                                            <option value="<?php echo $ren->id ?>" ><?php echo $ren->nombre." ".$ren->redimiento?> </option>
                                                        <?php endif;?>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Tipo</label>
                                                    <select class="form-control cc-exp form-control-sm" name="save[tipo]" id="tipo">
                                                        <option value="1">Moto</option>
                                                        <option value="2">Automovil</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Marca</label>
                                                    <input id="marca" name="save[marca]" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="Marca" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Placas</label>
                                                    <input id="placas" name="save[placas]" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="Placas" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                           
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Modelo</label>
                                                    <input id="modelo" name="save[modelo]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Modelo" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Serie chasis</label>
                                                    <input id="n_serie" name="save[n_serie]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Número serie" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número de serie Motor</label>
                                                    <input id="n_serie_motor" name="save[n_serie_motor]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Número serie" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                        </div>

                                        <div class="row">
                                        <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">KM</label>
                                                    <input id="kilometros" name="save[kilometros]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="KM" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Año</label>
                                                    <select class="form-control cc-exp form-control-sm" name="save[anio]" id="anio">
                                                        <option value="2015">2015</option>
                                                        <option value="2016">2016</option>
                                                        <option value="2017">2017</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2020">2020</option>
                                                        <option value="2021">2021</option>
                                                        
                                                        
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Combustible</label>
                                                    <select class="form-control cc-exp form-control-sm" name="save[combustible]" id="combustible">
                                                        <option value="1">Gasolina</option>
                                                        <option value="2">Diésel</option>
                                                        <option value="3">Híbrido</option>
                                                        <option value="4">Eléctrico</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                        <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Color</label>
                                                    <input id="color" name="save[color]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Color" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Puestos</label>
                                                    <select class="form-control cc-exp form-control-sm" name="save[puestos]" id="puestos">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                                                    <select name="save[id_sucursal]" id="id_sucursal" class="form-control">
                                                        <option value="">-- Selecciona sucursal --</option>
                                                        @foreach ($sucursales as $s => $sucursal)
                                                            <option value="{{ $sucursal->id }}">{{ $sucursal->sucursal }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                           <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Seguro</label>
                                                    <select class="form-control cc-exp form-control-sm" name="save[seguro]" id="seguro">
                                                        <option value="1">Activo</option>
                                                        <option value="2">Vencido</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Aseguradora</label>
                                                    <input id="aseguradora" name="save[aseguradora]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Aseguradora" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número de poliza</label>
                                                    <input id="no_poliza" name="save[no_poliza]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Número de poliza" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                           <div class="col-2">
                                           Vigencia poliza
                                           <input type="text" id="datepicker" name="save[vigencia_poliza]" id="vigrncia_pliza" class="form-control cc-exp form-control-sm" >
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Cilindros</label>
                                                    <input id="cilindros" name="save[cilindros]" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Cilindros" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>


                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Lavadores</label>
                                                    
                                                    <?php $options_su = array();?>
                                                        @foreach ($lavadores as $s => $lavador)
                                                            <?php $options_su[$lavador->lavadorId] = $lavador->lavadorNombre;?>
                                                           
                                                        @endforeach
                                                    <?php
                                                        
                                                       
                                                        
                                                        $js = 'class="form-control cc-exp form-control-sm" id="id_lavador"';
                                                        echo form_dropdown('save[id_lavador]', $options_su, '',$js);
                                                    ?>
                                                </div>
                                            </div>

                                        </div>
                                        <div align="right">
                                            <button type="button" id="guardar" type="submit" class="btn btn-lg btn-info ">
                                                <i class="fa fa-edit fa-lg"></i>&nbsp;
                                                <span id="payment-button-amount">Guardar</span>
                                                <span id="payment-button-sending" style="display:none;">Sending…</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                </div>
                <!--/.col-->
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Marca</th>
                                        <th>Placas</th>
                                        <th>Color</th>
                                        <th>Modelo</th>
                                        
                                        <th>KM</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                 <?php if(is_array($unidades)):?>
                                <?php foreach($unidades as $row):?>
                                    <tr>
                                        <th><?php  if($row->tipo == 1){echo "Moto";}else{echo "Automovil";};?></th>
                                        <th><?php echo $row->marca;?></th>
                                        <th><?php echo $row->placas;?></th>
                                        <th><?php echo $row->color;?></th>
                                        <th><?php echo $row->modelo;?></th>
                                       
                                        <th><?php echo $row->kilometros;?></th>
                                        <th>
                                            <a href="<?php echo base_url()?>index.php/unidades/ver/<?php echo $row->id;?>">
                                                <button type="button" class="btn btn-lg btn-info ">Ver/editar</button>
                                            </a>
                                        </th>
                                    </tr>
                                <?php endforeach;?>
                                <?php endif;?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
           
       // $( "#datepicker" ).datepicker();
        $( "#datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        
    });
            $("#cargando").hide();
            $('#guardar').on('click',function(event) {
                event.preventDefault();
                $("#enviar").hide();
                $("#cargando").show();
                var url_sis ="<?php echo base_url(); ?>index.php/unidades/alta_guardar";
                // Get form
                var form = $('#alta_usuario')[0];
                // Create an FormData object
                var data = new FormData(form);
                console.log(data);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url_sis,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        exito_redirect("DATOS GUARDADOS CON EXITO", "success",
                            "<?php echo base_url(); ?>index.php/unidades/alta"
                        );
                        $("#enviar").show();
                        $("#cargando").hide();

                    },
                    error: function(e) {
                        //$("#result").text(e.responseText);
                        console.log("ERROR : ", e);
                        //$("#btnSubmit").prop("disabled", false);
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                        $("#enviar").show();
                        $("#cargando").hide();

                    }
                });
            });
    </script>
@endsection
