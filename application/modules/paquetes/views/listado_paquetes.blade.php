@layout('layout')
@section('contenido')
    {{$titulo}}
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Servicio</th>
                                    <th>Cliente</th>
                                    <th>Email cliente</th>
                                    <th>Teléfono</th>
                                    <th>Placas</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Año</th>
                                    <th>Color</th>
                                    <th>Lavador</th>
                                    <th>Fecha y hora servicio</th>
                                    <th>Dirección</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($info as $i => $servicio)
                                    <tr>
                                        <td>{{ $servicio->id.'-'.$servicio->pos }}</td>
                                        <td>{{ $servicio->servicioNombre }}</td>
                                        <td>{{ $servicio->nombre . ' ' . $servicio->apellido_paterno . ' ' . $servicio->apellido_materno }}
                                        </td>
                                        <td>{{ $servicio->email }}</td>
                                        <td>{{ $servicio->telefono }}</td>
                                        <td>{{ $servicio->placas }}</td>
                                        <td>{{ $servicio->marca }}</td>
                                        <td>{{ $servicio->modelo }}</td>
                                        <td>{{ $servicio->anio }}</td>
                                        <td>{{ $servicio->color }}</td>
                                        <td>{{ $servicio->lavadorNombre }}</td>
                                        <th>{{ $servicio->fecha_cita . ' ' . $servicio->hora_cita }}</th>
                                        <td>{{ $servicio->estado . ', ' . $servicio->municipio . ', ' . $servicio->calle . ' ' . $servicio->numero_ext . ' ' . $servicio->numero_int . ' ' . $servicio->colonia }}
                                        </td>
                                        <td>
                                            @if (PermisoAccion('editar_servicio'))
                                                <a href="{{ site_url('paquetes/generar_paquetes/' . encrypt($servicio->id)) }}"
                                                    class="js_editar" aria-hidden="true" data-toggle="tooltip"
                                                    data-placement="top" title="Editar"> <i class="fa fa-edit"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script>
        var site_url = "{{ site_url() }}";

        const callbackCancelarServicio = () => {
            var url = site_url + "/servicios/cancelar_servicio/";
            ajaxJson(url, {
                "id": id
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('El servicio no se pudo cancelar, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Servicio cancelado correctamente", function() {
                        $(aPos).find('i').css('color', 'red');
                        $(aPos).removeClass('js_cancelar');
                    });
                }
            });
        }
        const confirmar_rechazar = () => {
            var url = site_url + "/servicios/confirmar_rechazar/";
            ajaxJson(url, {
                "id": id,
                "valor": valor
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('Error de petición, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Registro actualizado correctamente.", function() {
                        $(".close").trigger("click");
                        aPosi = $(aPos).find('i');
                        if (valor == 1) {
                            //la está confirmando
                            $(aPos).data('valor', 0);
                            $(aPos).attr('title', 'Confirmar');
                            $(aPosi).removeClass('fa-square-o');
                            $(aPosi).addClass('fa-check-square-o');
                            $(aPos).attr('data-original-title', 'Confirmar');
                        } else {
                            //la está rechazando
                            $(aPos).data('valor', 1);
                            $(aPos).attr('title', 'No confirmada');
                            $(aPosi).removeClass('fa-check-square-o');
                            $(aPosi).addClass('fa-square-o');
                            $(aPos).attr('data-original-title', 'No confirmada');

                        }
                        $('[data-toggle="tooltip"]').tooltip()
                    });

                }
            });
        }
        $("body").on("click", '.js_cancelar', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            aPos = $(this);
            ConfirmCustom("¿Está seguro de cancelar el servicio?", callbackCancelarServicio, "", "Confirmar",
                "Cancelar");
        });
        $("body").on("click", '.js_confirmar', function(e) {
            e.preventDefault();
            //valor es 0 cuando va rechazar y 1 cuando lo va confirmar
            aPos = $(this);
            valor = $(this).data('valor');
            if (valor == 1) {
                mensaje = "¿Está seguro de confirmar el servicio?";
            } else {
                mensaje = "¿Está seguro de poner como no confirmado el servicio?";
            }
            id = $(this).data('id');
            ConfirmCustom(mensaje, confirmar_rechazar, "", "Confirmar", "Cancelar");
        });

    </script>
@endsection
