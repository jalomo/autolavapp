@layout('layout_correo')
@section('contenido')
    <tr style="border-collapse:collapse;">
        <td style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:left top;background-color:#FAFAFA;"
            bgcolor="#fafafa" align="left">
            <table width="100%" cellspacing="0" cellpadding="0"
                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                    <td width="540" valign="top" align="center" style="padding:0;Margin:0;">
                        <table width="100%" cellspacing="0" cellpadding="0" role="presentation"
                            style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr class="text-center">
                                <td>
                                    <p>Estimado Cliente <strong>{{ isset($informacion->contacto)?$informacion->contacto:$informacion->nombre.' '.$informacion->apellido_paterno.' '.$informacion->apellido_materno }}</strong>
                                    </p>
                                    <p class="justify" style="text-align: justify">
                                        El motivo de la presente, es comunicarle que en Xehos AutoLavado®️ Hemos tratado de
                                        localizarle
                                        para recomendarle realizar el <strong>SERVICIO DE LAVADO</strong>, a su Vehículo,
                                        motivo por el cual se le hace una cálida invitación, para ponerse en contacto con
                                        nosotros en Tels.: <strong><a href="tel:3338352058">33 3835 2058</a></strong> /
                                        WhatsApp.: <strong>33 2805 6406</strong>. O al e-mail: <a
                                            href="mailto:reservaciones@xehos.com"> <strong>reservaciones@xehos.com</strong> </a>
                                        O bien realizar su reservación en nuestras Apps; Xehos AutoLavado®️ y también en
                                        <a target="_blank" href="https://xehos.com">https://xehos.com</a>
                                    </p>
                                    <p style="text-align: justify">
                                        Agradeciendo sus finas atenciones, esperamos contar con su reservación y
                                        preferencia.
                                    </p>
                                    <p style="text-align: justify">
                                        Le invitamos a que consulte nuestro aviso de privacidad vigente en <a target="_blank"
                                            href="https://xehos.com/avisodeprivacidad">https://xehos.com/avisodeprivacidad</a>
                                    </p>
                                    <div style="text-align: center">
                                        <p>
                                            Reciba un cordial Saludo.
                                        </p>
                                    </div>

                                    <p style="text-align: center;">
                                        <br>
                                        <br>
                                        <span style="font-size: 10px;">
                                            En caso de que al recibir este aviso ya hubiera usted, efectuado el servicio a
                                            que nos referimos, favor de hacer caso omiso.
                                        </span>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@endsection
