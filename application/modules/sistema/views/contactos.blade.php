@layout('layout')
@section('contenido')

<div class="container-fluid panel-body">
	<h1 class="mt-4">Directorio de contactos</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
		<li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
	</ol>
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="tab" href="#clientes">Clientes</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#recomendadores">Recomendadores</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#lavadores">Lavadores</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div id="clientes" class="container tab-pane active"><br>
			<h3>Listado de clientes</h3>
			<div class="row">
				<?php
				if (isset($usuarios) && $usuarios) {
					foreach ($usuarios as $usuario) { ?>
						<div class="col-md-3 mt-3">
							<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:160px">
								<div style="padding:10px" class="">
									<div class="pull-right">
										<button class="btn btn-default btn-lg" style="background-color:#f6f7f8;" title="Comenzar chat" class="btn btn-primary" onclick="gotoChat(this)" data-telefono="{{ $usuario->telefono }}"><i  <?php echo $usuario->notificacion ? 'style="color:#17a2b8"' : 'style="color:#22ae22"'; ?> class="fa fa-comment fa-2x"></i></button>
									</div>
									<i class="fa fa-user-circle fa-3x"></i>
									{{ $usuario->nombre}}<br />
									<p>
										<b>Email: </b> {{ strtolower($usuario->email) }}<br />
										<b>Teléfono: </b> {{ $usuario->telefono }}<br />
									</p>
									<?php if($usuario->notificacion == true) { ?>
									<span class="text-info text-right bold"><b>Chat pendiente</b></span>
									<?php } ?>
								</div>
							</div>
						</div>
				<?php
					}
				} ?>
			</div>
		</div>
		<div id="recomendadores" class="container tab-pane fade"><br>
			<h3>Listado de recomendadores</h3>
			<div class="row">
				<?php
				if (isset($recomendadores) && $recomendadores) {
					foreach ($recomendadores as $recomendador) { ?>
						<div class="col-md-3 mt-3">
							<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:160px">
								<div style="padding:10px" class="">
									<div class="pull-right">
										<button class="btn btn-default btn-lg" style="background-color:#f6f7f8;" title="Comenzar chat" class="btn btn-primary" onclick="gotoChat(this)" data-telefono="{{ $recomendador->celular }}"><i <?php echo $recomendador->notificacion ? 'style="color:#17a2b8"' : 'style="color:#22ae22"'; ?>  class="fa fa-comment fa-2x"></i></button>
									</div>
									<i class="fa fa-user-circle fa-3x"></i>
									{{ $recomendador->nombre}}<br />
									<p>
										<b>Email: </b> {{ isset($recomendador->correo) ? strtolower($recomendador->correo) : '' }}<br />
										<b>Teléfono: </b> {{ $recomendador->celular }}<br />
									</p>
									<?php if($recomendador->notificacion == true) { ?>
									<span class="text-info text-right bold"><b>Chat pendiente</b></span>
									<?php } ?>
								</div>
							</div>
						</div>
				<?php
					}
				} ?>
			</div>
		</div>
		<div id="lavadores" class="container tab-pane fade"><br>
			<h3>Listado de lavadores</h3>
			<div class="row">
				<?php
				if (isset($lavadores) && $lavadores) {
					foreach ($lavadores as $lavador) {  ?>
						<div class="col-md-3 mt-3">
							<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:160px">
								<div style="padding:10px" class="">
									<div class="pull-right">
										<button class="btn btn-default btn-lg" style="background-color:#f6f7f8;" title="Comenzar chat" class="btn btn-primary" onclick="gotoChat(this)" data-telefono="{{ $lavador->lavadorTelefono }}"><i <?php echo $lavador->notificacion ? 'style="color:#17a2b8"' : 'style="color:#22ae22"'; ?> class="fa fa-comment fa-2x"></i></button>
									</div>
									<i class="fa fa-user-circle fa-3x"></i>
									{{ $lavador->lavadorNombre}}<br />
									<p>
										<b>Email: </b> {{ strtolower($lavador->lavadorEmail) }}<br />
										<b>Teléfono: </b> {{ $lavador->lavadorTelefono }}<br />
									</p>
									<?php if($lavador->notificacion == true) { ?>
									<span class="text-info text-right bold"><b>Chat pendiente</b></span>
									<?php } ?>
								</div>
							</div>
						</div>
				<?php
					}
				} ?>
			</div>
		</div>
	</div>
</div>
@endsection

<script type="text/javascript">
	var site_url = "{{site_url()}}"

	function gotoChat(_this) {
		window.location.href = site_url + '/sistema/chat?telefono_destinatario=' + $(_this).data('telefono')
	}
</script>