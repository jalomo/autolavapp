<?php
class Minventario extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->limit(100)->get($tabla)->result();
        return $result;
  	}

    public function get_table_limit($campo,$tabla){
        $result = $this->db->order_by($campo,"DESC")->limit(500)->get($tabla)->result();
        return $result;
    }

    public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        $result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
        return $result;
    }

  	public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function update_table_row($table,$data,$id_table,$id){
    		$result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
  	}

  	public function buscar_datos_orden($dato_bq ='')
  	{
  		$q = $this->db->like('id',$dato_bq)
            ->or_like('placas',$dato_bq)
            ->or_like('folio_mostrar',$dato_bq)
            ->select('id,nombre_cliente,lavadorNombre,id,precio,nombre,apellido_paterno,apellido_materno,folio_mostrar')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('v_info_servicios')->result();
        
        return $q;
  	}

    public function datos_servicio($id_servicio ='')
    {
        $q = $this->db->like('id',$id_servicio)
            ->or_like('folio_mostrar',$id_servicio)   
            ->select('*')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('v_info_servicios')->result();
        
        return $q;
    }

    public function listado_inventarios()
    {
        $result = $this->db->select('folio_info, nombre_cliente, nombre_asesor, fecha_alta, id_orden, id')
            ->order_by('id',"DESC")
            ->limit(500)
            ->get('diagnostico')
            ->result();
    }

    public function get_table($table){
        $data = $this->db->get($table)->result();
        return $data;
    }

    public function buscar_cupon_servicio($id_servicio ='')
    {
        $result = $this->db->where('serv.id ',$id_servicio)
            ->join('cupones AS cupon','cupon.id = serv.id_cupon_servicio')
            ->select('cupon.cupon, cupon.descuento')
            ->order_by('serv.id','DESC')
            ->get('servicio_lavado AS serv')
            ->result();
        
        return $result;
    }

    public function id_paso_seguimiento($id_servicio= ""){
        $q = $this->db->where('id_servicio',$id_servicio)
        ->select('id,id_lavador')
        ->limit(1)
        ->get('proceso_servicio_lavado');

        if($q->num_rows()==1){
            $retorno = [$q->row()->id,$q->row()->id_lavador];
        }else{
            $retorno = [];
        }
        return $retorno;
    }


}