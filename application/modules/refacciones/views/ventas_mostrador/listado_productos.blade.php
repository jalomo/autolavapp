@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">Lista</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No. identificación</th>
                            <th>Descripcion</th>
                            <th>Precio unitario</th>
                            <th>Precio público (70%)</th>
                            <th>Unidad</th>
                            <th>Ubicación</th>
                            <th>Existencia total</th>
                            <th>Existencia almacen principal </th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (isset($listado))
                            @foreach ($listado as $item)    
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->no_identificacion }}</td>
                                    <td>{{ $item->descripcion }}</td>
                                    <td>$ {{ $item->valor_unitario }}</td>
                                    <td>
                                        $ {{ obtenerPorcentaje(70,$item->valor_unitario)}}
                                    </td>
                                    <td>{{ $item->unidad }}</td>
                                    <td>{{ $item->ubicacionProducto }}</td>
                                    <td>{{$item->cantidad_actual}}</td>
                                    <td>{{$item->cantidad_almacen_primario}}</td>
                                    <td>
                                        @if ($item->cantidad_almacen_primario < 1)
                                        {{ '--' }}
                                        @else
                                        <a class="btn btn-success btn_comprar" href="{{ site_url('refacciones/salidas/realizarVenta/'.$item->producto_id.'/'.$menu) }}"> 
                                            <i class="fas fa-shopping-cart"></i> 
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                        <th>#</th>
                            <th>No. identificación</th>
                            <th>Descripcion</th>
                            <th>Precio unitario</th>
                            <th>Precio público (70%)</th>
                            <th>Unidad</th>
                            <th>Ubicación</th>
                            <th>Existencia total</th>
                            <th>Existencia almacen principal </th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('included_js')
<script src="{{ base_url('js/custom/jquery.dataTable.1.10.20.min.js') }}" crossorigin="anonymous"></script>
<script>
    var band = false;
    ajax.get(`api/inventario`, {}, function(response, headers) {
        if (headers.status == 200) {
            $.each(response, function(index, value) {
                if (value.estatus_inventario_id == 1) {
                    band = true;
                    $(".btn_comprar").remove();
                    utils.displayWarningDialog("Existe un inventario en proceso. Favor de esperar a que finalize", "error", function() {
                        return false;
                    });
                    return false;
                }
            });
        }
    });
    $(document).ready(function() {
        $('#tbl_productos').DataTable({})
    });

    $("#tbl_productos").on("click", ".btn-borrar", function() {
        var id = $(this).data('id')
        borrar(id)
    });

    function borrar(id) {
        utils.displayWarningDialog("Desea borrar el registro??", "warning", function(data) {
            if (data.value) {
                ajax.delete(`/api/productos/${id}`, null, function(response, headers) {
                    if (headers.status != 204) {
                        return utils.displayWarningDialog(headers.message)
                    }
                    location.reload(true)
                })

            }
        }, true)
    }

</script>

@endsection