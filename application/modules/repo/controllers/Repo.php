<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Repo extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
       
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url',"astillero"));

        date_default_timezone_set('America/Mexico_City');
       // if($this->session->userdata('id_c')){}else{redirect('login/');}
       // error_reporting(0);

    }

   

    

    public function index($id_incidencia){
    $data['documentos'] = $this->Mgeneral->get_result('id_incidencia',$id_incidencia,'documentos');//$this->Mgeneral->get_table('documentos');
    $data['id_incidencia'] = $id_incidencia;
    $data['titulo'] = "Estado" ;
    $data['titulo_dos'] = "Alta Estado" ;
      
    $this->blade->render('repo/alta', $data);
   
    
  }


  public function subir_info($id_incidencia){

    
    
    $dossier ='/var/www/web3/cs/autolavapp/statics/documentos/'; 

    $fichero_subido = $dossier . basename($_FILES['documento']['name']);
            
             if(move_uploaded_file($_FILES['documento']['tmp_name'], $fichero_subido)) 
             {
                  echo 'Upload effectué avec succès !';
             }
             else 
             {
                  echo 'Echec de l\'upload !';
             }

  $data_fotos['url_documento'] = basename($_FILES['documento']['name']);
  $data_fotos['descripcion'] = $this->input->post('descripcion');
  $data_fotos['fecha_creacion'] = date("Y-m-d H:i:s"); 
  $data_fotos['id_incidencia'] = $id_incidencia; 
  $data_fotos['url'] = $this->input->post('url');
  $this->Mgeneral->save_register('documentos', $data_fotos);
   redirect('repo/index/'.$id_incidencia);
  }

  
}