<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proactivo_agencias extends MX_Controller
{
  private $filename = "import_data"; // 
  public function __construct()
  { //
    parent::__construct();
    $this->load->model('M_proactivo_agencias', 'mp', TRUE);
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->helper(array('general', 'correo'));
    $this->load->library(array('session', 'email'));
    date_default_timezone_set('America/Mexico_City');
    if (!PermisoModulo('proactivo')) {
      redirect(site_url('login'));
    }
  }
  //MAGIC PARA GUARDAR COMENTARIOS
  public function comentarios()
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id') == '') {
      redirect('login');
    }
    if ($this->input->post()) {
      if (isset($_POST['nunca_contactar'])) {
        $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
      } else {
        $this->form_validation->set_rules('comentario', 'comentario', 'trim');
      }
      $this->form_validation->set_rules('cronoFecha', 'fecha', 'trim|required');


      if ($this->form_validation->run()) {
        $this->mp->saveComentario();
        exit();
      } else {
        $errors = array(
          'comentario' => form_error('comentario'),
          'cronoFecha' => form_error('cronoFecha'),
        );
        echo json_encode($errors);
        exit();
      }
    }
    $data['id'] = $this->input->get('id');
    $data['fecha'] = date2sql($this->input->get('fecha'));
    $data['cliente'] = $this->input->get('cliente');
    $data['id_agencia'] = $this->input->get('id_agencia');
    $data['input_comentario'] = form_textarea('comentario', "", 'class="form-control" id="comentario" rows="2"');

    $this->blade->render('comentarios', $data);
  }
  public function correo()
  {
    $cuerpo = $this->blade->render('correo_intentos', array(), true);
    enviar_correo(strtolower("albertopitava@gmail.com"), "Notificación Contacto Proactivo!", $cuerpo, array());
  }
  public function correo_cp()
  {
    $cuerpo = $this->blade->render('proactivo/correo_contacto_proactivo', array(), true);
    //Guardar historial
    $data = [
      'id_servicio' => $_POST['id'],
      'id_agencia' => $_POST['id_agencia'],
      'created_at' => date('Y-m-d H:i:s'),
      'id_usuario' => $this->session->userdata('id')
    ];
    $this->db->insert('correos_enviados_cp', $data);
    enviar_correo(strtolower($_POST['email']), "Carta previa Contacto Proactivo!", $cuerpo, array());
    echo 1;
    exit();
  }
  public function historial_proactivo()
  {
    $data['id_agencia'] = form_dropdown('id_agencia', array_combos($this->Mgeneral->get_table('catalogo_agencias'), 'id', 'agencia', TRUE), '', 'class="form-control busqueda" id="id_agencia"');
    $this->blade->render('historial_proactivo_agencias', $data);
  }
  public function buscar_historial_proactivo()
  {
    $fecha_inicio = date2sql($this->input->post('finicio'));
    $fecha_fin = date2sql($this->input->post('ffin'));
    $this->db->where('date(p.created_at) >=', $fecha_inicio);
    $this->db->where('date(p.created_at) <=', $fecha_fin);
    $this->db->where('p.id_agencia', $_POST['id_agencia']);
    $data['proactivo'] = $this->db
      ->select('c.*,p.fecha_contacto,p.fecha_contacto,c.intentos,c.fecha_programacion,a.adminNombre as usuario,cat.agencia')
      ->join('contacto_proactivo_agencias c', 'p.id_servicio = c.id')
      ->join('admin a', 'a.adminId=p.id_usuario')
      ->join('catalogo_agencias cat', 'cat.id=p.id_agencia')
      ->group_by('c.id')
      ->get('proactivo_historial p')
      ->result();
    $this->blade->set_data($data)->render('tabla_historial_agencias');
  }
  public function index($busqueda = 0)
  {
    $data['id_agencia'] = form_dropdown('id_agencia', array_combos($this->Mgeneral->get_table('catalogo_agencias'), 'id', 'agencia', TRUE), '', 'class="form-control busqueda" id="id_agencia"');
    $this->blade->render('lista_proactivos', $data);
  }
  public function getDatosProactivo()
  {
    $datos = $_POST;
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];
    if ($this->input->post('fecha') == '') {
      $fecha_final = date('Y-m-d');
    } else {
      $fecha_final = date2sql($this->input->post('fecha'));
    }

    $newdate = strtotime('-7 days', strtotime($fecha_final));
    $fecha_inicial = date('Y-m-d', $newdate);

    //obtener total
    $total = $this->mp->getDataByDate($fecha_inicial, $fecha_inicial, true, $_POST['id_agencia']);
    //Obtener todos los registros
    if (isset($_POST['buscar_campo'])) {
      $this->filtros($_POST['buscar_campo']);
    }
    $info = $this->mp->getDataByDate($fecha_inicial, $fecha_inicial, false, $_POST['id_agencia']);
    $array_info = array();
    foreach ($info as $key => $value) {
      $array_info[$value->serie] = $value;
    }
    $data = array();
    foreach ($array_info as $key => $value) {
      $dat = array();
      $acciones = '';
      $acciones .= '<a href="servicios/generar_servicio/0/' . '1/' . $value->id . '/' . $value->id_agencia . '" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Agendar cita"><i class="fa fa-plus"></i></a>';
      $acciones .= '<a href="" data-id_agencia="' . $value->id_agencia . '" data-id="' . $value->id . '" class="js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios" data-fecha="' . $value->fecha_programacion . '" data-cliente="' . $value->nombre . ' ' . $value->apellido_paterno . ' ' . $value->apellido_materno . '"><i class="fa fa-comments"></i></a>';
      $acciones .= '<a href="" data-id_agencia="' . $value->id_agencia . '" data-id="' . $value->id . '" class="js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"><i class="fa fa-info"></i></a>';
      $acciones .= '<a target="_blank" href="https://api.whatsapp.com/send?phone='.CONST_WS_CONTACTO.'&text=¡Estimado%20cliente!%20'.$value->nombre . " " . $value->apellido_paterno . " " . $value->apellido_materno .'%20¡XehosAutolavado!%20lo%20contactamos%20para%20recomendarle%20hacer%20su%20cita%20para%20el%20lavado%20de%20su%20auto"><i class="fa fa-whatsapp"></i>';
      $color_correo = '#1473A4';
      $clase_correo = 'js_enviado';
      if (!$this->mp->correo_enviado($value->id, $value->id_agencia)) {
        $color_correo = '#878787';
        $clase_correo = 'js_correo';
      }
      $acciones .= '<a style="color:' . $color_correo . '" href="" data-id_agencia="' . $value->id_agencia . '" data-id="' . $value->id . '" data-email="' . $value->email . '" class="' . $clase_correo . '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar correo"><i class="fa fa-envelope"></i></a>';
      $dat[] = $acciones;
      $dat[] = $value->id;
      $dat[] = $value->agencia;
      $dat[] = $value->id_cita;
      $dat[] = date_eng2esp_1($value->fecha_programacion);
      $dat[] = $value->intentos;
      $dat[] = $value->kilometraje;
      $dat[] = $value->placas;
      $dat[] = $value->nombre . ' ' . $value->apellido_paterno . ' ' . $value->apellido_materno;
      $dat[] = $value->email;
      $dat[] = $value->telefono_movil;
      $dat[] = $value->otro_telefono;
      $dat[] = $value->marca;
      $dat[] = $value->modelo;
      $dat[] = $value->serie;
      $dat[] = $value->servicio;
      $data[] = $dat;
    }
    $retornar = array(
      'draw' => intval($mismo),
      'recordsTotal' => intval($total),
      'recordsFiltered' => intval($total),
      'data' => $data
    );
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function filtros($busqueda = '')
  {
    if ($busqueda != '') {
      $this->db->or_like('id', $busqueda);
      $this->db->or_like('intentos', $busqueda);
      $this->db->or_like('vehiculo_placas', $busqueda);
      $this->db->or_like('nombres', $busqueda);
      $this->db->or_like('apellido_paterno', $busqueda);
      $this->db->or_like('apellido_materno', $busqueda);
      $this->db->or_like('email', $busqueda);
      $this->db->or_like('telefono', $busqueda);
      $this->db->or_like('fecha_programacion', $busqueda);
      $this->db->or_like('marca', $busqueda);
      $this->db->or_like('modelo', $busqueda);
      $this->db->or_like('serie', $busqueda);
    }
  }
  public function activarN()
  {
    $this->mp->ProgramarNotificacion();
  }
  public function historial_comentarios()
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id') == '') {
      redirect('login');
    }
    $data['comentarios'] = $this->mp->getHistorialComentarios($this->input->get('id'), $this->input->get('id_agencia'));
    $this->blade->render('historial_comentarios_agencia', $data);
  }
  //new
  public function generar_cp_agencia_bk()
  {
    $data['id_agencia'] = form_dropdown('id_agencia', array_combos($this->Mgeneral->get_table('catalogo_agencias'), 'id', 'agencia', TRUE), '', 'class="form-control busqueda" id="id_agencia"');
    $this->blade->render('generar_cp_agencia', $data);
  }
  public function generar_mes()
  {
    $id_agencia = $_POST['id_agencia'];
    $data['month'] = date('m');
    $data['year'] = date('Y');
    $data['nameMes'] = $this->mp->getNameMes($data['month']);
    $data['validar'] = $this->mp->validaContactByMesYear($data['month'], $data['year'], $id_agencia);
    $data['registros'] = $this->mp->getProactivoMes($id_agencia);
    $data['id_agencia'] = $id_agencia;
    $this->blade->set_data($data)->render('proactivo_por_mes');
  }
  public function generar_registros_proactivo()
  {
    $info = json_decode($this->mp->getDataCPAgencia($_POST['mes'], $_POST['anio'], $_POST['id_agencia']));
    foreach ($info->data as $d => $registro) {
      $data_array = [
        'id_agencia' => $_POST['id_agencia'],
        'id_cita' => $registro->id_cita,
        'fecha_programacion' => date($registro->fecha_entrega_unidad),
        'numero_interno' => $registro->numero_interno,
        'nombre_compania' => $registro->nombre_compania,
        'nombre_contacto_compania' => $registro->nombre_contacto_compania,
        'ap_contacto' => $registro->ap_contacto,
        'am_contacto' => $registro->am_contacto,
        'rfc' => $registro->rfc,
        'correo_compania' => $registro->correo_compania,
        'calle' => $registro->calle,
        'nointerior' => $registro->nointerior,
        'noexterior' => $registro->noexterior,
        'colonia' => $registro->colonia,
        'municipio' => $registro->municipio,
        'cp' => $registro->cp,
        'estado' => $registro->estado,
        'telefono_movil' => $registro->telefono_movil,
        'otro_telefono' => $registro->otro_telefono,
        'kilometraje' => $registro->vehiculo_kilometraje,
        'transmision' => $registro->transmision,
        'numero_cliente' => $registro->numero_cliente,
        'color' => $registro->color,
        'marca' => $registro->vehiculo_marca,
        'tipo_cliente' => $registro->tipo_cliente,
        'cilindros' => $registro->cilindros,
        'email' => $registro->email,
        'vehiculo_anio' => $registro->vehiculo_anio,
        'modelo' => $registro->vehiculo_modelo,
        'version' => $registro->vehiculo_version,
        'placas' => $registro->vehiculo_placas,
        'serie' => $registro->vehiculo_numero_serie,
        'nombre' => $registro->datos_nombres,
        'apellido_paterno' => $registro->datos_apellido_paterno,
        'apellido_materno' => $registro->datos_apellido_materno,
        'telefono' => $registro->telefono_movil,
        'servicio' => $registro->estatus_cita
      ];
      $this->db->insert('contacto_proactivo_agencias', $data_array);
    }
    $datos_proactivo = array(
      'mes' => $_POST['mes'],
      'anio' => $_POST['anio'],
      'id_agencia' => $_POST['id_agencia'],
      'idusuario' => $this->session->userdata('id'),
      'created_at' => date('Y-m-d H:i:s')
    );
    //Insertar que ya se hizo el del mes
    $this->db->insert('proactivo_mes_agencias', $datos_proactivo);
    echo 1;
    exit();
  }

  public function citas()
  {
    $this->db->where('activo',1);
    $data['id_agencia'] = form_dropdown('id_agencia', array_combos($this->Mgeneral->get_table('catalogo_agencias'), 'id', 'agencia', TRUE), '', 'class="form-control busqueda" id="id_agencia"');
    $this->blade->render('citas', $data);
  }
  public function buscar_cita_agencia()
  {
    $info_agencia = $this->mp->getDataCatAgencia($_POST['id_agencia']);
    $data['url_agencia'] = $info_agencia->url . '/index.php/xehos/agendar_cita';
    $data['agencia'] = $info_agencia->agencia;
    $this->blade->render('formulario_agencia', $data);
  }
  public function subir_archivo()
  {
    $data = array();
    if (isset($_POST['preview'])) {
      $upload = $this->mp->upload_file($this->filename);
      if ($upload['result'] == "success") {
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx');
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
        $data['sheet'] = $sheet;
        $data['id_agencia'] = $_POST['id_agencia'];
      } else {
        $data['upload_error'] = $upload['error'];
      }
    }
    $this->db->where('genera_individual',1);
    $data['id_agencia'] = form_dropdown('id_agencia', array_combos($this->Mgeneral->get_table('catalogo_agencias'), 'id', 'agencia', TRUE), '', 'class="form-control busqueda" id="id_agencia"');
    $this->blade->render('importar_archivo', $data);
  }
  public function import()
	{
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); 
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
		$data = array();
		$numrow = 1;
		foreach ($sheet as $row) {
			if ($numrow > 1) {
				array_push($data, array(
					'id_agencia' => $_POST['id_agencia_save'],
					'id_cita' => 0,
					'fecha_programacion' => $row['A'],
					'numero_interno' => $row['B'],
					'nombre_compania' => $row['C'],
					'nombre_contacto_compania' => $row['D'],
					'ap_contacto' => $row['E'],
					'am_contacto' => $row['F'],
					'rfc' => $row['G'],
					'correo_compania' => $row['H'],
					'calle' => $row['I'],
					'nointerior' => $row['J'],
					'noexterior' => $row['K'],
					'colonia' => $row['L'],
					'municipio' => $row['M'],
					'cp' => $row['N'],
					'estado' => $row['O'],
					'telefono_movil' => $row['P'],
					'otro_telefono' => $row['Q'],
					'kilometraje' => $row['R'],
					'transmision' => $row['S'],
					'numero_cliente' => $row['T'],
					'color' => $row['U'],
					'marca' => $row['V'],
					'tipo_cliente' => $row['W'],
					'cilindros' => $row['X'],
					'email' => $row['Y'],
					'vehiculo_anio' => $row['Z'],
					'modelo' => $row['AA'],
					'version' => $row['AB'],
					'placas' => $row['AC'],
					'serie' => $row['AD'],
					'nombre' => $row['AE'],
					'apellido_paterno' => $row['AF'],
					'apellido_materno' => $row['AG'],
					'telefono' => $row['AH'],
					'servicio' => $row['AI'],
					'intentos' => 0,
					'no_contactar' => 0,
					'nunca_contactar' => 0,
          'created_at' => date('Y-m-d H:i:s'),
          'id_usuario' => $this->session->userdata('id')
				));
			}
			$numrow++;
		}
		$this->mp->insert_multiple($data);
		redirect("proactivo_agencias/subir_archivo");
	}
}
