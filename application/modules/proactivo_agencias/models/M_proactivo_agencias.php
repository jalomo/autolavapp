<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_proactivo_agencias extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }
  //guarda los comentariosa
  public function saveComentario()
  {
    $id = $this->input->post('id');
    $datos = array(
      'comentario' => $this->input->post('comentario'),
      'id_agencia' => $this->input->post('id_agencia_send'),
      'id_servicio' => $id,
      'fecha_creacion' => date('Y-m-d H:i:s'),
      'id_usuario' => $this->session->userdata('id'),
      'tipo_comentario' => 1,
      'fecha_notificacion' => date2sql($this->input->post('cronoFecha')) . ' ' . $this->input->post('cronoHora'),
    );
    $this->db->insert('historial_comentarios', $datos);
    $this->db->set('fecha_programacion', date2sql($this->input->post('cronoFecha')))->where('id', $id)->update('contacto_proactivo_agencias');
    //Actualizar intentos

    if ($this->input->post('comentario') != '') {
      $this->UpdateIntentos($id);
      $array_historial = array(
        'id_servicio' => $id,
        'created_at' => date('Y-m-d H:i:s'),
        'id_usuario' => $this->session->userdata('id'),
        'comentario' => $this->input->post('comentario'),
        'id_agencia' => $this->input->post('id_agencia_send'),
        'fecha_contacto' => date2sql($this->input->post('fecha')),
        'fecha_notificacion' => date2sql($this->input->post('cronoFecha')) . ' ' . $this->input->post('cronoHora')
      );
      $this->db->insert('proactivo_historial', $array_historial);
    }
    if ($this->input->post('no_contactar')) {
      $date = date2sql($this->input->post('cronoFecha'));
      $newdate = strtotime('+10 days', strtotime($date));
      $date = date('Y-m-d', $newdate);
      $dia = date("D", strtotime($date));
      //Si el día es sábado agregar un día, si es domingo 2
      // if ($dia == 'Sat') {
      //   $newdate = strtotime('+2 day', strtotime($date));
      //   $date = date('Y-m-d', $newdate);
      // }
      if ($dia == 'Sun') {
        $newdate = strtotime('+1 day', strtotime($date));
        $date = date('Y-m-d', $newdate);
      }
      $this->db->where('id', $this->input->post('id'))->set('no_contactar', 1)->set('fecha_programacion', $date)->set('intentos', 0)->update('contacto_proactivo_agencias');
    }
    if ($this->input->post('nunca_contactar')) {
      $this->db->where('id', $this->input->post('id'))->set('nunca_contactar', 1)->update('contacto_proactivo_agencias');
    }
    if (!$this->input->post('no_contactar') && !$this->input->post('nunca_contactar') && $_POST['comentario'] != '') {
      $date = date2sql($this->input->post('cronoFecha'));
      $newdate = strtotime('+10 days', strtotime($date));
      $date_notification = date('Y-m-d', $newdate);
      $hour_notification = ($_POST['cronoHora'] != '') ? $_POST['cronoHora'] : '12:00';
      $mensaje = $_POST['comentario'];
      $agencia = $this->getDataCatAgencia($_POST['id_agencia_send'])->agencia;
      $this->ProgramarNotificacion($mensaje, $date_notification . ' ' . $hour_notification, $agencia, $this->session->userdata('telefono'));
    }
    echo 1;
    exit();
  }
  public function getComentariosReagendar_magic($id = '')
  {
    return $this->db->where('id', $id)->get('historial_comentarios')->result();
  }
  public function getHistorialComentarios($id_servicio = '', $id_agencia)
  {
    return $this->db->where('id_servicio', $id_servicio)
      ->where('id_agencia', $id_agencia)
      ->join('catalogo_agencias c', 'h.id_agencia = c.id')
      ->where('tipo_comentario', 1)
      ->select('h.*,c.agencia')
      ->get('historial_comentarios h')->result();
  }

  //Actualizar los intentos
  public function UpdateIntentos($id = '')
  {
    $q = $this->db->where('id', $id)->select('intentos')->from('contacto_proactivo_agencias')->get();
    if ($q->num_rows() == 1) {
      $intentos =  $q->row()->intentos;
      $this->db->where('id', $id)->set('intentos', $intentos + 1)->update('contacto_proactivo_agencias');
      //Enviar correo ya cuando son mas de 5 intentos
      if (($intentos + 1) >= 3) {
        //Guardar en el historial > 5 intentos
        $array_historial_intentos = array(
          'id_servicio' => $id,
          'created_at' => date('Y-m-d H:i:s'),
          'id_usuario' => $this->session->userdata('id'),
          'id_agencia' => $_POST['id_agencia_send']
        );
        $this->db->insert('proactivo_historial_3intentos', $array_historial_intentos);
        $datos['informacion'] = $this->getInformacionId($id);
        $cuerpo = $this->blade->render('proactivo/correo_intentos', $datos, true);
        enviar_correo(strtolower($datos['informacion']->email), "¡Notificación Contacto Proactivo!", $cuerpo, array());
      }
    }
  }
  public function getInformacionId($id = '')
  {
    return $this->db->where('id', $id)->get('contacto_proactivo_agencias')->row();
  }

  public function getDataByDate($fecha_inicial = '', $fecha_final = '', $total = false, $id_agencia = '')
  {
    $pagina = $_POST['start'];
    $porpagina = $_POST['length'];
    $this->db->where('fecha_programacion', $fecha_inicial);
    $this->db->where('nunca_contactar', 0);
    $this->db->where('id_agencia', $id_agencia);
    $this->db->order_by('fecha_programacion', 'asc');
    if ($total) {
      $this->db->select('count(id) as total');
      $q = $this->db->get('contacto_proactivo_agencias');
      return $q->row()->total;
    } else {
      $this->db->limit($porpagina, $pagina);
    }
    $this->db->join('catalogo_agencias cat', 'cat.id=c.id_agencia');
    $this->db->select('c.*,cat.agencia');
    return $this->db->get('contacto_proactivo_agencias c')->result();
  }
  //Obtener lo que ya se ha generado para hacer el proactivo
  function getProactivoMes($id_agencia = '')
  {
    return $this->db->join('catalogo_agencias a', 'a.id=p.id_agencia')
      ->select('p.*,a.agencia')
      ->where('id_agencia', $id_agencia)
      ->get('proactivo_mes_agencias p')->result();
  }

  //Validar si ya está generado el contacto por mes
  function validaContactByMesYear($mes = '', $anio = '', $id_agencia)
  {
    $q = $this->db->where('mes', $mes)->where('anio', $anio)->where('id_agencia', $id_agencia)->get('proactivo_mes_agencias');
    if ($q->num_rows() == 1) {
      return false;
    } else {
      return true;
    }
  }
  public function getNameMes($mes = '')
  {
    switch ($mes) {
      case '01':
        return 'Enero';
        break;
      case '02':
        return 'Febrero';
        break;
      case '03':
        return 'Marzo';
        break;
      case '04':
        return 'Abril';
        break;
      case '05':
        return 'Mayo';
        break;
      case '06':
        return 'Junio';
        break;
      case '07':
        return 'Julio';
        break;
      case '08':
        return 'Agosto';
        break;
      case '09':
        return 'Septiembre';
        break;
      case '10':
        return 'Octubre';
        break;
      case '11':
        return 'Noviembre';
        break;
      case '12':
        return 'Diciembre';
        break;
      default:
        return '';
        break;
    }
  }
  public function ProgramarNotificacion($mensaje = '', $fecha = '', $sucursal = '', $celular = '')
  {
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/proactivo",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"texto\"\r\n\r\n" . $mensaje . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"fecha\"\r\n\r\n" . $fecha . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sucursal\"\r\n\r\n" . $sucursal . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"celular\"\r\n\r\n" . $celular . "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
        "postman-token: d6300177-ea75-5ef7-f170-20edfe7cc050"
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
  }
  //Función para saber si ya se envío el correo
  public function correo_enviado($id, $id_agencia)
  {
    $q = $this->db->where('id_servicio', $id)->where('id_agencia', $id_agencia)->get('correos_enviados_cp')->result();
    if (count($q) > 0) {
      return true;
    }
    return false;
  }
  public function getUrlAgencia($id_agencia)
  {
    $q = $this->db->where('id', $id_agencia)->get('catalogo_agencias');
    if ($q->num_rows() == 1) {
      return $q->row()->url;
    }
    return '';
  }
  public function getDataCPAgencia($mes, $anio, $id_agencia)
  {
    $url = $this->getUrlAgencia($id_agencia) . '/api/getDataOrdenesCP/' . $mes . '/' . $anio;
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "postman-token: 529eb010-359c-2090-7ec7-3a39c062690b"
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return $response;
  }
  public function getDataCPAgenciaFecha($fecha, $id_agencia)
  {
    $url = $this->getUrlAgencia($id_agencia) . '/Api/getDataOrdenesCP/' . $fecha;
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "postman-token: 529eb010-359c-2090-7ec7-3a39c062690b"
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return $response;
  }
  //Validar si ya se generó el cp por agencia y por día
  public function validarCPAgenciaFecha($fecha = '', $id_agencia = '')
  {
    $q = $this->db->limit(1)->where('id_agencia', $id_agencia)->where('fecha', $fecha)->get('proactivo_dia_agencias');
    if ($q->num_rows() == 1) {
      return true;
    }
    return false;
  }
  public function getDataCatAgencia($id_agencia)
  {
    $q = $this->db->where('id', $id_agencia)->get('catalogo_agencias');

    if ($q->num_rows() == 0) {
      return '';
    } else {
      return $q->row();
    }
  }
  public function upload_file($filename)
  {
    $this->load->library('upload');

    $config['upload_path'] = './excel/';
    $config['allowed_types'] = 'xlsx';
    $config['max_size']  = '2048';
    $config['overwrite'] = true;
    $config['file_name'] = $filename;

    $this->upload->initialize($config);
    if ($this->upload->do_upload('file')) {
      $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
      return $return;
    } else {
      $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
      return $return;
    }
  }
  public function insert_multiple($data)
  {

    $this->db->insert_batch('contacto_proactivo_agencias', $data);
  }
}
