@layout('layout')
@section('contenido')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="ui-typography">
                <div class="row">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Editar  lavador</strong>
                        </div>
                        <div class="card-body">
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <form action="" method="post" novalidate="novalidate" id="alta_usuario">
                                        <div class="row">
                                        <div class="col-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número Lavador</label>
                                                    <input id="numero_lavador" name="numero_lavador" type="text"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $lavador->lavadorNombre;?>"
                                                        placeholder="Número Lavador" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                                    <input id="lavadorNombre" name="lavadorNombre" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Nombre" autocomplete="cc-exp" value="<?php echo $lavador->lavadorNombre;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Email</label>
                                                    <input id="lavadorEmail" name="lavadorEmail" type="email"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $lavador->lavadorEmail;?>"
                                                        placeholder="Email" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Contraseña</label>
                                                    <input id="lavadorPassword" name="lavadorPassword" type="password"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $lavador->lavadorPassword;?>"
                                                        placeholder="Contraseña" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                           <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Teléfono</label>
                                                    <input id="lavadorTelefono" name="lavadorTelefono" type="text"
                                                        class="form-control cc-exp form-control-sm" 
                                                        placeholder="Teléfono" autocomplete="cc-exp" value="<?php echo $lavador->lavadorTelefono;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Tipo sangre</label>
                                                    <select name="tipo_sangre" id="tipo_sangre" class="form-control">
                                                        <?php if($lavador->tipo_sangre == "A/+"):?>
                                                          <option value="A/+" selected>A/+</option>
                                                        <?php else:?>
                                                            <option value="A/+">A/+</option>
                                                        <?php endif;?>

                                                        <?php if($lavador->tipo_sangre == "A/-"):?>
                                                            <option value="A/-" selected>A/-</option>
                                                        <?php else:?>
                                                            <option value="A/-">A/-</option>
                                                        <?php endif;?>

                                                        <?php if($lavador->tipo_sangre == "O/+"):?>
                                                            <option value="O/+" selected>O/+</option>
                                                        <?php else:?>
                                                            <option value="O/+">O/+</option>
                                                        <?php endif;?>

                                                        <?php if($lavador->tipo_sangre == "O/-"):?>
                                                            <option value="O/-" selected>O/-</option>
                                                        <?php else:?>
                                                            <option value="O/-">O/-</option>
                                                        <?php endif;?>


                                                        <?php if($lavador->tipo_sangre == "B/+"):?>
                                                            <option value="B/+" selected>B/+</option>
                                                        <?php else:?>
                                                            <option value="B/+">B/+</option>
                                                        <?php endif;?>

                                                        <?php if($lavador->tipo_sangre == "B/-"):?>
                                                            <option value="B/-" selected>B/-</option>
                                                        <?php else:?>
                                                            <option value="B/-">B/-</option>
                                                        <?php endif;?>

                                                        <?php if($lavador->tipo_sangre == "AB/+"):?>
                                                            <option value="AB/+" selected>AB/+</option>
                                                        <?php else:?>
                                                            <option value="AB/+">AB/+</option>
                                                        <?php endif;?>

                                                        <?php if($lavador->tipo_sangre == "AB/-"):?>
                                                            <option value="AB/-" selected>AB/-</option>
                                                        <?php else:?>
                                                            <option value="AB/-">AB/-</option>
                                                        <?php endif;?>
                                                        
                                                        
                                                    </select>
                                                    <!--input id="tipo_sangre" name="tipo_sangre" type="text"
                                                        class="form-control cc-exp form-control-sm"
                                                        placeholder="tipo sangre" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span-->
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Fecha entrada</label>
                                                    <input id="fecha_entrada" name="fecha_entrada" type="text"
                                                        class="form-control cc-exp form-control-sm"
                                                        placeholder="Fecha entrada" autocomplete="cc-exp" value="<?php echo $lavador->fecha_entrada;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Fecha vencimiento</label>
                                                    <input id="fecha_vencimiento" name="fecha_vencimiento" type="text"
                                                        class="form-control cc-exp form-control-sm"
                                                        placeholder="Fecha vencimiento" autocomplete="cc-exp" value="<?php echo $lavador->fecha_vencimiento;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                                                    <select name="id_sucursal" id="id_sucursal" class="form-control">
                                                        <option value="">-- Selecciona sucursal --</option>
                                                        @foreach ($sucursales as $s => $sucursal)
                                                            @if($lavador->id_sucursal == $sucursal->id)
                                                            <option value="{{ $sucursal->id }}" selected>{{ $sucursal->sucursal }}</option>
                                                            @else
                                                            <option value="{{ $sucursal->id }}" >{{ $sucursal->sucursal }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                            <img height="50" src="<?php
                                        echo base_url();
                                        echo $lavador->lavadorFoto;
                                        ?>">
                                                <div class="control-group">
                                                    <label class="control-label" for="basicinput">Imagen</label>
                                                    <div class="controls">
                                                        <input type="file" id="image" name="image" placeholder=""
                                                            class="span8" required="">
                                                        <!--span class="help-inline">500px X 500px</span-->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <a target="_blank"
                                                    href="<?php echo base_url() . $lavador->url_documento; ?>">
                                                    <button type="button" class="btn btn-info">ver documento</button>
                                                </a>
                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Documento</label>
                                                        <div class="controls">
                                                            <input type="file" id="documento" name="documento" placeholder=""
                                                                class="span8" required="">
                                                            <!--span class="help-inline">500px X 500px</span-->
                                                        </div>
                                                    </div>
                                            </div>


                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número licencia</label>
                                                    <input id="numero_licencia" name="numero_licencia" type="text"
                                                        class="form-control cc-exp form-control-sm" value="<?php echo $lavador->numero_licencia;?>"
                                                        placeholder="Número licencia" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Fecha expedición</label>
                                                    <input id="fecha_expedicion" name="fecha_expedicion" type="text"
                                                        class="form-control cc-exp form-control-sm"
                                                        placeholder="Fecha " autocomplete="cc-exp" value="<?php echo $lavador->fecha_expedicion;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Fecha expiración</label>
                                                    <input id="fecha_expiracion" name="fecha_expiracion" type="text"
                                                        class="form-control cc-exp form-control-sm"
                                                        placeholder="Fecha " autocomplete="cc-exp" value="<?php echo $lavador->fecha_expiracion;?>">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>


                                            
                                        </div>
                                        <div align="right">
                                            <button type="button" id="guardar" type="submit" class="btn btn-lg btn-info ">
                                                <i class="fa fa-edit fa-lg"></i>&nbsp;
                                                <span id="payment-button-amount">Editar</span>
                                                <span id="payment-button-sending" style="display:none;">Sending…</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                </div>
                <!--/.col-->
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
    
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
           
       // $( "#datepicker" ).datepicker();
        $( "#fecha_entrada" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $( "#fecha_vencimiento" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $( "#fecha_expedicion" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });

        $( "#fecha_expiracion" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        
    });
            $("#cargando").hide();
            $('#guardar').on('click',function(event) {
                event.preventDefault();
                $("#enviar").hide();
                $("#cargando").show();
                var url_sis ="<?php echo base_url(); ?>index.php/lavadores/editar_guardar/<?php echo $lavador->lavadorId?>";
                // Get form
                var form = $('#alta_usuario')[0];
                // Create an FormData object
                var data = new FormData(form);
                console.log(data);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url_sis,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        exito_redirect("DATOS GUARDADOS CON EXITO", "success",
                            "<?php echo base_url(); ?>index.php/lavadores/alta"
                        );
                        $("#enviar").show();
                        $("#cargando").hide();

                    },
                    error: function(e) {
                        //$("#result").text(e.responseText);
                        console.log("ERROR : ", e);
                        //$("#btnSubmit").prop("disabled", false);
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                        $("#enviar").show();
                        $("#cargando").hide();

                    }
                });
            });
    </script>
@endsection
