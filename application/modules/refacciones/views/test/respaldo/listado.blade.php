@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div style="margin:10px;">
                <div class="alert alert-success" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
                </div>
                <div class="alert alert-warning" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
                </div>
            
                <div class="panel-body">
                    <div class="col-md-12">
                        <h3 align="center">Presupuestos Multipunto</h3>
                        <br>
                        <div class="panel panel-default">
                            <div class="panel-body" style="border:2px solid black;">
                                <div class="row">
                                    <div class="col-md-1">
                                        <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel/5";?>';" name="" value="Regresar">
                                    </div>
                                    <!-- Si la vista es de refacciones, mostramos los filtros por estatus del cliente -->
                                    <div class="col-md-2">
                                        <!--<h5 for="">Afectados por el cliente: </h5>
                                        <select class="form-control" id="filtro_cliente" style="font-size:12px;width:100%;">
                                            <option value="">SELECCIONE ...</option>
                                            <option value="" style="">SIN CONFIRMAR</option>
                                            <option value="Si" style="color: darkgreen; font-weight: bold;">CONFIRMADA</option>
                                            <option value="No" style="color: red ; font-weight: bold;">RECHAZADA</option>
                                            <option value="Val" style="color: darkorange; font-weight: bold;">DETALLADA</option>
                                        </select>-->
                                    </div>
                                    
                                    <!-- Si la vista es de refacciones, mostramos los filtros por estatus de las refacciones-->
                                    <div class="col-md-3">
                                        <h5 for="">Estado ventanilla: </h5>
                                        <select class="form-control" id="filtro_ventanillas" style="font-size:12px;width:100%;">
                                            <option value="">SELECCIONE ...</option>
                                            <option value="0" style="">SIN SOLICITAR</option>
                                            <option value="1" style="background-color: #f5ff51;">SOLICIATADAS</option>
                                            <option value="3" style="background-color: #5bc0de;">RECIBIDAS</option>
                                            <option value="2" style="background-color: #c7ecc7;">ENTREGADAS</option>
                                        </select>
                                    </div>
            
                                    <!-- formulario de busqueda -->
                                    <div class="col-md-3">
                                        <h5>Busqueda Gral.:</h5>
                                        <div class="input-group">
                                            <!--<div class="input-group-addon">
                                                <i class="fa fa-search"></i>
                                            </div>-->
                                            <input type="text" class="form-control" id="busqueda_ventanilla" placeholder="Buscar...">
                                        </div>
                                        <input type="hidden" name="" id="indicador" value="Ventanilla">
                                    </div>
                                    <!-- /.formulario de busqueda -->
            
                                    <div class="col-md-3">
                                        <br><br>
                                        <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_venatnilla" style="margin-right: 15px;"> 
                                            <i class="fa fa-search"></i>
                                        </button>
            
                                        <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                            <i class="fa fa-trash"></i>
                                            <!-- Limpiar Busqueda-->
                                        </button>
                                    </div>
                                </div>
            
                                <br>
                                <div class="form-group" align="center" style="overflow-x: scroll;overflow-y: scroll;">
                                    <i class="fas fa-spinner cargaIcono"></i>
                                    <table class="table table-bordered table-responsive" style="width:100%;">
                                        <thead>
                                            <tr style="font-size:14px;background-color: #ddd;">
                                                <!--<td align="center" style="width: 10%;"></td>-->
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>NO. ORDEN</strong></td>
                                                <!--<td align="center" style="width: 10%;vertical-align: middle;"><strong>ORDEN DMS</strong></td>-->
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>TIPO PRESUPUESTO</strong></td>
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>SERIE</strong></td>
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>VEHÍCULO</strong></td>
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                                <!--<td align="center" style="width: 20%;"><strong>FIRMA ASESOR</strong></td>-->
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>APRUEBA CLIENTE</strong></td>
                                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>EDO. REFACCIONES</strong></td>
                                                <td align="center" colspan="3" style="width: 10%;vertical-align: middle;"><strong>ACCIONES</strong></td>
                                            </tr>
                                        </thead>
                                        <tbody class="campos_buscar"> 
                                            <!-- Verificamos que se hayan cargado los datos para mostrar-->
                                            <?php if (isset($id_cita)): ?>
                                                <!-- Imprimimos los datos-->
                                                <?php foreach ($id_cita as $index => $valor): ?>
                                                    <tr style="font-size:11px;">
                                                        
                                                        <td align="center" style="width:10%;vertical-align: middle;background-color: <?php if ($envia_ventanilla[$index] == "1") echo "#68ce68"; elseif($envia_ventanilla[$index] == "2") echo "#b776e6;color:white"; else echo "#fffff"; ?>;">
                                                            <?php echo $id_cita[$index]; ?>
                                                            <?php if ($envia_ventanilla[$index] == "2"): ?>
                                                                <p style="font-size:9px;color:blue;">Se envio al asesor</p>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td align="center" style="width:15%;vertical-align: middle;">
                                                            <?php echo $tipo[$index]; ?>
                                                        </td>
                                                        <td align="center" style="width:15%;vertical-align: middle;">
                                                            <?php echo $serie[$index]; ?>
                                                        </td>
                                                        <td align="center" style="width:15%;vertical-align: middle;">
                                                            <?php echo $modelo[$index]; ?>
                                                        </td>
                                                        <td align="center" style="width:15%;vertical-align: middle;">
                                                            <?php echo $placas[$index]; ?>
                                                        </td>
                                                        <td align="center" style="width:10%;vertical-align: middle;">
                                                            <?php echo $asesor[$index]; ?>
                                                        </td>
                                                        <td align="center" style="width:15%;vertical-align: middle;">
                                                            <?php echo $tecnico[$index]; ?>
                                                        </td>
            
                                                        <td align="center" style="width:10%;vertical-align: middle;">
                                                            <?php if ($acepta_cliente[$index] != ""): ?>
                                                                <?php if ($acepta_cliente[$index] == "Si"): ?>
                                                                    <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                                <?php elseif ($acepta_cliente[$index] == "Val"): ?>
                                                                    <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                                <?php else: ?>
                                                                    <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                                <?php endif; ?>
                                                            <?php else: ?>
                                                                <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                                            <?php endif; ?>
                                                        </td>
            
                                                        <td align="center" style="width:15%;vertical-align: middle; <?php echo $color[$index];?>">
                                                            <?php if (($estado_refacciones[$index] == "0")&&($acepta_cliente[$index] != "No")): ?>
                                                                SIN SOLICITAR
                                                            <?php elseif ($acepta_cliente[$index] == "No"): ?>
                                                                RECHAZADA
                                                            <?php elseif ($estado_refacciones[$index] == "1"): ?>
                                                                SOLICITADAS
                                                            <?php elseif ($estado_refacciones[$index] == "2"): ?>
                                                                RECIBIDAS
                                                            <?php elseif ($estado_refacciones[$index] == "3"): ?>
                                                                ENTREGADAS
                                                            <?php else: ?>
                                                                SIN SOLICITAR *
                                                            <?php endif ?>
                                                        </td>
            
                                                        <td align="center" style="width:15%;vertical-align: middle;">
                                                            <a href="<?=base_url().'OrdenServicio_Revision/'.$id_cita[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">ORDEN</a>
                                                        </td>
            
                                                        <td align="center" style="width:15%;vertical-align: middle;">
                                                            <!-- Si la cotizacion ya fue afectada y no fue rechazada (presupuesto tradicional) -->
                                                            <?php if (($acepta_cliente[$index] != "")&&($acepta_cliente[$index] != "No")&&($ruta[$index] == "1")): ?>
                                                                <?php if ($estado_refacciones[$index] == "0"): ?>
                                                                    <button type="button" class="btn" style="background-color: #f5ff51;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$id_cita[$index];?>';">
                                                                        SOLICITAR<br>REFACCIÓN
                                                                    </button>
                                                                <?php elseif ($estado_refacciones[$index] == "1"): ?>
                                                                    <button type="button" class="btn" style="background-color: #c7ecc7;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$id_cita[$index];?>';">
                                                                        RECIBIR<br>REFACCIÓN
                                                                    </button>
                                                                <?php elseif ($estado_refacciones[$index] == "2"): ?>
                                                                    <button type="button" class="btn" style="background-color: #5bc0de;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$id_cita[$index];?>';">
                                                                        ENTREGAR<br>REFACCIÓN
                                                                    </button>
                                                                <?php else: ?>
                                                                    <button type="button" class="btn btn-success" style="font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$id_cita[$index];?>';">
                                                                        VER
                                                                    </button>
                                                                <?php endif ?>
                                                            <!-- Si es una cotizacion con garantias y no se han entregado las refacciones (liberada de garantias) -->
                                                            <?php elseif ($ruta[$index] == "2"): ?>
                                                                <?php if ($estado_refacciones[$index] == "0"): ?>
                                                                    <button type="button" class="btn" style="background-color: #f5ff51;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Garantias/".$id_cita[$index];?>';">
                                                                        SOLICITAR<br>REFACCIÓN
                                                                    </button>
                                                                <?php elseif ($estado_refacciones[$index] == "1"): ?>
                                                                    <button type="button" class="btn" style="background-color: #c7ecc7;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Garantias/".$id_cita[$index];?>';">
                                                                        RECIBIR<br>REFACCIÓN
                                                                    </button>
                                                                <?php elseif ($estado_refacciones[$index] == "2"): ?>
                                                                    <button type="button" class="btn" style="background-color: #5bc0de;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Garantias/".$id_cita[$index];?>';">
                                                                        ENTREGAR<br>REFACCIÓN
                                                                    </button>
                                                                <?php else: ?>
                                                                    <button type="button" class="btn btn-success" style="font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Garantias/".$id_cita[$index];?>';">
                                                                        VER
                                                                    </button>
                                                                <?php endif ?>
                                                            <!-- si ya se afecto por el cliente -->
                                                            <?php elseif (($acepta_cliente[$index] != "")&&($ruta[$index] == "1")): ?>
                                                                <button type="button" class="btn btn-danger" style="color:white;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Final/".$id_cita[$index];?>';">
                                                                    VER
                                                                </button>
                                                            <!-- si ya se edita por ventanilla -->
                                                            <?php elseif ($envia_ventanilla[$index] != "0"): ?>
                                                                <button type="button" class="btn btn-info" style="color:white;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Ventanilla/".$id_cita[$index];?>';">
                                                                    VER
                                                                </button>
                                                            <!-- si no se ha liberado de ventanilla -->
                                                            <?php else: ?>
                                                                <button type="button" class="btn btn-primary" style="color:white;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Ventanilla/".$id_cita[$index];?>';">
                                                                    EDITAR
                                                                </button>
                                                            <?php endif; ?>
                                                        </td>
                
            
                                                        <td align="center" style="width:15%;vertical-align: middle;">
                                                            <!--Requisicion para presupuestos tradicionales -->
                                                            <?php if ($ruta[$index] == "1"): ?>
                                                                <?php if (($acepta_cliente[$index] != "")&&($acepta_cliente[$index] != "No")): ?>
                                                                    <?php if ($firma_requisicion[$index] != ""): ?>
                                                                        <a href="<?=base_url()."Requisicion_T/".$id_cita[$index];?>" class="btn btn-secondary" style="color:white;font-size: 10px;" target="_blank">REQUISICIÓN</a>
                                                                    <?php else: ?>
                                                                        <a href="<?=base_url()."Requisicion_Firma_T/".$id_cita[$index];?>" class="btn btn-success" style="color:white;font-size: 10px;">REQUISICIÓN<br>FIRMA</a>
                                                                    <?php endif ?>
                                                                <?php endif ?>
                                                            <!--Requisicion para presupuestos con garantias -->
                                                            <?php elseif ($ruta[$index] == "2"): ?>
                                                                <?php if ($firma_requisicion_garantias[$index] != ""): ?>
                                                                    <a href="<?=base_url()."Requisicion_G/".$id_cita[$index];?>" class="btn btn-secondary" style="color:white;font-size: 10px;" target="_blank">REQUISICIÓN</a>
                                                                <?php else: ?>
                                                                    <a href="<?=base_url()."Requisicion_Firma_G/".$id_cita[$index];?>" class="btn btn-success" style="color:white;font-size: 10px;">REQUISICIÓN<br>FIRMA</a>
                                                                <?php endif ?>
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <!-- Si no se cargaron los datos para mostrar -->
                                            <?php else: ?>
                                                <tr>
                                                    <td colspan="9" style="width:100%;" align="center">Sin registros que mostrar</td>
                                                </tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
            
                                    <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                                    <input type="hidden" name="" id="rolVista" value="listaVentanilla">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection