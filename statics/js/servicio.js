const getSucursales = () => {
	var url = site_url + "/servicios/getSucursales";
	$("#id_sucursal_ubicacion").empty();
	$("#id_sucursal_ubicacion").append(
		"<option value=''>-- Selecciona --</option>"
	);
	$("#id_sucursal_ubicacion").attr("disabled", true);
	id_estado = $("#id_estado").val();
	if (id_estado != "") {
		ajaxJson(
			url,
			{
				id_estado: id_estado,
			},
			"POST",
			"",
			function (result) {
				if (result.length != 0) {
					$("#id_sucursal_ubicacion").empty();
					$("#id_sucursal_ubicacion").removeAttr("disabled");
					result = JSON.parse(result);
					$("#id_sucursal_ubicacion").append(
						"<option value=''>-- Selecciona --</option>"
					);
					$.each(result, function (i, item) {
						$("#id_sucursal_ubicacion").append(
							"<option value= '" +
								result[i].id +
								"'>" +
								result[i].sucursal +
								"</option>"
						);
					});
				} else {
					$("#id_sucursal_ubicacion").empty();
					$("#id_sucursal_ubicacion").append(
						"<option value='0'>No se encontraron datos</option>"
					);
				}
			}
		);
		getMunicipios();
	} else {
		$("#id_sucursal_ubicacion").empty();
		$("#id_sucursal_ubicacion").append(
			"<option value=''>-- Selecciona --</option>"
		);
		$("#id_sucursal_ubicacion").attr("disabled", true);
	}
};
const getMunicipios = () => {
	var url = site_url + "/servicios/getMunicipios";
	$("#id_municipio").empty();
	$("#id_municipio").append(
		"<option value=''>-- Selecciona --</option>"
	);
	$("#id_municipio").attr("disabled", true);
	id_estado = $("#id_estado").val();
	if (id_estado != "") {
		ajaxJson(
			url,
			{
				id_estado: id_estado,
			},
			"POST",
			"",
			function (result) {
				if (result.length != 0) {
					$("#id_municipio").empty();
					$("#id_municipio").removeAttr("disabled");
					result = JSON.parse(result);
					$("#id_municipio").append(
						"<option value=''>-- Selecciona --</option>"
					);
					$.each(result, function (i, item) {
						$("#id_municipio").append(
							"<option value= '" +
								result[i].id +
								"'>" +
								result[i].municipio +
								"</option>"
						);
					});
				} else {
					$("#id_municipio").empty();
					$("#id_municipio").append(
						"<option value='0'>No se encontraron datos</option>"
					);
				}
			}
		);
	} else {
		$("#id_municipio").empty();
		$("#id_municipio").append(
			"<option value=''>-- Selecciona --</option>"
		);
		$("#id_municipio").attr("disabled", true);
	}
};
const getDatosUsuario = () => {
	var url = site_url + "/servicios/getDatosUsuarios";
	if ($("#id_usuario_list").val() != "") {
		ajaxJson(
			url,
			{
				id_usuario: $("#id_usuario_list").val(),
			},
			"POST",
			"",
			function (result) {
				if (result.length != 0) {
					result = JSON.parse(result);
					$.each(result.usuario, function (i, item) {
						if (i != "id" && i != "id_ubicacion") {
							$("#" + i).val(item);
							if ($("#" + i).hasClass("js_user")) {
								$("#" + i).prop("readonly", true);
							}
						}
					});
					if ($("#id_facturacion").val() == "") {
						$("#id_facturacion").val(0);
					}
					if ($("#id_facturacion").val() != 0) {
						$("#check_facturacion").trigger("click");
					}
					$("#id_estado").trigger("change");
					console.log(result.usuario.id_municipio);
					$("#id_municipio").val(result.usuario.id_municipio);
					$("#id_sucursal_ubicacion").trigger('change');
					//Hay un vehículo
					if (result.autos.length == 1) {
						$.each(result.autos[0], function (i, item) {
							if (i != "id") {
								$("#" + i).val(item);
								$("#" + i).prop("readonly", true);
							}
						});

						$("#id_marca").trigger("change");
						$("#id_modelo").val(result.autos[0].id_modelo);
						$("#id_auto").val(result.autos[0].id);
					} else if (result.autos.length > 1) {
						customModal(
							site_url + "/servicios/getVehiculos",
							{
								id_usuario: $("#id_usuario_list").val(),
							},
							"POST",
							"lg",
							"",
							"",
							"",
							"Cancelar",
							"",
							"modalVehiculos"
						);
					} else {
						$("#check_vehiculo").prop("checked", true);
					}
					var pos = {
						lat: parseFloat(result.usuario.latitud),
						lng: parseFloat(result.usuario.longitud),
					};
					latLng = new google.maps.LatLng(
						parseFloat(result.usuario.latitud),
						parseFloat(result.usuario.longitud)
					);

					inicializar_marcador(latLng);
					infowindow.setPosition(pos);
					infowindow.setContent("Ubicación encontrada");
					map.setCenter(pos);
				} else {
					$("#frm")[0].reset();
					$("input").removeAttr("readonly");
				}
			}
		);
	} else {
		$("#frm")[0].reset();
		$("input").removeAttr("readonly");
	}
};
const getModelos = () => {
	var url = site_url + "/servicios/getModelos";
	$("#id_modelo").empty();
	$("#id_modelo").append("<option value=''>-- Selecciona --</option>");
	$("#id_modelo").attr("disabled", true);
	id_marca = $("#id_marca").val();
	if (id_marca != "") {
		ajaxJson(
			url,
			{
				id_marca: id_marca,
			},
			"POST",
			"",
			function (result) {
				if (result.length != 0) {
					$("#id_modelo").empty();
					$("#id_modelo").removeAttr("disabled");
					result = JSON.parse(result);
					$("#id_modelo").append("<option value=''>-- Selecciona --</option>");
					$.each(result, function (i, item) {
						$("#id_modelo").append(
							"<option value= '" +
								result[i].id +
								"'>" +
								result[i].modelo +
								"</option>"
						);
					});
				} else {
					$("#id_modelo").empty();
					$("#id_modelo").append(
						"<option value='0'>No se encontraron datos</option>"
					);
				}
			}
		);
	} else {
		$("#id_modelo").empty();
		$("#id_modelo").append("<option value=''>-- Selecciona --</option>");
		$("#id_modelo").attr("disabled", true);
	}
};
const vehiculoNuevo = () => {
	if ($("#check_vehiculo").prop("checked")) {
		$("#id_auto").val(0);
		$("#placas").prop("readonly", false);
		$("#placas").val("");
		$("#id_color").val("");
		$("#id_color").val("");
		$("#id_marca").val("");
		$("#id_modelo").val("");
		$("#id_anio").val("");
	} else {
		$("#placas").trigger("change");
	}
};
const showFacturacion = () => {
	if ($("#check_facturacion").prop("checked")) {
		$("#div_facturacion").removeClass("d-none");
	} else {
		$("#div_facturacion").addClass("d-none");
	}
};
const getAutosByPlacas = () => {
    ajaxJson(site_url + "/servicios/getAutoByPlacas", {
        "placas": $("#placas").val()
    }, "POST", "", function(result) {
        result = JSON.parse(result);
        if (result.auto) {
            $.each(result.auto, function(i, item) {
                if (i != 'id' && i != 'id_usuario') {
                    $("#" + i).val(item);
                }
            });
            $("#id_marca").trigger('change');
            $("#id_modelo").val(result.auto.id_modelo)
            $("#id_auto").val(result.auto.id)
            $("#placas").prop('readonly', true)
        }
    });
}
const getFechas = () => {
	var url = site_url + "/servicios/getFechasBySucursal";
	$("#fecha").empty();
	$("#fecha").append("<option value=''>-- Selecciona --</option>");
	$("#fecha").attr("disabled", true);
	var data = {
		id_sucursal: $("#id_sucursal_ubicacion").val()
	}
	if($("#id_lavador").val()!=''){
		data.id_lavador = $("#id_lavador").val()
	}
	if (data.id_sucursal != "" || $("#id_lavador").val() !='') {
		ajaxJson(
			url,
			data,
			"POST",
			"",
			function (result) {
				result = JSON.parse(result);
				if (result.length != 0) {
					$("#fecha").empty();
					$("#fecha").removeAttr("disabled");
					$("#fecha").append("<option value=''>-- Selecciona --</option>");
					$.each(result, function (i, item) {
						$("#fecha").append(
							"<option value= '" +
								result[i].fecha +
								"'>" +
								result[i].fecha +
								"</option>"
						);
					});
				} else {
					$("#fecha").empty();
					$("#fecha").append("<option value='0'>No se encontraron datos</option>");
					$("#hora").empty();
					$("#hora").append("<option value='0'>No se encontraron datos</option>");
				}
			}
		);
	} else {
		$("#fecha").empty();
		$("#fecha").append("<option value=''>-- Selecciona --</option>");
		$("#fecha").attr("disabled", true);
		$("#hora").empty();
		$("#hora").append("<option value=''>-- Selecciona --</option>");
		$("#hora").attr("disabled", true);
	}
};
$("#id_estado").on("change", getSucursales);
$("#id_usuario_list").on("change", getDatosUsuario);
$("#id_marca").on("change", getModelos);
$("#check_vehiculo").on("click", vehiculoNuevo);
$("#check_facturacion").on("click", showFacturacion);
$("#placas").on('change',getAutosByPlacas);
$("#id_sucursal_ubicacion").on("change", getFechas);