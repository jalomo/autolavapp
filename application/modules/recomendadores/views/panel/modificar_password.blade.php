@layout('recomendadores/layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12">
            <form id="formulario_passord" method="post" action="" autocomplete="on">
                <h3 style="color:darkblue;">
                    <a href="#" class="btn btn-menu-titulo">
                        <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                    </a>
                    <?php if(isset($titulo_dos)): ?>
                        <?php echo $titulo_dos; ?>
                    <?php endif; ?>
                </h3> 

                <hr>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <label for="">
                            <i class="fa fa-check-circle-o valida" aria-hidden="true" id="valida_1"></i>
                            Contraseña actual : 
                        </label>
                        <input type="password" class="form-control" minlength="8" name="pass_actual" onblur="validar_password()" value="">
                        <div id="pass_actual_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <label for="">
                            <i class="fa fa-check-circle-o valida" aria-hidden="true" id="valida_2"></i>
                            Nueva Contraseña : 
                        </label>
                        <span style="color: darkblue;">*Minimo 8 caracteres </span>
                        <input type="password" class="form-control" minlength="8" name="pass_nvo" onblur="validar_pass()" value="">
                        <div id="pass_nvo_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <label for="">
                            <i class="fa fa-check-circle-o valida" aria-hidden="true" id="valida_3"></i>
                            Repetir Nueva Contraseña : 
                        </label>
                        <input type="password" class="form-control" minlength="8" name="pass_nvo_2" onblur="validar_coincidencia()" value="">
                        <div id="pass_nvo_2_error"></div>
                    </div>
                </div>

                <br>
                <div class="col-sm-12" align="center">
                    <br>
                    <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                    <br>
                    <h5 class="error" id="formulario_error"></h5>

                    <a href="<?= base_url().'index.php/recomendadores/panel/index' ?>" class="btn btn-dark" style="margin-right: 35px;">
                        REGRESAR
                    </a>
                    
                    <input type="hidden" name="formulario" value="2">
                    <input type="button" class="btn btn-success" id="enviar_formulario" value="ACTUALIZAR">
                    
                    <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                    <input type="hidden" name="listado" value="<?php if(isset($id)) echo $id; else echo '0'; ?>">
                    <br>    
                </div>
            </form>            
        </div>
    </div>
@endsection
@section('included_js')

    <script src="{{ base_url('statics/js/recomendadores/cambiar_password.js') }}"></script>

@endsection