@layout('recomendadores/layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12">
            <form id="formulario_recomendador" method="post" action="" autocomplete="on" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 style="color:darkblue;">
                            <a href="#" class="btn btn-menu-titulo">
                                <i class="fa fa-user-o" aria-hidden="true"></i>
                            </a>
                            <?php if(isset($titulo_dos)): ?>
                                <?php echo $titulo_dos; ?>
                            <?php endif; ?>
                        </h3>
                    </div>                     
                </div>

                <hr>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Nombre : </label>
                        <input type="text" class="form-control" name="nombre" value="<?php if(isset($nombre)) echo $nombre; ?>">
                        <div id="nombre_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Apellido Paterno : </label>
                        <input type="text" class="form-control" name="ap_paterno" value="<?php if(isset($ap_paterno)) echo $ap_paterno; ?>">
                        <div id="ap_paterno_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Apellido Materno : </label>
                        <input type="text" class="form-control" name="am_paterno" value="<?php if(isset($am_paterno)) echo $am_paterno; ?>">
                        <div id="am_paterno_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Celular : </label>
                        <input type="text" class="form-control" name="celular" onblur="validar_telefono()" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($celular)) echo $celular; ?>">
                        <input type="hidden" id="old_tel" value="<?php if(isset($celular)) echo $celular; ?>">
                        <div id="celular_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Correo Electrónico : </label>
                        <input type="text" class="form-control" name="email" value="<?php if(isset($email)) echo $email; ?>">
                        <div id="email_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Usuario : </label>
                        <input type="text" class="form-control" name="nUsuario" onblur="validar_uso()" value="<?php if(isset($nUsuario)) echo $nUsuario; ?>">
                        <div id="nUsuario_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="">R.F.C : </label>
                        <input type="text" class="form-control" name="rfc" maxlength="14" style="text-transform: uppercase;" value="<?php if(isset($rfc)) echo $rfc; ?>">
                        <div id="rfc_error"></div>
                    </div>
                    <div class="col-sm-6">
                        <label for="">Sucursal de registro : </label>
                        <input type="text" class="form-control" value="<?php if(isset($reg_sucursal)) echo $reg_sucursal; ?>" disabled>
                    </div>
                </div>

                <br>
                <h5 style="color:darkblue;">
                    Datos del Domicilio
                </h5>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="">Calle : </label>
                        <input type="text" class="form-control" name="calle" value="<?php if(isset($calle)) echo $calle; ?>">
                        <div id="calle_error"></div>
                    </div>
                    <div class="col-sm-2">
                        <label for="">N° Interno : </label>
                        <input type="text" class="form-control" name="n_int" value="<?php if(isset($n_int)) echo $n_int; else echo 'SN'; ?>">
                        <div id="n_int_error"></div>
                    </div>
                    <div class="col-sm-2">
                        <label for="">N° Externo : </label>
                        <input type="text" class="form-control" name="n_ext" value="<?php if(isset($n_ext)) echo $n_ext; else echo 'SN'; ?>">
                        <div id="n_ext_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Colonia : </label>
                        <input type="text" class="form-control" name="colonia" value="<?php if(isset($colonia)) echo $colonia; ?>">
                        <div id="colonia_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Municipio : </label>
                        <input type="text" class="form-control" name="ciudad" value="<?php if(isset($municipio)) echo $municipio; ?>">
                        <div id="ciudad_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Estado : </label>
                        <input type="text" class="form-control" name="edo" value="<?php if(isset($edo)) echo (($edo != '') ? $edo : $reg_edo); ?>">
                        <div id="edo_error"></div>
                    </div>
                </div>

                <br>
                <div class="col-sm-12" align="center">
                    <br>
                    <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                    <br>
                    <h5 class="error" id="formulario_error"></h5>
                    
                    <input type="hidden" name="formulario" value="2">
                    <input type="button" class="btn btn-success" id="enviar_formulario" value="ACTUALIZAR"> 

                    <a href="<?= base_url().'index.php/recomendadores/panel/modificar_password' ?>" class="btn btn-warning" style="margin-left: 30px;">
                        CAMBIAR CONTRASEÑA
                    </a>
                    
                    <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                    <input type="hidden" name="listado" value="<?php if(isset($id)) echo $id; else echo '0'; ?>">
                    <br>    
                </div>
            </form>            
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <form id="formulario_banco" method="post" action="" autocomplete="on">
                <h3 style="color:darkblue;">
                    <a href="#" class="btn btn-menu-titulo">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                    </a>
                    Datos bancarios
                </h3> 

                <hr>
                <div class="row">
                    <div class="col-sm-8">
                        <label for="">CLABE bancaria : </label>
                        <input type="text" class="form-control" name="clave_bancaria" maxlength="19" onblur="buscar_banco()" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($clave_bancaria)) echo $clave_bancaria; ?>">
                        <div id="clave_bancaria_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-5">
                        <label for="">Banco : </label>
                        <select id="banco" name="n_banco" class="form-control">
                            <option value=""> -- Seleccionar --</option>
                            @foreach ($cat_bancos as $s => $banco_cat) 
                                <option value="<?=$banco_cat->id ?>" <?= (($n_banco == $banco_cat->id) ? "selected" : "")?>> 
                                    <?= $banco_cat->nombre  ?> 
                                </option>
                            @endforeach
                        </select>
                        <input type="hidden" class="form-control" id="clave_bco" value="<?php if(isset($clave_sat)) echo $clave_sat; ?>">
                        <div id="banco_error"></div>
                    </div>
                    <div class="col-sm-7">
                        <label for="">Nombre / Razón Social : </label>
                        <input type="text" class="form-control" id="nombre_banco" value="<?php if(isset($nombre_banco)) echo $nombre_banco; ?>" disabled>
                        <input type="hidden" class="form-control" name="banco" value="<?php if(isset($banco)) echo $banco; ?>">
                        <div id="nombre_banco_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Sucursal : </label>
                        <input type="text" class="form-control" name="sucursal" value="<?php if(isset($sucursal)) echo $sucursal; ?>">
                        <div id="sucursal_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">N° de Cuenta : </label>
                        <input type="text" class="form-control" name="cuenta" maxlength="11" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($cuenta)) echo $cuenta; ?>">
                        <div id="cuenta_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">N° de Tarjeta : </label>
                        <input type="text" class="form-control" name="n_tarjeta"  maxlength="18" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value="<?php if(isset($n_tarjeta)) echo $n_tarjeta; ?>">
                        <div id="n_tarjeta_error"></div>
                    </div>
                </div>

                <br>
                <div class="col-sm-12" align="center">
                    <br>
                    <i id="cargaIcono2" class="fa fa-spinner cargaIcono fa-spin" style="display: none;"></i>
                    <br>
                    <h5 class="error" id="formulario_error2"></h5>
                    
                    <input type="hidden" name="formulario" value="2">
                    <input type="button" class="btn btn-success" id="envio_form_bco" value="ACTUALIZAR DATOS BANCARIOS">
                    
                    <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                    <input type="hidden" name="listado" value="<?php if(isset($id)) echo $id; else echo '0'; ?>">
                    <br>    
                </div>
            </form>            
        </div>
    </div>
@endsection
@section('included_js')

    <script src="{{ base_url('statics/js/recomendadores/formulario.js') }}"></script>
    <script src="{{ base_url('statics/js/recomendadores/formulario_banco.js') }}"></script>

@endsection