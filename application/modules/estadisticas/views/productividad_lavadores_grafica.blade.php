@layout('layout')
@section('included_css')
    <style>
        #container {
            max-width: 400px;
            margin: 0 auto;
        }

        .highcharts-figure,
        .highcharts-data-table table {
            min-width: 380px;
            max-width: 600px;
            margin: 0 auto;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

    </style>
@endsection
@section('contenido')
    <h2>{{ $titulo_dos }}</h2>
    <br>
    <form id="frm" method="POST">
        @if ($this->session->userdata('id_rol') == 1)
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Sucursal</label>
                    {{ $sucursales }}
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-4">
                <label for="">Lavador</label>
                {{ $id_lavador }}
                <span class="error error_id_lavador"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Fecha inicio</label>
                <input type="date" id="fecha_inicio" name="fecha_inicio" class="form-control" value="{{ date('Y-m-d') }}">
            </div>
            <div class="col-sm-4">
                <label for="">Fecha fin</label>
                <input type="date" id="fecha_fin" name="fecha_fin" class="form-control" value="{{ date('Y-m-d') }}">
            </div>
        </div>
    </form>
    <br>
    <div class="row text-right">
        <div class="col-sm-12 pull-right">
            <button id="buscar" class="btn btn-info">Buscar</button>
        </div>
    </div>
    <br>
    <div id="div_estadisticas"></div>

@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="statics/js/lavadores.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        const id_rol = "{{ $this->session->userdata('id_rol') }}";
        const buscar = () => {
            if (($("#fecha_inicio").val() == '' && $("#fecha_fin").val() == '') || ($("#fecha_inicio").val() != '' && $(
                    "#fecha_fin").val() != '') && $("#id_lavador").val() != '') {
                if (id_rol == 1 && $("#id_sucursal").val() == '') {
                    ErrorCustom('Es necesario seleccionar la sucursal');
                } else {
                    var url = site_url + "/estadisticas/buscar_grafica_productividad_lavadores";
                    ajaxLoad(url, $("#frm").serialize(), "div_estadisticas", "POST", function() {

                    });
                }
            } else {
                ErrorCustom('Es necesario ingresar todos los parámetros de búsqueda');
            }
        }
        $("#buscar").on('click', buscar);

    </script>

@endsection
