var _appsFunction = function () {

	this.Recalcular = function ($this) {
        var HorasDia = $('input#HorasDia').val();
        var calculado = parseFloat( $this.value / HorasDia ).toFixed(2)
        $('input#SalarioHora').val('$'+ (new Intl.NumberFormat('en-US').format(calculado)));
        $('input[name=SalarioHora]').val(calculado);
        $('input[name=SDICapturado]').val($this.value);
		
	},
	this.regresar = function () {
		window.location.href = PATH+'/nomina/inicio/percepcionesdeducciones';
    }		
	this.guardar = function () {
		var data_send =  $('form#general_form').serializeArray();
        data_send.push({ 'name': 'id','value':id });
        data_send.push({ 'name': 'id_trabajador','value':id_trabajador });
		

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/catalogosconsultas/historico_salarios/alta_guardar',
            data: data_send,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					}).then((result) => {
						window.location.href = PATH + '/nomina/catalogosconsultas/historico_salarios/index/'+id_trabajador;
					});
				}

            }

        });
    }		
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	//Apps.init();
});
