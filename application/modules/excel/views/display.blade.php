@layout('layout')
@section('contenido')
<h1 class="text-center">Información importada</h1>
<div class="table-responsive">
    <table class="table table-hover tablesorter">
        <thead>
            <tr>
                <th class="header">vin</th>
                <th class="header">asc</th>         
                <th class="header">nombre_propietario</th>
                <th class="header">telefono</th>
                <th class="header">Dirección</th>
                <th class="header">email</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($vinList) && !empty($vinList)) {
                foreach ($vinList as $key => $element) {
                    ?>
                    <tr>
                        <td><?php echo $element['vin']; ?></td>   
                        <td><?php echo $element['asc1']; ?></td>     
                        <td><?php echo $element['primer_nombre'].' '.$element['segundo_nombre']
                        .' '.$element['apellidos']; ?></td> 
                        <td><?php echo $element['telefono']; ?></td>
                        <td><?php echo $element['direccion']; ?></td>
                        <td><?php echo $element['email']; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="9">No hay registros.</td>    
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>
@endsection