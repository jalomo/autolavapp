@layout('layout')
@section('included_css')
    <style>
        .highcharts-figure,
        .highcharts-data-table table {
            min-width: 310px;
            max-width: 800px;
            margin: 1em auto;
        }

        #container {
            height: 400px;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

    </style>
    <link rel="stylesheet" href="<?php echo base_url(); ?>statics/css/bootstrap-multiselect.css">
@endsection
@section('contenido')
    <h2>{{ $titulo_dos }}</h2>
    <br>
    <form id="frm" action="" method="POST">
        @if($this->session->userdata('id_rol')==1)
        <div class="row">
            <div class="col-sm-6">
                <label for="">Sucursal</label>
                {{$sucursales}}
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-sm-6">
                <label for="">Fecha inicio</label>
                <input type="date" id="fecha_inicio" name="fecha_inicio" class="form-control" value="{{date('Y-m-d')}}">
            </div>
            <div class="col-sm-6">
                <label for="">Fecha fin</label>
                <input type="date" id="fecha_fin" name="fecha_fin" class="form-control" value="{{date('Y-m-d')}}">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Origen</label>
                {{$origen}}
            </div>
            <div class="col-sm-4">
                <label for="">Cita</label>
                <select class="form-control multiple" name="confirmadas[]" id="confirmadas" multiple="multiple">
                    <option value="1">Confirmadas</option>
                    <option value="0">No Confirmadas</option>
                </select>
            </div>
            <div class="col-sm-4">
                <label for="">Cancelados</label>
                <select class="form-control multiple" name="cancelados[]" id="cancelados" multiple="multiple">
                    <option value="1">Cancelado</option>
                    <option value="0">No cancelados</option>
                </select>
            </div>
        </div>
    </form>
    <br>
    <div class="row">
        <div class="col-sm-4">
            <button type="button" id="exportarExcel" data-action="excel/cantidad_servicios"
                class="btn btn-danger js_exportar"><i style="color: white"
                    class="fa fa-file-excel-o"></i> Exportar EXCEL</button>
        </div>
        <div class="col-sm-8 text-right">
            <button id="buscar" class="btn btn-info">Buscar</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="table-servicios"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <figure class="highcharts-figure">
                <div id="container"></div>
                <p class="highcharts-description">

                </p>
            </figure>
        </div>
    </div>

@endsection
@section('included_js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="{{ base_url('statics/js/bootstrap-multiselect.js') }}"></script>
    <script>
        var site_url = "{{ site_url() }}";
        const id_rol = "{{$this->session->userdata('id_rol')}}";
        $('.multiple').multiselect({
            enableFiltering: true,
            enableFullValueFiltering: true,
            buttonWidth: '100%',
            includeSelectAllOption: true,
            selectAllJustVisible: false,
            buttonClass: 'btn btn-success',
        });
        const data = [];
        @foreach($servicios as $s => $servicio)
            data.push({
                        name: "{{$servicio->origen}}",
                        y: parseFloat("{{$servicio->total}}"),
                        drilldown: "{{$servicio->origen}}"
                    });
        @endforeach
        var chart = Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Cantidad de servicios generados'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total de servicios'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> de total<br/>'
            },

            series: [{
                name: "Servicios",
                colorByPoint: true,
                data: data
            }]
        });
        const buscar = () => {
            if ($("#fecha_inicio").val() && $("#fecha_fin").val()) {
                if(id_rol==1 && $("#id_sucursal").val()==''){
                    ErrorCustom('Es necesario seleccionar la sucursal');
                }else{
                    var url = site_url + "/estadisticas/buscar_total_servicios";
                    ajaxJson(url, $("#frm").serialize(), "POST", "", function(result) {
                        result = JSON.parse(result);
                        updateChart(result.servicios)
                        buscar_servicios();
                    });
                }
            } else {
                ErrorCustom('Es necesario ingresar ambas fechas');
            }
        }
        const buscar_servicios = () => {
            var url = site_url + "/estadisticas/buscar_servicios";
            ajaxLoad(url, $("#frm").serialize(), "table-servicios", "POST", function() {

            });
        }
        const updateChart = (servicios) => {
            const data = [];
            servicios.map((servicio)=>{
                data.push({
                        name: servicio.origen,
                        y: parseFloat(servicio.total),
                        drilldown: servicio.origen
                    });
            })
            var newSeries = [{
                data: data
            }];
            chart.update({
                series: newSeries
            });
        }
        $("#buscar").on('click', buscar);
        $(".js_exportar").on('click', function() {
            var action = $(this).data('action');
            $('#frm').attr('action', action);
            $('#frm').submit();
        });

    </script>

@endsection
