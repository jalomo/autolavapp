$("#enviar_formulario").on('click', function (e){
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#cargaIcono").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        var base = $("#sitio").val();
        var paqueteDatos = new FormData(document.getElementById('formulario_recomendador'));
        $.ajax({
            url: base+"recomendadores/panel/actualizar_recomendador",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        location.href = base+"index.php/recomendadores/panel/index";
                    } else {
                        $("#formulario_error").text(resp);
                    }

                }else{
                    $("#formulario_error").text("ERROR");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
    }
});

function validar() {
    var retorno = true;
    var campo_a = $("input[name='nombre']").val();
    if (campo_a == "") {
        var error_a = $("#nombre_error");
        error_a.empty();
        $("#nombre_error").css("display","inline-block");
        error_a.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_a = $("#nombre_error");
        error_a.empty();
    }

    var campo_b = $("input[name='celular']").val();
    if (campo_b.length != 10) {
        var error_b = $("#celular_error");
        error_b.empty();
        $("#celular_error").css("display","inline-block");
        error_b.append('<label class="form-text text-danger">Campo incompleto</label>');
        retorno = false;
    }else{
        var error_b = $("#celular_error");
        error_b.empty();
    }

    var correo_2 = $("input[name='email']").val();
    if (!validar_email(correo_2)) {
        var error_2 = $("#email_error");
        $("#email_error").css("display","inline-block");
        error_2.append('<label class="form-text text-danger">Formato invalido.</label>');
        retorno = false;
    }else{
        var error_2 = $("#email_error");
        error_2.empty();
    }

    var campo_c = $("input[name='nUsuario']").val();
    if (campo_c == "") {
        var error_c = $("#nUsuario_error");
        error_c.empty();
        $("#nUsuario_error").css("display","inline-block");
        error_c.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_c = $("#nUsuario_error");
        error_c.empty();
    }

    var campo_D = $("input[name='rfc']").val();
    if (campo_D == "") {
        var error_D = $("#rfc_error");
        error_D.empty();
        $("#rfc_error").css("display","inline-block");
        error_D.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_D = $("#rfc_error");
        error_D.empty();
        if (campo_D.length < 12) {
            $("#rfc_error").css("display","inline-block");
            error_D.append('<label class="form-text text-danger">Campo incompleto</label>');
            retorno = false;
        } 
    }

    var campo_q1 = $("input[name='calle']").val();
    if (campo_q1 == "") {
        var error_q1 = $("#calle_error");
        error_q1.empty();
        $("#calle_error").css("display","inline-block");
        error_q1.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_q1 = $("#calle_error");
        error_q1.empty();
    }

    var campo_q2 = $("input[name='colonia']").val();
    if (campo_q2 == "") {
        var error_q2 = $("#colonia_error");
        error_q2.empty();
        $("#colonia_error").css("display","inline-block");
        error_q2.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_q2 = $("#colonia_error");
        error_q2.empty();
    }

    var campo_q3 = $("input[name='ciudad']").val();
    if (campo_q3 == "") {
        var error_q3 = $("#ciudad_error");
        error_q3.empty();
        $("#ciudad_error").css("display","inline-block");
        error_q3.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_q3 = $("#ciudad_error");
        error_q3.empty();
    }

    var campo_q4 = $("input[name='edo']").val();
    if (campo_q4 == "") {
        var error_q4 = $("#edo_error");
        error_q4.empty();
        $("#edo_error").css("display","inline-block");
        error_q4.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_q4 = $("#edo_error");
        error_q4.empty();
    }

    return retorno;
}

function validar_email( email ) 
{
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

function validar_uso() {
    var base = $("#sitio").val();
    var url_sis = base+"recomendadores/login/usuario_disponible";
    var nombre_usuario = $("input[name='nUsuario']").val();
    $.ajax({
        url: url_sis,
        method: 'post',
        data: {
            usuario : nombre_usuario
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler           </p>")<1) {
                var error_c = $("#nUsuario_error");
                error_c.empty();

                if (resp == "OK") {
                    $("#nUsuario_error").css("display","inline-block");
                    error_c.append('<label class="form-text text-success">NOMBRE DE USUARIO DISPONIBLE</label>');
                    $("#btnregistro").attr('disabled',false);
                } else {
                    $("#nUsuario_error").css("display","inline-block");
                    error_c.append('<label class="form-text text-danger">'+resp+'</label>');
                    $("#btnregistro").attr('disabled',true);
                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

function validar_telefono() {
    var base = $("#sitio").val();
    var url_sis = base+"recomendadores/login/telefono_disponible";
    var telefono = $("input[name='celular']").val();
    var old_telefono = $("#old_tel").val();
    if ((telefono.length == 10)&&(telefono != old_telefono)) {
        $.ajax({
            url: url_sis,
            method: 'post',
            data: {
                telefono : telefono
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    var error_c = $("#celular_error");
                    error_c.empty();

                    if (resp == "OK") {
                        $("#celular_error").css("display","inline-block");
                        error_c.append('<label class="form-text text-success">TELÉFONO DISPONIBLE</label>');
                    } else {
                        $("#celular_error").css("display","inline-block");
                        error_c.append('<label class="form-text text-danger">'+resp+'</label>');
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else{
        var error_c = $("#celular_error");
        error_c.empty();

        $("#celular_error").css("display","inline-block");
        if (telefono == old_telefono) {
            error_c.append('<label class="form-text text-danger">TELÉFONO SIN MODIFICAR</label>');
        }else{
            error_c.append('<label class="form-text text-danger">TELÉFONO INVALIDO</label>');    
        }        
    }
}