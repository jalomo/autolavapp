<form action="" id="frm_comentarios">
	<input type="hidden" name="id" id="id" value="{{$id}}">
	<input type="hidden" name="fecha" id="fecha" value="{{$fecha}}">
	<input type="hidden" name="cliente" id="cliente" value="{{$cliente}}">
	<div class="row">
		<div class="col-sm-6">
			<label for="">Fecha inicio</label>
            <div class='input-group date' id='datetimepicker2'>
                <input type="text" class="form-control" value="{{$fecha}}" name="cronoFecha" id="cronoFecha">
                <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </span>
            </div>
            <span class="error error_cronoFecha"></span>
		</div>
		<div class="col-lg-6">
			<label for="">Hora</label>
			<input type="time" class="form-control" value="" name="cronoHora">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<strong>No contactar cliente en 10 días</strong>
			<input type="checkbox" name="no_contactar" id="no_contactar">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div style="margin-top: 10px" class="alert alert-warning alert-dismissible fade show" role="alert">
				<strong>Al marcar el campo de "No contactar cliente en 10 días" automáticamente se actualizará la fecha de contacto al cliente 10 días después</strong>.
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<strong>No contactar cliente jamás</strong>
			<input type="checkbox" name="nunca_contactar" id="nunca_contactar">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div style="margin-top: 10px" class="alert alert-warning alert-dismissible fade show" role="alert">
				<strong>Al marcar el campo de "No contactar cliente jamás" se dará de baja el cliente del contacto proactivo</strong>.
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<label>Comentario</label>
			{{$input_comentario}}
			<span class="error error_comentario"></span>
		</div>
	</div>
</form>
<script>
	var fecha_actual = "{{$fecha}}";
	$('#datetimepicker2').datetimepicker({
		minDate: fecha_actual,
        format: 'DD/MM/YYYY',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        daysOfWeekDisabled: [0],
        locale: 'es'
    });
    $("#cronoFecha").val("");
    $("#cronoFecha").val("{{date_eng2esp_1($fecha)}}");
</script>