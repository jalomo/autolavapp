@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form action="" method="post">
                <h3>Receptor</h3>
                <?php echo renderInputText("text", "nombre", "Nombre", $data->receptor->nombre, true); ?>
                <?php echo renderInputText("text", "rfc", "Rfc", $data->receptor->rfc, true); ?>
                <?php echo renderInputText("text", "usocfdi", "usocfdi", $data->receptor->usocfdi, true); ?>
            </form>
            <hr>
            <form action="" method="post">
                <h3>Emisor</h3>
                <?php echo renderInputText("text", "nombre", "Nombre", $data->emisor->nombre, true); ?>
                <?php echo renderInputText("text", "rfc", "Rfc", $data->emisor->rfc, true); ?>
                <?php echo renderInputText("text", "regimen_fiscal", "Regimen Fiscal", $data->emisor->regimen_fiscal, true); ?>
            </form>
            <hr>
            <form action="" method="post">
                <h3>Conceptos</h3>
                <?php foreach ($data->conceptos as $key => $conceptos) : ?>
                <div class="row">
                    <div class="col-md-6">
                            <?php echo renderInputText("text", "concepto_descripcion", "Descripción", $conceptos->concepto_descripcion, true); ?>
                            <?php echo renderInputText("text", "concepto_claveprodserv", "Clave producto servicio", $conceptos->concepto_claveprodserv, true); ?>
                            <?php echo renderInputText("text", "concepto_no_identificacion", "No de identificación", $conceptos->concepto_no_identificacion, true); ?>
                            <?php echo renderInputText("text", "concepto_clave_unidad", "Clave unidad", $conceptos->concepto_clave_unidad, true); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText("text", "concepto_cantidad", "Cantidad", $conceptos->concepto_cantidad, true); ?>
                            <?php echo renderInputText("text", "concept_valor_unitario", "Valor unitario", $conceptos->concept_valor_unitario, true); ?>
                            <?php echo renderInputText("text", "concepto_importe", "Importe", $conceptos->concepto_importe, true); ?>
                        </div>
                    </div>
                    <hr>
                <?php endforeach; ?>
            </form>
            <hr>
            <form id="form-readonly" data-readonly="{{$readOnly}}">
                <h3>Factura</h3>
                <div class="row">
                    <div class="col-md-6">
                        <?php renderInputText("text", "no_identificacion", "Lugar de expedicóon", $data->lugar_expedicion, true); ?>
                        <?php renderInputText("text", "forma_pago", "Forma de pago", $data->forma_pago, true); ?>
                        <?php renderInputText("text", "metodo_pago", "Metodo de pago", $data->metodo_pago, true); ?>
                        <?php renderInputText("text", "folio", "Folio", $data->folio, true); ?>
                        <?php renderInputText("text", "no_identificacion", "Serie", $data->serie, true); ?>
                    </div>
                    <div class="col-md-6">
                        <?php renderInputText("text", "no_identificacion", "Tipo de comprobante", $data->tipo_comprobante, true); ?>
                        <?php renderInputText("text", "tipo_cambio", "Tipo de cambio", $data->tipo_cambio, true); ?>
                        <?php renderInputText("text", "moneda", "Moneda", $data->moneda, true); ?>
                        <?php renderInputText("text", "fecha", "Fecha", $data->fecha, true); ?>
                        <?php renderInputText("text", "no_certificado", "No de certificado", $data->no_certificado, true); ?>
                    </div>
                    <div class="col-md-12">
                        <?php renderInputText("text", "total", "Total", $data->total, true); ?>
                        <?php renderInputText("text", "subtotal", "Subtotal", $data->subtotal, true); ?>
                        <?php renderInputTextArea("certificado", "Certificado", $data->certificado, true); ?>
                        <?php echo renderInputTextArea("sello", "Sello", $data->sello, true); ?>
                    </div>
                </div>
            </form>
            <hr>
            <form>
                <h3>Timbre fiscal</h3>
                <div class="row">
                    <div class="col-md-12">
                        <?php renderInputTextArea("sello_cfd", "Sello cfd", $data->timbre_fiscal->sello_cfd, true); ?>
                        <?php renderInputText("text", "no_certificado_sat", "Número de certificado", $data->timbre_fiscal->no_certificado_sat, true); ?>
                        <?php renderInputText("text", "rfc_prov_certif", "Rfc proveedor", $data->timbre_fiscal->rfc_prov_certif, true); ?>
                        <?php echo renderInputText("text","fecha_timbrado", "Fecha de timbrado", $data->timbre_fiscal->fecha_timbrado, true); ?>
                        <?php echo renderInputTextArea("sello", "Sello sat", $data->timbre_fiscal->sello_sat, true); ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('included_js')

@endsection