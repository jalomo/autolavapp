<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Usuarios extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));
    date_default_timezone_set('America/Mexico_City');

    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
  }

  public function lista()
  {
    $data['titulo'] = "Usuarios";
    $data['titulo_dos'] = "";
    $data['rows'] = $this->Mgeneral->get_table('usuarios');
    $this->blade->render('usuarios/lista', $data);
  }
  public function editar($id)
  {
    $titulo['titulo'] = "Usuarios";
    $titulo['titulo_dos'] = "";
    $rows['row'] = $this->Mgeneral->get_row('usuarioID', $id, 'usuarios');
    $header = $this->load->view('main/header', '', TRUE);
    $menu = $this->load->view('main/menu', '', TRUE);
    $header_dos = ""; //$this->load->view('main/header_dos', '', TRUE);
    $titulo = $this->load->view('main/titulo', $titulo, TRUE);
    $contenido = $this->load->view('usuarios/editar', $rows, TRUE);
    $footer = $this->load->view('main/footer', '', TRUE);
    $this->load->view('main/main', array(
      'header' => $header,
      'menu' => $menu,
      'header_dos' => $header_dos,
      'titulo' => $titulo,
      'contenido' => $contenido,
      'footer' => $footer,
      'included_js' => array(
        'statics/js/bootbox.min.js',
        'statics/js/general.js',
        'statics/tema/assets/js/popper.min.js',
        'statics/tema/assets/js/plugins.js',
        'statics/tema/assets/js/main.js',
        'statics/tema/assets/js/lib/data-table/datatables.min.js',
        'statics/tema/assets/js/lib/data-table/dataTables.bootstrap.min.js',
        'statics/tema/assets/js/lib/data-table/dataTables.buttons.min.js',
        'statics/tema/assets/js/lib/data-table/buttons.bootstrap.min.js',
        'statics/tema/assets/js/lib/data-table/jszip.min.js',
        'statics/tema/assets/js/lib/data-table/pdfmake.min.js',
        'statics/tema/assets/js/lib/data-table/vfs_fonts.js',
        'statics/tema/assets/js/lib/data-table/buttons.html5.min.js',
        'statics/tema/assets/js/lib/data-table/buttons.print.min.js',
        'statics/tema/assets/js/lib/data-table/buttons.colVis.min.js',
        'statics/tema/assets/js/lib/data-table/datatables-init.js'
      )
    ));
  }

  public function add_puntos($id)
  {

    $data['usuarioPuntos'] = $this->input->post('usuarioPuntos');

    $this->Mgeneral->update_table_row('usuarios', $data, 'usuarioID', $id);
    echo "1";
  }
  function editar_usuario($id = 0)
  {
    if ($this->input->post()) {
      $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
      $this->form_validation->set_rules('apellido_paterno', 'apellido paterno', 'trim|required');
      $this->form_validation->set_rules('apellido_materno', 'apellido materno', 'trim|required');
      $this->form_validation->set_rules('email', 'email', 'trim|valid_email|required');
      $this->form_validation->set_rules('telefono', 'telefono', 'trim|exact_length[10]|required|numeric');
      $this->form_validation->set_rules('password', 'password', 'trim|required');
      if ($this->form_validation->run()) {
        //Validar correo
        $correo = $this->db->limit(1)->where('email',$_POST['email'])->where('id !=',$_POST['id'])->get('usuarios');
        if($correo->num_rows()==1){
          echo -1;exit();
        }
        //Validar teléfono
        $telefono = $this->db->limit(1)->where('telefono',$_POST['telefono'])->where('id !=',$_POST['id'])->get('usuarios');
        if($telefono->num_rows()==1){
          echo -2;exit();
        }
        $data['nombre'] = $this->input->post('nombre');
        $data['apellido_paterno'] = $this->input->post('apellido_paterno');
        $data['apellido_materno'] = $this->input->post('apellido_materno');
        $data['email'] = $this->input->post('email');
        $data['telefono'] = $this->input->post('telefono');
        $data['password'] = $this->input->post('password');
        $this->db->where('id', $id)->update('usuarios', $data);
        echo 1; exit();
      } else {
        $errors = array(
          'nombre' => form_error('nombre'),
          'apellido_paterno' => form_error('apellido_paterno'),
          'apellido_materno' => form_error('apellido_materno'),
          'email' => form_error('email'),
          'telefono' => form_error('telefono'),
          'password' => form_error('password')
        );
        echo json_encode($errors);
        exit();
      }
    }
    $info_usuario = $this->db->where('id', $id)->get('usuarios')->row();
    $data['id'] = $id;
    $data['nombre'] = form_input('nombre', set_value('nombre', exist_obj($info_usuario, 'nombre')), 'class="form-control" id="nombre"');
    $data['apellido_paterno'] = form_input('apellido_paterno', set_value('apellido_paterno', exist_obj($info_usuario, 'apellido_paterno')), 'class="form-control" id="apellido_paterno"');
    $data['apellido_materno'] = form_input('apellido_materno', set_value('apellido_materno', exist_obj($info_usuario, 'apellido_materno')), 'class="form-control" id="apellido_materno"');
    $data['email'] = form_input('email', set_value('email', exist_obj($info_usuario, 'email')), 'class="form-control" id="email"');
    $data['telefono'] = form_input('telefono', set_value('telefono', exist_obj($info_usuario, 'telefono')), 'class="form-control" id="telefono" maxlength="10');
    $data['password'] = form_password('password','', 'class="form-control" id="password"');
    $this->blade->render('editar_usuario', $data);
  }
}
