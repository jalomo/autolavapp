<form action="" id="frm_phones">
	<input type="hidden" name="idinfo" id="idinfo" value="{{$idinfo}}">
	<div class="row">
		<div class="col-sm-6">
			<label>Teléfono partícular</label>
			{{$input_tparticular}}
			<span class="error error_tparticular"></span>
		</div>
		<div class="col-sm-6">
			<label>Teléfono celular</label>
			{{$input_tcelular}}
			<span class="error error_tcelular"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label>Teléfono trabajo</label>
			{{$input_ttrabajo}}
			<span class="error error_ttrabajo"></span>
		</div>
		<div class="col-sm-6">
			<label>Correo electrónico</label>
			{{$input_mail}}
			<span class="error error_mail"></span>
		</div>
	</div>
</form>