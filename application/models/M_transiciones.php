<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_transiciones extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insertTransicion()
    {
        //id_status_actual 
        //usuario : nombre de usuario que hace el cambio
        //id_status_nuevo : es el nuevo estatus
        
        //Verificar si no existe la transaccion traerme la de creación de la cita
        //Si el estatus actual es 1 es que se va hacer por primera vez el cambio
        if ($_POST['id_status_actual'] == 1) {
            $inicio = $this->getLastTransition($this->input->post('id_servicio'));
        } else {
            $inicio = $this->getLastTransition($this->input->post('id_servicio'));
        }
        
        if ($inicio == '') {
            $inicio = $this->getFechaCreacion($this->input->post('id_servicio'));
        }
        $datos_transiciones = array(
            'status_anterior' => $this->getStatusById($this->input->post('id_status_actual')),
            'status_nuevo' => $this->getStatusById($this->input->post('id_status_nuevo')),
            'created_at' => date('Y-m-d H:i:s'),
            'inicio' => $inicio,
            'fin' => date('Y-m-d H:i:s'),
            'usuario' => $_POST['usuario'],
            'id_servicio' => $this->input->post('id_servicio'),
            'id_lavador' => $this->getLavador($_POST['id_servicio']),
            'minutos_transcurridos' => dateDiffMinutes($inicio, date('Y-m-d H:i:s')),
            'estatus_graficar' => ($this->input->post('id_status_nuevo')>=4)?1:0
        );
        $this->db->insert('transiciones_estatus', $datos_transiciones);
        $this->db->where('id',$_POST['id_servicio'])->set('id_estatus_lavado',$_POST['id_status_nuevo'])->update('servicio_lavado');
        if($_POST['id_status_nuevo']==6){
            $this->db->where('id',$_POST['id_servicio'])->set('fecha_entrega_unidad',date('Y-m-d H:i:s'))->update('servicio_lavado');
        }
        return 1;
    }
    //Obtener la última transacción
    public function getLastTransition($id_servicio = '')
    {
        $q = $this->db->where('id_servicio', $id_servicio)->limit(1)->order_by('id', 'desc')->get('transiciones_estatus');
        if ($q->num_rows() == 1) {
            return $q->row()->fin;
        } else {
            return '';
        }
    }
    public function getFechaCreacion($id = '')
    {
        $q = $this->db->where('id', $id)->select('created_at')->get('servicio_lavado');
        if ($q->num_rows() == 1) {
            return $q->row()->created_at;
        } else {
            return '';
        }
    }
    //Obtiene el nombre del estatus
    public function getStatusById($id = '')
    {
        $q = $this->db->where('id', $id)->select('estatus')->get('cat_estatus_lavado');
        if ($q->num_rows() == 1) {
            return $q->row()->estatus;
        } else {
            return '';
        }
    }
    //Obtiene el estatus anterior de las transiciones por servicio
    public function getLastStatusTransiciones($id_servicio = '')
    {
        $q = $this->db->where('id', $id_servicio)->select('status_nuevo')->order_by('id', 'desc')->limit(1)->get('transiciones_estatus');
        if ($q->num_rows() == 1) {
            return $q->row()->status_nuevo;
        } else {
            return '';
        }
    }
    public function getLavador($id_servicio){
        $q = $this->db->where('id', $id_servicio)->select('id_lavador')->get('servicio_lavado');
        if ($q->num_rows() == 1) {
            return $q->row()->id_lavador;
        } else {
            return '';
        }
    }
}
