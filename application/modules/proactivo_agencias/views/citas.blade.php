@layout('layout')
@section('contenido')
    <h1 class="text-center">Citas por agencia</h1>
    <form action="" id="frm" method="POST">
        <div class="row">
            <div class='col-sm-3'>
                <label for="">Selecciona la agencia</label>
                {{ $id_agencia }}
                <span class="error error_id_agencia"></span>
            </div>
            <div class="col-md-2 col-sm-2" style="margin-top:30px;">
                <button type="button" id="buscar" name="buscar" class="btn btn-info">Buscar</button>
            </div>
        </div>
    </form>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="tabla-items">
            </div>
        </div>
    </div>
@endsection
@section('included_js')
    <script>
        $("body").on('click', '#buscar', function() {
            $(".error").empty();if($("#id_agencia").val()==''){
                $(".error_id_agencia").empty().append('Es necesario seleccionar la agencia').css('color', 'red')
            } else {
                buscarInformacion();
            }
        });
        function buscarInformacion() {
            var url = site_url + "/proactivo_agencias/buscar_cita_agencia";
            ajaxLoad(url, $("#frm").serialize(), "tabla-items", "POST", function() {
                
            });
        }

    </script>
@endsection
