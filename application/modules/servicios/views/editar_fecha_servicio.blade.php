@layout('layout')
@section('contenido')
    <div class="row">
        <div class="col-sm-4">
            <label for="">Lavador actual : </label>
            <input class="form-control" type="text" readonly value="{{ $horario->lavadorNombre }}">
        </div>
        <div class="col-sm-4">
            <label for="">Fecha actual : </label>
            <input class="form-control" type="text" readonly value="{{ $horario->fecha }}">
        </div>
        <div class="col-sm-4">
            <label for="">Hora actual : </label>
            <input class="form-control" type="text" readonly value="{{ $horario->hora }}">
        </div>
    </div>
    <br>
    <h2>Actualizar información</h2>
    <form id="frm" action="" method="post" novalidate="novalidate" id="alta_usuario">
        <input type="hidden" value="{{ $horario_actual }}" id="horario_actual" name="horario_actual">
        <input type="hidden" value="{{ $id_servicio }}" id="id_servicio" name="id_servicio">
        <input type="hidden" value="{{ $id_auto }}" id="id_auto" name="id_auto">
        <input type="hidden" value="{{ $id_sucursal }}" id="id_sucursal" name="id_sucursal">
        <input type="hidden" value="{{ $folio_mostrar }}" id="folio_mostrar" name="folio_mostrar">
        <input type="hidden" value="{{ $id_usuario }}" id="id_usuario" name="id_usuario">
        <input type="hidden" value="{{ $id_servicio_lavado }}" id="id_servicio_lavado" name="id_servicio_lavado">
        
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1"># Serie</label>
                {{ $numero_serie }}
                <span class="error error_numero_serie"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Kilometraje</label>
                {{ $kilometraje }}
                <span class="error error_kilometraje"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Placas</label>
                {{ $placas }}
                <span class="error error_placas"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Lavador</label>
                {{ $id_lavador }}
                <span class="error error_id_lavador"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Fecha</label>
                {{ $fecha }}
                <span class="error error_fecha"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Hora</label>
                {{ $id_horario }}
                <span class="error error_id_horario"></span>
            </div>
        </div>
    </form>
    <br>
    <div align="right">
        <button type="button" id="guardar" class="btn btn-lg btn-info ">
            <i class="fa fa-edit fa-lg"></i>&nbsp;
            <span>Guardar</span>
            <span id="payment-button-sending" style="display:none;">Sending…</span>
        </button>
    </div>
@endsection
@section('included_js')
    <script>
        var site_url = "{{ site_url() }}";
        const guardar = () => {
            var url = site_url + '/servicios/editarHorario';
            ajaxJson(url, $("#frm").serialize(), "POST", "", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(data, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    if (result == 1) {
                        ExitoCustom("Información guardada correctamente", function() {
                            window.location.reload();
                        });
                    } else if (result == -2) {
                        ErrorCustom('El horario ya fue ocupado, elige otro');
                    } else if (result == -3) {
                        ErrorCustom('La fecha seleccionada debe ser mayor o igual a la fecha actual');
                    }
                }
            });
        }
        const getFechas = () => {
            var url = site_url + "/servicios/getFechasDisponiblesLavador";
            $("#fecha").empty();
            $("#fecha").append("<option value=''>-- Selecciona --</option>");
            $("#fecha").attr("disabled", true);
            id_lavador = $("#id_lavador").val();
            id_sucursal = $("#id_sucursal").val();
            if (id_lavador != '') {
                ajaxJson(url, {
                    "id_lavador": id_lavador
                }, "POST", "", function(result) {
                    if (result.length != 0) {
                        $("#fecha").empty();
                        $("#fecha").removeAttr("disabled");
                        result = JSON.parse(result);
                        $("#fecha").append("<option value=''>-- Selecciona --</option>");
                        $.each(result, function(i, item) {
                            $("#fecha").append("<option value= '" + result[i].fecha + "'>" +
                                result[i].fecha +
                                "</option>");
                        });
                    } else {
                        $("#fecha").empty();
                        $("#fecha").append(
                            "<option value='0'>No se encontraron datos</option>");
                    }
                });
            } else {
                $("#fecha").empty();
                $("#fecha").append("<option value=''>-- Selecciona --</option>");
                $("#fecha").attr("disabled", true);
            }
        }
        const getHorarios = () => {
            var url = site_url + "/servicios/getHorariosDisponibles";
            $("#id_horario").empty();
            $("#id_horario").append("<option value=''>-- Selecciona --</option>");
            $("#id_horario").attr("disabled", true);
            fecha = $("#fecha").val();
            id_lavador = $("#id_lavador").val();
            if (fecha != '' && id_lavador != '') {
                ajaxJson(url, {
                    "fecha": fecha,
                    "id_lavador": id_lavador,
                    "id_sucursal": $("#id_sucursal").val()
                }, "POST", "", function(result) {
                    if (result.length != 0) {
                        $("#id_horario").empty();
                        $("#id_horario").removeAttr("disabled");
                        result = JSON.parse(result);
                        $("#id_horario").append("<option value=''>-- Selecciona --</option>");
                        $.each(result, function(i, item) {
                            $("#id_horario").append("<option value= '" + result[i].id + "'>" +
                                result[i].hora +
                                "</option>");
                        });
                    } else {
                        $("#id_horario").empty();
                        $("#id_horario").append(
                            "<option value='0'>No se encontraron datos</option>");
                    }
                });
            } else {
                $("#hora").empty();
                $("#hora").append("<option value=''>-- Selecciona --</option>");
                $("#hora").attr("disabled", true);
            }
        }
        $("#guardar").on('click', guardar);
        $("#id_lavador").on('change', getFechas);
        $("#fecha").on("change", getHorarios);

    </script>
@endsection
