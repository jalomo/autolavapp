$("#enviar_formulario").on('click', function (e){
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#cargaIcono").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        $("input").attr('disabled',false);

        var base = $("#sitio").val();
        var paqueteDatos = new FormData(document.getElementById('formulario_passord'));

        $.ajax({
            url: base+"recomendadores/panel/actualizar_passord",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                //console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        location.href = base+"index.php/recomendadores/login/cerrar_sesion";
                    } else {
                        $("#formulario_error").text(resp);
                    }
                }else{
                    $("#formulario_error").text("ERROR");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
    }
});

function validar() {
    var retorno = true;

    var input = $("input[name='pass_nvo']").val();
    if (input.length < 8) {
        $("#valida_2").css("display","none");
        var error_2 = $("#pass_nvo_error");
        retorno = false;
        $("#pass_nvo_error").css("display","inline-block");
        error_2.append('<label class="form-text text-danger">La contraseña debe tener al menos 8 caracteres</label>');
    }else{
        $("#valida_2").css("display","inline-block");
        var error_2 = $("#pass_nvo_error");
        error_2.empty();
    }

    var input_2 = $("input[name='pass_nvo_2']").val();
    if (input_2 != input) {
        $("#valida_3").css("display","none");
        var error_3 = $("#pass_nvo_2_error");
        $("#pass_nvo_2_error").css("display","inline-block");
        error_3.append('<label class="form-text text-danger">Las contraseñas no coinciden</label>');
    }else{
        $("#valida_3").css("display","inline-block");
        var error_3 = $("#pass_nvo_2_error");
        error_3.empty();
    }

    return retorno;
}

function validar_password() {
    var pass = $("input[name='pass_actual']").val();
    var base = $("#sitio").val();

    var url_sis = base+"recomendadores/panel/revisar_pass";
    $.ajax({
        url: url_sis,
        method: 'post',
        data: {
            pass : pass
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler           </p>")<1) {
                var error_8 = $("#pass_actual_error");
                error_8.empty();

                if (resp != "OK") {
                    $("#valida_1").css("display","none");
                    error_8.append('<label class="form-text text-danger">Contraseña incorrecta</label>');
                } else{
                    $("#valida_1").css("display","inline-block");
                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });     
}

function validar_pass() {
    var error_2 = $("#pass_nvo_error");
    error_2.empty();

    var input = $("input[name='pass_nvo']").val();
    if (input.length < 8) {
        $("#valida_2").css("display","none");
        $("#pass_nvo_error").css("display","inline-block");
        error_2.append('<label class="form-text text-danger">La contraseña debe tener al menos 8 caracteres</label>');
    }else{
        $("#valida_2").css("display","inline-block");
    }
}

function validar_coincidencia() {
    var input = $("input[name='pass_nvo']").val();
    var input_2 = $("input[name='pass_nvo_2']").val();

    var error_2 = $("#pass_nvo_2_error");
    error_2.empty();

    if (input_2 != input) {
        $("#valida_3").css("display","none");
        $("#pass_nvo_2_error").css("display","inline-block");
        error_2.append('<label class="form-text text-danger">Las contraseñas no coinciden</label>');
    }else{
        $("#valida_3").css("display","inline-block");
    }
}