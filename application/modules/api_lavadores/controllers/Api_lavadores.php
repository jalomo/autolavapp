<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/

require "/var/www/web/xehos/jwt/vendor/autoload.php";
use \Firebase\JWT\JWT;
class Api_lavadores extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
   

    }

    public function servicio(){
        $id_servicio = $this->input->post('id_servicio');
        $data['latitud'] = 19.254135;
        $data['longitud'] = -103.696513;
        $data['marca'] = "Nissan";
        $data['modelo'] = "Nissan";
        $data['placas'] = "Nissan";
        $data['color'] = "Nissan";
        $data['telefono'] = "Nissan";
        $data['fecha'] = "02/10/2020 16:30";
        echo json_encode($data);
    }

    public function terminar_servicio(){
        $id_servicio = $this->input->post('id_servicio');
        if(isset($id_servicio)){
            $data['error'] = 0;
            echo json_encode($data); 
        }else{
            $data['error'] = 1;
           echo json_encode($data); 
        }
        
    }


    public function iniciar_servicio(){
        $id_servicio = $this->input->post('id_servicio');
        if(isset($id_servicio)){
            $data['error'] = 0;
            echo json_encode($data); 
        }else{
            $data['error'] = 1;
           echo json_encode($data); 
        }
        
    }

    

   public function login(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $this->db->where('lavadorEmail', $email);
        $this->db->where('lavadorPassword', $password);
        $data = $this->db->get('lavadores')->row();
        $response = "";
        if(is_object($data)){

            $secret_key = "jalomo123";
            $issuer_claim = "XEHOS";
            $audience_claim = "XEHOS_AUTOLAVAPP";
            $issuedat_claim = 1356999524; // issued at
            $notbefore_claim = 1357000000; //not before
            $token = array(
                "iss" => $issuer_claim,
                "aud" => $audience_claim,
                "iat" => $issuedat_claim,
                "nbf" => $notbefore_claim,
                "data" => array(
                    "id" => $id,
                    "firstname" => $firstname,
                    "lastname" => $lastname,
                    "email" => $email
            ));
    
            //http_response_code(200);
    
            $jwt = JWT::encode($token, $secret_key);
       

            $response['error'] = 0;
            $response['id'] = $data->lavadorId;
            $response['mensaje'] = "";
            $response['nombre'] = $data->lavador_nombre;
            $response['token'] = $jwt;
        }else{
            $response['error'] = 1;
            $response['mensaje'] = "Usuario o contraseña incorrectos";
        }
        echo json_encode($response);
               
   }

   public function agenda(){
    $id_usuario = $this->input->post('id_usuario');
    $fecha = $this->input->post('fecha');

     $data = array();

     $aux['hora'] = "12:00";
     $aux['id_hora'] = "1";
     $aux['id_servicio'] = 1;
     
     array_push($data,$aux);

     $aux1['hora'] = "13:00";
     $aux1['id_hora'] = "2";
     $aux1['id_servicio'] = 2;
    
     array_push($data,$aux1);

     $aux3['hora'] = "13:30";
     $aux3['id_hora'] = "3";
     $aux3['id_servicio'] = 3;
     
     array_push($data,$aux3);

     echo json_encode($data);
    
   }

   public function enviar_coordenadas(){
        $data['latitud'] = $this->input->post('latitud');
        $data['longitud']= $this->input->post('longitud');
        $data['id_lavador'] = $this->input->post('id_lavador');
        $data['fecha_Creacion'] = date('Y-m-d H:i:s');
        $this->Mgeneral->save_register('coordenadas_lavador', $data);
        echo json_encode($data);


   }


   /** 
     * Get header Authorization
     * */
    function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /**
     * get access token from header
     * */
    function getBearerToken() {
        $headers = getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }


   

}