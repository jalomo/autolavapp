// filtro_estatus_id
var tabla_piezas = $('#tabla_piezas').DataTable({
    language: {
        url: PATH_LANGUAGE
    },
    "ajax": {
        url: PATH_API + "api/pedidoproducto/listado",
        type: 'GET',
        data: {
            proveedor_id: () => $('#filtro_proveedor_id').val(),
            ma_pedido_id: () => $('#ma_pedido_id').val()
        }
    },
    columns: [{
            title: "#",
            data: 'id',
        },
        {
            title: "No. identificación",
            data: 'no_identificacion',
        },
        {
            title: "Descripcion",
            data: 'descripcion',
        },
        {
            title: "Precio unitario",
            data: 'valor_unitario',
        },
        {
            title: "Unidad",
            data: 'unidad',
        },
        {
            title: "Proveedor",
            data: ({ proveedor_nombre }) => proveedor_nombre ? proveedor_nombre : 'sin asignar'
        },
        {
            title: "Estatus",
            data: function({ estatus_id, cantidad_backorder }) {
                // console.log(data)
                if (estatus_id == 1) {
                    return 'En proceso'
                } else if (estatus_id == 2) {
                    return 'Pedidas'
                } else if (estatus_id == 3) {
                    return 'No pedidas'
                } else if (estatus_id == 4) {
                    return 'Backorder: ' + cantidad_backorder
                }

            }
        },
        {
            title: "Piezas a pedir",
            data: 'cantidad_solicitada',
        },
        {
            title: "-",
            data: ({ id, proveedor_id }) => !proveedor_id ? "<button class='btn btn-primary' onclick='openmodalproveedor(" + id + ")'><i class='fas fa-list'></i></a>" : '-'
        }
    ],
    "createdRow": function(row, data, dataIndex) {
        if (data.estatus_id == 2) {
            $(row).find('td:eq(6)').css('background-color', '#8cdd8c');
        } else if (data.estatus_id == 4) {
            $(row).find('td:eq(6)').css('background-color', '#db524d80');
        }

        if (!data.proveedor_nombre) {
            $(row).find('td:eq(5)').css('background-color', '#db524d80');
        }
    }
});

var tabla_stock = $('#tabla_stock').DataTable({
    language: {
        url: PATH_LANGUAGE
    },
    "ajax": {
        url: PATH_API + "api/productos",
        type: 'GET',
        dataSrc: "",
    },
    columns: [{
            title: "#",
            data: 'id',
        },
        {
            title: "No. identificación",
            data: 'no_identificacion',
        },
        {
            title: "Descripcion",
            data: 'descripcion',
        },
        {
            title: "Precio unitario",
            data: 'valor_unitario',
        },
        {
            title: "Unidad",
            data: 'unidad',
        },
        // {
        //     title: "Cantidad actual",
        //     data: 'cantidad',
        // },
        {
            data: function(data) {
                return "<button data-target='#modaldetalle'  data-toggle='modal' class='btn btn-primary btn-modal' data-id=" + data.id + "><i class='fas fa-list'></i></a>";
            }
        }

    ]
});

$('#tabla_stock').on('click', '.btn-modal', function() {
    // return console.log("entra")
    let id = $(this).data('id');
    // return console.log(id)
    // $("#modaldetalle").modal('show');
    ajax.get(`api/productos/stockActual?producto_id=${id}`, {}, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            let data = response[0];
            $("#producto_id").val(id);
            $("#descripcion").val(data.descripcion);
            $("#no_identificacion").val(data.no_identificacion);
            $("#valor_unitario").val(data.valor_unitario);
        }
    })
});

$('#tabla_piezas').on('click', '.modal-estatus', function() {
    let id = $(this).data('id');
    $("#modalcambioestatus").modal('show');
    $("#id_pedido_producto").val(id);
});

$('#tabla_piezas').on('click', '.modal-proveedor', function() {
    let id = $(this).data('id');
    $("#frmmodalproveedor")[0].reset();
    $("#modalproveedor").modal('show');
    $("#selected_pedidoproducto_id").val(id);
    ajax.get(`api/proveedorpedidoproducto/getById/${id}`, {}, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            let lista = "<ul class='list-group'>"
            response.map(item => {
                lista += "<li class='list-group-item'><span style='color:black'>Proveedor :</span> " + item.proveedor_nombre + " <br><span style='color:black'>cantidad:</span> " + item.cantidad_proveedor + "</li>"
            })
            lista += "</ul>"
            $("#contendor_proveedores").html(lista);
        }
    })
});

$('#btn-filtrar').on('click', function() {
    tabla_piezas.ajax.reload();
});


$('#btn-confirmar').on('click', function() {

    if ($("#cantidad_solicitada").val() == '') {
        toastr.error("Indicar la cantidad!")
        return false;
    }

    if ($("#proveedor_id").val() == '') {
        toastr.error("Indicar proveedor!")
        return false;
    }

    let data = {
        "cantidad_solicitada": $("#cantidad_solicitada").val(),
        "producto_id": $("#producto_id").val(),
        "ma_pedido_id": $("#ma_pedido_id").val(),
        "proveedor_id": $("#proveedor_id").val()
    };

    ajax.post(`api/pedidoproducto`, data, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            $("#modaldetalle").modal('hide');
            $("#frm-producto")[0].reset();
            tabla_piezas.ajax.reload();
        }
    })
});


const updateproovedor = () => {
    const proveedor_id = document.getElementById('select_proveedor_id').value;
    selected_pedidoproducto_id = document.getElementById('selected_pedidoproducto_id').value
    if ($("#select_proveedor_id").val() == '') {
        toastr.error("Indicar proveedor!")
        return false;
    }

    if ($("#selected_pedidoproducto_id").val() == '') {
        toastr.error("no se ha cargado la informacion!")
        return false;
    }

    ajax.put(`api/pedidoproducto/${selected_pedidoproducto_id}`, {
        proveedor_id
    }, function(response, headers) {
        if (headers.status == 201 || headers.status == 200) {
            $("#modalproveedor").modal('hide');
            $("#frmmodalproveedor")[0].reset();
            tabla_piezas.ajax.reload();
        }
    })
}

const openmodalproveedor = (selected_pedidoproducto_id) => {
    document.getElementById('selected_pedidoproducto_id').value = selected_pedidoproducto_id
    $("#modalproveedor").modal('show');
}