<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
    <head>
        <title>INVENTARIO <?php if (isset($folio_info)) echo $folio_info; else echo "X"; ?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="estilos.css" />
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <style>
            @page {
                sheet-size: A4;
                size: auto; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 10mm; 
                /*margin-top: 5mm; /* <any of the usual CSS values for margins> */
                /*margin-left: 5mm;*/
                /*margin-right: 5mm;*/
                /*margin-bottom: 10mm;*/
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 8mm; /* <any of the usual CSS values for margins> */
                margin-footer: 8mm; /* <any of the usual CSS values for margins> */
            }

            .page_break { 
                page-break-before: always; 
            }

            td,tr{
                padding: 0px 1px 1px 0px !important;
                font-size: 10px;
                color: #239b56  ;
            }

            td{
                border: 0.5px solid white;
            }

            .titulo_pdf {
                background-color: #52bf66;
                color:white;
                border-top-left-radius: 15px;
                border-top-right-radius: 15px;
            }

            .cuadro_pdf {
                background-color: #dbf4b9;
                color:#52BF66;
                border-bottom-right-radius: 15px;
                border-bottom-left-radius: 15px;
            }

            .td_pdf {
                perspective: 1px;
                /*border-collapse: separate;*/
                background-color: #dbf4b9;
                color: #239b56  ;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                border-bottom-right-radius: 10px;
                border-bottom-left-radius: 10px;
                padding: 5px;
                /*height: 20px;*/
                font-size: 9px;
            }

            .tr_pdf {
                padding-top: 3px;
                /*padding-left: 10px;*/
                perspective: 1px;
                border-collapse: separate;
                height: 20px;
                background-color: #52bf66;
                color:#ffffff;
                border-radius: 10px 10px 10px 10px;
            }

            .cabecera{
                background-color: white;
                color: #52bf66;
                font-weight: bold;
                padding-left: 15px;
            }

            .parrafo_contrato{
                text-align: justify; 
                font-size:10px;
                width: 100%;
                padding-right: 8px;
                padding-left: 8px;
            }

            .col-md-4{
                width: 33%;
                float: left;
            }

            .col-md-3{
                width: 25%;
            }

            .col-md-6{
                width: 50%;
            }

            .col-md-12{
                width: 100%;
            }
        </style>
    </head>
     
    <body>
        <div>
            <div style="margin-top: 10px;">
                <table style="width:99%;">
                    <tr>
                        <td style="width: 130px;" align="center"> 
                            <img src="<?= base_url(); ?>statics/inventario/imgs/logos/xehos.jpg" class="logo" style="height:2cm;">
                        </td>
                        <td style="color:#52bf66;font-size:9px;" align="center">
                            <strong>
                                <span class="color-blue" style="font-size:13px;color: black;" align="center">
                                    SISTEMAS OPERATIVOS HEXADECIMAL. S.A. DE C.V. 
                                </span>
                            </strong>
                            
                            <br>
                            AV. IGNACIO SANDOVAL, #1939. LOC L6. PLAZA MILAN. <br>
                            COL. PASEO DE LA CANTERA. COLIMA COLIMA. C.P. 28017.<br>
                            correo: info@xehos.com RFC: SOH190520AR4.   Tels. 33 3835 2043 / 33 3835 2058 <br>
                            Horarios de Servicio: Lunes a Domingo de 8:00 AM a 6:00PM
                        </td>
                        <td align="center" style="color:#52bf66;width:100px;">
                            <img src="<?php echo base_url(); ?>statics/inventario/imgs/logos/logoSohex.jpg" class="logo" style="height: 1.3cm;">
                        </td>
                    </tr>
                </table>
            </div>

            <div style="margin-top: 10px;">
                <table style="width: 99%;">
                    <tr>
                        <td colspan="2"><br></td>
                        <td style="width: 220px; font-size: 8px;text-align: justify;" rowspan="4">
                            <img src="<?php echo base_url(); ?>statics/inventario/imgs/logos/logovertical.jpg" class="logo" style="height: 2cm;">
                        </td>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="" style="width: 230px;">
                            <div class="td_pdf" style="font-size: 9px;width: 230px;">
                                OPERADOR DE SERVICIO: &nbsp;&nbsp;
                                <span style="color: black;">
                                    <?php if (isset($orden_telefono_asesor)) echo $orden_telefono_asesor; else echo "X"; ?>
                                </span>
                            </div>
                        </td>
                        <td style="width: 20px;"><br></td>
                        
                        <td style="width: 20px;"><br></td>
                        <td style="width: 220px; font-size: 9px;" align="center">
                            ORDEN DE SERVICIO
                        </td>
                    </tr>
                    <tr>
                        <td class="td_pdf" style="width: 230px;">
                            <div class="td_pdf" style="height:15px;font-size: 9px;width: 100%;">
                                TELEFONO: &nbsp;&nbsp;
                                <span style="color: black;">
                                    <?php if (isset($orden_telefono_asesor)) echo $orden_telefono_asesor; else echo "X"; ?>
                                </span>
                            </div>
                        </td>
                        <td style="width: 20px;"><br></td>

                        <td style="width: 20px;"><br></td>

                        <td class="td_pdf" style="font-size: 9px;" align="center">
                            <div class="td_pdf" style="height:20px;font-size: 9px;width: 100%;" align="center">
                                <span style="color: black;font-size: 12px" align="center">
                                    <?php if (isset($folio_info)) echo $folio_info; else echo "X"; ?>
                                </span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                        <td colspan="2"><br></td>
                    </tr>
                </table>
            </div>

            <div style="margin-top: 10px;">
                
            </div>

            <div style="margin-top: 10px;">
                <table style="width:99%;">
                    <tr>
                        <td colspan="3" class="tr_pdf" align="center" style="width:100%;font-size: 10px;">
                            <!--<div class="tr_pdf" style="font-size: 10px;width: 100%;">-->
                                DATOS DEL CONSUMIDOR
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Nombre(s) del Consumidor:</span><br>
                                <span style="color: black;">
                                    <?php if (isset($orden_datos_nombres)) echo $orden_datos_nombres; else echo "X"; ?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Apellido Paterno:</span><br>
                                <span style="color: black;">
                                    <?php if (isset($orden_datos_apellido_paterno)) echo $orden_datos_apellido_paterno; else echo "X"; ?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Apellido Materno:</span><br>
                                <span style="color: black;">
                                    <?php if (isset($orden_datos_apellido_materno)) echo $orden_datos_apellido_materno; else echo "X"; ?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="td_pdf" style="font-size: 9px;width: 100%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Nombre de la compañía</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_nombre_compania)) echo $orden_nombre_compania; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Nombre(s) del  contacto de la compañía</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_nombre_contacto_compania)) echo $orden_nombre_contacto_compania; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Apellido Paterno del Contacto</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_ap_contacto)) echo $orden_ap_contacto; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Apellido Materno del Contacto</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_am_contacto)) echo $orden_am_contacto; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Correo Electrónico del Cosumidor</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_datos_email)) echo $orden_datos_email; else echo "X";?>
                                </span>
                            <!--</div>-->
                          </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>RFC</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_rfc)) echo $orden_rfc; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Correo Electrónico de la Compañia</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_correo_compania)) echo $orden_correo_compania; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="td_pdf" style="font-size: 9px;width: 100%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Dirección (Calle, Nº Exterior, Nº Interior, Colonia)</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_domicilio)) echo $orden_domicilio; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Municipio / Delegación </span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_municipio)) echo $orden_municipio; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Código Postal</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_cp)) echo $orden_cp; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Estado</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_estado)) echo $orden_estado; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="td_pdf" style="font-size: 9px;width: 67%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Teléfono</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_telefono_movil)) echo $orden_telefono_movil; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Otro teléfono</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_otro_telefono)) echo $orden_otro_telefono; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                </table>
            </div>

            <div style="margin-top: 8px;">
                <table style="width:89%;margin-left: 60px;">
                    <tr>
                        <td colspan="3" align="center" class="tr_pdf" style="font-size: 10px;width: 100%;">
                            <!--<div class="tr_pdf" style="font-size: 10px;width: 100%;">-->
                                DATOS DEL VÉHÍCULO
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Placas del vehículo</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_vehiculo_placas)) echo $orden_vehiculo_placas; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Número de identificación vehícular</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_vehiculo_identificacion)) echo $orden_vehiculo_identificacion; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Kilometraje</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_vehiculo_kilometraje)) echo $orden_vehiculo_kilometraje; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Marca / Línea del vehículo</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_vehiculo_marca)) echo $orden_vehiculo_marca; else echo "X";?>
                                    <?php if(isset($orden_categoria)) echo " - ".$orden_categoria;?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Año Modelo</span><br> 
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($orden_vehiculo_anio)) echo $orden_vehiculo_anio; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="font-size: 9px;width: 33%;">
                            <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                <span>Color</span><br>
                                <span style="color: black;font-size: 7px;">
                                    <?php if(isset($color)) echo $color; else echo "X";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                </table>
            </div>

            <div style="margin-top: 10px;">
                <table style="width:100%;font-size:8px;color: black;">
                    <tr>
                        <td colspan="5" align="center" class="tr_pdf" style="font-size: 10px;width: 100%;">
                            <!--<div class="tr_pdf" style="font-size: 10px;width: 100%;">-->
                                DESCRIPCIÓN DEL TRABAJO Y DESGLOCE DEL PRESUPUESTO (COTIZACIÓN)
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="td_pdf" style="height: 15px;width:8%;">
                            <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                Cantidad
                            <!--</div>-->
                        </td>
                        <td align="center" class="td_pdf" style="">
                            <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                Descripción
                            <!--</div>-->
                        </td>
                        <td align="center" class="td_pdf" style="font-size: 8px;width: 10%">
                            <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                Precio U.
                            <!--</div>-->
                        </td>
                        <td align="center" class="td_pdf" style="width: 10%">
                            <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                Descuento
                            <!--</div>-->
                        </td>
                        <td align="center" class="td_pdf" style="width: 16%">
                            <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                Total
                            <!--</div>-->
                        </td>
                    </tr>

                    <?php if (isset($item_descripcion)): ?>
                        <?php foreach ($item_descripcion as $index => $valor): ?>
                            <tr>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <span style="color: black;font-size:8px;">
                                            <?php echo $item_cantidad[$index]; ?>
                                        </span>
                                    <!--</div>-->
                                </td>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <span style="color: black;font-size:8px;">
                                            <?php echo $item_descripcion[$index]; ?>
                                        </span>
                                    <!--</div>-->
                                </td>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <span style="color: black;font-size:8px;">
                                            <?php echo $item_precio_unitario[$index]; ?>
                                        </span>
                                    <!--</div>-->
                                </td>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <span style="color: black;font-size:8px;">
                                            <?php echo $item_descuento[$index]; ?>
                                        </span>
                                    <!--</div>-->
                                </td>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <span style="color: black;font-size:8px;">
                                            <?php echo $item_total[$index]; ?>
                                        </span>
                                    <!--</div>-->
                                </td>
                            </tr>
                            <?php if($index > 3) break;  ?>
                        <?php endforeach; ?>

                        <?php if ($totalRenglones<3): ?>
                            <?php for ($i = 0 ; $i < 3-$totalRenglones; $i++): ?>
                                <tr>
                                    <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                        <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                            <br>
                                        <!--</div>-->
                                    </td>
                                    <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                        <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                            <br>
                                        <!--</div>-->
                                    </td>
                                    <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                        <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                            <br>
                                        <!--</div>-->
                                    </td>
                                    <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                        <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                            <br>
                                        <!--</div>-->
                                    </td>
                                    <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                        <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                            <br>
                                        <!--</div>-->
                                    </td>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>

                    <?php else: ?>
                        <?php for ($i = 0 ; $i < 3; $i++): ?>
                            <tr>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <br>
                                    <!--</div>-->
                                </td>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <br>
                                    <!--</div>-->
                                </td>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <br>
                                    <!--</div>-->
                                </td>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <br>
                                    <!--</div>-->
                                </td>
                                <td align="center" class="td_pdf" style="height: 15px;" align="center">
                                    <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                        <br>
                                    <!--</div>-->
                                </td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif ?>

                    <tr>
                        <td colspan="4" class="td_pdf" style="height: 15px;" align="right">
                            <!--<div class="td_pdf" style="height: 15px;" align="right">-->
                                <span style="font-size:8px;">SUB-TOTAL :</span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="height: 15px;" align="center">
                            <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                <span style="color: black;font-size:8px;">
                                    <?php if(isset($subtotal_items)) echo number_format($subtotal_items,2); else echo "$0.00";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="td_pdf" style="height: 15px;" align="right">
                            <!--<div class="td_pdf" style="height: 15px;" align="right">-->
                                <span style="font-size:8px;">I.V.A. :</span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="height: 15px;" align="center">
                            <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                <span style="color: black;font-size:8px;">
                                    <?php if(isset($iva_items)) echo number_format($iva_items,2); else echo "$0.00";?>
                                </span>
                            <!--</div>-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="td_pdf" style="height: 15px;" align="right">
                            <!--<div class="td_pdf" style="height: 15px;" align="right">-->
                                <span style="font-size:8px;">TOTAL</span>
                            <!--</div>-->
                        </td>
                        <td class="td_pdf" style="height: 15px;" align="center">
                            <!--<div class="td_pdf" style="height: 15px;" align="center">-->
                                <strong style="color: black;font-size:8px;">
                                    <?php if(isset($presupuesto_items)) echo number_format($presupuesto_items,2); else echo "$0.00";?>
                                </strong>
                            <!--</div>-->
                        </td>
                    </tr>   
                </table>
            </div>
        </div>

        <div class="page_break">
            <div class="titulo_pdf" style="font-size: 10px;width: 100%;margin-top: 2px;" align="center">
                INSPECCIÓN VISUAL E INVENTARIO EN RECEPCIÓN
            </div>
            <div class="cuadro_pdf" style="padding: 8px;padding-left: 20px;">
                <table style="width: 100%;background-color: white;margin-left: 10px:margin-right :10px;">
                    <tr>
                        <td style="width: 300px;">
                            <br>
                            <img src="<?php echo base_url().'statics/inventario/imgs/carroceria.jpg'; ?>" alt="" style="height:28px;width:205px;margin-left: 10px;">
                            <br>
                            <div style="width: 230px;margin-top: 20px;margin-left: 20px;">
                                <?php if (isset($img_diagrama)): ?>
                                    <?php if ($img_diagrama != ""): ?>
                                        <img src="<?php echo base_url().$img_diagrama; ?>" style="width: 230px;height: 170px;">
                                    <?php else: ?> 
                                        <img src="<?php echo base_url().'statics/inventario/imgs/car_1.jpg'; ?>" style="width: 230px;height: 170px;">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/car_1.jpg'; ?>" style="width: 230px;height: 170px;">
                                <?php endif; ?>
                            </div>

                            <label style="font-size: 13px;font-weight: bold;">Nivel de Gasolina: </label>
                            <div style="width: 100%;" align="center">
                                <?php if (isset($nivel_gasolina)): ?>
                                    <?php if ($nivel_gasolina ==  "0"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_a.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "1"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_b.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "2"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_c.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "3"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_d.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "4"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_e.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "5"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_f.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "6"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_g.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "7"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_h.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "8"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_i.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "9"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_j.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "10"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_k.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "11"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_l.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "12"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_m.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "13"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_n.png'; ?>" style="height: 95px;">
                                    <?php elseif ($nivel_gasolina ==  "14"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_o.png'; ?>" style="height: 95px;">
                                    
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_1.jpg'; ?>" style="height: 95px;">
                                <?php endif ?>
                            </div>

                            <br>
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="4" style="">
                                        ¿Deja artículos personales?
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2" style="padding-right: 10px;">
                                        <label for="">&nbsp;Si&nbsp;</label>
                                        <?php if (isset($articulos_vehiculo)): ?>
                                            <?php if (strtoupper($articulos_vehiculo) == "1"): ?>
                                                <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf1.jpg'; ?>" style="width:15px;" alt="">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:10px;" alt="">
                                        <?php endif; ?>
                                        &nbsp;
                                    </td>
                                    <td align="left" colspan="2" style="padding-left: 10px;">
                                        <label for="">No</label>
                                        <?php if (isset($articulos_vehiculo)): ?>
                                            <?php if (strtoupper($articulos_vehiculo) == "0"): ?>
                                                <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf1.jpg'; ?>" style="width:15px;" alt="">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:10px;" alt="">
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ¿Cuales?
                                    </td>
                                    <td colspan="3" style="border-bottom:1px solid #52bf66;color: black;">
                                        <?php if(isset($articulos))  echo $articulos; else echo "Ninguno";?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="" style="">
                                        ¿Desea reportar algo más?
                                    </td>
                                    <td colspan="3" style="border-bottom:1px solid #52bf66;color: black;">
                                        <?php if(isset($reporte_articulos))  echo $reporte_articulos; else echo "Ninguno";?>
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="width:240px;">
                            <table style="width:100%;" >
                                <tr>
                                    <td colspan="2" class="tr_pdf" style="font-size: 10px;width: 100%;">
                                        <!--<div class="tr_pdf" style="font-size: 10px;width: 100%;">-->
                                            Interiores &nbsp;&nbsp;
                                            Si(<img src="<?php echo base_url().'statics/inventario/imgs/check2.jpg'; ?>" style="width:10px;" alt="">)&nbsp;&nbsp;
                                            /&nbsp;&nbsp;No(<strong style="font-size: 8px;font-weight: bold;">X</strong>)&nbsp;&nbsp;
                                            /&nbsp;&nbsp;No cuenta (NC)
                                            <br>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;width: 180px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 180px;height:15px;">-->
                                            Llavero.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;width: 60px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 60px;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Llavero.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Llavero.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Llavero.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Seguro rines.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("SeguroRines.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("SeguroRines.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("SeguroRines.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Indicadores de falla Activados:
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td_pdf" style="font-size: 8px;width: 100%;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;">-->
                                            <table class="" style="width:100%; font-size:7px;color:#52bf66;" >
                                                <tr>
                                                    <td align="center" style="padding: 0px 0px 0px 0px;">
                                                        <img src="<?php echo base_url();?>statics/inventario/imgs/icono_5.jpg" style="height:10px;">
                                                    </td>
                                                    <td align="center" style="padding: 0px 0px 0px 0px;">
                                                        <img src="<?php echo base_url();?>statics/inventario/imgs/icono_2.jpg" style="height:10px;">
                                                    </td>
                                                    <td align="center" style="padding: 0px 0px 0px 0px;">
                                                        <img src="<?php echo base_url();?>statics/inventario/imgs/icono_7.jpg" style="height:10px;">
                                                    </td>
                                                    <td align="center" style="padding: 0px 0px 0px 0px;">
                                                        <img src="<?php echo base_url();?>statics/inventario/imgs/icono_1.jpg" style="height:10px;">
                                                    </td>
                                                    <td align="center" style="padding: 0px 0px 0px 0px;">
                                                        <img src="<?php echo base_url();?>statics/inventario/imgs/icono_6.jpg" style="height:10px;">
                                                    </td>
                                                    <td align="center" style="padding: 0px 0px 0px 0px;">
                                                        <img src="<?php echo base_url();?>statics/inventario/imgs/icono_4.jpg" style="height:10px;">
                                                    </td>
                                                    <td align="center" style="padding: 0px 0px 0px 0px;">
                                                        <img src="<?php echo base_url();?>statics/inventario/imgs/icono_3.jpg" style="height:10px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <?php if (isset($interiores)): ?>
                                                            <?php if (in_array("IndicadorGasolina.Si",$interiores)): ?>
                                                                <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                            <?php elseif (in_array("IndicadorGasolina.No",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                            <?php elseif (in_array("IndicadorGasolina.NC",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($interiores)): ?>
                                                            <?php if (in_array("IndicadorMantenimento.Si",$interiores)): ?>
                                                                <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                            <?php elseif (in_array("IndicadorMantenimento.No",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                            <?php elseif (in_array("IndicadorMantenimento.NC",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($interiores)): ?>
                                                            <?php if (in_array("SistemaABS.Si",$interiores)): ?>
                                                                <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                            <?php elseif (in_array("SistemaABS.No",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                            <?php elseif (in_array("SistemaABS.NC",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($interiores)): ?>
                                                            <?php if (in_array("IndicadorFrenos.Si",$interiores)): ?>
                                                                <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                            <?php elseif (in_array("IndicadorFrenos.No",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                            <?php elseif (in_array("IndicadorFrenos.NC",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($interiores)): ?>
                                                            <?php if (in_array("IndicadorBolsaAire.Si",$interiores)): ?>
                                                                <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                            <?php elseif (in_array("IndicadorBolsaAire.No",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                            <?php elseif (in_array("IndicadorBolsaAire.NC",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($interiores)): ?>
                                                            <?php if (in_array("IndicadorTPMS.Si",$interiores)): ?>
                                                                <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                            <?php elseif (in_array("IndicadorTPMS.No",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                            <?php elseif (in_array("IndicadorTPMS.NC",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($interiores)): ?>
                                                            <?php if (in_array("IndicadorBateria.Si",$interiores)): ?>
                                                                <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                            <?php elseif (in_array("IndicadorBateria.No",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                            <?php elseif (in_array("IndicadorBateria.NC",$interiores)): ?>
                                                                <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Indicador de falla activados.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("IndicadorDeFalla.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("IndicadorDeFalla.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("IndicadorDeFalla.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Rociadores y Limpiaparabrisas.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Rociadores.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Rociadores.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Rociadores.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Claxon.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Claxon.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Claxon.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Claxon.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="td_pdf" style="font-size: 9px;width: 100%;">
                                        <!--<div class="td_pdf" style="font-size: 9px;width: 100%;">-->
                                            Luces:
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            &nbsp;&nbsp;&nbsp;&nbsp;Delanteras.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("LucesDelanteras.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("LucesDelanteras.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("LucesDelanteras.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            &nbsp;&nbsp;&nbsp;&nbsp;Traseras.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("LucesTraseras.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("LucesTraseras.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("LucesTraseras.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            &nbsp;&nbsp;&nbsp;&nbsp;Stop.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("LucesStop.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("LucesStop.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("LucesStop.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Radio / Caratulas.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Caratulas.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Caratulas.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Caratulas.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Pantallas.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Pantallas.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Pantallas.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Pantallas.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            A/C
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("AA.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("AA.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("AA.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Encendedor.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Encendedor.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Encendedor.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Encendedor.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Vidrios.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Vidrios.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Vidrios.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Vidrios.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Espejos.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Espejos.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Espejos.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Espejos.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Seguros eléctricos.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("SegurosEléctricos.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("SegurosEléctricos.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("SegurosEléctricos.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Disco compacto.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("CD.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("CD.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("CD.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Asientos y vestiduras.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Vestiduras.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Vestiduras.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Vestiduras.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Tapetes.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("Tapetes.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Tapetes.No",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Tapetes.NC",$interiores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                            </table>
                        </td>

                        <td style="width: 240px;">
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="2" class="tr_pdf" style="font-size: 10px;width: 100%;">
                                        <!--<div class="tr_pdf" style="font-size: 10px;width: 100%;">-->
                                            Cajuela&nbsp;&nbsp;&nbsp;&nbsp;
                                            Si(<img src="<?php echo base_url().'statics/inventario/imgs/check2.jpg'; ?>" style="width:10px;" alt="">)&nbsp;&nbsp;
                                            /&nbsp;&nbsp;No(<strong style="font-size: 8px;font-weight: bold;">X</strong>)&nbsp;&nbsp;
                                            /&nbsp;&nbsp;No cuenta (NC)
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 160px;height:15px;">-->
                                            Herramienta.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 40px;height:15px;" align="center">-->
                                            <?php if (isset($cajuela)): ?>
                                                <?php if (in_array("Herramienta.Si",$cajuela)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Herramienta.No",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Herramienta.NC",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Gato / Llave.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($cajuela)): ?>
                                                <?php if (in_array("eLlave.Si",$cajuela)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("eLlave.No",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("eLlave.NC",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Reflejantes.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($cajuela)): ?>
                                                <?php if (in_array("Reflejantes.Si",$cajuela)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Reflejantes.No",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Reflejantes.NC",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Cables.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($cajuela)): ?>
                                                <?php if (in_array("Cables.Si",$cajuela)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Cables.No",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Cables.NC",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Extintor.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($cajuela)): ?>
                                                <?php if (in_array("Extintor.Si",$cajuela)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Extintor.No",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Extintor.NC",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Llanta Refacción.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($cajuela)): ?>
                                                <?php if (in_array("LlantaRefaccion.Si",$cajuela)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("LlantaRefaccion.No",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("LlantaRefaccion.NC",$cajuela)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                            </table>

                            <br>
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="2" class="tr_pdf" style="font-size: 10px;width: 100%;">
                                        <!--<div class="tr_pdf" style="font-size: 10px;width: 100%;">-->
                                            Exteriores&nbsp;&nbsp;&nbsp;&nbsp;
                                            Si(<img src="<?php echo base_url().'statics/inventario/imgs/check2.jpg'; ?>" style="width:10px;" alt="">)&nbsp;&nbsp;
                                            /&nbsp;&nbsp;No(<strong style="font-size: 8px;font-weight: bold;">X</strong>)&nbsp;&nbsp;
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Tapones rueda.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($exteriores)): ?>
                                                <?php if (in_array("TaponesRueda.Si",$exteriores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("TaponesRueda.No",$exteriores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("TaponesRueda.NC",$exteriores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Gomas de limpiadores.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($exteriores)): ?>
                                                <?php if (in_array("Gotas.Si",$exteriores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Gotas.No",$exteriores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Gotas.NC",$exteriores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Antena.
                                        <!--/div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($exteriores)): ?>
                                                <?php if (in_array("Antena.Si",$exteriores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("Antena.No",$exteriores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("Antena.NC",$exteriores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--/div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Tapón de gasolina.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($exteriores)): ?>
                                                <?php if (in_array("TaponGasolina.Si",$exteriores)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("TaponGasolina.No",$exteriores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("TaponGasolina.NC",$exteriores)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                            </table>

                            <br>
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="2" class="tr_pdf" style="font-size: 10px;width: 100%;">
                                        <!--<div class="tr_pdf" style="font-size: 10px;width: 100%;">-->
                                            Documentación&nbsp;&nbsp;&nbsp;&nbsp;
                                            Si(<img src="<?php echo base_url().'statics/inventario/imgs/check2.jpg'; ?>" style="width:10px;" alt="">)&nbsp;&nbsp;
                                            /&nbsp;&nbsp;No(<strong style="font-size: 8px;font-weight: bold;">X</strong>)&nbsp;&nbsp;
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Póliza garantía/Manual prop.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($documentacion)): ?>
                                                <?php if (in_array("PolizaGarantia.Si",$documentacion)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("PolizaGarantia.No",$documentacion)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("PolizaGarantia.NC",$documentacion)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Seguro de Rines.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($documentacion)): ?>
                                                <?php if (in_array("SeguroRinesDoc.Si",$documentacion)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("SeguroRinesDoc.No",$documentacion)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("SeguroRinesDoc.NC",$documentacion)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Certificado verificación.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($documentacion)): ?>
                                                <?php if (in_array("cVerificacion.Si",$documentacion)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("cVerificacion.No",$documentacion)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("cVerificacion.NC",$documentacion)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;">-->
                                            Tarjeta de circulación.
                                        <!--</div>-->
                                    </td>
                                    <td class="td_pdf" style="font-size: 8px;height:15px;" align="center">
                                        <!--<div class="td_pdf" style="font-size: 8px;width: 100%;height:15px;" align="center">-->
                                            <?php if (isset($documentacion)): ?>
                                                <?php if (in_array("tCirculacion.Si",$documentacion)): ?>
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/Check.png'; ?>" style="width:10px;" >
                                                <?php elseif (in_array("tCirculacion.No",$documentacion)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">X</strong>
                                                <?php elseif (in_array("tCirculacion.NC",$documentacion)): ?>
                                                    <strong style="font-size: 8px;font-weight: bold;color:black;">NC</strong>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <!--</div>-->
                                    </td>
                                </tr>
                            </table>
                        </td> 
                    </tr>
                </table>
            </div>

            <div class="titulo_pdf" style="font-size: 10px;width: 100%;margin-top: 5px;" align="center">
                IMPORTANTE:
            </div>
            <div class="cuadro_pdf" style="padding: 8px;">
                <table style="background-color: white;">
                    <tr>
                        <td colspan="2" style="" align="justify">
                            Manifiesto bajo protesta que soy el dueño del vehículo, o tengo orden y/o autorización de él para utilizarlo y ordenar éste servicio, en su representación, autorizando en su nombre y en el propio la realización de los trabajos descritos, así como el uso de materiales e insumos necesarios para efectuarlos, comprometiéndome en su nombre y en el propio a pagar el importe del servicio; también manifiesto mi conformidad con el inventario realizado y que consta en el ejemplar del presente que recibo; en cualquier caso que las partes acuerden que el vehículo vaya a ser recogido o entregado por personal del PROVEEDOR en el domicilio del CONSUMIDOR, ello solo será mediante previo acuerdo u orden del CONSUMIDOR por escrito en la orden de servicio digital vía App. O habiendo pasado los filtros de seguridad, para el debido resguardo del vehículo y debidamente aceptada por el PROVEEDOR, con un costo de
                            $ <u style="color: black;">&nbsp;&nbsp;<?php if(isset($costo_diagnostico)) echo $costo_diagnostico; else ""; ?>&nbsp;&nbsp;</u> (MONEDA NACIONAL); será en todo caso obligación del personal del PROVEEDOR identificarse plenamente ante el CONSUMIDOR como tal.
                            <br>
                            Finalmente manifiesto mi conformidad con los términos y condiciones previstas en el contrato inscrito en el presente, el cual manifiesto que he leído, obligándome en lo personal y en nombre del propietario del automóvil en los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día 
                            <u style="color: black;">&nbsp;&nbsp;<?php if(isset($dia_diagnostico)) echo $dia_diagnostico; else "";?>&nbsp;&nbsp;</u> de
                            <u style="color: black;">&nbsp;&nbsp;<?php if(isset($mes_diagnostico)) echo $mes_diagnostico; else echo "X";?>&nbsp;&nbsp;</u> de
                            <u style="color: black;">&nbsp;&nbsp;<?php if(isset($anio_diagnostico)) echo $anio_diagnostico; else echo "X";?>&nbsp;&nbsp;</u>.
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 50%; color: black;">
                            <br><br>
                            <?php if (isset($firma_asesor)): ?>
                                <?php if ($firma_asesor != ""): ?>
                                    <img src="<?php echo base_url().$firma_asesor; ?>" style="height: 1.2cm;">
                                <?php endif; ?>
                            <?php endif; ?>
                            <br>
                            <?php if(isset($nombre_asesor)) echo $nombre_asesor; ?>
                        </td>
                        <td align="center" style="width: 50%; color: black;">
                            <br><br>
                            <?php if (isset($firma_cliente)): ?>
                              <?php if ($firma_cliente != ""): ?>
                                  <img src="<?php echo base_url().$firma_cliente; ?>" style="height: 1.2cm;">
                              <?php endif; ?>
                            <?php endif; ?>
                            <br>
                            <?php if(isset($nombre_cliente)) echo $nombre_cliente; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 50%; border-top: 1px solid #52bf66;">
                            <label for="">Nombre y firma del operador de servicio.</label><br>
                        </td>
                        <td align="center" style="width: 50%; border-top: 1px solid #52bf66;">
                            <label for="">Firma del consumidor</label><br>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div style="margin-top: 3px;">
            <div class="titulo_pdf" style="font-size: 10px;width: 100%;margin-top: 10px;" align="center">
                CONTRATO DE ADHESIÓN
            </div>
            <div class="cuadro_pdf" style="padding: 11px;">
                <table style="background-color: white;width: 100%;">
                    <tr>
                        <td style="width: 500px;padding: 0px;" rowspan="3">
                            <p align="justify" class="parrafo_contrato">
                                SISTEMAS OPERATIVOS HEXADECIMAL, S.A. DE C.V.  <br>
                                AV. IGNACIO SANDOVAL 1939. LOC. 6 PLAZA MILAN.  <br>
                                COL. PASEO DE LA CANTERA. C.P. 28017, COLIMA., COLIMA. <br>
                                RFC: SOH190520AR4.   Tels. 33 3835 2043 / 33 3835 2058 <br>
                                Email: info@xehos.com (Quejas y Reclamaciones)
                            </p>
                        </td>
                        <td style="width: 170px;padding: 0px;font-size: 9px;height: 10px;" align="right">
                            Folio No.
                        </td>
                        <td style="width: 100px;border-bottom: 0.5px solid #52bf66 ; color:black;font-size: 10px;padding: 0px;" align="center">
                            <?php if(isset($folio)) echo $folio; else ""; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0px;font-size: 9px;height: 10px;" align="right">
                            Fecha:
                        </td>
                        <td style="border-bottom: 0.5px solid #52bf66 ; color:black;font-size: 10px;padding: 0px;" align="center">
                            <?php if(isset($fecha_contrato_visual)) echo $fecha_contrato_visual; else ""; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0px;font-size: 9px;height: 10px;" align="right">
                            Hora:
                        </td>
                        <td style="border-bottom: 0.5px solid #52bf66 ; color:black;font-size: 10px;padding: 0px;" align="center">
                            <?php if(isset($hora_contrato)) echo $hora_contrato; else ""; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                CONTRATO DE PRESTACIÓN DE SERVICIO DE AUTOLAVADO Y DETALLADO  DE VEHÍCULOS QUE CELEBRAN POR UNA PARTE
                                <u style="color: black;">&nbsp;&nbsp;<?php if(isset($empresa_contrato)) echo $empresa_contrato; else "";?>&nbsp;&nbsp;</u>
                                , AUTORIZADO 
                                <u style="color: black;">&nbsp;&nbsp;<?php if(isset($autoriza_contrato)) echo $autoriza_contrato; else "";?>&nbsp;&nbsp;</u>. 
                                Y POR LA OTRA EL CONSUMIDOR CUYO NOMBRE SE SEÑALA EN EL INICIO DEL PRESENTE, A QUIENES EN LO SUCESIVO Y PARA EFECTO DEL PRESENTE CONTRATO SE LE DENOMINARA “EL PROVEEDOR” Y “EL CONSUMIDOR”, RESPECTIVAMENTE. 
                            </p>
                        </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DECLARACIONES
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                Las partes denominadas CONSUMIDOR y PROVEEDOR, se reconocen las personalidades con las cuales se ostentan y que se encuentran especificadas en este documento (orden de servicio y contrato), por lo cual, están dispuestas a sujetarse a las condiciones que se establecen en las siguientes: 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            CLÁUSULAS
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>1.- </b> Objeto: El PROVEEDOR realizará todo el  Lavado. Limpieza. Detallado y/o Semi-Detallados, solicitados por el CONSUMIDOR que suscribe el presente contrato, a las que se someterá el vehículo que al inicio se detalla, para obtener las condiciones de limpieza de acuerdo a lo solicitado por el CONSUMIDOR y que serán realizadas a cargo y por cuenta del CONSUMIDOR. EL PROVEEDOR no condicionará en modo alguno la prestación de servicios de Lavado. Limpieza. Detallado y/o Semi-Detallados del vehículo, a la adquisición o renta de otros productos o servicios en el mismo servicio, predeterminada. El precio total de los servicios contratados se establece en el presupuesto que forma parte del presente y se describe en su inicio; dicho precio será pagado por el CONSUMIDOR antes de darle el servicio al vehículo;  Lavado. Detallado y/o Semi-Detallados, todo pago efectuado por el CONSUMIDOR deberá efectuarse en el instrumento App. Ios y Android, descargables de tiendas oficiales. O en la terminal punto de venta, portada por el operador que prestara el servicio del PROVEEDOR solo de forma digital o tarjetas debito y/o tarjeta de crédito  y en Moneda Nacional o extranjera al tipo de cambio vigente al día de pago, NO SE RECIBEN PAGOS EN EFECTIVO. Solo mediante tarjeta de crédito o transferencia bancaria efectuada con anterioridad a realizar el servicio al vehículo. 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DE LAS CONDICIONES GENERALES:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>2.- </b> Las partes están de acuerdo en que las condiciones generales en las que se encuentra el automóvil de acuerdo con el inventario visual al momento de su recepción, son las que se definen en la orden de servicio-presupuesto que se encuentra al inicio del presente contrato. El CONSUMIDOR tendrá la obligación de aceptar dicho inventario con firma fotostática, misma que será de forma digital, con la que da aceptación y en conformidad con lo expuesto 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DEL PRESUPUESTO:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>3.- </b>  El PROVEEDOR se obliga ante el CONSUMIDOR a respetar el presupuesto contenido en el presente contrato. 
                                <br>
                                <b>4.- </b> El CONSUMIDOR se obliga pagar a el PROVEEDOR por el servicio solicitado para su automóvil la cantidad de <u style="color: black;">&nbsp;&nbsp;<?php if(isset($cantidad_contrato)) echo $cantidad_contrato; else "";?>&nbsp;&nbsp;</u>. (MONEDA NACIONAL), 
                                siempre y cuando no sea un vehículo distinto a lo solicitado en el servicio, en caso contrario será modificada la orden de servicio y se realizaran ajustes en costos y/o presupuestos por prestación de servicio. 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DE LA PRESTACIÓN DE SERVICIOS:
                        </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>5.- </b> Las partes convienen en que la fecha de aceptación del presupuesto es la que se indica en el inicio de este contrato.
                                <br>
                                <b>6.- </b> El PROVEEDOR se obliga a realizar el servicio de  Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado, en hora previamente establecido en el presupuesto. El PROVEEDOR se obliga a emplear productos adecuados y afines para los servicios contratados salvo que el CONSUMIDOR ordene o autorice expresamente y por escrito se utilicen otros o las provea, de conformidad con lo establecido en el artículo 60 de la Ley Federal de Protección al Consumidor, caso en el cual el Lavado. Detallado y/o Semi-Detallados del vehículo tendrá garantía por lluvias, en lo que se relacione a ese servicio, durante las primeras 24 horas continuas a su servicio. Siendo este un Lavado Express solo en el exterior del vehículo.
                                <br>
                                <b>7.-</b> El PROVEEDOR no podrá usar el vehículo para fines propios o de terceros. El PROVEEDOR se hace responsable por los daños causados al vehículo, como consecuencia del servicio efectuados por parte de su personal. Cuando el CONSUMIDOR, solicite que él o un representante suyo sea quien conduzca el automóvil para poder realizar el servicio, el riesgo, será por su cuenta.
                                <br>
                                <b>8.- </b> El PROVEEDOR se hace responsable por las posibles, daños o pérdidas parciales o totales imputables a él o a sus subalternos que sufra el vehículo, el equipo y los aditamentos adicionales que el CONSUMIDOR le haya notificado que existen en el momento de la recepción del mismo, o que se causen a terceros, mientras el vehículo se encuentre bajo su resguardo, salvo los ocasionados por desperfectos mecánicos o a resultas de piezas gastadas o sentidas, por incendio motivado por deficiencia eléctrica, o fallas en el sistema de combustible, y no causadas por el PROVEEDOR; para tal efecto el PROVEEDOR cuenta con un seguro suficiente para cubrir dichas eventualidades, bajo póliza expedida por compañía de seguros autorizada al efecto. El PROVEEDOR no se hace responsable por la pérdida de objetos dejados en el interior del automóvil, aun con la cajuela cerrada, salvo que estos le hayan sido notificados y puestos bajo su resguardo al momento de la recepción del automóvil. El PROVEEDOR tampoco se hace responsable por daños causados por fuerza mayor o caso fortuito, ni por la situación legal del automóvil cuando éste previamente haya sido robado o se hubiere utilizado en la comisión de algún ilícito; lo anterior, salvo que alguna de éstas cuestiones resultara legalmente imputable al PROVEEDOR; así mismo, el CONSUMIDOR, libera al PROVEEDOR de cualquier responsabilidad que surja o pueda surgir con relación al origen, posesión o cualquier otro derecho inherente al vehículo o a partes y componentes del mismo, obligándose en lo personal y en nombre del propietario, a responder de su procedencia. 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DEL PRECIO Y FORMAS DE PAGO:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>9.- </b> El CONSUMIDOR acepta haber tenido a su disposición los precios de la tarifa de mano de obra, Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado. El PROVEEDOR; los incrementos que resulten durante el servicio, por costos no previsibles y/o incrementos que resulten al momento de la ejecución del servicio por ser un vehículo distinto al cual se le realizara el servicio, deberán ser autorizados por el CONSUMIDOR, antes de realizar el servicio, y teniendo que pagar la s diferencias correspondientes, equivalentes al automóvil, El tiempo que, en su caso, que transcurra para cumplir esta condición, modificará la hora de entrega, en la misma proporción. Todas las quejas y sugerencias serán atendidas en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio.
                                <br>
                                <b>10.- </b> Será obligación del PROVEEDOR expedir la factura correspondiente por los servicios y productos que preste o enajene; el importe total del servicio, así como el precio por concepto del servicio, quedará especificado en ella, conforme a la ley.
                                <br>
                                <b>11.- </b> El CONSUMIDOR se obliga a pagar de contado al PROVEEDOR (conforme a lo establecido en la cláusula 1), en los medios dispuestos ; DIGITALES APP. WEB, de éste y POR ADELANTADO AL SERVICO CONTRATADO. A realizar al  automóvil, el importe del servicio, de conformidad con el presupuesto elaborado para tal efecto.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DE LA ENTREGA:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>12.- </b>  El PROVEEDOR se obliga a hacer la entrega del automóvil LIMPIO, en la hora establecida en este contrato, pudiendo ampliarse dicho plazo en caso fortuito o de fuerza mayor, en cuyo caso será obligación del PROVEEDOR dar aviso previo al CONSUMIDOR de la causa y del tiempo que se ampliará el plazo de entrega. Que en ese caso no podrá ser más de 30 Min. 
                                <br>
                                <b>13.- </b> La basura o residuos, derivados del servicio de lavado, quedarán a disposición del CONSUMIDOR al momento de hacerle entrega del automóvil.
                                <br>
                                <b>14.- </b> El CONSUMIDOR se obliga a hacer el pago, antes de realizarse el servicio. Y al momento que reciba el automóvil con el servicio de Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado  realizado , se entenderá que esto fue a completa satisfacción del CONSUMIDOR en lo que respecta a sus condiciones generales de acuerdo al inventario visual, mismas que fueron descritos en la orden de servicio, así como también en cuanto al Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado efectuada, sin afectar sus derechos a ejercer la garantía.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DE LA RESCISIÓN Y PENAS CONVENCIONALES:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>15.- </b>  Es causa de rescisión del presente contrato: A.- Que EL PROVEEDOR incumpla en la hora y lugar de entrega del vehículo por causas propias, caso en el cual el CONSUMIDOR notificará por escrito del incumplimiento al PROVEEDOR, y éste hará entrega inmediata del vehículo debidamente con su servicio conforme y presupuesto establecido, descontando del precio pactado para la prestación del servicio, la suma equivalente al 2% (dos por ciento) del total del servicio, que se pacta como pena convencional; B.- Que el CONSUMIDOR incumpla con el pago del servicio ordenado, en el término previsto en la cláusula 19, caso en el cual el PROVEEDOR le notificará por escrito su incumplimiento, y podrá optar por exigir la recisión o cumplimiento forzoso de la obligación, cobrando la misma pena pactada del 2% al CONSUMIDOR, por la mora.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DE LAS GARANTÍAS:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>16.- </b>  <u>Las reparaciones a que se refiere el presupuesto aceptado por el CONSUMIDOR y éste contrato están garantizadas por 24 horas naturales contados a partir de la hora de la entrega del vehículo ya Lavado</u>. Detallado y/o Semi-Detallados, según sea el servicio contratado, Si el vehículo es intervenido por un tercero, el PROVEEDOR no será responsable y la garantía quedará sin efecto. Las reclamaciones por garantía o solicitud de la misma  se harán vía App. Correo y/o telefónica  del PROVEEDOR, para lo cual el CONSUMIDOR pueda optar por su derecho, el servicio efectuado por el PROVEEDOR en cumplimiento a la garantía del servicio serán sin cargo alguno para el CONSUMIDOR. Esta garantía cubre cualquier lluvia o llovizna  por causas imputables al mismo y solo será válida siempre y cuando el automóvil se haya utilizado en condiciones de uso normal, se hayan observado en su uso las indicaciones de manejo y servicio que se le hubiera dado. En todo caso, el PROVEEDOR será corresponsable y solidario con los terceros del cumplimiento o incumplimiento de las garantías por ellos otorgadas en lo que se relacione al servicios a éste realizados, siempre que hayan sido contratados ante el CONSUMIDOR.
                                <br>
                                <b>17.- </b> Toda reclamación dentro del término de garantía, deberá ser realizada ante el PROVEEDOR que efectuó el servicio y en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio del presente contrato. En caso de que sea necesario hacer válida la garantía en un domicilio diverso al del PROVEEDOR, los gastos por ello deberán ser cubiertos por éste, siempre y cuando la garantía proceda, dichos gastos sean indispensables para tal fin, y sea igualmente indispensable realizar el servicio al vehículo en domicilio diverso al del PROVEEDOR, pero en dado caso, si el automóvil esta fuera de la entidad donde se localiza el PROVEEDOR, éste inmediatamente después de que tenga conocimiento, podrá indicar al CONSUMIDOR en donde se encuentra una filial de la marca más cercana, para hacer efectiva la garantía por conducto de ésta, si procede, debiendo acreditar con la factura correspondiente al servicio efectuado, con el objeto, de no hacer ningún cargo por ello al CONSUMIDOR. Con el objeto de dar cumplimiento a las obligaciones que ésta cláusula le impone, El PROVEEDOR señala como teléfonos de atención al CONSUMIDOR los que aparecen en éste contrato. Para los efectos de la atención y resolución de quejas y reclamaciones, estas deberán ser presentadas dentro de días y horas hábiles, que son los detallados en la parte superior del presente, en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio, al  PROVEEDOR, detallando en forma expresa la causa o motivo de la reclamación; la Gerencia de Servicio procederá a analizar la queja y resolverá lo conducente dentro de un lapso de tres días hábiles, procediendo al servicio del vehículo o manifestando las causas de improcedencia de la reclamación, en forma escrita. 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DE LA INFORMACIÓN Y PUBLICIDAD:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>18.- </b>  El PROVEEDOR se obliga a observar en todo momento lo dispuesto por los capítulos III y IV de la Ley Federal de Protección al Consumidor, en cuanto a la información, publicidad, promociones y ofertas.
                                <br>
                                <b>19.- </b> El CONSUMIDOR  (&nbsp;Si&nbsp;&nbsp; 
                                <?php if (isset($publicidad_contrato)): ?>
                                    <?php if (strtoupper($publicidad_contrato) == "1"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf1.jpg'; ?>" style="width:10px;" alt="">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                                &nbsp;&nbsp;&nbsp;
                                No&nbsp;&nbsp;
                                <?php if (isset($publicidad_contrato)): ?>
                                    <?php if (strtoupper($publicidad_contrato) == "0"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf1.jpg'; ?>" style="width:10px;" alt="">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                                &nbsp;)
                                acepta que el PROVEEDOR ceda o transmita a tercero, con fines mercadotécnicos o publicitarios, la información proporcionada por él con motivo del presente contrato, y  (&nbsp;Si&nbsp;&nbsp;&nbsp;
                                <?php if (isset($envio_publicidad)): ?>
                                    <?php if (strtoupper($envio_publicidad) == "1"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf1.jpg'; ?>" style="width:10px;" alt="">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                No&nbsp;&nbsp;
                                <?php if (isset($envio_publicidad)): ?>
                                    <?php if (strtoupper($envio_publicidad) == "0"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf1.jpg'; ?>" style="width:10px;" alt="">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                                &nbsp;)
                                acepta que el PROVEEDOR le envíe publicidad sobre bienes y servicios, firmando en éste espacio. 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="center">
                            <?php if (isset($firma_cliente_c1)): ?>
                                <?php if ($firma_cliente_c1 != ""): ?>
                                    <img src="<?php echo base_url().$firma_cliente_c1; ?>" alt="" style="height:1.2cm;">
                                <?php else: ?> 
                                    <img src="<?php echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:1.2cm;">
                                <?php endif; ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:1.2cm;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            DE LA INFORMACIÓN Y PUBLICIDAD:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                <b>20.- </b>  La Procuraduría Federal del Consumidor, es competente para conocer y resolver en la vía administrativa de cualquier controversia que se suscite sobre la interpretación o cumplimiento del presente contrato, por lo que las partes están de acuerdo en someterse a ella en términos de ley, para resolver sobre la interpretación o cumplimiento de los términos del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-174-SCFI-2007, Prácticas Comerciales- Elementos de Información para la Prestación de Servicios en General y cualquier otra disposición aplicable. Sin perjuicio de lo anterior en caso de persistir la inconformidad, las partes se someten a la jurisdicción de los tribunales competentes del domicilio del PROVEEDOR, renunciando en forma expresa a cualquier otra jurisdicción o al fuero que pudiera corresponderles en razón de sus domicilios presente o futuros o por cualquier otra razón. 
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="center">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 30px;" rowspan="2"><br></td>
                                    <td align="center" style="width: 300px;border-bottom: 0.5px solid #084f8c ;color:#52bf66;">
                                        <?php if (isset($ccfirma_asesor)): ?>
                                            <?php if ($ccfirma_asesor != ""): ?>
                                                <img src="<?php echo base_url().$ccfirma_asesor; ?>" alt="" style="height: 1.2cm;">
                                            <?php else: ?> 
                                                <img src="<?php echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:1.2cm;">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:1.2cm;">
                                        <?php endif; ?>

                                        <br>
                                        <b style="color: black;">&nbsp;&nbsp;<?php if(isset($cnombre_asesor)) echo $cnombre_asesor;?></b>
                                    </td>
                                    <td style="width: 20px;" rowspan="2"><br></td>
                                    <td align="center" style="width: 300px;border-bottom: 0.5px solid #084f8c ;color:#52bf66;">
                                        <?php if (isset($ccfirma_cliente)): ?>
                                            <?php if ($ccfirma_cliente != ""): ?>
                                                <img src="<?php echo base_url().$ccfirma_cliente; ?>" alt="" style="height: 1.2cm;">
                                            <?php else: ?> 
                                                <img src="<?php echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; ?>" alt="" style="height: 1.2cm;">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; ?>" alt="" style="height: 1.2cm;">
                                        <?php endif; ?>

                                        <br>
                                        <b style="color: black;">&nbsp;&nbsp;<?php if(isset($cnombre_cliente)) echo $cnombre_cliente;?></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="font-size: 8px;">
                                        EL PROVEEDOR (NOMBRE Y FIRMA). <br>
                                        OPERADOR 
                                    </td>

                                    <td align="center" style="font-size: 8px;">
                                        EL CONSUMIDOR (NOMBRE Y FIRMA) <br>
                                        CLIENTE  
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                            AVISO DE PRIVACIDAD:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:9px;" colspan="3" align="justify">
                            <p align="justify" class="parrafo_contrato">
                                EL Aviso de Privacidad Integral podrá ser consultado en nuestra página en internet www.xehos.com/privacidad/, o lo puede solicitar al correo electrónico info@xehos.com, u obtener personalmente en el Área de Atención a la Privacidad.
                                <br>
                                (&nbsp;Si&nbsp;&nbsp; 
                                <?php if (isset($privacidad_contrato)): ?>
                                    <?php if (strtoupper($privacidad_contrato) == "1"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf1.jpg'; ?>" style="width:10px;" alt="">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                                &nbsp;&nbsp;&nbsp;
                                No&nbsp;&nbsp;
                                <?php if (isset($privacidad_contrato)): ?>
                                    <?php if (strtoupper($privacidad_contrato) == "0"): ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf1.jpg'; ?>" style="width:10px;" alt="">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                                &nbsp;)
                                Al marcar el recuadro precedente y remitir mis datos, otorgo mi consentimiento para que mis datos personales sean tratados conforme a lo señalado en el Aviso de Privacidad.
                                <br>
                                Este contrato fue registrado ante la Procuraduría Federal del Consumidor. Registro público de contratos de adhesión y aprobado e inscrito con el número <u style="color: black;">&nbsp;&nbsp;<?php if(isset($registro_publico)) echo $registro_publico; else "";?>&nbsp;&nbsp;</u> Expediente No.  <u style="color: black;">&nbsp;&nbsp;<?php if(isset($expediente_publico)) echo $expediente_publico; else "";?>&nbsp;&nbsp;</u> de la fecha <u style="color: black;">&nbsp;&nbsp;<?php if(isset($registro_publico_dia)) echo $registro_publico_dia; else "";?>&nbsp;&nbsp;</u> de <u style="color: black;">&nbsp;&nbsp;<?php if(isset($registro_publico_mes)) echo $registro_publico_mes; else "";?>&nbsp;&nbsp;</u> de <u style="color: black;">&nbsp;&nbsp;<?php if(isset($registro_publico_anio)) echo $registro_publico_anio; else "";?>&nbsp;&nbsp;</u>.
                                <br>
                                Cualquier variación del presente contrato en perjuicio de EL CONSUMIDOR, frente al contrato de adhesión registrado, se tendrá por no puesta. 
                            </p>

                            <label style="font-size: 12px;font-weight: bold;">
                                “DESEO ADHERIRME AL CONTRATO TIPO DE SERVICIO DE AUTOLAVADO DETALLADO Y SEMI DETALLO  DE VEHICULOS XEHOS AUTOLAVADO DE PROFECO”
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>

<?php 
    try {
        $html = ob_get_clean(); 
        ob_clean();

        $mpdf = new \mPDF('utf-8', 'A4-P');
        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','debug' => TRUE,'allow_output_buffering' => TRUE]);
        
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->showImageErrors = true;
        $mpdf->WriteHTML($html);
        
        $mpdf->Output();

    //} catch (Html2PdfException $e) {
    } catch (Exception $e) {
        echo "No se pudo cargar el PDF<br>";
        echo $e;
    }

 ?>