function inicializaTabla() {

    $('#tabla_caja').DataTable({
        language: {
            url: PATH_LANGUAGE,

        },
        // footerCallback: function(row, data, start, end, display) {
        //     var api = this.api(),
        //         data;
        //     // Remove the formatting to get integer data for summation
        //     var intVal = function(i) {
        //         return typeof i === 'string' ?
        //             i.replace(/[\$,]/g, '') * 1 :
        //             typeof i === 'number' ?
        //             i : 0;
        //     };
        //     // Total over all pages
        //     total_saldo_neto = api
        //         .column(6)
        //         .data()
        //         .reduce(function(a, b) {
        //             return intVal(a) + intVal(b);
        //         }, 0);
        //     $("#total_saldo_neto_tabla").html('$' + total_saldo_neto);

        //     total_pago = api
        //         .column(7)
        //         .data()
        //         .reduce(function(a, b) {
        //             return intVal(a) + intVal(b);
        //         }, 0);
        //     $("#total_pagar_tabla").html('$' + total_pago);

        //     total_abonado = api
        //         .column(8)
        //         .data()
        //         .reduce(function(a, b) {
        //             console.log(a);
        //             return intVal(a) + intVal(b);
        //         }, 0);
        //     $("#total_abonado_tabla").html('$' + total_abonado);
        // },
        "ajax": {
            url: PATH_API + "api/corte-caja/getByUsuario?usuario_registro=" + user_id,
            type: 'GET',
            dataSrc: "",

        },
        columns: [{
                title: "Caja",
                data: 'CajaNombre',
            },
            {
                title: "Movimiento",
                data: 'MovimientoNombre',
            },
            {
                title: "Usuario corte",
                render: function(data, type, row) {
                    return row.nombre + ' ' + row.apellido_paterno + ' ' + row.apellido_materno;
                }
            },
            {
                title: "Fecha",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha_transaccion)
                }
            },
            {
                title: '-',
                render: function(data, type, row) {
                    return '<button title="Detalle venta" onclick="detalleCorte(this)" data-usuario_id="' + row.usuario_registro + '" data-fecha_corte="' + row.fecha_transaccion + '" class="btn btn-default"><i class="fas fa-list"></i></button>';

                }
            }
        ]
    });
}

function detalleCorte(_this) {
    setTimeout(() => {
        window.location.href = PATH + '/caja/reportes/detalle_corte?usuario_id=' + $(_this).data('usuario_id') + '&fecha_corte=' + $(_this).data('fecha_corte');
    }, 200);
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
        'folio': $("#folio").val(),
        'proveedor_id': $("#proveedor_id option:selected").val(),
        'estatus_cuenta_id': $("#estatus_cuenta_id option:selected").val()
    });

    $('#tbl_cxp').DataTable().ajax.url(PATH_API + 'api/cuentas-por-pagar?' + params).load()
}

function limpiarfiltro() {
    $("#folio").val('');
    $("#proveedor_id").val('');
    $("#estatus_cuenta_id").val('');
    $("#proveedor_id").trigger('change');
    var params = $.param({
        'folio': '',
        'proveedor_id': '',
        'estatus_cuenta_id': ''
    });

    $('#tbl_cxp').DataTable().ajax.url(PATH_API + 'api/cuentas-por-pagar?' + params).load()
}

this.inicializaTabla();
$("#proveedor_id").select2();