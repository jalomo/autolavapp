@layout('layout')
<style>
.row_campania{
  background: green;
  color: white;
  padding: 7px;
  margin-top: 7px;
}
</style>
@section('css_vista')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('contenido')
    <form action="{{site_url('citas/info_cliente_busqueda_shara_all')}}" id="frm" method="POST">
      <input type="hidden" name="proactivo" id="proactivo" value="{{$proactivo}}">
        <div class="row">
             <div class="col-md-3">
              <label for="">Buscar por campo</label>
              <input class="form-control" type="text" name="buscar_campo" id="buscar_campo">
            </div>
            <div class='col-md-3'>
              <label for="">Fecha inicio</label>
                <div class="form-group1">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type="text" name="finicio" id="finicio" value="">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
                <span style="color: red" class="error error_fini"></span>
            </div>
            <div class='col-md-3'>
              <label for="">Fecha Fin</label>
                <div class="form-group1">
                    <div class='input-group date' id='datetimepicker2'>
                        <input type="text" name="ffin" id="ffin" value="">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
                <span style="color: red" class="error error_ffin"></span>

            </div>
      </div>
      <div class="row">
            <div class="col-md-3">
              <label for="">Buscar por Orden (LETRA)</label>
              <input class="form-control" type="text" name="letra_orden" id="letra_orden" maxlength="1">
            </div>
            <div class="col-md-3">
              <label for="">Tipo de orden</label>
              {{$drop_orden}}
            </div>
            <div class="col-md-2 col-sm-2" style="margin-top:30px;">
              <button type="button" id="buscar" name="buscar" class="btn btn-info">Buscar</button>
            </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6 col-sm-6">
              <button type="button" id="indicadores" name="indicadores" class="btn btn-info">Indicadores multipunto</button>
              <button type="button" id="rechazadas" name="rechazadas" class="btn btn-info">Presupuestos no autorizados</button>
          </div>
          <div class="col-sm-6 text-right">
            <button id="pdf" class="btn btn-info" style="margin-right: 20px">
              <i style="color: white" class="fa fa-file-pdf-o"></i> Exportar PDF
            </button>
        </div>
        </div>
    </form>
  <div class="row">
    <div class="col-sm-12">
      <span style="background-color: green;color: white;margin-left: 10px;">Unidades con campaña</span>
    </div>
  </div>
  <br>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-responsive table-hover"  id="tabla" class="display" style="width:100%">
		        <thead>
		            <tr class="tr_principal verde">
                  @if($proactivo)
                    <th>id</th>
                    <th>nombre </th>
                    <th>ap </th>
                    <th>am </th>
                    <th>placas </th>
                    <th>tel_principal </th>
                    <th>tel_secundario </th>
                    <th>tel_adicional </th>
                    <th>correo </th>
                    <th>Acciones</th>
                  @else
                        <th>id </th>
                        <th>Serie</th>
                        <th># Orden</th>
                        <th>Plcas</th>
                        <th>Tipo de orden</th>
                        <th>Código Cliente</th>
                        <th># Torreo</th>
                        <th># Asesor</th>
                        <th>Fecha de recepción</th>
                        <th>Hora de recepción</th>
                        <th>Cargo actual</th>
                        <th>Km actual</th>
                        <th>Cliente problema</th>
                        <th>Fecha entrega estimada</th>
                        <th>Hora entrega estimada</th>
                        <th>Fecha remisión</th>
                        <th>Hora remisión</th>
                        <th>Nombre</th>
                        <th>Razón social</th>
                        <th>Calle</th>
                        <th>Número exterior</th>
                        <th>Número interior</th>
                        <th>Colonia</th>
                        <th>Municipio</th>
                        <th>Estado</th>
                        <th>C.P.</th>
                        <th>Teléfono principal</th>
                        <th>Teléfono secundario</th>
                        <th>Teléfono adicional</th>
                        <th>Correo</th>
                        <th>R.F.C</th>
                        <th>Dirigirse con</th>
                        <th># Siniestro</th>
                        <th># Poliza</th>
                        <th>Estatus</th>
                        <th>Vehículo</th>
                        <th>Año vehículo</th>
                        <th>Asesor</th>
                        <th>R.F.C. Asesor</th>
                        <th>Nombre cliente</th>
                        <th>Acciones</th>

                  @endif

		            </tr>
		        </thead>
		        <tbody>

		        </tbody>
		    </table>
		</div>
	</div>
@endsection

@section('included_js')

	<script>
		var site_url = "{{site_url()}}";
        $('.date').datetimepicker({
          format: 'DD/MM/YYYY',
          icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-arrow-up",
              down: "fa fa-arrow-down"
          },
           locale: 'es'
        });
		$(document).ready(function() {
            iniciarTabla();
            $("#buscar").on('click',function(){
                $(".error").empty();
                var tipo_busqueda = $("#tipo_busqueda").val();
                var finicio = $("#finicio").val();
                var ffin = $("#ffin").val();
                var table = $('#tabla').DataTable();
                table.destroy();
                iniciarTabla();

            });
            
        });
        function iniciarTabla(){

            var tabla = $("#tabla").dataTable({
                paging: true,
                bFilter: true,
                processing: true,
                responsive: true,
                serverSide:true,
                ajax: {
                  url: site_url+"/citas/getDatosDt",
                  type: 'POST',
                   //////////////////////////////////////////////////////////////// AQUI PUEDO AGREGAR MAS CAMPOS EN LA PETICION
                  data: function(data){
                     data.fecha_inicio = $("#finicio").val();
                     data.fecha_fin = $("#ffin").val();
                     data.tipo_busqueda = $("#tipo_busqueda").val();
                     data.buscar_campo = $("#buscar_campo").val();
                     data.proactivo = $("#proactivo").val();
                     data.letra_orden = $("#letra_orden").val();
                     data.tipo_orden = $("#tipo_orden").val();
                    // if(filtros.pagina){
                    //   data.start = parseInt(filtros.pagina);
                    // }
                    empieza = data.start;
                    por_pagina = data.length;
                  }
                },
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "oLanguage": {
                   "oPaginate": {
                       "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                       "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                    '<option value="5">5</option>' +
                    '<option value="10">10</option>' +
                    '<option value="20">20</option>' +
                   '<option value="30">30</option>' +
                    '<option value="40">40</option>' +
                    '<option value="50">50</option>' +
                    '<option value="-1">Todos</option>' +
                    '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "mensaje",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar"
                },
            });
        }
    $("body").on("click",'.js_comentarios',function(e){
    e.preventDefault();
    id_cita = $(this).data('id');
       var url =site_url+"/citas/comentarios_reagendar_MAGIC/0";
       customModal(url,{"id_cita":id_cita},"GET","md",ingresarComentario,"","Guardar","Cancelar","Ingresar comentario","modal1");
    });
    $("body").on("click",'.js_historial',function(e){
    e.preventDefault();
    id_cita = $(this).data('id');
       var url =site_url+"/citas/historial_comentarios_reagendar_magic/0";
       customModal(url,{"id_cita":id_cita},"GET","md","","","","Cerrar","Historial de comentarios","modal1");
    });

    //Creamos el modal para el pdf de la informacion del cliente
    $("body").on("click",'.js_info',function(e){
        e.preventDefault();
        id_cita = $(this).data('id');
        var url = site_url+"/citas/info_cliente_busqueda_shara/";
        customModal(url,{"id_cita":id_cita},"POST","lg","","","","Salir","Ficha Cliente","modal1");
    });

  function ingresarComentario(){
    var url =site_url+"/citas/comentarios_reagendar_MAGIC";
    ajaxJson(url,$("#frm_comentarios").serialize(),"POST","",function(result){
      if(isNaN(result)){
        data = JSON.parse( result );
        //Se recorre el json y se coloca el error en la div correspondiente
        $.each(data, function(i, item) {
           $.each(data, function(i, item) {
                      $(".error_"+i).empty();
                      $(".error_"+i).append(item);
                      $(".error_"+i).css("color","red");
                  });
        });
      }else{
        if(result <0){
          ErrorCustom('No se pudo guardar el comentario, por favor intenta de nuevo');
        }else{
          $(".close").trigger('click');
           ExitoCustom("Comentario guardado con éxito");

        }
      }
    });
  }

  $("body").on("click",'.js_refacciones',function(e){
    e.preventDefault();
      var url =site_url+"/citas/refacciones_shara/";
      customModal(url,{},"POST","lg","","","","Salir","Refacciones","modal1");
    });
  $("body").on("click",'#indicadores',function(e){
       var url =site_url+"/citas/modal_multipunto";
       customModal(url,{},"GET","lg","","","","Salir","Indicadores Multipunto","modalMultipunto");
  });
  $("body").on("click",'#rechazadas',function(e){
       var url =site_url+"/citas/modal_cotizaciones_rechazadas";
       customModal(url,{},"GET","lg","","","","Salir","Presupuestos no autorizados","modalrechazadas");
  });
  //Creamos el modal para el pdf de la informacion del cliente
  $("body").on("click",'#pdf',function(e){
        e.preventDefault();
        var url = site_url+"/citas/exportar_historial_pdf/";
        customModal(url,$("#frm").serialize(),"POST","lg","","","","Salir","Exportar","modalExport");
    });
	</script>
@endsection
