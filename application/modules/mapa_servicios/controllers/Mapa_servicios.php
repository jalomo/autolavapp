<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Mapa_servicios extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mmapa_servicios', 'm_ser', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation',));
    date_default_timezone_set('America/Mexico_City');
    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
  }

  public function index()
  {    
    $data['servicios'] = $this->m_ser->get_table('ubicacion_usuario');
    $data['titulo'] = "Zonas";
    $data['titulo_dos'] = "Alta zona";
    $this->blade->render('mapa_servicios', $data);
  }

  public function get_servicios(){
    $servicios = $this->m_ser->get_table('ubicacion_usuario');
    echo json_encode($servicios);
  }

  public function lavadores()
  {    
    $folio = $this->input->get('folio');
    $data['lavadores'] = $this->m_ser->get_table('lavadores');"";//$this->m_ser->get_table('ubicacion_usuario');
    $data['titulo'] = "Zonas";
    $data['titulo_dos'] = "Alta zona";

    if (isset($folio)&&!empty($folio)){
      $res = $this->m_ser->get_row('folio_mostrar',$folio,'v_info_servicios');

      if(is_object($res)){
       
        $data['coordenadas'] = $this->m_ser->get_result('id_servicio',$res->id,'coordenadas_lavador');
        $data['folio'] = $folio;
      }else{
        $data['folio'] = '';

      }
    }else{
       
    }

    
    $this->blade->render('lavadores', $data);
  }

  
}