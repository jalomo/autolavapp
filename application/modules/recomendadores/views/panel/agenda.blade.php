@layout('recomendadores/layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12">
            <form id="formulario_contacto" method="post" action="" autocomplete="on">
                <h3 style="color:darkblue;">
                    <a href="#" class="btn btn-menu-titulo">
                        <i class="fa fa-address-book-o" aria-hidden="true"></i>
                    </a>
                    <?php if(isset($titulo_dos)): ?>
                        <?php echo $titulo_dos; ?>
                    <?php endif; ?>
                </h3>

                <hr>
                <div class="row">
                    <div class="col-sm-5">
                        <label for="">Nombre : </label>
                        <input type="text" class="form-control" name="nombre" value="<?php if(isset($nombre)) echo $nombre; ?>">
                        <div id="nombre_error"></div>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Telefono : </label>
                        <input type="text" class="form-control" name="telefono" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($telefono)) echo $telefono; ?>">
                        <div id="telefono_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Notas : </label>
                        <textarea class="form-control" id="notas" name="notas" rows="1"><?php if(isset($notas)) echo $notas; ?></textarea>
                        <div id="n_tarjeta_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <br>
                        <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                        <br>
                        <h5 class="error" id="formulario_error"></h5>
                        
                        <input type="hidden" name="formulario" value="1">

                        <a href="<?= base_url().'index.php/recomendadores/panel/cupones' ?>" class="btn btn-dark" style="margin-right: 35px;">
                            REGRESAR
                        </a>
                        
                        <input type="button" class="btn btn-success" id="enviar_formulario" value="GUARDAR">
                        
                        <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                        <input type="hidden" name="listado" value="<?php if(isset($id)) echo $id; else echo '0'; ?>">
                        <br>    
                    </div>
                </div>
            </form>            
        </div>
    </div>

    <br><hr>
    <div class="row">
        <div class="col-sm-12 table-responsive">
            <h4 style="text-align: center;">
                Mis contactos
            </h4>
            
            <br>
            <table <?php if ($mis_contatos != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>Notas</th>
                        <th>Fecha alta</th>
                        <th colspan="2"></th> 
                    </tr>
                </thead>
                <tbody id="cuerpo">
                    @if (isset($mis_contatos))
                        @if ($mis_contatos != NULL)
                            @foreach ($mis_contatos as $i => $registro)
                                <tr>
                                    <td style="vertical-align: middle;" align="center">
                                        <?= $i + 1;?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <?= $registro->contacto ?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <?= $registro->telefono ?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <?= (($registro->nota != "") ? $registro->nota : "Sin notas") ?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                         <?php 
                                            $fecha = new DateTime($registro->fecha_alta);
                                            echo $fecha->format('d-m-Y');
                                        ?>
                                    </td>
                                    <td style="vertical-align: middle;" align="center">
                                        <a onclick="editar_contacto(<?= $registro->id ?>)" title="Editar"> 
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle;" align="center">
                                        <a style="cursor: pointer" data-id="{{ $registro->id }}" class="js_cancelar" aria-hidden="true"  data-toggle="tooltip" data-placement="top" title="Eliminar">
                                            <i style="color:red;" class="fa fa-ban"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7" align="center">
                                    Sin contactos registrados
                                </td>
                            </tr>
                        @endif
                    @else
                        <tr>
                            <td colspan="7" align="center">
                                Sin registros
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <br>
        </div>
    </div>
            

@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script src="{{ base_url('statics/js/recomendadores/formulario_contacto.js') }}"></script>

    <script>
        var site_url = "{{ site_url() }}";

        const callbackCancelarServicio = () => {
            var url = site_url + "recomendadores/panel/eliminar_contacto/";
            ajaxJson(url, {
                "id": id
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('El contacto no se pudo eliminar, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Contacto eliminado correctamente", function() {
                        location.href = site_url+"/recomendadores/panel/agenda";
                    });
                }
            });
        }

        $("body").on("click", '.js_cancelar', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            aPos = $(this);
            ConfirmCustom("¿Está seguro de eliminar este contacto?", callbackCancelarServicio, "", "Confirmar",
                "Cancelar");
        });

    </script>
@endsection