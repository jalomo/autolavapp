@layout('layout')
@section('contenido')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="ui-typography">
                <div class="row">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Alta de incidencia</strong>
                        </div>
                        <div class="card-body">
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <form action="" method="post" novalidate="novalidate" id="alta_usuario">
                                        <div class="row">
                                            
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Titulo</label>
                                                    <input id="titulo" name="save[titulo]" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="titulo" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Costo</label>
                                                    <input id="costo" name="save[costo]" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="costo" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                        

                                        

                                        <div class="row">
                                        
                                            
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Uinidad</label>
                                                    <select name="save[id_unidad]" id="id_unidad" class="form-control">
                                                        <option value="">-- Selecciona sucursal --</option>
                                                        @foreach ($unidades as $s => $unidad)
                                                            <option value="{{ $unidad->id }}">{{ $unidad->placas }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        

                                        <div class="row">
                                           <div class="col-2">
                                           Fecha incidente
                                           <input type="text" id="datepicker" name="save[fecha]" id="fecha" class="form-control cc-exp form-control-sm" >
                                            </div>

                                            


                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Lavadores</label>
                                                    
                                                    <?php $options_su = array();?>
                                                        @foreach ($lavadores as $s => $lavador)
                                                            <?php $options_su[$lavador->lavadorId] = $lavador->lavadorNombre;?>
                                                           
                                                        @endforeach
                                                    <?php
                                                        
                                                       
                                                        
                                                        $js = 'class="form-control cc-exp form-control-sm" id="id_lavador"';
                                                        echo form_dropdown('save[id_lavador]', $options_su, '',$js);
                                                    ?>
                                                </div>
                                            </div>

                                            <!--div class="col-4">
                                                <div class="control-group">
                                                    <label class="control-label" for="basicinput">Imagen</label>
                                                    <div class="controls">
                                                        <input type="file" id="documento" name="documento" placeholder=""
                                                            class="span8" required="">
                                                       
                                                    </div>
                                                </div>
                                            </div-->

                                        </div>
                                        <div align="right">
                                            <button type="button" id="guardar" type="submit" class="btn btn-lg btn-info ">
                                                <i class="fa fa-edit fa-lg"></i>&nbsp;
                                                <span id="payment-button-amount">Guardar</span>
                                                <span id="payment-button-sending" style="display:none;">Sending…</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                </div>
                <!--/.col-->
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Titulo</th>
                                        <th>Fecha</th>
                                        <th>Costo</th>
                                        <th>Uinidad</th>
                                        <th>Lavador</th>
                                        <th>Opciones</th>
                                        
                                        
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                 <?php if(is_array($incidencias)):?>
                                <?php foreach($incidencias as $row):?>
                                    <tr>
                                        <th><?php echo $row->titulo;?></th>
                                        <th><?php echo $row->fecha;?></th>
                                        <th><?php echo $row->costo;?></th>
                                        <th><?php echo nombre_unidad($row->id_unidad);?></th>
                                        <th><?php echo nombre_lavador($row->id_lavador);?></th>

                                        <td>
                                            <a 
                                                href="<?php echo base_url() ;?>index.php/repo/index/<?php echo $row->id;?>">
                                                <button type="button" class="btn btn-info">Repositorio </button>
                                            </a>
                                        </td>
                                       
                                       
                                        
                                    </tr>
                                <?php endforeach;?>
                                <?php endif;?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
           
       // $( "#datepicker" ).datepicker();
        $( "#datepicker" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
        
    });
            $("#cargando").hide();
            $('#guardar').on('click',function(event) {
                event.preventDefault();
                $("#enviar").hide();
                $("#cargando").show();
                var url_sis ="<?php echo base_url(); ?>index.php/unidades/alta_guardar_incidencia";
                // Get form
                var form = $('#alta_usuario')[0];
                // Create an FormData object
                var data = new FormData(form);
                console.log(data);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url_sis,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        exito_redirect("DATOS GUARDADOS CON EXITO", "success",
                            "<?php echo base_url(); ?>index.php/unidades/inicidecias"
                        );
                        $("#enviar").show();
                        $("#cargando").hide();

                    },
                    error: function(e) {
                        //$("#result").text(e.responseText);
                        console.log("ERROR : ", e);
                        //$("#btnSubmit").prop("disabled", false);
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                        $("#enviar").show();
                        $("#cargando").hide();

                    }
                });
            });
    </script>
@endsection
