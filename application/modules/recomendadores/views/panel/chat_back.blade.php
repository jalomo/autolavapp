@layout('recomendadores/layout')

@section('included_css')
<style>
    * {
        box-sizing: border-box;
    }

    .chat_window {
        background-color: #fff;
        border: 2px solid #ccc;
    }

    .messages {
        position: relative;
        list-style: none;
        padding: 20px 10px 0 10px;
        margin: 0;
        height: 400px;
        overflow: scroll;
    }

    .messages .message {
        clear: both;
        overflow: hidden;
        margin-bottom: 20px;
        transition: all 0.5s linear;
        opacity: 0;
    }

    .messages .message.left .avatar {
        background-color: #2c2929;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        float: left;
        text-align: center;
        padding-top: 20px;
        color: white;
        overflow: hidden;
        width: 60px;
    }

    .messages .message.left .text_wrapper {
        background-color: #f1f1f1;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        margin-left: 20px;
    }

    .messages .message.left .text_wrapper::after,
    .messages .message.left .text_wrapper::before {
        right: 100%;
        border-right-color: #f1f1f1;
    }

    .messages .message.left .text {
        color: #2c2929;
    }

    .messages .message.right .avatar {
        background-color: #356aac;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        float: right;
        text-align: center;
        padding-top: 20px;
        color: white;
        overflow: hidden;
        width: 60px;
    }

    .messages .message.right .text_wrapper {
        background-color: #becaff;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        margin-right: 20px;
        float: right;
    }

    .messages .message.right .text_wrapper::after,
    .messages .message.right .text_wrapper::before {
        left: 100%;
        border-left-color: #becaff;
    }

    .messages .message.right .text {
        color: #45829b;
    }

    .messages .message.appeared {
        opacity: 1;
    }

    .messages .message .avatar {
        width: 60px;
        height: 60px;
        border-radius: 50%;
        display: inline-block;
    }

    .messages .message .text_wrapper {
        display: inline-block;
        padding: 20px;
        border-radius: 6px;
        width: calc(100% - 85px);
        min-width: 100px;
        position: relative;
    }

    .messages .message .text_wrapper::after,
    .messages .message .text_wrapper:before {
        top: 18px;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
    }

    .messages .message .text_wrapper::after {
        border-width: 13px;
        margin-top: 0px;
    }

    .messages .message .text_wrapper::before {
        border-width: 15px;
        margin-top: -2px;
    }

    .messages .message .text_wrapper .text {
        font-size: 18px;
        font-weight: 300;
    }

    .bottom_wrapper {
        position: relative;
        width: 100%;
        background-color: #fff;
        padding: 20px 20px;
        bottom: 0;
    }

    .bottom_wrapper .message_input_wrapper {
        display: inline-block;
        height: 50px;
        border-radius: 25px;
        border: 1px solid #bcbdc0;
        width: calc(100% - 160px);
        position: relative;
        padding: 0 20px;
    }

    .bottom_wrapper .message_input_wrapper .message_input {
        border: none;
        height: 100%;
        box-sizing: border-box;
        width: calc(100% - 40px);
        position: absolute;
        outline-width: 0;
        background-color: white;
    }

    .bottom_wrapper .send_message {
        width: 140px;
        height: 50px;
        display: inline-block;
        border-radius: 50px;
        background-color: #a3d063;
        border: 2px solid #a3d063;
        color: #fff;
        cursor: pointer;
        transition: all 0.2s linear;
        text-align: center;
        float: right;
    }

    .bottom_wrapper .sending {
        display: none;
        width: 140px;
        height: 50px;
        border-radius: 50px;
        background-color: #a3d063;
        border: 2px solid #a3d063;
        color: #fff;
        cursor: pointer;
        transition: all 0.2s linear;
        text-align: center;
        float: right;
    }

    .bottom_wrapper .send_message:hover {
        color: #a3d063;
        background-color: #fff;
    }

    .bottom_wrapper .send_message .text {
        font-size: 18px;
        font-weight: 300;
        display: inline-block;
        line-height: 48px;
    }

    .bottom_wrapper .sending .text {
        font-size: 18px;
        font-weight: 300;
        display: inline-block;
        line-height: 48px;
    }
</style>
@section('contenido')
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css" id="theme-styles">

<div class="container-fluid panel-body">
    <h1 class="mt-4">CHAT</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ ucwords($this->uri->segment(3)) }}</li>
    </ol>
    <div class="row">
        <div class="col-md-12 text-right mt-3 mb-4">
            <a href="javascript:window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> &nbsp;Regresar al panel</a>
        </div>
        <div class="col-md-12">
            <div class="chat_window">
                <div id="cargando" class="spin text-center">
                    <i style="font-size:150px" class="fa fa-spinner fa-pulse fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
                <ul class="messages" id="chat">

                </ul>
                <div class="bottom_wrapper clearfix">
                    <div class="message_input_wrapper"> 
                    <input class="message_input" id="mensaje_chat" placeholder="Escribe el mensaje"></div>
                    <div class="send_message" onclick="enviarNotificacion()">
                        <div class="icon"></div>
                        <div class="text">Enviar</div>
                    </div>
                    <div class="sending">
                        <div class="icon"></div>
                        <div class="text">Enviando ...</div>
                    </div>
                </div>
                <input type="hidden" id="telefono_cliente" value="<?php echo $telefono_cliente; ?>" />
                <input type="hidden" id="telefono_destinatario" value="<?php echo $telefono_destinatario; ?>" />
            </div>
        </div>
    </div>
</div>
@endsection
@section('included_js')
@include('main/scripts_dt')
<script type="text/javascript">
    var site_url = "{{site_url()}}";
    var notnotification = true;

    $(document).ready(function() {
        getNotificacionesChat();
        setInterval(getNotificacionesChat, 7000);
    });

    function getNotificacionesChat() {
        var objDiv = document.getElementById("chat");
        objDiv.scrollTop = objDiv.scrollHeight;
        var url = site_url + "/sistema/ApiChatRecomendador/getchat";
        ajaxJson(url, {
            telefono1: $("#telefono_destinatario").val(),
            telefono2: $("#telefono_cliente").val(),
        }, "GET", "", function(response) {
            $("#chat").html('');
            result = JSON.parse(response);
            if (result && result.length >= 1) {
                $.each(result, function(key, value) {
                    tipo = $("#telefono_cliente").val() == value.celular ? 'left' : 'right';
                    $("#cargando").hide();
                    $("#chat").append('<li class="message ' + tipo + ' appeared">' +
                        '<div class="avatar">' + value.destinatario + '</div>' +
                        '<div class="text_wrapper">' +
                        '<div class="text"> ' + value.mensaje + '</div>' +
                        '<div class="text-right">' + value.fecha_creacion + '</div>' +
                        '</div>' +
                        '</li>');
                });
            } else {
                $("#cargando").hide();
            }
        });
    }

    function marcarLeido() {
        $.post('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/ver_notificacion_numero', {
            celular: $("#telefono_cliente").val(),
            celular2: $("#telefono_destinatario").val()
        }, function(response) {
            // toastr.success("Notificación marcada como leída");
        });
    }

    function enviarNotificacion(_this) {
        marcarLeido();
        if ($('#mensaje_chat').val().length > 1) {
            var url = site_url + "/sistema/ApiChatRecomendador/apiNotificacion";
            ajaxJson(url, {
                mensaje: $('#mensaje_chat').val(),
                telefono: $("#telefono_destinatario").val(),
                celular2: $("#telefono_cliente").val(),
                mensaje_respuesta: $(_this).data('respuesta')
            }, "GET", "", function(response) {
                if (response) {
                    $('#mensaje_chat').val('');
                    $(".send_message").show();
                    $(".sending").hide();

                } else {
                    Swal.fire({
                        icon: 'error',
                        text: 'Ocurrio un error al enviar el mensaje',
                    });
                    $(".send_message").show();
                    $(".sending").hide();
                }
            });
        } else {
            toastr.error("Favor de indicar el mensaje");
        }
    }

    function obtenerFechaMostrar(fecha) {
        const dia = 2,
            mes = 1,
            anio = 0;
        split = fecha.split(' ');
        fecha = split[0].split('-');
        return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio] + ' ' + split[1];
    }
</script>
@endsection