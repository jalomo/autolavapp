<!doctype html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <!--html class="no-js" lang=""--> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->config->item('TITULO');?></title>
        <meta name="description" content="Sufee Admin - HTML5 Admin Template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/png" href="<?= base_url(); ?>statics/inventario/imgs/logos/xehos2.png"/>
        <!--<link rel="apple-touch-icon" href="apple-icon.png">
        <link rel="shortcut icon" href="favicon.ico">-->

        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/themify-icons.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/flag-icon.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/css/cs-skin-elastic.css">
        <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
        <link rel="stylesheet" href="<?php echo base_url();?>statics/tema/assets/scss/style.css">
        
        <style type="text/css">
            .cargaIcono {
                -webkit-animation: rotation 1s infinite linear;
                font-size: 55px;
                color: darkblue;
                display: none;
            }

            .btn-logo{
                margin-left: 10px;
            }
        </style>

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
        <!--EOD Styles-->
        <link rel="stylesheet" href="<?php echo base_url();?>statics/css/styleNETO.css">
        <script src="https://kit.fontawesome.com/fe47b5dbda.js" crossorigin="anonymous"></script>
    </head>

    <body class="login-image">
        <div class="sufee-login d-flex align-content-center flex-wrap">
            <div class="container">
                <div class="login-content">
                    <div class="login-logo" style="background: #fff">
                        <div id="errorMessageLogin" style="color: #FF0000; display: none"></div>
                        <div id="errorLoginData" style="color: #FF0000; display: none"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" align="center">
                        <img src="<?= base_url(); ?>statics/img/logohorizontal.png" class="logo" style="height: 4cm;">
                        </div>
                    </div>
                    <br>
                    <div class="login-form">
                        <form id="form_login" method="post">
                            <div class="form-group">
                                <label>Usuario</label>
                                <input type="text" class="form-control" placeholder="Usuario" name="recordUsername" required/>
                                <div id="recordUsername_error"></div>
                            </div>
                            
                            <div class="form-group">
                                <label>Contraseña</label>
                                <input type="password" class="form-control" placeholder="Contraseña" name="recordPassword" required/>
                                <div id="recordPassword_error"></div>
                            </div>
                            
                            <br>
                            <h5 style="color:white;" id="formulario_error"></h5>
                            <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>

                            <button type="submit" id="entrar" class="btn btn-success btn-flat m-b-30 m-t-30">Entrar</button>
                        </form>

                        <hr>
                        <a href="<?=base_url() ?>index.php/recomendadores/login/registro" type="button" class="btn btn-primary btn-flat m-b-30 m-t-30">Registro</a>
                        <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                    </div>

                    <br>
                    <div class="row socialmedia">
                        <div class="col-sm-12" align="center">
                            <a href="<?= CONST_FACEBOOK ?>" class="btn-logo" target="_blank">
                            <i class="fab fa-facebook"></i>
                            </a>

                            <a href="<?= CONST_INSTAGRAM ?>" class="btn-logo" target="_blank">
                            <i class="fab fa-instagram"></i>
                            </a>

                            <a href="<?= CONST_TWITTER ?>" class="btn-logo" target="_blank">
                            <i class="fab fa-twitter"></i>
                            </a>

                            <a href="<?= CONST_YOUTUBE ?>" class="btn-logo" target="_blank">
                            <i class="fab fa-youtube"></i>
                            </a>

                            <a href="<?= CONST_WHATSAPP ?>" class="btn-logo" target="_blank">
                            <i class="fab fa-whatsapp"></i>
                            </a>
                        </div>
                    </div>
                    
                    <br>
                    <br>
                </div>
            </div>
        </div>


        <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
        <script src="<?php echo base_url();?>statics/tema/assets/js/vendor/jquery-2.1.4.min.js"></script>
        <!--script type="text/javascript" src="<?php echo base_url().'statics/js/libraries/jquery.js'; ?>"></script-->
        <!--script src="<?php echo base_url();?>statics/tema/assets/js/popper.min.js"></script>
        <script src="<?php echo base_url();?>statics/tema/assets/js/plugins.js"></script-->
        <!--script src="<?php echo base_url();?>statics/tema/assets/js/main.js"></script-->

        <script src="{{ base_url('statics/js/recomendadores/login.js') }}"></script>
    </body>
</html>
