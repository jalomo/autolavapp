<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require "/var/www/web/xehos/jwt/vendor/autoload.php";
use \Firebase\JWT\JWT;
class Api_lavador extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Operador', 'consulta', TRUE);
        //$this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
    }


    public function index()
    {
    	echo "hola";
    }

    public function login()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $telefono = $_POST["telefono"];
        $password = $_POST["password"];
        $token = $_POST["token"];
        $contenedor = [];

        $usuario_data = $this->consulta->get_result_field("lavadorTelefono",$telefono,"activo","1","lavadores");
        // echo '<pre>'; print_r($usuario_data); exit();
        foreach ($usuario_data as $row){
            $pass_db = $row->lavadorPassword;
            $usuario = $row->lavadorNombre;
            $id_usuario = $row->lavadorId;
            $telefono = $row->lavadorTelefono;
            $sucursal = $row->id_sucursal;
        }
  
        if (isset($pass_db)) {
            //Verificamos que las sesiones coincidan
            if ((md5($password) === $pass_db)||($password === $pass_db)) { 
                //Armamos los valores de respuesta
                //$contenedor["error"] = "0";
                $contenedor["status"] =  "OK";
                $contenedor["id_usuario"] = $id_usuario;
                $contenedor["usuario"] = $usuario;
                $contenedor["telefono"] = $telefono;
                $contenedor["sucursal"] = $sucursal;

                $contenido["token"] = $token;

                $secret_key = "XehosXd459iaW";
                $issuer_claim = "XEHOS";
                $audience_claim = "XEHOS_AUTOLAVAPP";
                $issuedat_claim = 1356999524; // issued at
                $notbefore_claim = 1357000000; //not before
                $token = array(
                    "iss" => $issuer_claim,
                    "aud" => $audience_claim,
                    "iat" => $issuedat_claim,
                    "nbf" => $notbefore_claim,
                    "data" => array(
                        "id" => $id,
                        "firstname" => $firstname,
                        "lastname" => $lastname,
                        "email" => $email
                ));
        
                //http_response_code(200);
        
                $jwt = JWT::encode($token, $secret_key);

                $contenedor['token'] = $jwt;

                $actualizar = $this->consulta->update_table_row('lavadores', $contenido, 'lavadorId', $id_usuario);
                if ($actualizar) {
                    $contenedor["mensaje"] = "TOKEN ACTUALIZADO";
                } else {
                    $contenedor["mensaje"] = "EL TOKEN NO SE PUDO ACTUALIZAR";
                }

            } else { 
                $contenedor["error"] = "2";
                $contenedor["mensaje"] = "EL PASSWORD ES INCORRECTO.";
            } 
        } else {
            $contenedor["error"] = "1";
            $contenedor["mensaje"] = "EL TELÉFONO NO ESTA DADO DE ALTA COMO OPERADOR.";
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    public function mi_agenda()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $id_lavador = $_POST["id_lavador"];
        $fecha = $_POST["fecha"];
        $contenedor = [];

        $listado = $this->consulta->servicios_lavador_fecha($id_lavador,$fecha);

        if ($listado != NULL) {
            foreach ($listado as $row) {
                if ($row->cancelado == "1") {
                    $estatus = "Cancelado";
                } elseif ($row->reagendado == "1") {
                    $estatus = "Reagendado";
                }else {
                    $estatus = $row->estatus;
                }
                
                $contenedor[] = array(
                    "id_servicio" => $row->id,
                    "servico" => $row->servicioNombre,
                    "folio" => $row->folio_mostrar,
                    "hora_programada" => $row->hora,
                    "estatus" => $estatus,
                );
            }
            $contenedor["error"] = "0";
            $contenedor["mensaje"] = "";
        }else{
            $contenedor["error"] = "1";
            $contenedor["mensaje"] = "NO SE ENCONTRARÓN CITAS PARA ESTE DÍA.";
        }
        
        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    public function ver_servicio()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $id_servicio = $_POST["id_servicio"];
        
        $contenedor = [];

        $info_servicio = $this->consulta->datos_servicio($id_servicio);

        if ($info_servicio != NULL) {
            foreach ($info_servicio as $row) {
                if ($row->cancelado == "1") {
                    $estatus = "Cancelado";
                } elseif ($row->reagendado == "1") {
                    $estatus = "Reagendado";
                }else {
                    $estatus = $row->estatus;
                }
                
                $contenedor["id_servicio"] = $id_servicio;
                $contenedor["servicio"] = $row->servicioNombre;
                $contenedor["folio"] = $row->folio_mostrar;
                $contenedor["metodo_pago"] = $row->metodo_pago;
                $contenedor["fecha"] = $row->fecha;
                $contenedor["hora"] = $row->hora;
                $contenedor["estatus"] = $estatus;
                $contenedor["cliente_domicilio"] = $row->calle. " int. " . $row->numero_int . " ext. " . $row->numero_ext . " Col. " . $row->colonia . " " . $row->municipio . " " . $row->estado;
                $contenedor["cliente_email"] = $row->email;
                $contenedor["cliente_telefono"] = $row->telefono;
                $contenedor["cliente_latitud"] = $row->latitud;
                $contenedor["cliente_longitud"] = $row->longitud;
                $contenedor["unidad_color"] = $row->color;
                $contenedor["unidad_marca"] = $row->marca;
                $contenedor["unidad_modelo"] = $row->modelo;
                $contenedor["unidad_tipo_auto"] = $row->tipo_auto;
                $contenedor["unidad_placas"] = $row->placas;
                $contenedor["unidad_numero_serie"] = $row->numero_serie;
                $contenedor["unidad_kilometraje"] = $row->kilometraje;
            }

            $avance_proceso = $this->consulta->pasos_proceso($id_servicio);

            if ($avance_proceso != NULL) {
                //$proceso = $avance_proceso[0];
                $contenedor["unidad_inventario"] = $avance_proceso[0]->inventario;
            }

            $contenedor["error"] = "0";
            $contenedor["mensaje"] = "";
        }else{
            $contenedor["error"] = "1";
            $contenedor["mensaje"] = "NO SE ENCONTRÓ EL SERVICIO.";
        }
        
        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    public function seguimineto_ruta()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $id_servicio = $_POST["id_servicio"];
        $id_lavador = $_POST["id_lavador"];
        $paso = $_POST["paso"];
        
        $contenedor = [];

        //Si se envia el inicio de ruta
        if ($paso == "1") {
            $avance_proceso = $this->consulta->pasos_proceso($id_servicio);

            if ($avance_proceso != NULL) {
                $proceso = $avance_proceso[0];
            }

            if (!isset($proceso->inicia_ruta)) {
                //Verificamos si el servicio esta pagado
                $tipo_pago = $this->consulta->tipo_pago($id_servicio);

                $validar_seguro = $this->validar_seguro_lluvia($id_servicio);            

                //Si el servicio ya esta pagado y no es pago por terminal
                $pago = 0;
                $seguro_lluvia = 0;
                if (($tipo_pago[0] != NULL)&&(($tipo_pago[0] != '2')||($validar_seguro === TRUE))) {
                    $pago = 1;
                    if ($validar_seguro === TRUE) {
                        $seguro_lluvia = 1;
                    }
                }

                $proceso = array(
                    "id" => NULL,
                    "id_servicio" => $id_servicio,
                    "id_lavador" => $id_lavador,
                    "inicia_ruta" => 1,
                    "pago" => $pago,
                    "evidencia_pago" => $pago,
                    "seguro_lluvia" => $seguro_lluvia,
                    "fecha_alta" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->save_register('proceso_servicio_lavado',$proceso);

                $this->guardar_movimiento($id_servicio,"Se inicia la ruta  (app)");

                //Cambiamos el estatus
                $this->cambio_status('2',$id_servicio);

                $contenedor["error"] = "0";
                $contenedor["mensaje"] = "SE INICIA LA RUTA DEL LAVADOR";

            } else {
                $contenedor["error"] = "0";
                $contenedor["mensaje"] = "RUTA INICIADA PREVIAMENTE";
            }
        } elseif ($paso == "2") {
            $proceso = array(
                "pausar_ruta" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($id_servicio,"Ruta pausada (app)");

            $contenedor["error"] = "0";
            $contenedor["mensaje"] = "RUTA PAUSADA";

        } elseif ($paso == "3") {
             $proceso = array(
                "reanudar_ruta" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($id_servicio,"Ruta Reanudada (app)");

            $contenedor["error"] = "0";
            $contenedor["mensaje"] = "RUTA REANUDADA";

        } elseif ($paso == "4") {
            $proceso = array(
                "termina_ruta" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($id_servicio,"Finaliza la ruta / Se llego al sitio (app)");

            //Cambiamos el estatus
            $this->cambio_status('3',$id_servicio);

            $contenedor["error"] = "0";
            $contenedor["mensaje"] = "RUTA FINALIZADA";
        }else{
            $contenedor["error"] = "1";
            $contenedor["mensaje"] = "NO SE INDICO EL TIPO DE PROCESO";
        }
        
        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    public function grabar_posicion()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $id_servicio = $_POST["id_servicio"];
        $id_lavador = $_POST["id_lavador"];
        $lat_3 = $_POST["latitud"];
        $lng_3 = $_POST["longitud"];
        
        $contenedor = [];

        //Gradamos la coordenada
        $proceso = array(
            "id" => NULL,
            "id_lavador" => $id_lavador,
            "latitud" => $lat_3,
            "longitud" => $lng_3,
            "id_servicio" => $id_servicio,
            "fecha_creacion" => date("Y-m-d H:i:s")
        );

        $add_punto = $this->consulta->save_register('coordenadas_lavador',$proceso);

        if ($add_punto != 0) {
            $contenedor["error"] = "0";
            $contenedor["mensaje"] = "OK";
        } else {
            $contenedor["error"] = "1";
            $contenedor["mensaje"] = "NO SE PUDO GUARDAR LA POSICIÓN";
        }
        
        //Actualizamos la coordenada del lavador
        $posicion_lavador = array(
            "latitud_lavador" => $lat_3,
            "longitud_lavador" => $lng_3,
        );

        $this->consulta->update_table_row('servicio_lavado',$posicion_lavador,'id',$id_servicio);

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    public function cancelar_servicio()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $id_servicio = $_POST["id_servicio"];
        $id_lavador = $_POST["id_lavador"];
        $motivo = $_POST["motivos"];
        
        $contenedor = [];

        //Actualizamos en la tabla de servicio
        $cancelado = array(
            "cancelado" => 1
        );

        $this->consulta->update_table_row('servicio_lavado',$cancelado,'id',$id_servicio);

        //Marcamos el paso
        $id_paso = $this->consulta->id_paso_seguimiento($id_servicio,$id_lavador);

        $proceso = array(
            "servicio_cancelado" => 1,
            "fecha_actualiza" => date("Y-m-d H:i:s")
        );

        $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

        $this->guardar_movimiento($id_servicio,"Servicio cancelado (app): ".(($motivo != "") ? $motivo : "Sin especificar"));

        if ($actualizar) {
            $contenedor["error"] = "0";
            $contenedor["mensaje"] = "OK";
        } else {
            $contenedor["error"] = "1";
            $contenedor["mensaje"] = "NO SE PUDO ACTUALIZAR EL PROCESO DE CANCELADO";
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    public function guardar_evidencia()
    {
        $lavador = $_POST["id_lavador"];

        if(count($_FILES['archivo']['tmp_name']) > 0){
            //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
            for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
                $_FILES['tempFile']['name'] = $_FILES['archivo']['name'][$i];
                $_FILES['tempFile']['type'] = $_FILES['archivo']['type'][$i];
                $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'][$i];
                $_FILES['tempFile']['error'] = $_FILES['archivo']['error'][$i];
                $_FILES['tempFile']['size'] = $_FILES['archivo']['size'][$i];

                //Url donde se guardara la imagen
                $urlDoc = "videos";
                //Generamos un nombre random
                $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $urlNombre = date("YmdHi")."_";
                for($j=0; $j<=7; $j++ ){
                    $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
                }

                //Validamos que exista la carpeta destino   base_url()
                if(!file_exists($urlDoc)){
                    mkdir($urlDoc, 0647, true);
                }

                //Configuramos las propiedades permitidas para la imagen
                $config['upload_path'] = $urlDoc;
                $config['file_name'] = $urlNombre;
                $config['allowed_types'] = "*";

                //Cargamos la libreria y la inicializamos
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('tempFile')) {
                    $data['uploadError'] = $this->upload->display_errors();
                    // echo $this->upload->display_errors();
                    //$respuesta = "NO SE PUDIERON CARGAR TODOS LOS ARCHIVOS";
                    $respuesta = "";
                }else{
                    //Si se pudo cargar la imagen la guardamos
                    $fileData = $this->upload->data();
                    $ext = explode(".",$_FILES['tempFile']['name']);
                    $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];

                    $evidencia = array(
                        "id" => NULL,
                        "id_servicio" => $_POST["servicio"],
                        "id_lavador" => $lavador,
                        "repositorio" => "2",
                        "titulo" => ((isset($_POST["tt_evidencia"])) ? $_POST["tt_evidencia"] : "Evidencia Lavado"),
                        "comentarios" => $_POST["comentarios"],
                        "url" => $path,
                        "activo" => 1,
                        "fecha_alta" => date("Y-m-d H:i:s")
                    );

                    $servicio = $this->consulta->save_register('evidencia_proceso_servicio',$evidencia);

                    $respuesta = "OK";
                }
            }
        }else{
            $respuesta = "SIN ARCHIVOS";
        }

        if (($respuesta == "OK")&&($_POST["paso_evidencia"] == "1")) {
            $id_paso = $this->consulta->id_paso_seguimiento($_POST["servicio"],$lavador);

            $proceso = array(
                "evidencia_inicio" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            $this->guardar_movimiento($_POST["servicio"],"Evidencia: ".$_POST["tt_evidencia"]);
        }
    }



//--------------------------------------------------------------------------------------------------------

    public function guardar_movimiento($id_servicio='',$movimiento = '')
    {
        //Recuperamos los datos del servicio
        $datos_servicio = $this->consulta->datos_base_servicio($id_servicio);

        if (count($datos_servicio)>0) {
            $contenedor = array(
                "id" => NULL,
                "id_servicio" => $id_servicio,
                "id_lavador" => $datos_servicio[1],
                "id_cliente" => $datos_servicio[0],
                "movimento" => $movimiento,
                "tipo_usuario" => "Lavador app",
                "fecha_alta" => date("Y-m-d H:i:s")
            );

            $this->consulta->save_register('historial_procesos_servicio',$contenedor);
        }

        return TRUE;
    }

    public function validar_seguro_lluvia($id_servicio = '')
    {
        $data = json_decode(file_get_contents('php://input'), true);
        // $curl is the handle of the resource
        $curl = curl_init();
        // set the URL and other options
        curl_setopt($curl, CURLOPT_URL, "https://xehos.com/xehos_web/api/segurolluvia?servicio_id=".$id_servicio);
        //set the content type to application/json
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //return response instead of outputting
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // execute and pass the result to browser
        $server_output = curl_exec($curl);
        // close the cURL resource
        curl_close($curl);
        //var_dump($server_output );
        $respuesta = json_decode($server_output);
        //var_dump($respuesta->seguro_lluvia);
        return ((isset($respuesta->seguro_lluvia)) ? $respuesta->seguro_lluvia : FALSE);
    }

    public function sms_general_texto($celular = '', $mensaje = '')
    {
        $sucursal = "XEHON";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje_xehos");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "celular=" . $celular . "&mensaje=" . $mensaje . "&sucursal=" . $sucursal . ""
        );
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close($ch);
    }

    public function cambio_status($status = '',$id_servicio = '')
    {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://hexadms.com/xehos/index.php/api/cambiarStatus");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "id_status_actual=" . ((int)$status - 1) . 
            "&usuario=" . $this->session->userdata('usuario') . 
            "&id_status_nuevo=" . $status .
            "&id_servicio=" . $id_servicio . ""
        );
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close($ch);
    }


}