
<div class="modal fade" id="modalautorizacion" tabindex="-1" role="dialog" aria-labelledby="modalautorizacion" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_proveedor">Autorizar</h5>
            </div>
            <div class="modal-body">
                <form id="frmautorizar" class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "usuario", "Usuario", ''); ?>
                        <input type="hidden" name="ma_pedido_id" id="ma_pedido_id">
                    </div>
                    <div class="col-md-12">
                        <?php echo renderInputText("password", "password", "Contraseña", ''); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="aturizar" onclick="validarAutorizar()" type="button" class="btn btn-primary">
                    <i class="fas fa-key"></i> Autorizar
                </button>
            </div>
        </div>
    </div>
</div>