@layout('layout')
@section('contenido')
    <div class="conteiner">
        <div class="row">
        
        <form action="" method="post" novalidate="novalidate" id="alta_usuario">
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                        <input id="sucursal" required name="sucursal" type="text"
                            class="form-control cc-exp form-control-sm" value="" placeholder="Sucursal">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Nomenclatura</label>
                        <input id="nomenclatura" required name="nomenclatura" type="text"
                            class="form-control cc-exp form-control-sm" value="" placeholder="nomenclatura">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1"> Estado</label>
                        <select name="id_estado" id="id_estado" class="form-control">
                            <option value="">-- Selecciona  --</option>
                            @foreach ($estado as $s => $estados)
                                <option value="{{ $estados->id }}">{{ $estados->estado }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-12">
                    <div align="right">
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-edit fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Guardar</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
        
         </div>
        
    </div>

        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">


                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sucursal</th>
                                        <th>Nomenclatura</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (is_array($rows)): ?>
                                    <?php foreach ($rows as $row): ?>
                                    <tr>
                                        <td><?php echo $row->sucursal; ?></td>
                                        <td><?php echo $row->nomenclatura; ?></td>
                                        <td><?php echo nombre_estado($row->id_estado); ?></td>
                                        <td>
                                          <a href="<?php echo base_url(); ?>index.php/sucursales/editar/<?php echo $row->id; ?>">
                                            <button type="button" class="btn btn-info">Editar</button>
                                          </a>
                                          <a href="<?php echo base_url()?>index.php/zonas/alta/<?php echo $row->id; ?>">
                                                <button type="button" class="btn btn-warning " id="">Zona</button>
                                             </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script>
        var site_url = "{{site_url()}}";
        $(document).ready(function() {
            $("#cargando").hide();
            $('#alta_usuario').submit(function(event) {
                event.preventDefault();
                $("#enviar").hide();
                $("#cargando").show();
                var url_sis =
                    "<?php echo base_url(); ?>index.php/sucursales/alta_guardar";
                // Get form
                var form = $('#alta_usuario')[0];
                // Create an FormData object
                var data = new FormData(form);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url_sis,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        //  $("#result").text(data);
                        console.log("SUCCESS : ", data);
                        //  $("#btnSubmit").prop("disabled", false);
                        exito_redirect("DATOS GUARDADOS CON EXITO", "success",
                            "<?php echo base_url(); ?>index.php/sucursales/alta"
                        );
                        $("#enviar").show();
                        $("#cargando").hide();
                    },
                    error: function(e) {
                        console.log("ERROR : ", e);
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                        $("#enviar").show();
                        $("#cargando").hide();

                    }
                });
            });

        });

    </script>
@endsection
