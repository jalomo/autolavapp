$("#btnregistro").on('click', function (e){
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#cargaIcono").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        var base = $("#sitio").val();
        var paqueteDatos = new FormData(document.getElementById('registro'));
        $.ajax({
            url: base+"recomendadores/login/registro_recomendador",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        location.href = base+"index.php/recomendadores/login/index";
                    } else {
                        $("#formulario_error").text(resp);
                    }

                }else{
                    $("#formulario_error").text("ERROR");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
    }
});

function validar() {
    var retorno = true;
    var campo_a = $("input[name='nombre']").val();
    if (campo_a == "") {
        var error_a = $("#nombre_error");
        error_a.empty();
        $("#nombre_error").css("display","inline-block");
        $("#nombre_error").css("background-color","antiquewhite");
        error_a.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_a = $("#nombre_error");
        error_a.empty();
    }

    var campo_b = $("input[name='celular']").val();
    if (campo_b.length < 10) {
        var error_b = $("#celular_error");
        error_b.empty();
        $("#celular_error").css("display","inline-block");
        $("#celular_error").css("background-color","antiquewhite");
        error_b.append('<label class="form-text text-danger">Campo incompleto</label>');
        retorno = false;
    }else{
        var error_b = $("#celular_error");
        error_b.empty();
    }

    var correo_2 = $("input[name='email']").val();
    if (!validar_email(correo_2)) {
        var error_2 = $("#email_error");
        $("#email_error").css("display","inline-block");
        $("#email_error").css("background-color","antiquewhite");
        error_2.append('<label class="form-text text-danger">Formato invalido.</label>');
        retorno = false;
    }else{
        var error_2 = $("#email_error");
        error_2.empty();
    }

    var campo_c = $("input[name='nUsuario']").val();
    if (campo_c == "") {
        var error_c = $("#nUsuario_error");
        error_c.empty();
        $("#nUsuario_error").css("display","inline-block");
        $("#nUsuario_error").css("background-color","antiquewhite");
        error_c.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_c = $("#nUsuario_error");
        error_c.empty();
    }

    var campo_d = $("input[name='nPass']").val();
    if (campo_d == "") {
        var error_d = $("#nPass_error");
        error_d.empty();
        $("#nPass_error").css("display","inline-block");
        $("#nPass_error").css("background-color","antiquewhite");
        error_d.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_d = $("#nPass_error");
        error_d.empty();
    }

    var campo_edo = $("#estados").val();
    if (campo_edo == "") {
        var error_edo = $("#estados_error");
        error_edo.empty();
        $("#estados_error").css("display","inline-block");
        $("#estados_error").css("background-color","antiquewhite");
        error_edo.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_edo = $("#estados_error");
        error_edo.empty();
    }

    var campo_scu = $("#sucursal").val();
    if (campo_scu == "") {
        var error_scu = $("#sucursal_error");
        error_scu.empty();
        $("#sucursal_error").css("display","inline-block");
        $("#sucursal_error").css("background-color","antiquewhite");
        error_scu.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_scu = $("#sucursal_error");
        error_scu.empty();
    }

    return retorno;
}

function validar_email( email ) 
{
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

function validar_uso() {
    var base = $("#sitio").val();
    var url_sis = base+"recomendadores/login/usuario_disponible";
    var nombre_usuario = $("input[name='nUsuario']").val();
    $.ajax({
        url: url_sis,
        method: 'post',
        data: {
            usuario : nombre_usuario
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler           </p>")<1) {
                var error_c = $("#nUsuario_error");
                error_c.empty();

                if (resp == "OK") {
                    $("#nUsuario_error").css("display","inline-block");
                    $("#nUsuario_error").css("background-color","aliceblue");
                    error_c.append('<label class="form-text text-success">NOMBRE DE USUARIO DISPONIBLE</label>');
                    $("#btnregistro").attr('disabled',false);
                } else {
                    $("#nUsuario_error").css("display","inline-block");
                    $("#nUsuario_error").css("background-color","antiquewhite");
                    error_c.append('<label class="form-text text-danger">'+resp+'</label>');
                    $("#btnregistro").attr('disabled',true);
                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

function validar_telefono() {
    var base = $("#sitio").val();
    var url_sis = base+"recomendadores/login/telefono_disponible";
    var telefono = $("input[name='celular']").val();
    if (telefono.length == 10) {
        $.ajax({
            url: url_sis,
            method: 'post',
            data: {
                telefono : telefono
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    var error_c = $("#celular_error");
                    error_c.empty();

                    if (resp == "OK") {
                        $("#celular_error").css("display","inline-block");
                        $("#celular_error").css("background-color","aliceblue");
                        error_c.append('<label class="form-text text-success">TELÉFONO DISPONIBLE</label>');
                        $("#btnregistro").attr('disabled',false);
                    } else {
                        $("#celular_error").css("display","inline-block");
                        $("#celular_error").css("background-color","antiquewhite");
                        error_c.append('<label class="form-text text-danger">'+resp+'</label>');
                        $("#btnregistro").attr('disabled',true);
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else{
        var error_c = $("#celular_error");
        error_c.empty();

        $("#celular_error").css("display","inline-block");
        $("#celular_error").css("background-color","antiquewhite");
        error_c.append('<label class="form-text text-danger">TELÉFONO INVALIDO</label>');
        $("#btnregistro").attr('disabled',true);
    }
}

$('#estados').on('change', function() {
    var estado = $("#estados").val();
    if (estado != "") {
        var base = $("#sitio").val();
        var url_sis = base+"recomendadores/login/recuperar_sucursal";

        $.ajax({
            url: url_sis,
            method: 'post',
            data: {
                estado : estado
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    var registros = resp.split("|");
                    var select = $("#sucursal");
                        select.empty();

                    if (registros.length > 0) {
                        
                        select.append("<option value=''> -- Seleccionar --</option>");
                        var datos = null;

                        for (var i = 0; i < registros.length; i++) {
                            datos = registros[i].split("=");
                            if (datos.length > 1) {
                                select.append("<option value='"+datos[0]+"'>"+datos[1]+"</option>");
                            }
                        }
                    } else {
                        select.append("<option value=''> -- Sin resultados --</option>");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }   
});