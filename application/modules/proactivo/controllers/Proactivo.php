<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proactivo extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_proactivo', 'mp', TRUE);
    $this->load->helper(array('general', 'correo'));
    $this->load->library(array('session', 'email'));
    date_default_timezone_set('America/Mexico_City');
    if (!PermisoModulo('proactivo')) {
      redirect(site_url('login'));
    }
  }
  //MAGIC PARA GUARDAR COMENTARIOS
  public function comentarios()
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id') == '') {
      redirect('login');
    }
    if ($this->input->post()) {
      if (isset($_POST['nunca_contactar'])) {
        $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
      } else {
        $this->form_validation->set_rules('comentario', 'comentario', 'trim');
      }
      $this->form_validation->set_rules('cronoFecha', 'fecha', 'trim|required');


      if ($this->form_validation->run()) {
        $this->mp->saveComentario();
        exit();
      } else {
        $errors = array(
          'comentario' => form_error('comentario'),
          'cronoFecha' => form_error('cronoFecha'),
        );
        echo json_encode($errors);
        exit();
      }
    }
    $data['id'] = $this->input->get('id');
    $data['fecha'] = date2sql($this->input->get('fecha'));
    $data['cliente'] = $this->input->get('cliente');
    $data['input_comentario'] = form_textarea('comentario', "", 'class="form-control" id="comentario" rows="2"');

    $this->blade->render('comentarios', $data);
  }
  public function correo()
  {
    $cuerpo = $this->blade->render('correo_intentos', array(), true);
    enviar_correo(strtolower("albertopitava@gmail.com"), "Notificación Contacto Proactivo!", $cuerpo, array());
  }
  public function correo_cp()
  {
    $cuerpo = $this->blade->render('correo_contacto_proactivo', array(), true);
    //Guardar historial
    $data = [
      'id_servicio' => $_POST['id'],
      'created_at' => date('Y-m-d H:i:s'),
      'id_usuario' => $this->session->userdata('id')
    ];
    $this->db->insert('correos_enviados_cp', $data);
    enviar_correo(strtolower($_POST['email']), "Carta previa Contacto Proactivo!", $cuerpo, array());
    echo 1;
    exit();
  }
  public function correo_cp_bk()
  {
    $fecha_final = date2sql($this->input->post('fecha'));
    $newdate = strtotime('-10 days', strtotime($fecha_final));
    $fecha_inicial = date('Y-m-d', $newdate);
    $info = $this->mp->getDataByDate($fecha_inicial, $fecha_inicial, false);
    $array_info = array();
    foreach ($info as $key => $value) {
      $array_info[$value->numero_serie] = $value;
    }
    foreach ($array_info as $a => $data) {
      $cuerpo = $this->blade->render('correo_contacto_proactivo', array(), true);
      enviar_correo(strtolower($data->email), "Carta previa Contacto Proactivo!", $cuerpo, array());
    }
  }

  public function historial_proactivo()
  {
    $data['proactivo'] = $this->db->select('c.*,p.fecha_contacto,p.fecha_contacto,c.intentos,c.fecha_programacion')
      ->join('v_info_servicios c', 'p.id_servicio = c.id')
      ->group_by('c.id')
      ->get('proactivo_historial p')
      ->result();

    $this->blade->render('historial_proactivo', $data);
  }
  public function buscar_historial_proactivo()
  {
    $fecha_inicio = date2sql($this->input->post('finicio'));
    $fecha_fin = date2sql($this->input->post('ffin'));
    $this->db->where('date(p.created_at) >=', $fecha_inicio);
    $this->db->where('date(p.created_at) <=', $fecha_fin);
    $data['proactivo'] = $this->db->select('c.*,p.fecha_contacto,p.fecha_contacto,c.intentos,c.fecha_programacion')
      ->join('v_info_servicios c', 'p.id_servicio = c.id')
      ->group_by('c.id')
      ->get('proactivo_historial p')
      ->result();
    $this->blade->set_data($data)->render('tabla_historial');
  }
  public function index($busqueda = 0)
  {
    $this->blade->render('lista_proactivos');
  }
  public function getDatosProactivo()
  {
    $datos = $_POST;
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];
    if ($this->input->post('fecha') == '') {
      $fecha_final = date('Y-m-d');
    } else {
      $fecha_final = date2sql($this->input->post('fecha'));
    }

    $newdate = strtotime('-10 days', strtotime($fecha_final));
    $fecha_inicial = date('Y-m-d', $newdate);

    //obtener total
    $total = $this->mp->getDataByDate($fecha_inicial, $fecha_inicial, true);
    //Obtener todos los registros
    if (isset($_POST['buscar_campo'])) {
      $this->filtros($_POST['buscar_campo']);
    }
    $info = $this->mp->getDataByDate($fecha_inicial, $fecha_inicial, false);
    $array_info = array();
    foreach ($info as $key => $value) {
      $array_info[$value->numero_serie] = $value;
    }
    $data = array();
    foreach ($array_info as $key => $value) {
      $dat = array();
      $acciones = '';
      $acciones .= '<a href="servicios/generar_servicio/' . $value->id . '/1' . '" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Agendar cita"><i class="fa fa-plus"></i></a>';
      $acciones .= '<a href="" data-id="' . $value->id . '" class="js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios" data-fecha="' . $value->fecha_programacion . '" data-cliente="' . $value->nombre . ' ' . $value->apellido_paterno . ' ' . $value->apellido_materno . '"><i class="fa fa-comments"></i></a>';
      $acciones .= '<a href="" data-id="' . $value->id . '" class="js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"><i class="fa fa-info"></i></a>';
      $acciones .= '<a target="_blank" href="https://api.whatsapp.com/send?phone='.CONST_WS_CONTACTO.'&text=¡Estimado%20cliente!%20'.$value->nombre . " " . $value->apellido_paterno . " " . $value->apellido_materno .'%20¡XehosAutolavado!%20lo%20contactamos%20para%20recomendarle%20hacer%20su%20cita%20para%20el%20lavado%20de%20su%20auto"><i class="fa fa-whatsapp"></i>';
      $color_correo = '#1473A4';
      $clase_correo = 'js_enviado';
      if (!$this->mp->correo_enviado($value->id)) {
        $color_correo = '#878787';
        $clase_correo = 'js_correo';
      }
      $acciones .= '<a style="color:' . $color_correo . '" href="" data-id="' . $value->id . '" data-email="' . $value->email . '" class="' . $clase_correo . '" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar correo"><i class="fa fa-envelope"></i></a>';
      $dat[] = $acciones;
      $dat[] = $value->id;
      $dat[] = date_eng2esp_1($value->fecha_programacion);
      $dat[] = $value->intentos;
      $dat[] = $value->kilometraje;
      $dat[] = $value->placas;
      $dat[] = $value->nombre . ' ' . $value->apellido_paterno . ' ' . $value->apellido_materno;
      $dat[] = $value->email;
      $dat[] = $value->telefono;
      $dat[] = $value->marca;
      $dat[] = $value->modelo;
      $dat[] = $value->numero_serie;
      $dat[] = $value->servicioNombre;
      $data[] = $dat;
    }


    $retornar = array(
      'draw' => intval($mismo),
      'recordsTotal' => intval($total),
      'recordsFiltered' => intval($total),
      'data' => $data
    );
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function filtros($busqueda = '')
  {
    if ($busqueda != '') {
      $this->db->or_like('id', $busqueda);
      $this->db->or_like('intentos', $busqueda);
      $this->db->or_like('vehiculo_placas', $busqueda);
      $this->db->or_like('nombres', $busqueda);
      $this->db->or_like('apellido_paterno', $busqueda);
      $this->db->or_like('apellido_materno', $busqueda);
      $this->db->or_like('email', $busqueda);
      $this->db->or_like('telefono', $busqueda);
      $this->db->or_like('fecha_programacion', $busqueda);
      $this->db->or_like('marca', $busqueda);
      $this->db->or_like('modelo', $busqueda);
      $this->db->or_like('serie', $busqueda);
    }
  }
  public function activarN()
  {
    $this->mp->ProgramarNotificacion('Hola mundo','2020-12-23 12:00:00','Matriz','3123094368');
  }
  public function historial_comentarios()
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id') == '') {
      redirect('login');
    }
    $data['comentarios'] = $this->mp->getHistorialComentarios($this->input->get('id'));
    $this->blade->render('proactivo/historial_comentarios', $data);
  }
  public function probando()
  {
    $date_10 = strtotime('+10 day', strtotime(date('Y-m-d')));
    $date = date ( 'Y-m-d' , $date_10 );
    $dia = date("D", strtotime($date));
    
    //Si el día es sábado agregar un día, si es domingo 2
    if ($dia == 'Sun') {
      $date_act = strtotime('+1 day', strtotime('2020-12-18'));
      $fecha_confirmacion = date ( 'Y-m-d' , $date_act );
    }else{
      $fecha_confirmacion = '2020-12-18'; //;
    }
    echo $fecha_confirmacion;
  }
}
