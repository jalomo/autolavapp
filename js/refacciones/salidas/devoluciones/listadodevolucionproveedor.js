function inicializaTabla() {
	var tabla_devolucion = $('#tabla_devolucion_proveedor').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/orden-compra/busqueda-compras",
			type: 'GET',
		},
		columns: [
			{
				title: "No-Orden",
				data: 'id',
			},
			{
				title: "Folio",
				data: 'folio',
			},
			{
				title: "Proveedor",
				data: 'proveedor_nombre',
			},
			{
				title: "Tipo pago",
				data: 'tipoPago',
            },
			{
				title: "Estatus",
				data: 'estatusCompra',
			},
			{
				title: "Fecha de compra",
				render: function(data, type, row) {
                    return obtenerFechaMostrar(row.created_at)
				}
            },
            {
                title: '-',
                render: function(data, type, row) {
					var btnPieza = '';
					if (row.id_estatus_compra == 2){
						btnDevolucion = '<button title="Devolución orden compra" onclick="irDevolucion(this)" data-orden_compra_id="' + row.id + '"  data-estatus_compra_id="' + row.id_estatus_compra + '" class="btn btn-primary"><i class="fas fa-list"></i></button>';
						btnPieza = '<button title="Devolución por pieza" onclick="irDevolucionPieza(this)" data-orden_compra_id="' + row.id + '" class="btn btn-primary ml-2"><i class="fas fa-archive"></i></button>';
					} else {
						btnDevolucion = '<button title="Detalle devolución" onclick="irDevolucion(this)" data-orden_compra_id="' + row.id + '" data-estatus_compra_id="' + row.id_estatus_compra + '" class="btn btn-danger"><i class="fas fa-list"></i></button>';
					}
					return btnDevolucion + ' '+ btnPieza;
				}
            }
		]
	});
}

function irDevolucion(_this){
    setTimeout(() => {
        var orden_compra_id = $(_this).data('orden_compra_id');
        var estatus_compra_id = $(_this).data('estatus_compra_id');
        window.location.href = PATH + '/refacciones/salidas/detallesDevolucion?orden_compra_id=' + orden_compra_id + '&estatus_compra_id=' + estatus_compra_id
    }, 200);
}

function irDevolucionPieza(_this){
    setTimeout(() => {
        var orden_compra_id = $(_this).data('orden_compra_id');
        window.location.href = PATH + '/refacciones/salidas/devolverPieza?orden_compra_id=' + orden_compra_id 
    }, 200);
}

function obtenerFechaMostrar(fecha){
    const dia = 2, mes = 1, anio = 0;
    fecha = fecha.split(' ');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
		'proveedor_id': $("#proveedor_id option:selected").val(),
		'folio' : $("#folio").val()
    });

    $('#tabla_devolucion_proveedor').DataTable().ajax.url(PATH_API + 'api/orden-compra/busqueda-compras?' + params).load()
}

function limpiarFiltro() {
	$("#folio").val('');
	$("#proveedor_id").val('');
	$("#proveedor_id").trigger('change');
    var params = $.param({
        'folio': '',
        'proveedor_id': '',
    });

    $('#tabla_devolucion_proveedor').DataTable().ajax.url(PATH_API + 'api/orden-compra/busqueda-compras?' + params).load()
}

this.inicializaTabla();
$("#proveedor_id").select2();

