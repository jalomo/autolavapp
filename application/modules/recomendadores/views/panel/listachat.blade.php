@layout('recomendadores/layout')

@section('included_css')
@section('contenido')

<div class="container-fluid panel-body">
	<h1 class="mt-4">Usuarios del sistema</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item ">{{ ucwords($titulo) }}</li>
		<li class="breadcrumb-item">{{ ($titulo_dos) }}</li>
	</ol>
	@if(isset($datos) && $datos['celular'])

	<div class="row">
		<div class="col-md-3">
			<h2>Mis datos</h2>
			{{ $datos['nombre']}} {{ $datos['apellido_paterno']}} {{ $datos['apellido_materno']}} <br/>
			{{ $datos['celular'] }} <br/>
			{{ $datos['correo']}}
			<input type="hidden" value="{{ $datos['celular']}}" id="telefono_cliente" />
		</div>
		<div class="col-md-9">
			<div class="row">
				<?php
				if (isset($usuarios) && $usuarios) {
					foreach ($usuarios as $usuario) {  ?>
						<div class="col-md-4 mt-3">
							<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:160px">
								<div style="padding:10px" class="">
									<i class="fa fa-user-circle fa-3x"></i>
									<h4>{{ ucwords($usuario->adminNombre) }}</h4>
									<p>
										<b> {{ $usuario->cargo }}</b>
									</p>
									<button class="btn btn-default btn-lg" style="background-color:#fff; color:#323232; width:100%;" title="Comenzar chat"  onclick="gotoChat(this)" data-telefono="{{ $usuario->telefono }}"><i style="color:#22ae22" class="fa fa-comment fa-2x"></i> <span style="font-size:16px">Comenzar chat</span></button>
								</div>
							</div>
						</div>
				<?php
					}
				} ?>
			</div>
		</div>
	</div>
	@else
	<h3 class="mt-5 mb-5 text-center">El usuario no esta registrado en el sistema</h3>
	@endif
</div>
@endsection
<script type="text/javascript">
	var site_url = "{{site_url()}}";

	function gotoChat(_this) {
		window.location.href = site_url + '/recomendadores/panel/chat?telefono_cliente=' + document.getElementById("telefono_cliente").value + '&telefono_destinatario=' +  $(_this).data('telefono')
	}
</script>