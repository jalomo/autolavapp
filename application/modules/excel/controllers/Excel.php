<?php
//https://techarise.com/import-excel-file-mysql-codeigniter/
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . "/third_party/PHPExcel.php";
class Excel extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_excel', 'mexcel');
        $this->load->model('estadisticas/M_estadisticas', 'me', TRUE);
        $this->load->library('excel');
    }
    public function index()
    {
        $data['page'] = 'import';
        $data['title'] = 'Import XLSX | TechArise';
        $this->blade->render('index', $data);
    }
    // create xlsx
    public function dinero_servicios()
    {
        // create file name
        $fileName = 'dinero_servicios-' . time() . '.xlsx';
        // load excel library
        $this->load->library('excel');
        $info = $this->me->getDataServicios();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Fecha Servicio');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Lavador');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Servicio');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Cliente');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Placas');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Modelo');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Marca');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Precio');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Sucursal');
        // set Row
        $rowCount = 2;
        foreach ($info as $list) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount,$list->fecha_creacion_servicio);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,$list->lavadorNombre);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount,$list->servicioNombre);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount,$list->nombre . ' ' . $list->apellido_paterno . ' ' . $list->apellido_materno);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount,$list->placas);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount,$list->modelo);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount,$list->marca);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount,number_format($list->precio, 2));
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount,$list->sucursal);
            $rowCount++;
        }

        $filename = "dinero_servicios_" . date("Y-m-d-H-i-s") . ".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');
    }
    public function cantidad_servicios()
    {
        // load excel library
        $this->load->library('excel');
        $info = $this->me->getDataServicios();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Fecha Servicio');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Lavador');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Servicio');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Cliente');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Placas');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Modelo');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Marca');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Precio');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Origen');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Confirmada');
        // set Row
        $rowCount = 2;
        foreach ($info as $list) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->fecha_creacion_servicio);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->lavadorNombre);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->servicioNombre);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->nombre . ' ' . $list->apellido_paterno . ' ' . $list->apellido_materno);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->placas);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->modelo);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->marca);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($list->precio, 2));
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $list->origen);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, ($list->confirmado)?'Si':'No');
            $rowCount++;
        }

        $filename = "cantidad_servicios_" . date("Y-m-d-H-i-s") . ".csv";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');
    }
    function random($num)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        for ($i = 0; $i < $num; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }
    public function unidades_nuevas()
    {
        $data['page'] = 'import';
        $data['title'] = 'Import XLSX | TechArise';
        $this->blade->render('unidades_nuevas', $data);
    }
    // import excel data
    public function save_unidades_nuevas()
    {
        if ($this->input->post('importfile')) {
            $path = './upload_excel/';
            $random = $this->random(10);
            $fetchData = array();
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|jpg|png';
            $config['remove_spaces'] = TRUE;
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $error = array('error' => $this->upload->display_errors());
                $path_delete = '';
            } else {
                $data = array('upload_data' => $this->upload->data());
                $path_delete = $data['upload_data']['file_path'] . $data['upload_data']['file_name'];
            }

            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                    . '": ' . $e->getMessage());
            }
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('serie', 'fechafac', 'cliente', 'contacto', 'direccion', 'telefono', 'telefono2', 'telefono3', 'correo', 'vendedor', 'tipovta', 'economico', 'descripcion', 'rfc', 'poblacion', 'placas');

            $makeArray = array('serie' => 'serie', 'fechafac' => 'fechafac', 'cliente' => 'cliente', 'contacto' => 'contacto', 'direccion' => 'direccion', 'telefono' => 'telefono', 'telefono2' => 'telefono2', 'telefono3' => 'telefono3', 'correo' => 'correo', 'vendedor' => 'vendedor', 'tipovta' => 'tipovta', 'economico' => 'economico', 'descripcion' => 'descripcion', 'rfc' => 'rfc', 'poblacion' => 'poblacion', 'placas' => 'placas');

            $SheetDataKey = array();
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    } else {
                    }
                }
            }
            $data = array_diff_key($makeArray, $SheetDataKey);

            if (empty($data)) {
                $flag = 1;
            }
            $flag = 1;
            //$this->db->delete('asc_campanias');
            if ($flag == 1) {
                for ($i = 2; $i <= $arrayCount; $i++) {

                    $serie = $SheetDataKey['serie'];
                    $fechafac = $SheetDataKey['fechafac'];
                    $cliente = $SheetDataKey['cliente'];
                    $contacto = $SheetDataKey['contacto'];
                    $direccion = $SheetDataKey['direccion'];
                    $telefono = $SheetDataKey['telefono'];
                    $telefono2 = $SheetDataKey['telefono2'];
                    $telefono3 = $SheetDataKey['telefono3'];
                    $correo = $SheetDataKey['correo'];
                    $vendedor = $SheetDataKey['vendedor'];
                    $tipovta = $SheetDataKey['tipovta'];
                    $economico = $SheetDataKey['economico'];
                    $descripcion = $SheetDataKey['descripcion'];
                    $rfc = $SheetDataKey['rfc'];
                    $poblacion = $SheetDataKey['poblacion'];
                    $placas = $SheetDataKey['placas'];


                    $serie = filter_var(trim($allDataInSheet[$i][$serie]), FILTER_SANITIZE_STRING);
                    $fechafac = filter_var(trim($allDataInSheet[$i][$fechafac]), FILTER_SANITIZE_STRING);
                    $cliente = filter_var(trim($allDataInSheet[$i][$cliente]), FILTER_SANITIZE_STRING);
                    $contacto = filter_var(trim($allDataInSheet[$i][$contacto]), FILTER_SANITIZE_STRING);
                    $direccion = filter_var(trim($allDataInSheet[$i][$direccion]), FILTER_SANITIZE_STRING);
                    $telefono = filter_var(trim($allDataInSheet[$i][$telefono]), FILTER_SANITIZE_STRING);
                    $telefono2 = filter_var(trim($allDataInSheet[$i][$telefono2]), FILTER_SANITIZE_STRING);
                    $telefono3 = filter_var(trim($allDataInSheet[$i][$telefono3]), FILTER_SANITIZE_STRING);
                    $correo = filter_var(trim($allDataInSheet[$i][$correo]), FILTER_SANITIZE_STRING);
                    $vendedor = filter_var(trim($allDataInSheet[$i][$vendedor]), FILTER_SANITIZE_STRING);
                    $tipovta = filter_var(trim($allDataInSheet[$i][$tipovta]), FILTER_SANITIZE_STRING);
                    $economico = filter_var(trim($allDataInSheet[$i][$economico]), FILTER_SANITIZE_STRING);
                    $descripcion = filter_var(trim($allDataInSheet[$i][$descripcion]), FILTER_SANITIZE_STRING);
                    $rfc = filter_var(trim($allDataInSheet[$i][$rfc]), FILTER_SANITIZE_STRING);
                    $poblacion = filter_var(trim($allDataInSheet[$i][$poblacion]), FILTER_SANITIZE_STRING);
                    $placas = filter_var(trim($allDataInSheet[$i][$placas]), FILTER_SANITIZE_STRING);

                    $fetchData[] = array('serie' => $serie, 'fechafac' => $fechafac, 'cliente' => $cliente, 'contacto' => $contacto, 'direccion' => $direccion, 'telefono' => $telefono, 'telefono2' => $telefono2, 'telefono3' => $telefono3, 'correo' => $correo, 'vendedor' => $vendedor, 'tipovta' => $tipovta, 'economico' => $economico, 'descripcion' => $descripcion, 'rfc' => $rfc, 'poblacion' => $poblacion, 'placas' => $placas, 'random' => $random);
                }
                $data['unidades'] = $fetchData;
                $this->mexcel->setBatchImport($fetchData);
                $this->mexcel->importDataUnidades($path_delete);
            } else {
                echo "Please import correct fil";
            }
        }
        $this->blade->render('display_unidades', $data);
    }
}
