<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Ciudades extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');

         if($this->session->userdata('id')){}else{redirect('login/');}

    }

    public function alta(){
      $data['titulo'] = "Estado" ;
      $data['titulo_dos'] = "Alta Estado" ;
      $data['rows'] =  $this->db->get('estados')->result();//$this->db->join('sucursales s','s.id=e.id_sucursal')->select('e.*,s.sucursal')->get('estados e')->result();
      //$data['sucursales'] = $this->db->get('sucursales')->result();
      $this->blade->render('ciudades/alta', $data);
    }

    public function alta_guardar(){
      $data['estado'] = $this->input->post('estado');
      $data['id_sucursal'] = 0;// $this->input->post('id_sucursal');
     $this->Mgeneral->save_register('estados', $data);
    }

   



}
