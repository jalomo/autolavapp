@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-10"></div>
        <div class="col-md-2">
        <input type="hidden" name="user_id"  id="user_id" value="{{ !empty($this->session->userdata('id')) ? $this->session->userdata('id') : '' }}">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="tabla-facturas">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Folio</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Total</th>
                    <th scope="col">Subtotal</th>
                    <th scope="col">Estatus</th>
                    <th scope="col">detalles</th>
                    <th scope="col">Factura</th>
                    <th>Cancelar</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                            <tr>
                                <td scope="row">{{ $item->id }}</td>
                                <td> {{ $item->folio }}</td>
                                <td> {{ obtenerFechaEnLetra($item->fecha) }}</td>
                                <td>$ {{ $item->total }}</td>
                                <td>{{ $item->subtotal }}</td>
                                <td>{{ $item->estatus_factura }}</td>t
                                <td><a href="{{ base_url('refacciones/factura/vistafactura/'.$item->id) }}" class="btn btn-primary" type="button">Ver</a></td>
                                <td>
                                    <a target="_blank" href="{{ API_URL_DEV.'api/facturas/descargar/'.$item->file_name }}" class="btn btn-success" type="button">
                                        <i class="fa fa-list" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <td>
                                    @if ($item->estatus_id == 2)
                                        - -
                                    @else
                                    <button data-facturaid={{ $item->id }} class="btn btn-danger btn_cancelar" id="">
                                        <i class="fa fa-trash" aria-hidden="true"></i> Cancelar
                                    </button>
                                    @endif
                                </td>
                            </tr>
                    @endforeach
                        
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection

@section('included_js')
@include('main/scripts_dt')
<script type="text/javascript">
$("#tabla-facturas").DataTable({});

    $(".btn_cancelar").on("click", function(){
        $(this).attr('disabled',true);
        var factura_id = $(this).data('facturaid');
        var user_id = $("#user_id").val();
        utils.displayWarningDialog("Desea cancelar la factura??", "warning", function(data) {
            if (data.value) {
                ajax.post(`api/orden-compra/cancelar-factura`,{
                    factura_id,
                    user_id
                }, function(response, headers) {
                    if(headers.status == 400){
                        return utils.displayWarningDialog("Esta factura ya cuenta con ventas!!", "warning", function(data) {}); 
                    }

                    if(headers.status == 200){
                        return utils.displayWarningDialog("Factura cancelada", "warning", function(data) {
                            location.reload(true);
                        }); 
                    }
                })

            }
        }, true)
    });

</script>

@endsection