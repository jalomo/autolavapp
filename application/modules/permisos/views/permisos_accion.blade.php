@layout('layout')
<style>
</style>
@section('contenido')

<form action="" id="frm">
  <div class="container1">
    <h2>Permisos de usuarios por acción</h2>

    <table class="table table-fixed table-hover">
      <thead>
        <tr>
          <th>Usuario</th>
          <th>Agendar servicio</th>
          <th>Editar servicio</th>
          <th>Eliminar servicio</th>
          <th>Cancelar servicio</th>
          <th>Editar fecha servicio</th>
          <th>Editar usuario</th>
        </tr>
      </thead>
      <tbody>
        @foreach($usuarios as $u => $usuario)
        <tr>
          <td>{{$usuario->adminId.'-'.$usuario->adminNombre}}</td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'agendar_servicio'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="agendar_servicio[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="agendar_servicio[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'editar_servicio'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="editar_servicio[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="editar_servicio[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'eliminar_servicio'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="eliminar_servicio[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="eliminar_servicio[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'cancelar_servicio'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="cancelar_servicio[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="cancelar_servicio[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'editar_fecha_servicio'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="editar_fecha_servicio[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="editar_fecha_servicio[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'editar_usuario'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="editar_usuario[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="editar_usuario[{{$usuario->adminId}}]">
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </form>
  <div class="row pull-right">
    <div class="col-sm-12">
      <button type="button" class="btn btn-success" id="guardar_permisos">Guardar Permisos</button>
    </div>
  </div>
  <br>
  <br>
</div>
@endsection

@section('included_js')
<script>
  var site_url = "{{site_url()}}";
  $("#guardar_permisos").on('click',function(){
    var url =site_url+"/permisos/updatePermisos/0";
    ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
      if(result){
        ExitoCustom("Permisos actualizados correctamente",function(){
          window.location.reload();
        });
      }else{
        ErrorCustom("Error al actualizar los permisos",function(){
          window.location.reload();
        });
      }
    });
  });
</script>
@endsection
