@layout('layout')
@section('included_css')
    <style>
        .highcharts-figure,
        .highcharts-data-table table {
            min-width: 310px;
            max-width: 800px;
            margin: 1em auto;
        }

        #container {
            height: 400px;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

    </style>
@endsection
@section('contenido')
    <h2>{{ $titulo_dos }}</h2>
    <br>
    <form id="frm" method="POST">
        @if ($this->session->userdata('id_rol') == 1)
            <div class="row">
                <div class="col-sm-6">
                    <label for="">Sucursal</label>
                    {{ $sucursales }}
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-6">
                <label for="">Servicio</label>
                {{ $id_servicio }}
            </div>
            <div class="col-sm-6">
                <label for="">Lavador</label>
                {{ $id_lavador }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label for="">Fecha inicio</label>
                <input type="date" id="fecha_inicio" name="fecha_inicio" class="form-control" value="{{ date('Y-m-d') }}">
            </div>
            <div class="col-sm-6">
                <label for="">Fecha fin</label>
                <input type="date" id="fecha_fin" name="fecha_fin" class="form-control" value="{{ date('Y-m-d') }}">
            </div>
        </div>
    </form>
    <br>
    <div class="row">
        <div class="col-sm-4">
            <button type="button" id="exportarExcel" data-action="excel/dinero_servicios"
                class="btn btn-danger js_exportar"><i style="color: white" class="fa fa-file-excel-o"></i> Exportar
                EXCEL</button>
        </div>
        <div class="col-sm-8 text-right">
            <button id="buscar" class="btn btn-info">Buscar</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="div_estadisticas">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Fecha Servicio</th>
                            <th>Lavador</th>
                            <th>Servicio</th>
                            <th>Cliente</th>
                            <th>Placas</th>
                            <th>Modelo</th>
                            <th>Marca</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach ($info as $i => $data)
                            <tr>
                                <td>{{ $data->fecha_creacion_servicio }}</td>
                                <td>{{ $data->lavadorNombre }}</td>
                                <td>{{ $data->servicioNombre }}</td>
                                <td>{{ $data->nombre . ' ' . $data->apellido_paterno . ' ' . $data->apellido_materno }}</td>
                                <td>{{ $data->placas }}</td>
                                <td>{{ $data->modelo }}</td>
                                <td>{{ $data->marca }}</td>
                                <td>${{ number_format($data->precio, 2) }}</td>
                                <?php $total = $total + $data->precio; ?>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <td colspan="8">
                            <h4 class="text-right">Total: ${{ number_format($total, 2) }}</h4>
                        </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script src="statics/js/lavadores.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        const id_rol = "{{ $this->session->userdata('id_rol') }}";
        const buscar = () => {
            if (($("#fecha_inicio").val() == '' && $("#fecha_fin").val() == '') || ($("#fecha_inicio").val() != '' && $(
                    "#fecha_fin").val() != '')) {
                if (id_rol == 1 && $("#id_sucursal").val() == '') {
                    ErrorCustom('Es necesario seleccionar la sucursal');
                } else {
                    var url = site_url + "/estadisticas/buscar_servicios_dinero";
                    ajaxLoad(url, $("#frm").serialize(), "div_estadisticas", "POST", function() {

                    });
                }
            } else {
                ErrorCustom('Es necesario ingresar ambas fechas');
            }
        }
        $(".js_exportar").on('click', function() {
            var action = $(this).data('action');
            $('#frm').attr('action', action);
            $('#frm').submit();
        });
        
        $("#buscar").on('click', buscar);
    </script>

@endsection
