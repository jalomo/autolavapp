@layout('recomendadores/layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-9 table-responsive">
            <h3 style="color:darkblue;">
                <a href="#" class="btn btn-menu-titulo">
                    <i class="fa fa-calculator" aria-hidden="true"></i>
                </a>
                <?php if(isset($titulo_dos)): ?>
                    <?php echo $titulo_dos; ?>
                <?php endif; ?>
            </h3>
        </div>
        <div class="col-sm-3" align="right">
            <a href="<?= base_url().'index.php/recomendadores/panel/cupones_enviados' ?>" class="btn btn-dark" style="margin-right: 35px;">
                REGRESAR
            </a>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12 table-responsive">  
            <table <?php if ($bonificaciones != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Cupón</th>
                        <th>Monto cobrado</th>
                        <th>Bonificacion por cupón</th>
                        <th>Fecha de cobro</th>
                    </tr>
                </thead>
                <tbody id="cuerpo">
                    @if ($bonificaciones != NULL)
                        @foreach ($bonificaciones as $i => $registro)
                            <tr>
                                <td style="vertical-align: middle;" align="center">
                                    <?= $i + 1;?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= $registro->cupon ?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= $registro->monto_pagado ?>
                                </td>
                                <td style="vertical-align: middle;">
                                    <?= $registro->beneficio ?>
                                </td>
                                <td style="vertical-align: middle;" align="center">
                                    <?php 
                                        $fecha = new DateTime($registro->fecha_pago);
                                        echo $fecha->format('d-m-Y');
                                        echo " ".$fecha->format('H:i');
                                    ?>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" align="center">
                                Sin cupones
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <br><br>
        </div>
    </div>
            

@endsection
@section('included_js')
    @include('main/scripts_dt')
@endsection