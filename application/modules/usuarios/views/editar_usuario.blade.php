<form action="" method="POST" id="frm">
	<input type="hidden" name="id" id="id" value="{{$id}}">
	<div class="row">
		<div class="col-sm-4">
			<label class="control-label mb-1">Nombre</label>
			{{ $nombre }}
			<span class="error error_nombre"></span>
		</div>
		<div class="col-sm-4">
			<label class="control-label mb-1">Apellido paterno</label>
			{{ $apellido_paterno }}
			<span class="error error_apellido_paterno"></span>
		</div>
		<div class="col-sm-4">
			<label class="control-label mb-1">Apellido materno</label>
			{{ $apellido_materno }}
			<span class="error error_apellido_materno"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<label class="control-label mb-1">Correo electrónico</label>
			{{ $email }}
			<span class="error error_email"></span>
		</div>
		<div class="col-sm-4">
			<label class="control-label mb-1">Teléfono</label>
			{{ $telefono }}
			<span class="error error_telefono"></span>
		</div>
		<div class="col-sm-4">
			<label class="control-label mb-1">Contraseña</label>
			{{ $password }}
			<span class="error error_password"></span>
		</div>
	</div>
</form>