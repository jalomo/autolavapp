$("#enviar_formulario").on('click', function (e){
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#cargaIcono").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        $("input").attr('disabled',false);
        //$("select").attr('disabled',false);

        var base = $("#sitio").val();
        var paqueteDatos = new FormData(document.getElementById('formulario_contacto'));

        $.ajax({
            url: base+"recomendadores/panel/guardar_contacto",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        location.href = base+"index.php/recomendadores/panel/agenda";
                    } else {
                        $("#formulario_error").text(resp);
                    }

                }else{
                    $("#formulario_error").text("ERROR");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
    }
});

function validar() {
    var retorno = true;
    var campo_a = $("input[name='nombre']").val();
    if (campo_a == "") {
        var error_a = $("#nombre_error");
        error_a.empty();
        $("#nombre_error").css("display","inline-block");
        error_a.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_a = $("#nombre_error");
        error_a.empty();
    }

    var correo_2 = $("input[name='telefono']").val();
    if (correo_2 == "") {
        var error_2 = $("#telefono_error");
        $("#telefono_error").css("display","inline-block");
        error_2.append('<label class="form-text text-danger">Formato invalido.</label>');
        retorno = false;
    }else{
        var error_2 = $("#telefono_error");
        error_2.empty();
    }

    return retorno;
}

function editar_contacto(id_contacto) {
    var base = $("#sitio").val();
    var url_sis = base+"recomendadores/panel/buscar_contacto";
    $.ajax({
        url: url_sis,
        method: 'post',
        data: {
            id_contacto : id_contacto
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler           </p>")<1) {
                var opciones = resp.split("=");

                if (opciones[0] == "OK") {
                    $("input[name='listado']").val(opciones[1]);
                    $("input[name='nombre']").val(opciones[2]);
                    $("input[name='telefono']").val(opciones[3]);
                    document.getElementById('notas').value = opciones[4]
                    $("input[name='formulario']").val("2");
                    $("#enviar_formulario").val("ACTUALIZAR");
                } 
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });     
}
