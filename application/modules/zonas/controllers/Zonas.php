<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Zonas extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_zonas', 'mz', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation',));
    date_default_timezone_set('America/Mexico_City');
    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
  }

  public function alta($id_ciudad)
  { 
    $data['poligono'] = $this->mz->get_result('poligonoIdCiudad',$id_ciudad,'poligonos');
    $data['id_ciudad'] = $id_ciudad;
    $data['titulo'] = "Zonas";
    $data['titulo_dos'] = "Alta zona";
    $this->blade->render('alta', $data);
  }

  public function guarda_zona($id_ciudad){
    $poligono = $this->input->post('poligono');
    var_dump($poligono);
    //die();
    
    $this->mz->delete_row('poligonoIdCiudad',$id_ciudad,'poligonos');
    
    
    
    foreach($poligono as $poli):
      $coor =  explode(",",$poli);
      $data['poligonoLatitud'] = $coor[0] ;
      $data['poligonoLongitud'] = $coor[1] ;
      $data['poligonoIdCiudad'] = $id_ciudad;
      $this->mz->save_register('poligonos', $data);
      
     

      


    endforeach;

   




  }
}
