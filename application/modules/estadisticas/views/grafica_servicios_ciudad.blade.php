<div class="row">
    <div class="col-sm-12">
        <figure class="highcharts-figure">
            <div id="container"></div>
        </figure>
    </div>
</div>

<script>
    var series = [];
        @foreach($info_grafica as $i => $info)
            var data = [];
            @foreach($info as $in =>$data)
                data.push(parseFloat("{{$data['total']}}"));
            @endforeach
            series.push({
                name: '{{$i}}',
                marker: {
                    symbol: 'square'
                },
                data: data
            })
        @endforeach
        Highcharts.chart('container', {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Cantidad de servicios por sucursal'
            },
            xAxis: {
                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic'
                ]
            },
            yAxis: {
                title: {
                    text: 'Cantidad'
                },
                labels: {
                    formatter: function() {
                        return this.value + '';
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: series
        });
</script>