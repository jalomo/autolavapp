<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
    label{
      color:#2980b9;
    }
  </style>
</head>
<body>

<div class="container">
  
  <form action="/action_page.php">
    <div class="row">
      <div class="col-12">
          <div class="form-group">
            <label for="email">RFC:</label>
            <input type="text" class="form-control" id="email" placeholder="RFC" name="email">
          </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
          <div class="form-group">
            <label for="email">Raón social:</label>
            <input type="text" class="form-control" id="email" placeholder="Razón social" name="email">
          </div>
      </div>
    </div>
    <div class="row">
    <div class="col-12">
                              <div class="form-group">
                                <label for="receptor_uso_CFDI">Uso del CFDI:</label>
                                <select class="form-control-sm alto form-control" id="receptor_uso_CFDI" name="receptor_uso_CFDI" >
                                  
                                  <option value="G01" selected>G01	Adquisición de mercancias</option>
                                  
                                  
                                  <option value="G02" selected>G02	Devoluciones, descuentos o bonificaciones</option>
                                  
                                  
                                  <option value="G03" selected>G03	Gastos en general</option>
                                  
                                  
                                  <option value="I01" selected>I01	Construcciones</option>
                                  
                                 
                                  <option value="I02" selected>I02	Mobilario y equipo de oficina por inversiones</option>
                                  
                                  
                                  <option value="I03" selected>I03	Equipo de transporte</option>
                                  
                                 
                                  <option value="I04" selected>I04	Equipo de computo y accesorios</option>
                                  
                                  
                                  <option value="I05" selected>I05	Dados, troqueles, moldes, matrices y herramental</option>
                                  
                                  
                                  <option value="I06" selected>I06	Comunicaciones telefónicas</option>
                                 
                                  
                                  <option value="I07" selected>I07	Comunicaciones satelitales</option>
                                  
                                  
                                  <option value="I08" selected>I08	Otra maquinaria y equipo</option>
                                  
                                  
                                  <option value="D01" selected>D01	Honorarios médicos, dentales y gastos hospitalarios.</option>
                                  
                                  
                                  <option value="D02" selected>D02	Gastos médicos por incapacidad o discapacidad</option>
                                 
                                  
                                  <option value="D03" selected>D03	Gastos funerales.</option>
                                  
                                  
                                  <option value="D04" selected>D04	Donativos.</option>
                                  
                                  
                                  <option value="D05" selected>D05	Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).</option>
                                 
                                  
                                  <option value="D06" selected>D06	Aportaciones voluntarias al SAR.</option>
                                  
                                  
                                  <option value="D07" selected>D07	Primas por seguros de gastos médicos.</option>
                                  
                                 
                                  <option value="D08" selected>D08	Gastos de transportación escolar obligatoria.</option>
                                 
                                  
                                  <option value="D09" selected>D09	Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.</option>
                                  
                                  
                                  <option value="D10" selected>D10	Pagos por servicios educativos (colegiaturas)</option>
                                  
                                  
                                  <option value="P01" selected>P01	Por definir</option>
                                  



                                </select>



                              </div>
                          </div>
    </div>
    <button type="button" class="btn btn-default">Guardar</button>
  </form>
</div>

</body>
</html>
