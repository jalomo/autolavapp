@layout('layout')
@section('contenido')
<h1 class="text-center">Información importada</h1>
<div class="table-responsive">
    <table class="table table-hover tablesorter">
        <thead>
            <tr>
                <th class="header">Cliente</th>
                <th class="header">Teléfono</th>       
                <th class="header">Dirección</th>                      
                <th class="header">Serie</th>
                <th class="header">Teléfono</th>
                <th class="header">Fecha factura</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($unidades) && !empty($unidades)) {
                foreach ($unidades as $key => $element) {
                    ?>
                    <tr>
                        <td><?php echo $element['cliente']; ?></td>   
                        <td><?php echo $element['telefono']; ?></td>  
                        <td><?php echo $element['direccion']; ?></td>                       
                        <td><?php echo $element['serie']; ?></td>
                        <td><?php echo $element['telefono']; ?></td>
                        <td><?php echo $element['fechafac']; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="6">No hay registros.</td>    
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>
@endsection