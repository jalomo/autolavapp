var _appsFunction = function () {
	this.init = function () {},
		// this.delete = function ($this) {
		// 	Swal.fire({
		// 		icon: 'warning',
		// 		title: '¿Esta seguro de eliminar el registro?',
		// 		//text: response.info,
		// 		showCancelButton: true,
		// 		confirmButtonText: "Si",
		// 		cancelButtonText: "No"
		// 	}).then((result) => {
		// 		if (result.value) {

		// 			$.ajax({
		// 				dataType: "json",
		// 				type: 'delete',
		// 				url: NOMINA_API + 'catalogos/clasificaciones/store',
		// 				data: {
		// 					'identity': $($this).attr('data-id')
		// 				},
		// 				success: function (response, status, xhr) {
		// 					Swal.fire({
		// 						icon: 'success',
		// 						title: '',
		// 						text: response.info,
		// 						confirmButtonText: "Aceptar"
		// 					}).then((result) => {
		// 						var table = $('table#listado').DataTable();
		// 						table.ajax.reload(null, false);
		// 					});
		// 				}

		// 			});
		// 		}
		// 	});
		// },
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,

			ajax: {
				url: PATH + '/nomina/inicio/percepcionesdeducciones/basefiscales_list?id='+identity,
				type: "POST"
			},
			columns: [
				{
					title: 'Num',
					data: 'BaseFiscalTipo_Num'
				},
				{
					title: 'Descripción',
					data: 'BaseFiscalTipo'
				},
				{
					title: 'Num. Cal.',
					data: 'Calendario_clave'
                },
                {
					title: 'Calendario',
					data: 'Calendario'
				},
                {
					title: 'Grav. / Exento',
					data: 'Montos_Descripcion'
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn btn-primary btn-sm' href='" + PATH + '/nomina/inicio/percepcionesdeducciones/basefiscales_modificar/' + data.id_PyD + "/"+data.id+"' title='Bases fiscales' > <i class='fas fa-pencil-alt'></i> </a>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
