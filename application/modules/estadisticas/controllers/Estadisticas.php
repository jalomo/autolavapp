<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Estadisticas extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->model('M_estadisticas', 'me', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation'));
    date_default_timezone_set('America/Mexico_City');
    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
    if (!PermisoModulo('estadisticas')) {
      redirect(site_url('login'));
    }
  }
  //Cantidad de citas agendadas App / Panel-TeleMarketing.
  public function cantidad_servicios()
  {
    //$data['servicios'] = $this->me->getServiciosByOrigen();
    $data['servicios'] = array();
    $data['origen'] = form_dropdown('origen[]', array_combos($this->Mgeneral->get_table('cat_origenes'), 'id', 'origen', FALSE), '', 'class="form-control busqueda multiple" multiple="multiple" id="origen"');
    $data['titulo'] = "Estadísticas";
    $data['titulo_dos'] = "Servicios agendados";
    $data['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $this->blade->render('cantidad_servicios', $data);
  }
  public function buscar_total_servicios()
  {
    $servicios = $this->me->getServiciosByOrigen();
    
    echo json_encode((array('servicios' => $servicios)));
  }
  // $ en Servicios, Filtros por mes / día / Operador 
  public function dinero_servicios()
  {
    $data['info'] = array();
    $data['titulo'] = "Estadísticas";
    $data['titulo_dos'] = "Dinero en servicios";
    $data['id_servicio'] = form_dropdown('id_servicio', array_combos($this->Mgeneral->get_table('servicios'), 'servicioId', 'servicioNombre', TRUE), '', 'class="form-control busqueda" id="id_servicio"');
    if ($this->session->userdata('id_rol') != 1) {
      $lavadores = array_combos($this->Mgeneral->get_result('id_sucursal', $this->session->userdata('id_sucursal'), 'lavadores'), 'lavadorId', 'lavadorNombre', TRUE);
    } else {
      $lavadores = array();
    }
    $data['id_lavador'] = form_dropdown('id_lavador', $lavadores, '', 'class="form-control busqueda" id="id_lavador"');

    $data['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $this->blade->render('dinero_servicios', $data);
  }
  public function buscar_servicios()
  {
    $data['info'] = $this->me->getDataServicios();
    $this->blade->render('tabla_servicios', $data);
  }
  public function buscar_servicios_dinero()
  {
    $data['info'] = $this->me->getDataServicios();
    $this->blade->render('tabla_dinero_servicios', $data);
  }
  public function productividad_lavadores_grafica()
  {
    $data['titulo_dos'] = "Grafica de productividad de lavadores por estatus";
    if ($this->session->userdata('id_rol') != 1) {
      $lavadores = array_combos($this->Mgeneral->get_result('id_sucursal', $this->session->userdata('id_sucursal'), 'lavadores'), 'lavadorId', 'lavadorNombre', TRUE);
    } else {
      $lavadores = array();
    }
    $data['id_lavador'] = form_dropdown('id_lavador', $lavadores, '', 'class="form-control busqueda" id="id_lavador"');
    $data['info'] = [];
    $data['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $this->blade->render('productividad_lavadores_grafica', $data);
  }
  public function buscar_grafica_productividad_lavadores()
  {
    $data['servicios'] = $this->Mgeneral->get_table('servicios');

    $array_data = [];
    foreach ($data['servicios'] as $s => $servicio) {
      $array_data[$servicio->servicioNombre] = $this->me->getDataTransicionesByEstatus($_POST['id_lavador'], $servicio->servicioId);
    }
    $data['info'] = $array_data;
    $this->blade->render('grafica_productividad_lavadores', $data);
  }
  //productividad_lavadores
  public function productividad_lavadores()
  {
    $data['titulo_dos'] = "Cantidad de servicio por operador  KPI´S Productividad";
    $data['id_servicio'] = form_dropdown('id_servicio', array_combos($this->Mgeneral->get_table('servicios'), 'servicioId', 'servicioNombre', TRUE), '', 'class="form-control busqueda" id="id_servicio"');
    if ($this->session->userdata('id_rol') != 1) {
      $lavadores = array_combos($this->Mgeneral->get_result('id_sucursal', $this->session->userdata('id_sucursal'), 'lavadores'), 'lavadorId', 'lavadorNombre', TRUE);
    } else {
      $lavadores = array();
    }
    $data['id_lavador'] = form_dropdown('id_lavador', $lavadores, '', 'class="form-control busqueda" id="id_lavador"');
    $data['info'] = [];
    $data['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $this->blade->render('productividad_lavadores', $data);
  }
  public function buscar_productividad_dinero()
  {
    $info = $this->me->getProductividad();
    $all_info = [];
    foreach ($info as $i => $informacion) {
      $all_info[$informacion->lavador][$informacion->folio_mostrar . '/' . $informacion->servicio][] = $informacion;
    }
    $data['info'] = $all_info;
    $this->blade->render('tabla_productividad_lavadores', $data);
  }
  //Por periodos
  public function estadisticas_periodos()
  {
    $datos['drop_estatus'] = form_dropdown('id_estatus_lavado', array_combos($this->Mgeneral->get_table('cat_estatus_lavado'), 'id', 'estatus', true), "", 'class="form-control busqueda" id="id_estatus_lavado" ');
    if ($this->session->userdata('id_rol') != 1) {
      $lavadores = array_combos($this->Mgeneral->get_result('id_sucursal', $this->session->userdata('id_sucursal'), 'lavadores'), 'lavadorId', 'lavadorNombre', TRUE);
    } else {
      $lavadores = array();
    }
    $datos['drop_lavadores'] = form_dropdown('id_lavador', $lavadores, '', 'class="form-control busqueda" id="id_lavador"');
    $datos['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $this->blade->render('estadisticas_periodos', $datos);
  }
  public function getGraficaPeriodo()
  {
    $fechas = array();
    $datos = $this->me->getDatosPeriodos();
    foreach ($datos as $key => $value) {
      $fechas[] = date2sql($value->fecha);
    }
    $array_final = $this->fecha($fechas);
    if (!isset($array_final['p1'])) {
      $datos_vista['p1'] = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    } else {
      $datos_vista['p1'] = $array_final['p1'];
    }
    if (!isset($array_final['p2'])) {
      $datos_vista['p2'] = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);;
    } else {
      $datos_vista['p2'] = $array_final['p2'];
    }
    if (!isset($array_final['p3'])) {
      $datos_vista['p3'] = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);;
    } else {
      $datos_vista['p3'] =  $array_final['p3'];
    }
    if (!isset($array_final['p4'])) {
      $datos_vista['p4'] = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);;
    } else {
      $datos_vista['p4'] =  $array_final['p4'];
    }
    $datos_vista['nombre_grafica'] = 'por estatus';
    echo $this->blade->render('graficas_periodos', $datos_vista, TRUE);
  }
  public function fecha($fecha = array())
  {
    $array = array();
    $valorp1 = 0;
    $valorp2 = 0;
    $valorp3 = 0;
    $valorp4 = 0;
    foreach ($fecha as $key => $value) {
      $dia = (int)date("d", strtotime($value));
      $mes = date("n", strtotime($value));
      if ($dia >= 1 && $dia <= 8) {
        //periodo 1
        if (isset($array['p1'][$mes])) {
          $valorp1 = $array['p1'][$mes] + 1;
          $array['p1'][$mes] = $valorp1;
        } else {
          $array['p1'][$mes] = 1;
        }
      } else if ($dia >= 9 && $dia <= 16) {
        //periodo 2
        if (isset($array['p2'][$mes])) {
          $valorp2 = $array['p2'][$mes] + 1;
          $array['p2'][$mes] = $valorp2;
        } else {
          $array['p2'][$mes] = 1;
        }
      } else if ($dia >= 17 && $dia <= 24) {
        //periodo 3
        if (isset($array['p3'][$mes])) {
          $valorp3 = $array['p3'][$mes] + 1;
          $array['p3'][$mes] = $valorp3;
        } else {
          $array['p3'][$mes] = 1;
        }
      } else if ($dia >= 25 && $dia <= 31) {
        //periodo 4
        if (isset($array['p4'][$mes])) {
          $valorp4 = $array['p4'][$mes] + 1;
          $array['p4'][$mes] = $valorp4;
        } else {
          $array['p4'][$mes] = 1;
        }
      }
    }

    $array_final = array();
    foreach ($array as $key => $value) {
      for ($i = 1; $i <= 12; $i++) {
        if (isset($value[$i])) {
          $array_final[$key][$i] = $value[$i];
        } else {
          $array_final[$key][$i] = 0;
        }
      }
    }
    return $array_final;
  }
  public function getLavadoresBySucursal()
  {
    $lavadores = $this->me->getLavadoresBySucursal($_POST['id_sucursal']);
    echo json_encode($lavadores);
  }
  //Gráfica por lluvia
  public function servicios_ciudad()
  {

    $data['titulo'] = "Estadísticas";
    $data['info_grafica'] = [];
    $data['titulo_dos'] = "Cantidad servicios por sucursal";
    $data['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $data['metodo_pago'] = form_dropdown('id_tipo_pago', array_combos($this->Mgeneral->get_result('activo', 1, 'cat_metodo_pago'), 'id', 'metodo_pago', TRUE), '', 'class="form-control busqueda" id="id_tipo_pago"');
    $this->blade->render('servicios_ciudad', $data);
  }
  public function buscar_servicios_ciudad()
  {
    $info = $this->me->getCantidadServicios();
    $data = [];
    foreach ($info as $i => $value) {
      $data[$value->sucursal][$value->mes] = $value;
    }
    $data['info_grafica'] = $this->ordenarMes($data);
    $this->blade->render('grafica_servicios_ciudad', $data);
  }
  public function metodos_pago()
  {

    $data['titulo'] = "Estadísticas";
    $data['info_grafica'] = [];
    $data['titulo_dos'] = "Cantidad servicios";
    $data['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $this->blade->render('metodos_pago', $data);
  }
  public function buscar_metodos_pago()
  {
    $data_metodos_pago = [];
    $data_meses = [];
    //Obtener la cantidad de servicios por método de pago
    $cantidad_metodo_pago = $this->me->getCantidadMetodosPago();
    foreach ($cantidad_metodo_pago as $i => $value) {
      $data_metodos_pago[$value->metodo_pago][$value->mes] = $value;
    }
    //Obtener la cantidad de servicios por mes
    $cantidad_servicios_sucursal = $this->me->cantidad_servicios_sucursal();
    foreach ($cantidad_servicios_sucursal as $c => $cantidad) {
      $data_meses[$cantidad->mes] = $cantidad;
    }
    $data['cantidad_servicios_sucursal'] = $this->orderOneMonth($data_meses);
    $data['cantidad_metodo_pago'] = $this->ordenarMes($data_metodos_pago);
    $this->blade->render('grafica_metodos_pago', $data);
  }
  public function ordenarMes($info_convert)
  {
    $data_grafica = [];
    $meses = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    foreach ($info_convert as $sucursal => $dato) {
      foreach ($meses as $m => $mes) {
        if (!isset($dato[$mes])) {
          $data_grafica[$sucursal][$mes]['mes'] = $mes;
          $data_grafica[$sucursal][$mes]['total'] = 0;
        } else {
          $data_grafica[$sucursal][$mes]['mes'] = $mes;
          $data_grafica[$sucursal][$mes]['total'] = $dato[$mes]->total;
        }
      }
    }
    return $data_grafica;
  }
  public function orderOneMonth($data)
  {
    $data_grafica = [];
    $meses = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    foreach ($meses as $m => $mes) {
      if (!isset($data[$mes])) {
        $data_grafica[$mes]['mes'] = $mes;
        $data_grafica[$mes]['total'] = 0;
      } else {
        $data_grafica[$mes]['mes'] = $mes;
        $data_grafica[$mes]['total'] = $data[$mes]->total;
      }
    }
    return $data_grafica;
  }
  //Cupones
  public function cupones()
  {

    $data['titulo'] = "Cupones";
    $data['info_grafica'] = [];
    $data['titulo_dos'] = "Cupones efectivos por ciudad";
    $data['sucursales'] = form_dropdown('id_sucursal', array_combos($this->Mgeneral->get_table('sucursales'), 'id', 'sucursal', TRUE), '', 'class="form-control js_user busqueda" id="id_sucursal"');
    $this->blade->render('cupones', $data);
  }
  public function buscar_cupones()
  {
    $this->db->where('id_tipo_pago',4);
    $info = $this->me->getCantidadServicios();
    $data = [];
    foreach ($info as $i => $value) {
      $data[$value->sucursal][$value->mes] = $value;
    }
    $data['info_grafica'] = $this->ordenarMes($data);
    $this->blade->render('grafica_cupones', $data);
  }
}
