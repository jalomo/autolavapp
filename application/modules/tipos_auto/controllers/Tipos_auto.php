<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Tipos_auto extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation',));
    date_default_timezone_set('America/Mexico_City');
    if ($this->session->userdata('id')) {
    } else {
      redirect('login/');
    }
  }

  public function alta()
  {
    $data['titulo'] = "Tipo de autos";
    $data['rows'] = $this->Mgeneral->get_table('cat_tipo_autos');
    $this->blade->render('alta', $data);
  }
  public function alta_guardar(){
    $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['imagen']['name']);
    $path_to_save = 'statics/tipo_auto/';
    if(!file_exists($path_to_save)){
      mkdir($path_to_save, 0777, true);
    }
    move_uploaded_file($_FILES['imagen']['tmp_name'], $path_to_save.$name);
    $data['imagen'] = $path_to_save.$name;
    $data['tipo_auto'] = $this->input->post('tipo_auto');
    $data['precio'] = $this->input->post('precio');
   $this->Mgeneral->save_register('cat_tipo_autos', $data);
  }

}
