@layout('layout')
@section('contenido')
    <h1>Anexar archivo por agencia</h1>
    <br>
    <form method="post" class="form-inline" action="<?php echo base_url("proactivo_agencias/subir_archivo"); ?>" enctype="multipart/form-data">
      <div class="row">
        <div class='col-sm-4'>
            <label for="">Selecciona la agencia</label>
            {{ $id_agencia }}
            <span class="error error_id_agencia"></span>
        </div>
        <div class="col-sm-6">
        <small><a href="<?php echo base_url("excel/format.xlsx"); ?>">Descargar formato</a> </small>
        <input type="file" class="form-control-file" id="file" name="file">
        </div>
        <div class="col-sm-2">
            <input type="submit" name="preview" value="Vista previa" class="btn btn-success btn-sm">
        </div>
      </div>
   
    </form>

    <?php
  if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
    if(isset($upload_error)){ // Jika proses upload gagal
      echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
      die; // stop skrip
    }
    // Buat sebuah tag form untuk proses import data ke database
    echo "<form method='post' action='".base_url("proactivo_agencias/import")."'>";
    echo '<input type="hidden" id="id_agencia_save" name="id_agencia_save" value="'.$_POST['id_agencia'].'"">';
    // Buat sebuah div untuk alert validasi kosong
    echo "<div style='color: red;' id='kosong'>
    all data Required <span id='jumlah_kosong'></span> data not empty.
    </div>";

    echo "<table border='1' class='table table-hover' cellpadding='8'>
    <tr>
      <th colspan='5'>Visualizar información</th>
    </tr>
    <tr>
      <th>Fecha de programación</th>
      <th>Correo</th>
      <th>Placas</th>
      <th>Serie</th>
    </tr>";

    $numrow = 1;
    $kosong = 0;
    // Lakukan perulangan dari data yang ada di excel
    // $sheet adalah variabel yang dikirim dari controller
    foreach($sheet as $row){
      // Ambil data pada excel sesuai Kolom
      $fecha_programacion = $row['A']; // Ambil data NIS
      $correo = $row['H']; // Ambil data nama
      $placas = $row['AC']; // Ambil data jenis kelamin
      $serie = $row['AD']; // Ambil data alamat

      // Cek jika semua data tidak diisi
      if($fecha_programacion == "" && $correo == "" && $placas == "" && $serie == "")
        continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

      // Cek $numrow apakah lebih dari 1
      // Artinya karena baris pertama adalah nama-nama kolom
      // Jadi dilewat saja, tidak usah diimport
      if($numrow > 1){
        // Validasi apakah semua data telah diisi
        $fecha_programacion_td = ( ! empty($fecha_programacion))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
        $correo_td = ( ! empty($correo))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
        $placas_td = ( ! empty($placas))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
        $serie_td = ( ! empty($serie))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah

        // Jika salah satu data ada yang kosong
        if($fecha_programacion == "" or $correo == "" or $placas == "" or $serie == ""){
          $kosong++; // Tambah 1 variabel $kosong
        }

        echo "<tr>";
        echo "<td".$fecha_programacion_td.">".$fecha_programacion."</td>";
        echo "<td".$correo_td.">".$correo."</td>";
        echo "<td".$placas_td.">".$placas."</td>";
        echo "<td".$serie_td.">".$serie."</td>";
        echo "</tr>";
      }

      $numrow++; // Tambah 1 setiap kali looping
    }

    echo "</table>";

    // Cek apakah variabel kosong lebih dari 0
    // Jika lebih dari 0, berarti ada data yang masih kosong
    if($kosong > 0){
    ?>
      <script>
      $(document).ready(function(){
        // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
        $("#jumlah_kosong").html('<?php echo $kosong; ?>');

        $("#kosong").show(); // Munculkan alert validasi kosong
      });
      </script>
    <?php
    }else{ // Jika semua data sudah diisi
      echo "<hr>";

      // Buat sebuah tombol untuk mengimport data ke database
      echo "<button type='submit' class='btn btn-info' name='import'>Procesar información</button>";
      echo "&nbsp;";
      echo "<a href='".base_url("proactivo_agencias/subir_archivo")."' class='btn btn-dark'>Cancelar</a>";
    }

    echo "</form>";
  }
  ?>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!---- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    -->  <script> 
  $(document).ready(function(){
    // Sembunyikan alert validasi kosong
    $("#kosong").hide();
  });
  </script>
@endsection