<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th><input type="checkbox" name="seleccionar_todo" id="seleccionar_todo"></th>
            <th style="text-align: center;">Hora</th>
            <th style="text-align: center;">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @if (count($citas) > 0)
            @foreach ($citas as $key => $value)
                <tr>
                    <td colspan="3">
                        <strong>Fecha: {{ date_eng2esp_1($key) }}</strong>
                    </td>
                </tr>
                @foreach ($value as $k => $cita)
                    <tr>
                        <td>
                            @if ($cita->activo)
                                <input class="js_seleccion check_{{ $cita->id }}; ?>" type="checkbox"
                                    name="seleccion[{{ $cita->id }}]" id="seleccion-{{ $cita->id }}" data-valor="0"
                                    data-id="{{ $cita->id }}">
                            @endif
                        </td>
                        <td>{{ substr($cita->hora, 0, 5) }}</td>
                        <td style="text-align: center;">
                            <?php
                            $horario = date_eng2esp_1($cita->fecha);
                            $hora = substr($cita->hora, 0, 5);
                            ?>
                            <a href="" data-id="{{ $cita->id }}" class="js_editar" aria-hidden="true"
                                data-toggle="tooltip" data-placement="top" title="Editar" data-horario="{{ $horario }}"
                                data-hora="{{ $hora }}"><i class="fa fa-edit"></i></a>
                            @if ($cita->activo)
                                <a href="" data-id="{{ $cita->id }}" class="js_activar " data-valor="0"
                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                    title="Desactivar horario"><i class="fa fa-check-square-o"
                                        aria-hidden="true"></i></a>
                            @else
                                <a href="" data-id="{{ $cita->id }}" class=" js_activar" data-valor="1"
                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                    title="Activar horario">
                                    <i class="fa fa-square-o" aria-hidden="true"></i>
                                </a>
                            @endif
                            @if ($cita->motivo != '')
                                <a class="" aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                    title="{{ $cita->motivo }}">
                                    <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endforeach
        @else
            <tr style="text-align: center">
                <td colspan="3">No hay registros para mostrar...</td>
            </tr>
        @endif

    </tbody>
</table>
