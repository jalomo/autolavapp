<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class PanelLavador extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set('America/Mexico_City');
  }

  public function index()
  {
    $data['titulo'] = "Panel";
    $data['titulo_dos'] = "Usuarios del sistema";
    $data['id'] = $this->input->get('lavador_id');
    $data['usuarios'] = $this->db->select('admin.*, ca_cargos.nombre as cargo')->where('telefono !=', null)->join('ca_cargos', 'admin.id_cargo = ca_cargos.id')->get('admin')->result();
    $data['clientes'] = $this->db
    ->select('usuarios.*, servicio_lavado.id as servicio_id, servicio_lavado.id_estatus_lavado, servicio_lavado.id_lavador')
    ->join('servicio_lavado', 'usuarios.id = servicio_lavado.id_usuario')
    ->where('usuarios.telefono !=', null)
    ->where_in('servicio_lavado.id_estatus_lavado', [2,3,4,5])
    ->where('servicio_lavado.id_lavador', $data['id'])
    ->get('usuarios')->result();
    $data['datos']  = $this->db->where('lavadorId', $data['id'])->get('lavadores')->row_array();
    $this->blade->render('sistema/panelLavador', $data);
  }

  public function  chat()
  {
    $data['titulo'] = "Sistema";
    $data['titulo_dos'] = "Chat";
    $telefono1 = $this->input->get('telefono_remitente');
    $telefono2 = $this->input->get('telefono_destinatario');
    $data['telefono_remitente'] = $telefono1;
    $data['telefono_destinatario'] = $telefono2;
    $data['remitente'] = $this->getUserTelefono($telefono1);
    $data['destinatario'] = $this->getUserTelefono($telefono2);
    $data['template2'] = "layoutuser";
    $this->blade->render('sistema/chat_webView', $data);
  }

  public function getUserTelefono($telefono)
  {
      $usuarios_admin = $this->db->select('adminNombre as nombre, telefono')->where('telefono', $telefono)->get('admin')->result_array();
      $usuarios_cliente = $this->db->select('nombre, telefono')->where('telefono', $telefono)->get('usuarios')->result_array();
      $usuarios_recomendador = $this->db->select('nombre, celular as telefono')->where('celular', $telefono)->get('recomendadores')->result_array();
      $usuarios_lavadores = $this->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "Lavador" as tipo')->where('lavadorTelefono', $telefono)->get('lavadores')->result_array();
      $usuarios = array_merge($usuarios_admin, $usuarios_cliente, $usuarios_recomendador, $usuarios_lavadores);
      if (is_array($usuarios)){
        return current($usuarios)['nombre'];
      }
  }

}
