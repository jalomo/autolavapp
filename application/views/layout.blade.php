<!doctype html>
<html class="no-js" lang="">
<base href="{{ base_url() }}">

<head>
    @include('main/header')
    @yield('included_css')
    <style>
        body {
            padding: 20px !important;
        }

        header {
            background-color: #eaeafd !important;
        }

        .for-notification {
            color: red;
        }

        .navbar {
            padding-top: 50px !important;
            background: transparent !important;
        }

        .noti-header {
            color: #434a5c
        }

        aside {
            background-image: url('statics/img/Patron.png') !important;
            background-size: cover !important;
            background-color: #7B95FF !important;
            border-radius: 20px !important;
            background-size: cover;
        }
        .navbar .navbar-nav li > a:hover, .navbar .navbar-nav li > a:hover .menu-icon{
            background-color: transparent !important;
            padding-left:1px;
            color:white
        }
        .navbar .navbar-nav li > a{
            color:white !important;
        }
        .navbar .navbar-nav li.menu-item-has-children .sub-menu{
            background: transparent !important;
        }
        .sub-menu .menu-icon{
            color:white !important
        }
        .card{
            border-radius: 20px;
        }

    </style>
    <script src="{{ base_url('statics/js/jquery.min.js') }}"></script>
    @if (!empty($this->session->userdata('id')))
        <script>
            var telefono = "<?php echo $this->session->userdata('telefono'); ?>";
            var user_id = "<?php echo $this->session->userdata('id'); ?>";
            var user_name = "<?php echo $this->session->userdata('adminNombre'); ?>";
            var site_url = "<?php echo site_url(); ?>";
            var base_url = "<?php echo base_url(); ?>";
            var notnotification = false;

        </script>
    @endif
</head>

<body style="background-color: #eaeafd">
    @include('main/menu')
    <div id="right-panel" class="right-panel">
        @include('main/header_dos')
        @yield('titulo')
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                @yield('contenido')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        API_URL = "<?php echo API_URL_DEV; ?>";
        PATH = "<?php echo site_url(); ?>";
        PATH_BASE = "<?php echo base_url(); ?>";
        PATH_API = "<?php echo API_URL_DEV ?>";
        PATH_LANGUAGE = "<?php echo base_url('js/spanish.json'); ?>";
        CONTROLLER = "<?php echo $this->router->class; ?>";
        FUNCTION = "<?php echo $this->router->method; ?>";
    </script>
    <script src="{{ base_url('statics/js/jquery.min.js') }}"></script>
    <script src="{{ base_url('statics/js/bootbox.min.js') }}"></script>
    <script src="{{ base_url('statics/js/general.js') }}"></script>
    <script src="{{ base_url('statics/js/isloading.js') }}"></script>
    <script src="{{ base_url('statics/js/utils.js') }}"></script>
    <script src="{{ base_url('statics/js/sweetalert2.min.js') }}"></script>
    <script src="{{ base_url('statics/tema/assets/js/popper.min.js') }}"></script>
    <script src="{{ base_url('statics/tema/assets/js/plugins.js') }}"></script>
    @yield('modal')
    @yield('included_js')
</body>

</html>
