
@layout('layout')
@section('contenido')
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
label{
  font-family: 'Roboto', sans-serif;
}
input{
  font-family: 'Roboto', sans-serif;
}
</style>
<script>
$(document).ready(function(){
$("#cargando").hide();
/*$('#alta_usuario1').submit(function(event){
  event.preventDefault();
  $("#enviar").hide();
  $("#cargando").show();

});
*/

});
</script>

<div class="content mt-3">
    <div class="animated fadeIn">

        <div class="ui-typography">
            <div class="row">

</div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <!--div class="card-header">
            <strong class="card-title">Alta Clientes</strong>

        </div-->
        <div class="card-body" style="background:#fff">
          <!-- Credit Card -->
          <div id="pay-invoice">
              <div class="card-body">


                  <form action="<?php echo base_url()?>index.php/repo/subir_info/<?php echo $id_incidencia;?>" method="post" novalidate="novalidate" id="alta_usuario1" enctype="multipart/form-data">

                      <div class="row">
                        <div class="col-6">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Descripción</label>
                                  <textarea id="descripcion" name="descripcion" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="Descripción" autocomplete="off"></textarea>
                                  
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          
                      </div>

                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">URL</label>
                                  <input id="url" name="url" type="text" class="form-control-sm form-control cc-exp" value="" placeholder="URL" autocomplete="off">
                                  <span class="help-block" data-valmsg-for="cc-exp" data-valmsg-replace="true"></span>
                              </div>
                          </div>
                          

                      </div>


                      <div class="row">
                          <div class="col-6">
                              <div class="form-group">
                                  <label for="cc-exp" class="control-label mb-1">Documento</label>
                                  <input type="file" value="Subir" class="btn btn-primary" name="documento" id="documento">
                              </div>
                          </div>
                          

                      </div>


                      


                     



                      <div align="right" class="col-4">
                        <button id="enviar" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-save fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Guardar</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                        <div align="center">
                         <button  id="cargando" class="btn btn-primary btn-lg"><i class="fa fa-spinner fa-spin"></i> Enviando</button>
                        </div>
                      </div>
                  </form>
              </div>
          </div>

        </div>
    </div> <!-- .card -->

  </div><!--/.col-->
    </div>


    </div><!-- .animated -->
</div><!-- .content -->


<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

        <div class="col-md-12">
            <div class="card">
                     

                <div class="card-body">
                 

                    
                      
                      
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Fecha</th>
                <th>Descripción</th>
                <th>URL</th>
                <th>Documento</th>
                
                
              </tr>
            </thead>
            <tbody>
              <?php if(is_array($documentos)):?>
              <?php foreach($documentos as $row):?>
                <tr>
                  <td><?php echo $row->fecha_creacion;?></td>
                  <td><?php echo $row->descripcion;?></td>
                  <td><?php echo $row->url;?></td>

                  <td><a href="<?php echo base_url()."statics/documentos/".$row->url_documento;?>">Descargar</a></td>
                   
                  
                  
                </tr>
             <?php endforeach;?>
             <?php endif;?>
            </tbody>
          </table>
                </div>
            </div>
        </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->


@endsection
@section('included_js')
    @include('main/scripts_dt')
    
@endsection
