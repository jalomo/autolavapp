@layout('recomendadores/layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-9 table-responsive">
            <h3 style="color:darkblue;">
                <a href="#" class="btn btn-menu-titulo">
                    <i class="fa fa-ticket" aria-hidden="true"></i>
                </a>
                <?php if(isset($titulo_dos)): ?>
                    <?php echo $titulo_dos; ?>
                <?php endif; ?>
            </h3>
        </div>
        <div class="col-sm-3" align="right">
            <a href="<?= base_url().'index.php/recomendadores/panel/agenda' ?>" class="btn btn-warning">
                MIS CONTACTOS
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 table-responsive">            
            <br>
            <table <?php if (isset($cupon)) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Cupon</th>
                        <th>Descripción</th>
                        <th>Limite</th>
                        <th>Usos</th>
                        <th>Estatus</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="cuerpo">
                    @if (isset($cupon))
                        @if (count($cupon)>0)
                            @foreach ($cupon as $i => $valor)
                                <tr>
                                    <td style="vertical-align: middle;" align="center">
                                        <?= $i + 1;?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <?= $cupon[$i]; ?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <?= $descipcion[$i]; ?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <?= (($limite_uso[$i] == "0") ? "Ilimitado" : $limite_uso[$i]); ?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <?= $usos[$i]; ?>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        @if ($activo[$i] == "1") 
                                            @if (($fecha_caducidad[$i] > date("Y-m-d")) || ($caducidad[$i] == "1")) 
                                                Activo
                                            @else
                                                Caducado
                                            @endif
                                        @else
                                            Inactivo
                                        @endif
                                    </td>
                                    <td style="vertical-align: middle;">
                                        @if ($activo[$i] == "1")
                                            @if (($fecha_caducidad[$i] > date("Y-m-d")) || ($caducidad[$i] == "1"))
                                                <a href="<?= base_url().'index.php/recomendadores/panel/enviar_cupon/'.$ruta_cupo[$i] ?>" class="btn btn-success" style="font-size: 10px!important;">
                                                    Enviar cupones
                                                </a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7" align="center">
                                    Sin cupones
                                </td>
                            </tr>
                        @endif
                    @else
                        <tr>
                            <td colspan="7" align="center">
                                Sin registros
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>

            <br><br><br>
        </div>
    </div>
            

@endsection
@section('included_js')
    @include('main/scripts_dt')
@endsection