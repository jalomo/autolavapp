<div class="row">
    @foreach ($info as $i => $servicio)
        <div class="col-sm-6">
            <figure class="highcharts-figure">
                <div id="grafica_{{str_replace(' ','',$i)}}">{{str_replace(' ','',$i)}}</div>
            </figure>
        </div>
    @endforeach
</div>
<script>
    @foreach ($info as $i => $servicio)
        var series = [];
        var background = [];

        var radius = 112;
        var innerRadius = 0;
    @foreach($servicio as $s => $data)
    innerRadius = radius-24;
        series.push({
            name:"{{$data->estatus}}" ,
            data: [{
                color: Highcharts.getOptions().colors["{{$s}}"],
                radius: radius+'%',
                innerRadius: innerRadius+'%',
                y: parseFloat("{{$data->promedio}}")
            }]
        })
        background.push(
            {
                outerRadius: radius+'%',
                innerRadius: innerRadius+'%',
                backgroundColor: Highcharts.color(Highcharts.getOptions().colors["{{$s}}"])
                    .setOpacity(0.3)
                    .get(),
                borderWidth: 0
            }
        )
        radius = radius-24;
    @endforeach
    Highcharts.chart('grafica_'+"{{str_replace(' ','',$i)}}", {
        chart: {
            type: 'solidgauge',
            height: '100%',
        },

        title: {
            text: "{{$i}}",
            style: {
                fontSize: '24px'
            }
        },

        tooltip: {
            borderWidth: 0,
            backgroundColor: 'none',
            shadow: false,
            style: {
                fontSize: '16px'
            },
            valueSuffix: 'min',
            pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}</span>',
            positioner: function(labelWidth) {
                return {
                    x: (this.chart.chartWidth - labelWidth) / 2,
                    y: (this.chart.plotHeight / 2) + 15
                };
            }
        },

        pane: {
            startAngle: 0,
            endAngle: 360,
            background:background
        },

        yAxis: {
            min: 0,
            max: 300,
            lineWidth: 0,
            tickPositions: []
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    enabled: false
                },
                linecap: 'round',
                stickyTracking: false,
                rounded: true
            }
        },

        series: series
    });
    @endforeach
</script>
