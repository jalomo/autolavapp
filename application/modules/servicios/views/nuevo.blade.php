@layout('layout')
@section('included_css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
@endsection
<link href="{{ base_url('statics/css/jquery.fileupload.css') }}" rel="stylesheet">
<style>
    .error p {
        color: red
    }

    html,
    body {
        height: 100%;
        width: 100%;
    }

    #map {
        height: 100%;
        width: 100% margin: 0 15px 30px 130px;
        padding: 0 25px;
    }

    #tab-ubicacion .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 50px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 350px;
        height: 30px;
        margin-top: 20px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    #tab-ubicacion .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    .error {
        color: red;
    }

    .rojo {
        color: red;
    }

    .custom-radios div {
        display: inline-block;
    }

    .custom-radios input[type="radio"] {
        display: none;
    }

    .custom-radios input[type="radio"]+label {
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 14px;
    }

    .custom-radios input[type="radio"]+label span {
        display: inline-block;
        width: 40px;
        height: 40px;
        margin: -1px 4px 0 0;
        vertical-align: middle;
        cursor: pointer;
        border-radius: 50%;
        border: 2px solid #FFFFFF;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
        background-repeat: no-repeat;
        background-position: center;
        text-align: center;
        line-height: 44px;
    }

    .custom-radios input[type="radio"]+label span img {
        opacity: 0;
        transition: all .3s ease;
    }

    .custom-radios input[type="radio"]#color-1+label span {
        background-color: #2ecc71;
    }

    .custom-radios input[type="radio"]#color-2+label span {
        background-color: #f1c40f;
    }

    .custom-radios input[type="radio"]#color-3+label span {
        background-color: #e74c3c;
    }

    .custom-radios input[type="radio"]:checked+label span {
        opacity: 1;
        background: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg) center center no-repeat;
        width: 40px;
        height: 40px;
        display: inline-block;
    }

</style>
@section('contenido')
    <form id="frm" method="post" novalidate="novalidate" id="nuevo-servicio">
        <input type="hidden" name="token" id="token" value="00001">
        <input type="hidden" name="id" id="id" value="{{ $proactivo == 0 ? $id : 0 }}">
        <input type="hidden" name="id_usuario" id="id_usuario" value="{{ $id_usuario }}">
        <input type="hidden" name="id_ubicacion" id="id_ubicacion" value="{{ $id_ubicacion }}">
        <input type="hidden" name="id_auto" id="id_auto" value="{{ $id_auto }}">
        <input type="hidden" name="id_horario" id="id_horario" value="{{ $id_horario }}">
        <input type="hidden" name="id_facturacion" id="id_facturacion" value="{{ $id_facturacion }}">
        <input type="hidden" name="evidencia" id="evidencia" value="{{ $evidencia }}">
        <input type="hidden" name="path_evidencia" id="path_evidencia" value="{{ $path_evidencia }}">
        <input type="hidden" name="proactivo" id="proactivo" value="{{ $proactivo }}">
        <input type="hidden" name="agencia" id="agencia" value="{{ $agencia }}">
        <input type="hidden" name="origen" id="origen" value="{{ $origen }}">
        <input type="hidden" name="url_agencia" id="url_agencia" value="{{ $url_agencia }}">
        <input type="hidden" name="id_lavador_actual" id="id_lavador_actual" value="{{ $id_lavador }}">

        <h2>Datos del cliente</h2> <br>
        @if ($id == 0)
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Cliente</label>
                    {{ $id_usuario_list }}
                    <span class="error error_id_usuario_list"></span>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1">Nombre</label>
                {{ $nombre }}
                <span class="error error_nombre"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Apellido paterno</label>
                {{ $apellido_paterno }}
                <span class="error error_apellido_paterno"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Apellido materno</label>
                {{ $apellido_materno }}
                <span class="error error_apellido_materno"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1">Correo electrónico</label>
                {{ $email }}
                <span class="error error_email"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Teléfono</label>
                {{ $telefono }}
                <span class="error error_telefono"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Ciudad</label>
                {{ $id_sucursal }}
                <span class="error error_id_sucursal"></span>
            </div>
        </div>
        Datos de facturación <input type="checkbox" id="check_facturacion" name="check_facturacion">
        <br>
        <br>
        <div id="div_facturacion" class="d-none">
            <h2>Datos de facturación</h2>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Referencia</label>
                    {{ $referencia }}
                    <span class="error error_referencia"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Razón social</label>
                    {{ $razon_social }}
                    <span class="error error_razon_social"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Domicilio</label>
                    {{ $domicilio }}
                    <span class="error error_domicilio"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Correo electrónico</label>
                    {{ $email_facturacion }}
                    <span class="error error_email_facturacion"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">CFDI</label>
                    {{ $id_cfdi }}
                    <span class="error error_id_cfdi"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Método de pago</label>
                    {{ $id_metodo_pago }}
                    <span class="error error_id_metodo_pago"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">R.F.C</label>
                    {{ $rfc }}
                    <span class="error error_rfc"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">C.P</label>
                    {{ $cp }}
                    <span class="error error_cp"></span>
                </div>
            </div>
        </div>
        <h2>Registro de vehículo</h2><br>
        ¿Vehículo nuevo? <input type="checkbox" id="check_vehiculo" name="check_vehiculo">
        @if ($agencia != 0)
            <div class="row">
                <div class="col-sm-12 text-right">
                    <h2 for="" id="lbl-campania"></h2>
                    <div class="custom-radios">
                        <div id="div-radio-1">
                            <input type="radio" id="color-1" name="campania" value="1" class="rd-campania">
                            <label for="color-1">
                                <span>
                                </span>
                            </label>
                        </div>
                        <div id="div-radio-2">
                            <input type="radio" id="color-2" name="campania" value="2" class="rd-campania">
                            <label for="color-2">
                                <span>
                                </span>
                            </label>
                        </div>
                        <div id="div-radio-3">
                            <input type="radio" id="color-3" name="campania" value="3" class="rd-campania">
                            <label for="color-3">
                                <span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1">Placas</label>
                {{ $placas }}
                <span class="error error_placas"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Color</label>
                {{ $id_color }}
                <span class="error error_id_color"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Marca</label>
                {{ $id_marca }}
                <span class="error error_id_marca"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1">Modelo</label>
                {{ $id_modelo }}
                <span class="error error_id_modelo"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Año</label>
                {{ $id_anio }}
                <span class="error error_id_anio"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1"># Serie</label>
                {{ $numero_serie }}
                <span class="error error_numero_serie"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1">Kilometraje</label>
                {{ $kilometraje }}
                <span class="error error_kilometraje"></span>
            </div>
        </div>
        <h2>Ubicación</h2><br>
        <div class="row">
            <div class="col-sm-12">
                <input id="pac-input" class="controls" type="text" placeholder="Encuentra tu ubicación">
                <div class="row">
                    <div class="col-md-12">
                        <label>Selecciona la ubicación del usuario (puedes escribir en el cuadro de texto o arrastrar el
                            icono de ubicación del mapa)</label>
                    </div>
                </div>
                <div style="width: 100%;height: 400px" id="map_subir"></div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1">Calle</label>
                {{ $calle }}
                <span class="error error_calle"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1"># Exterior</label>
                {{ $numero_ext }}
                <span class="error error_numero_ext"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1"># Interior</label>
                {{ $numero_int }}
                <span class="error error_numero_int"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1">Colonia</label>
                {{ $colonia }}
                <span class="error error_colonia"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Estado</label>
                {{ $id_estado }}
                <span class="error error_id_estado"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Sucursal</label>
                {{ $id_sucursal_ubicacion }}
                <span class="error error_id_sucursal_ubicacion"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label class="control-label mb-1">Municipio</label>
                {{ $id_municipio }}
                <span class="error error_id_municipio"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Latitud</label>
                {{ $latitud }}
                <span class="error error_latitud"></span>
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-1">Longitud</label>
                {{ $longitud }}
                <span class="error error_longitud"></span>
            </div>
        </div>
        <h3>Registro cita</h3><br>
        @if ($id_lavador != 0)
            <div class="row">
                <div class="col-sm-4">
                    <label class="control-label mb-1">Lavador / seleccionar automáticamente <input type="checkbox"
                            name="select_lavador" id="select_lavador"></label>
                    {{ $drop_lavadores }}
                    <span class="error error_id_lavador"></span>
                </div>
            </div>
        @endif
        <div class="row">
            @if ($id == 0 || $proactivo != 0)
                <div class="col-sm-4">
                    <label class="control-label mb-1">Fecha</label>
                    {{ $fecha }}
                    <span class="error error_fecha"></span>
                </div>
                <div class="col-sm-4">
                    <label class="control-label mb-1">Hora</label>
                    {{ $hora }}
                    <span class="error error_hora"></span>
                </div>
            @endif
            <div class="col-sm-4">
                <label class="control-label mb-1">Servicio</label>
                {{ $id_servicio }}
                <span class="error error_id_servicio"></span>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Comentarios</label>
                {{ $comentarios }}
            </div>
            <div class="col-sm-6">
                <label>Evidencia</label>
                {{ $this->blade->render('servicios/evidencia', ['evidencia' => $evidencia, 'id' => $id], true) }}
                <span class="">** Sólo se permiten imágenes</span>
                <span class="error error_evidencia"></span>
            </div>
        </div>
        <div align="right">
            <button type="button" id="guardar" class="btn btn-lg btn-info ">
                <i class="fa fa-edit fa-lg"></i>&nbsp;
                <span>Guardar</span>
                <span id="payment-button-sending" style="display:none;">Sending…</span>
            </button>
        </div>
    </form>
@endsection
@section('included_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var base_url = "{{ base_url() }}";

    </script>
    <script src="statics/js/jquery.ui.widget.js"></script>
    <script src="statics/js/jquery-ui.min.js"></script>
    <script src="statics/js/jquery.fileupload.js"></script>
    <script src="statics/js/evidencia_servicio.js"></script>
    <script src="statics/js/servicio.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM&callback=initMap&libraries=places"
        async defer></script>
    <script>
        var notnotification = true;
        var id = "{{ $id }}";
        var id_facturacion = "{{ $id_facturacion }}";
        var latitud = parseFloat('{{ $mapa_latitud }}');
        var longitud = parseFloat('{{ $mapa_longitud }}');
        var proactivo = "{{ $proactivo }}";
        var agencia = "{{ $agencia }}";
        $(document).ready(function() {
            //$(".busqueda").select2();
            $("#cargando").hide();
            const guardar = () => {
                var url = site_url + '/servicios/generar_servicio/0';
                ajaxJson(url, $("#frm").serialize(), "POST", "", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        //Se recorre el json y se coloca el error en la div correspondiente
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    } else {
                        if (result == 1) {
                            ExitoCustom("Información guardada correctamente", function() {
                                window.location.href = site_url + '/servicios';
                            });
                        } else if (result == -2) {
                            ErrorCustom('El horario ya fue ocupado, elige otro');
                        } else if (result == -3) {
                            ErrorCustom(
                                'El correo electrónico ya fue registrado, por favor elige otro');
                        }
                    }
                });
            }
            const getHorarios = () => {
                var url = site_url + "/servicios/getHorariosDisponibles";
                $("#hora").empty();
                $("#hora").append("<option value=''>-- Selecciona --</option>");
                $("#hora").attr("disabled", true);
                fecha = $("#fecha").val();
                var data = {
                    fecha: fecha,
                    id_sucursal: $("#id_sucursal_ubicacion").val()
                }
                if ($("#id_lavador").val() != '') {
                    data.id_lavador = $("#id_lavador").val()
                }
                var value = '';
                if (fecha != '') {
                    ajaxJson(url,
                        data,
                        "POST", "",
                        function(result) {
                            if (result.length != 0) {
                                $("#hora").empty();
                                $("#hora").removeAttr("disabled");
                                result = JSON.parse(result);
                                $("#hora").append("<option value=''>-- Selecciona --</option>");
                                $.each(result, function(i, item) {
                                    value = result[i].hora;
                                    if ($("#id_lavador").val() != '' && $("#id_lavador").val() != undefined) {
                                        value = result[i].id;
                                    }
                                    $("#hora").append("<option value= '" + value + "'>" +
                                        result[i].hora +
                                        "</option>");
                                });
                            } else {
                                $("#hora").empty();
                                $("#hora").append(
                                    "<option value='0'>No se encontraron datos</option>");
                            }
                        });
                } else {
                    $("#hora").empty();
                    $("#hora").append("<option value=''>-- Selecciona --</option>");
                    $("#hora").attr("disabled", true);
                }
            }

            const ValidarCampania = () => {
                const data = {
                    serie: $("#numero_serie").val()
                }
                $.ajax({
                    url: $("#url_agencia").val() + '/api/validarCampania',
                    data: data,
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'jsonp',
                    success: function(info) {},
                    error: function(e) {
                        alert('Failed!');
                    },
                    headers: {
                        'Access-Control-Allow-Origin': '*'
                    },
                });
            }
            const ValidarCampania11 = () => {
                ajaxJson($("#url_agencia").val() + '/api/validarCampania', {
                    "serie": $("#numero_serie").val()
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    if (result.registros) {
                        $("#lbl-campania").empty().append('La unidad cuenta con campaña(s) (' + result
                            .datos + ')');
                        $("#color-2").prop('checked', true)
                        $("#color-1").prop('disabled', true)
                        $("#color-2").prop('disabled', false)
                        $("#color-3").prop('disabled', false)

                    } else {
                        $("#lbl-campania").empty().append(
                            'La unidad NO cuenta con campaña'
                        );
                        $("#color-1").prop('disabled', false)
                        $("#color-2").prop('disabled', true)
                        $("#color-3").prop('disabled', true)
                        $("#color-1").prop('checked', true)
                    }
                });
            }
            $("#guardar").on('click', guardar);
            $("#fecha").on("change", getHorarios);
            $("#numero_serie").on('change', () => {
                if (agencia != 0 && $("#numero_serie").val() != '') {
                    ValidarCampania();
                }
            });
            $("#select_lavador").on('click', () => {
                if ($("#select_lavador").is(':checked')) {
                    $("#id_lavador").val('');
                } else {
                    $("#id_lavador").val($("#id_lavador_actual").val());
                }
                $("#fecha").empty();
                $("#fecha").append("<option value=''>-- Selecciona --</option>");
                $("#fecha").attr("disabled", true);
                $("#hora").empty();
                $("#hora").append("<option value=''>-- Selecciona --</option>");
                $("#hora").attr("disabled", true);
                getFechas();
            })
            $("body").on('click', '.select-auto', function(e) {
                e.preventDefault();
                ajaxJson(site_url + "/servicios/getAutoById", {
                    "id_auto": $(this).data('id')
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    if (result.auto.length != 0) {
                        $.each(result.auto, function(i, item) {
                            if (i != 'id') {
                                $("#" + i).val(item);
                                $("#" + i).prop('readonly', true);
                            }
                        });
                        $("#id_marca").trigger('change');
                        $("#id_modelo").val(result.auto.id_modelo)
                        $("#id_auto").val(result.auto.id)
                        $(".modalVehiculos").modal('hide')
                    }
                });

            });

            if (id != 0 && id_facturacion != 0) {
                $("#check_facturacion").prop('checked', true);
                $("#div_facturacion").removeClass('d-none');
            }
            if (proactivo != 0) {
                $("#fecha").val('')
                $("#hora").val('');
                $("#id_servicio").val('')
                $("#comentarios").val('')
            }
            if (proactivo != 0 && agencia != 0) {
                var url = site_url + '/servicios/getDataByAgencia';
                ajaxJson(url, {
                    id_agencia: agencia
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    $.each(result.data, function(i, item) {
                        if (i != 'id' && i != 'id_usuario') {
                            $("#" + i).val(item);
                        }
                    });
                    if ($("#telefono").val() == '') {
                        $("#telefono").val(result.data.telefono_movil)
                    }
                    $("#numero_serie").val(result.data.serie)
                    $("#numero_serie").trigger('change');
                    $("#numero_ext").val(result.data.noexterior)
                    $("#numero_int").val(result.data.nointerior)
                    $("#id_anio option").filter(function() {
                        //may want to use $.trim in here
                        return $(this).text() == result.data.vehiculo_anio;
                    }).prop('selected', true);
                    $("#id_estado option").filter(function() {
                        //may want to use $.trim in here
                        return $(this).text().toUpperCase() == result.data.estado.toUpperCase();
                    }).prop('selected', true);
                    $("#id_estado").trigger('change');
                    $("#id_municipio option").filter(function() {
                        //may want to use $.trim in here
                        return $(this).text().toUpperCase() == result.data.municipio.toUpperCase();
                    }).prop('selected', true);
                    $("#razon_social").val(result.data.nombre_compania)
                    $("#email_facturacion").val(result.data.correo_compania)
                    $("#id_marca option").filter(function() {
                        //may want to use $.trim in here
                        return $(this).text().toUpperCase() == result.data.marca.toUpperCase();
                    }).prop('selected', true);
                    $("#id_marca").trigger('change');
                    $("#id_modelo option").filter(function() {
                        //may want to use $.trim in here
                        return $(this).text().toUpperCase() == result.data.modelo.toUpperCase();
                    }).prop('selected', true);
                    $("#id_color option").filter(function() {
                        //may want to use $.trim in here
                        return $(this).text().toUpperCase() == result.data.color.toUpperCase();
                    }).prop('selected', true);

                });
            }

        });
        //mapa
        if (id == 0) {
            var zoom = 5;
        } else {
            zoom = 15;
        }

        function initMap() {
            map = new google.maps.Map(document.getElementById('map_subir'), {
                center: {
                    lat: latitud,
                    lng: longitud
                },
                zoom: zoom
            });
            infowindow = new google.maps.InfoWindow({
                map: map
            });
            input = (document.getElementById('pac-input'));
            var types = document.getElementById('type-selector');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);
            //inicializar el autocomplete
            autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);
            //---------------------------------
            // Try HTML5 geolocation.
            //si el id != 0 va poner la ubicación que trae
            if (id == 0 && agencia == 0) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        inicializar_marcador(latLng);
                        infowindow.setPosition(pos);
                        infowindow.setContent('Ubicación encontrada');
                        map.setCenter(pos);
                    }, function() {
                        handleLocationError(true, infowindow, map.getCenter());
                    });
                } else {
                    // Browser doesn't support Geolocation
                    var pos = {
                        lat: 19.245657599999998,
                        lng: -103.76368649999999
                    };
                    latLng = new google.maps.LatLng(parseFloat(pos.lat), parseFloat(pos.lng));

                    inicializar_marcador(latLng);
                    infowindow.setPosition(pos);
                    infowindow.setContent('Ubicación encontrada');
                    map.setCenter(pos);

                    handleLocationError(false, infowindow, map.getCenter());

                }

            } else {
                var pos = {
                    lat: 19.245657599999998,
                    lng: -103.76368649999999
                };
                latLng = new google.maps.LatLng(pos.latitud, pos.longitud);
                inicializar_marcador(latLng);
            }

        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            latLng = new google.maps.LatLng(latitud, longitud);
            inicializar_marcador(latLng);
            infowindow.setPosition(pos);
            infowindow.setContent(browserHasGeolocation ?
                'La geolocalización ha fallado.' :
                'Tu navegador no permite la geolocalización');
        }

        function inicializar_marcador(latLng = '') {
            marker = new google.maps.Marker({
                position: latLng,
                map: map,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });
            inicializar_autocomplete();
            openInfoWindow(marker);
            google.maps.event.addListener(marker, "dragend", function(event) {
                openInfoWindow(marker);
                //latitud
                //longitud
            }); //end addListener
        }

        function inicializar_autocomplete() {
            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Intenta de nuevo la búsqueda.");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17); // Why 17? Because it looks good.
                }
                marker.setIcon( /** @type {google.maps.Icon} */ ({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));

                //marker.setPosition(place.geometry.location);
                inicializar_marcador(place.geometry.location);
                marker.setVisible(true);
                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }
                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);
            });
        }

        function openInfoWindow(marker) {
            var markerLatLng = marker.getPosition();
            infowindow.setContent([
                'Haz click y arrastrame para actualizar la posición.'
            ].join(''));
            $("#latitud").val(markerLatLng.lat());
            $("#longitud").val(markerLatLng.lng());
            getAdress(markerLatLng.lat(), markerLatLng.lng())
            infowindow.open(map, marker);
        }

        function getAdress(lat, lng) {
            var url =
                `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCt6Oynr1XLg8y-CbD9wv1wsPQ5DZsYKeM`;
            ajaxJson(url, {}, "GET", "", function(result) {
                if (result) {
                    result = result.results[0].address_components
                    console.log(result);
                    $("#numero_ext").val(result[0].long_name);
                    $("#calle").val(result[1].long_name);
                    $("#colonia").val(result[2].long_name);
                }

            });
        }

    </script>
@endsection
