@layout('layout')

@section('included_css')

@endsection

<style>
    .error div {
        color: red
    }

    input[type="checkbox"] {
        transform: scale(1.5);
    }

    input[type="number"]::-webkit-outer-spin-button, 
    input[type="number"]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type="number"] {
        -moz-appearance: textfield;
    }

    #cargaIcono, #cargaIcono2 {
        /*-webkit-animation: rotation 1s infinite linear;*/
        font-size: 55px !important;
        color: darkblue;
        display: none;
    }

</style>

@section('contenido')
    <div class="row">
        <div class="col-sm-12">
            <form id="formulario_cupon" method="post" action="" autocomplete="on" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-12">
                        <div style="margin-left: 15px; margin-right: 15px;">
                            <h5>Datos del Cupón</h5>
                        </div>
                    </div>
                </div>
                
                <br>
                <div class="row">
                    <div class="col-sm-5">
                        @if (!isset($id))
                            <label for="">Recomendador : </label>
                            <select name="usuario" id="usuario" class="form-control">
                                <option value="">Usuarios recomendadores</option>
                                @foreach ($usuarios as $s => $user)
                                    <option value="{{ $user->id }}">{{ (($user->nombre != "") ? $user->nombre : $user->usuario)  }}</option>
                                @endforeach
                            </select>
                            <div id="usuario_error"></div>
                        @endif
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Clave : </label>&nbsp;
                        <input type="text" class="form-control" name="clave" value="<?php if(isset($cupon)) echo $cupon; ?>" style="width:100%;text-transform: uppercase;">
                        <div id="cupon_error"></div>
                    </div>
                    <div class="col-sm-8">
                        <label for="">Descripción : </label>&nbsp;
                        <input type="text" class="form-control" name="descripcion" value="<?php if(isset($descripcion)) echo $descripcion; else echo 'Cliente recomendador'; ?>" style="width:100%;">
                        <div id="descripcion_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Recomendador : </label>
                        <input type="text" class="form-control" name="nombre_usuario" value="<?php if(isset($nombre_usuario)) echo $nombre_usuario; ?>" style="width:100%;" disabled>
                        <div id="nombre_usuario_error"></div>
                    </div>
                    <div class="col-sm-5">
                        <label for="">Limite de usos : </label>
                        <label style="color:darkblue;font-size: 10px;">* Colocar "0" para ilimitado</label>
                        <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" name="limite" value="<?php if(isset($limite_uso)) echo $limite_uso; else echo '10'; ?>" style="width:100%;">
                        <div id="limite_error"></div>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Descuento (%) : </label>
                        <!--<input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" name="descuento" value="<?php if(isset($descuento)) echo $descuento; else echo '50'; ?>" style="width:100%;">-->
                        <select name="descuento" id="descuento" class="form-control">
                            <option value="25" <?php if(isset($descuento)) { if($descuento == "25") echo "selected"; } ?> >25 %</option>
                            <option value="50" <?php if(isset($descuento)) { if($descuento == "50") echo "selected"; } ?> >50 %</option>
                            <option value="75" <?php if(isset($descuento)) { if($descuento == "75") echo "selected"; } ?> >75 %</option>
                            <option value="100" <?php if(isset($descuento)) { if($descuento == "100") echo "selected"; } ?> >100 %</option>
                        </select>
                        <div id="descuento_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-3">
                        <label for="">Fecha de activación : </label>
                        <input type="date" class="form-control" min="<?php if(isset($fecha_activacion)) echo $fecha_activacion; else echo date('Y-m-d'); ?>" name="fecha_activo" id="fecha_activo" value="<?php if(isset($fecha_activacion)) echo $fecha_activacion; else echo date('Y-m-d'); ?>" style="width:100%;">
                        <div id="fecha_activo_error"></div>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Fecha de Caducidad : </label>
                        <input type="date" class="form-control" name="fecha_caducidad" id="fecha_caducidad" value="<?php if(isset($fecha_caducidad)) echo $fecha_caducidad; else echo date('Y-m-d'); ?>" style="width:100%;">
                        <div id="fecha_caducidad_error"></div>
                    </div>
                    <div class="col-sm-2">
                        <label for="">Sin Caducidad</label>
                        <br>
                        <input type="checkbox" style="margin-left: 30%;" name="tiempo_iliminado" <?php if(isset($indefinido)) {if($indefinido == "1") echo 'checked';} ?> >
                    </div>
                    <div class="col-sm-4">
                        <label for="">Bonificación por uso : </label>
                        <!--<input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" name="beneficio" value="<?php if(isset($beneficio)) echo $beneficio; else echo '10'; ?>" style="width:100%;">-->
                        <select name="beneficio" id="beneficio" class="form-control">
                            <?php for ($i = 10 ; $i < 110; $i += 5): ?>
                                <option value="<?= $i ?>" <?php if(isset($beneficio)) { if($beneficio == $i) echo "selected"; } ?> >$<?= $i ?></option>
                            <?php endfor ?>
                        </select>
                        <div id="beneficio_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-2">
                        <label for="">Servicios : </label>
                    </div>
                    <div class="col-sm-10">
                        <table>
                            @foreach ($servicios as $serv)
                                <tr>
                                    <td style="width: 1cm;">
                                        <input type="checkbox" class="form-control servicio" name="servicios[]" value="{{ $serv->servicioId}}" <?php if(isset($pqt)) {if(in_array($serv->servicioId, $pqt)) echo 'checked';} ?> >        
                                    </td>
                                    <td style="vertical-align: middle;">
                                        &nbsp;&nbsp; <label for="">{{ $serv->servicioNombre}}</label>   
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div id="servicios_error"></div>
                    </div>
                </div>

                <div class="col-sm-12" align="center">
                    <br>
                    <i id="cargaIcono" class="fa fa-spinner fa-spin"></i>
                    <br>
                    <h5 class="error" id="formulario_error"></h5>
                    <?php if (isset($id)): ?>
                        <input type="hidden" name="envio_formulario" value="2">
                        <input type="button" class="btn btn-success" name="enviar_formulario" id="enviar_formulario" value="ACTUALIZAR">
                    <?php else: ?>
                        <input type="hidden" name="envio_formulario" value="1">
                        <input type="button" class="btn btn-success" name="enviar_formulario" id="enviar_formulario" value="GUARDAR">
                    <?php endif ?>
                    <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                    <input type="hidden" name="servicio_cupon" value="<?php if(isset($id)) echo $id; else echo '0'; ?>">
                    <br>    
                </div>
            </form>
        </div>
    </div>

    <br>        

@endsection
@section('included_js')

    <script src="{{ base_url('statics/js/cupones/envio_formulario.js') }}"></script>
    <script src="{{ base_url('statics/js/cupones/interacciones.js') }}"></script>

@endsection