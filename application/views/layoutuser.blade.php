<!doctype html>
<html class="no-js" lang="">
<base href="{{ base_url() }}">

<head>
    @include('main/header')
    @yield('included_css')
    <script>
        var site_url = "<?php echo site_url(); ?>";
        var notnotification = false;
    </script>
</head>

<body>
    <div id="right-panel" class="right-panel">
        @yield('titulo')
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                @yield('contenido')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src = "{{ base_url('statics/js/jquery.min.js') }}" ></script>
    <script src = "{{ base_url('statics/js/bootbox.min.js') }}" ></script>
    <script src="{{ base_url('statics/js/general.js') }}"></script>
    <script src = "{{ base_url('statics/js/isloading.js') }}" ></script>
    <script src="{{ base_url('statics/tema/assets/js/popper.min.js') }}"></script>
    <script src="{{ base_url('statics/tema/assets/js/plugins.js') }}"></script>
    <!--<script src="{{ base_url('statics/tema/assets/js/main.js') }}"></script>-->
    @yield('included_js')
</body>

</html>
