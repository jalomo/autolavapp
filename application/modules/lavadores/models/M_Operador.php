<?php
class M_Operador extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        $result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
        return $result;
    }

    public function update_table_row($table,$data,$id_table,$id){
            $result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
    }

    public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function servicios_lavador_fecha($id_lavador='',$fecha = '')
    {
        $result = $this->db->where('l.lavadorId',$id_lavador)
            ->where('h.fecha',$fecha)

            ->join('horarios_lavadores AS h','sl.id_horario = h.id')
            ->join('lavadores AS l','sl.id_lavador = l.lavadorId')
            ->join('servicios AS s','s.servicioId = sl.id_servicio')
            ->join('cat_estatus_lavado AS cat_st','cat_st.id = sl.id_estatus_lavado')

            ->select('l.lavadorNombre, h.fecha, h.hora, cat_st.estatus, s.servicioNombre, sl.id, cat_st.color, cat_st.color_letra, sl.folio_mostrar, sl.id_estatus_lavado, sl.cancelado, sl.reagendado, sl.no_contactar')

            ->order_by('h.hora','ASC')
            ->get('servicio_lavado AS sl')
            ->result();
            //echo $this->db->last_query();die();
        return $result;
    }


    public function datos_servicio($servicio='')
    {
        $result = $this->db->where('serv.id ',$servicio)
            ->join('horarios_lavadores AS h','serv.id_horario = h.id','left')
            ->join('lavadores AS l','l.lavadorId = serv.id_lavador','left')
            ->join('servicios AS s','s.servicioId = serv.id_servicio','left')
            ->join('cat_estatus_lavado AS cat_st','cat_st.id = serv.id_estatus_lavado','left')
            ->join('cat_metodo_pago AS pago','pago.id = serv.id_tipo_pago','left')

            
            ->join('usuarios AS u','u.id = serv.id_usuario','left')
            ->join('ubicacion_servicio AS ub','ub.id = serv.id_ubicacion','left')
            ->join('cat_municipios AS cat_m','cat_m.id = ub.id_municipio','left')
            ->join('estados AS edo','edo.id = ub.id_estado','left')
            
            //Recuperamos el auto y sus especificaciones por catalo
            ->join('autos AS auto','auto.id = serv.id_auto','left')
            ->join('cat_colores AS color','color.id = auto.id_color','left')
            ->join('cat_marcas AS marca','marca.id = auto.id_marca','left')
            ->join('cat_modelos AS modelo','modelo.id = auto.id_modelo','left')
            ->join('cat_tipo_autos AS tipo_a','tipo_a.id = modelo.id_tipo_auto','left')

            ->select('u.nombre, u.email, u.telefono, u.apellido_paterno, u.apellido_materno, ub.latitud, ub.longitud,  ub.numero_ext, ub.numero_int, ub.colonia, ub.calle, cat_m.municipio, edo.estado, auto.placas, auto.numero_serie, auto.kilometraje, color.color, marca.marca, modelo.modelo, tipo_a.tipo_auto, l.lavadorNombre, h.fecha, h.hora, cat_st.estatus, s.servicioNombre, serv.folio_mostrar, serv.id_auto, serv.cancelado, serv.reagendado, pago.metodo_pago, serv.pago ')
            
            ->order_by('h.fecha DESC , h.hora DESC')
            ->get('servicio_lavado AS serv')
            ->result();
            //echo $this->db->last_query();die();
        return $result;
    }

    public function pasos_proceso($id_servicio = '')
    {
        $q = $this->db->where('id_servicio ',$id_servicio)       
        ->select('*')
        ->get('proceso_servicio_lavado')
        ->result();

        return $q;
    }

    public function tipo_pago($id_servicio = '')
    {
        $q = $this->db->where('id ',$id_servicio)       
        ->select('pago, id_tipo_pago')
        ->limit(1)
        ->get('servicio_lavado');

        if($q->num_rows()==1){
            $retorno = [$q->row()->id_tipo_pago,$q->row()->pago];
        }else{
            $retorno = [];
        }
        
        return $retorno;
    }

    public function datos_base_servicio($servicio='')
    {
        $q = $this->db->where('id ',$servicio)       
        ->select('id_usuario,id_lavador')
        ->get('servicio_lavado');

        if($q->num_rows()==1){
            $retorno = [$q->row()->id_usuario,$q->row()->id_lavador];
        }else{
            $retorno = [];
        }
        
        return $retorno;
    }



}