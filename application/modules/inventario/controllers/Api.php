<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Minventario', 'consulta', TRUE);
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set('America/Mexico_City');
    }


    public function index()
    {
    	echo "hola";
    }

    public function recuperar_evidencia()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $servicio = $_POST["id_servicio"];
        $contenedor = [];

        $array_imagen = ["JPEG","JPG","PNG","BMP","ICO","SVG","WEBP","GIF","PSD","HEIC","NEF","CRW","AI","ID","PSD","RAW","TIF","TIFF"];
        $array_doc = ["TXT","DOC","DOCX","DOCM","ODT","PDF","RTF","CSV","XLS","XLSX","XLSM","ODS","PPS","PPT","PPSX","PPTX","PPSM","PPTM","POTX","ODP"];
        $array_audio = ["MP3","WMA","WAV","FLAC","MIDI","OGG","M3U"];
        $array_video = ["AVI","DIVX","MOV","MP4","MPG","MKV","WMV","WPL"];

        $evidencia = $this->consulta->get_result_field("activo","1","id_orden",$servicio,"evidencia_inventario");
        $evidencia_lavado = $this->consulta->get_result_field("activo","1","id_servicio",$servicio,"evidencia_proceso_servicio");

        foreach ($evidencia as $row){
            $cadena_url = explode(".", $row->evidencia);
            $extension = strtoupper($cadena_url[count($cadena_url)-1]);

            if (in_array($extension, $array_imagen) ){
                $formato = "IMAGEN";
            } elseif (in_array($extension, $array_doc) ){
                $formato = "DOCUMENTO";
            } elseif (in_array($extension, $array_audio) ){
                $formato = "AUDIO";
            } elseif (in_array($extension, $array_video) ){
                $formato = "VIDEO";
            } else{
                $formato = "IMAGEN";
            }

            $contenedor[] = array(
                "titulo" => "Evidencia inventario",
                "comentarios" => "Sin comentarios",
                "url" => (($row->repositorio == "1") ? REPO_XEHOS.$row->evidencia : REPO_XEHOS_OP.$row->evidencia),
                "formato" => $formato
            );
        }

        foreach ($evidencia_lavado as $row_2){
            $cadena_url2 = explode(".", $row_2->url);
            $ext = strtoupper($cadena_url2[count($cadena_url2)-1]);

            if (in_array($extension, $array_imagen) ){
                $formato = "IMAGEN";
            } elseif (in_array($ext, $array_doc) ){
                $formato = "DOCUMENTO";
            } elseif (in_array($ext, $array_audio) ){
                $formato = "AUDIO";
            } elseif (in_array($ext, $array_video) ){
                $formato = "VIDEO";
            } else{
                //$formato = "SIN IDENTIFICAR";
                $formato = "IMAGEN";
            }

            $contenedor[] = array(
                "titulo" => $row_2->titulo,
                "comentarios" => $row_2->comentarios,
                "url" => (($row_2->repositorio == "1") ? REPO_XEHOS.$row_2->url : REPO_XEHOS_OP.$row_2->url),
                "formato" => $formato
            );
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

}