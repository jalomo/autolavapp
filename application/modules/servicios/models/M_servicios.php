<?php
class M_servicios extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function guardarUbicacion($id_ubicacion = 0)
    {   
        //ubicación
        $ubicacion = [
            'numero_int' => $this->input->post('numero_int'),
            'numero_ext' => $this->input->post('numero_ext'),
            'colonia' => $this->input->post('colonia'),
            'id_estado' => $this->input->post('id_estado'),
            'id_municipio' => $this->input->post('id_municipio'),
            'id_sucursal' => $this->input->post('id_sucursal'),
            'latitud' => $this->input->post('latitud'),
            'longitud' => $this->input->post('longitud'),
            'calle' => $this->input->post('calle'),
            'created_at' => date('Y-m-d H:i:s')
        ];
        if ($id_ubicacion == 0) {
            $this->Mgeneral->save_register('ubicacion_servicio', $ubicacion);
        } else {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('id', $id_ubicacion)->update('ubicacion_servicio', $ubicacion);
        }
    }
    public function guardarUbicacionUsuario($id_usuario = 0)
    {
        $ubicacion = $this->Mgeneral->get_row('id_usuario', $id_usuario, 'ubicacion_usuario');
        if (!$ubicacion) {
            //Solo si es la primera vez se guarda en ubicación usuario
            $data_ubicacion = [
                'id_usuario' => $id_usuario,
                'numero_int' => $this->input->post('numero_int'),
                'numero_ext' => $this->input->post('numero_ext'),
                'colonia' => $this->input->post('colonia'),
                'id_estado' => $this->input->post('id_estado'),
                'id_municipio' => $this->input->post('id_municipio'),
                'id_sucursal' => $this->input->post('id_sucursal'),
                'latitud' => $this->input->post('latitud'),
                'longitud' => $this->input->post('longitud'),
                'calle' => $this->input->post('calle'),
                'created_at' => date('Y-m-d H:i:s')
            ];
            $this->Mgeneral->save_register('ubicacion_usuario', $data_ubicacion);
        }
    }
    public function guardarAuto($id, $id_usuario = 0)
    {
        //auto
        $data['placas'] = $this->input->post('placas');
        $data['id_color'] = $this->input->post('id_color');
        $data['id_modelo'] = $this->input->post('id_modelo');
        $data['id_marca'] = $this->input->post('id_marca');
        $data['id_anio'] = $this->input->post('id_anio');
        $data['numero_serie'] = $this->input->post('numero_serie');
        $data['kilometraje'] = $this->input->post('kilometraje');
        $data['id_usuario'] = $id_usuario;
        $data['created_at'] = date('Y-m-d H:i:s');
        if ($id == 0) {
            $this->Mgeneral->save_register('autos', $data);
        } else {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('id', $id)->update('autos', $data);
        }
    }
    public function guardarDatosFacturación($id, $id_usuario)
    {
        //datos facturación
        $data['id_usuario'] = $id_usuario;
        $data['referencia'] = $this->input->post('referencia');
        $data['razon_social'] = $this->input->post('razon_social');
        $data['domicilio'] = $this->input->post('domicilio');
        $data['email_facturacion'] = $this->input->post('email_facturacion');
        $data['id_cfdi'] = $this->input->post('id_cfdi');
        $data['id_metodo_pago'] = $this->input->post('id_metodo_pago');
        $data['cp'] = $this->input->post('cp');
        $data['rfc'] = $this->input->post('rfc');
        $data['created_at'] = date('Y-m-d H:i:s');
        if ($id == 0) {
            $this->Mgeneral->save_register('datos_facturacion', $data);
        } else {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->db->where('id', $id)->update('datos_facturacion', $data);
        }
    }
    //guarda los lavadores
    public function guardarServicio()
    {

        $id = $this->input->post('id');
        $id_usuario = $this->input->post('id_usuario');
        $id_ubicacion = $this->input->post('id_ubicacion');
        $id_auto = $this->input->post('id_auto');
        $id_facturacion = $this->input->post('id_facturacion');
        if ($id == 0) {
            //Saber si viene ya el id horario 
            if (is_numeric($_POST['hora'])) {
                $horario_lavador = $this->getHorarioLavadorById($_POST['hora']);
            } else {
                $horario_lavador = $this->getIdLavador();
            }
            if (!$horario_lavador) {
                echo -2;
                exit();
                return;
            }
        }
        //Validar el usuario
        if ($this->existeUsuario($id_usuario)) {
            echo -3;
            exit();
            return;
        }
        //Guardar usuario
        $this->guardarUsuario($id_usuario);
        if ($id_usuario == 0) {
            $id_usuario = $this->db->insert_id();
        }
        //guardar el auto
        $this->guardarAuto($id_auto, $id_usuario);
        if ($id_auto == 0) {
            $id_auto = $this->db->insert_id();
        }

        //Guardar ubicación
        $this->guardarUbicacion($id_ubicacion);
        if ($id_ubicacion == 0) {
            $id_ubicacion = $this->db->insert_id();
        }
        $this->guardarUbicacionUsuario($id_usuario);
        //Si llega guardar la facturación
        if (isset($_POST['check_facturacion'])) {
            $this->guardarDatosFacturación($id_facturacion, $id_usuario);
            if ($id_facturacion == 0) {
                $id_facturacion = $this->db->insert_id();
            }
        }
        $servicio = [
            'id_usuario' => $id_usuario,
            'id_auto' => $id_auto,
            'id_servicio' => $_POST['id_servicio'],
            'comentarios' => $_POST['comentarios'],
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
            'latitud_lavador' => '',
            'longitud_lavador' => '',
            'id_ubicacion' => $id_ubicacion,
            'nombre_cliente' => '',
            'cupon' => '',
            'evidencia' => $_POST['evidencia'],
            'path_evidencia' => $_POST['path_evidencia'],
            'proactivo' => $_POST['proactivo'],
            'origen' => $_POST['origen'],
            'completo' => 1,
            'principal' => 1,
            'id_sucursal' => $_POST['id_sucursal_ubicacion'],
            'id_sucursal_creo' => $this->session->userdata('id_sucursal'),
            'id_tipo_pago' => 2,
            'subtotal' => getPrecioServicio($_POST['id_modelo'], $_POST['id_servicio']),
            'total' => getPrecioServicio($_POST['id_modelo'], $_POST['id_servicio']),
        ];
        if ($id == 0) {
            $servicio['id_lavador'] = $horario_lavador->id_lavador;
            $servicio['id_horario'] = $horario_lavador->id;
            $servicio['id_usuario_creo'] = $this->session->userdata('id');
            $servicio['folio_mostrar'] = getFolioMostrar($_POST['id_sucursal'], false);
            $servicio['folio_control'] = getFolioControl();
            $date_10 = strtotime('+10 day', strtotime($horario_lavador->fecha));
            $date = date('Y-m-d', $date_10);
            $dia = date("D", strtotime($date));
            //Si el día es domingo agregar 1 día
            if ($dia == 'Sun') {
                $date_act = strtotime('+1 day', strtotime($horario_lavador->fecha));
                $fecha_confirmacion = date('Y-m-d', $date_act);
            } else {
                $fecha_confirmacion = $horario_lavador->fecha; //;
            }
            $servicio['fecha_programacion'] = $fecha_confirmacion . ' ' . $horario_lavador->hora;
            $this->db->insert('servicio_lavado', $servicio);
            //Enviar notificación lavador
            $mensaje = 'SEHOX AUTOLAVADO, se asignó un nuevo servicio: ' .date_eng2esp_1($horario_lavador->fecha) . ' ' . $horario_lavador->hora.' Folio:'.$servicio['folio_mostrar'];
            $tel_lavador = $this->getTelLavador($horario_lavador->id_lavador);
            sms_general($tel_lavador, $mensaje);
            $datos_correo = [
                'nombre' => $this->input->post('nombre'),
                'apellido_paterno' => $this->input->post('apellido_paterno'),
                'apellido_materno' => $this->input->post('apellido_materno'),
                'id_servicio' => $this->db->insert_id(),
                'fecha_hora' => date_eng2esp_1($horario_lavador->fecha) . ' ' . $horario_lavador->hora,
                'lavador' => $this->getLavadorById($horario_lavador->id_lavador)->lavadorNombre,
                'servicio' => $this->Mgeneral->get_row('servicioId', $_POST['id_servicio'], 'servicios')->servicioNombre,
                'folio_mostrar' => $servicio['folio_mostrar']
            ];
            $cuerpo = $this->blade->render('confirmacion_cita', array('datos_cita' => $datos_correo, 'id_servicio' => $datos_correo['id_servicio'], 'asunto' => '¡Asignación de cita!'), true);
            enviar_correo($_POST['email'], "Confirmación de cita!", $cuerpo, array());
            $this->db->where('id', $horario_lavador->id)->set('ocupado', 1)->update('horarios_lavadores');
            //Si es proactivo guardar el historial
            if ($_POST['proactivo'] == 1) {
                $array_proactivo = [
                    'id_servicio' => $datos_correo['id_servicio'],
                    'id_usuario' => $this->session->userdata('id'),
                    'created_at' => date('Y-m-d H:i:s')
                ];
                $this->db->insert('historial_servicios_proactivo', $array_proactivo);
            }
        } else {
            $servicio['updated_at'] = date('Y-m-d H:i:s');
            $servicio['id_usuario_actualizo'] = $this->session->userdata('id');
            $this->db->where('id', $id)->update('servicio_lavado', $servicio);
        }

        echo 1;
        exit();
    }
    //Obtener el horario lavador por id
    public function getHorarioLavadorById($id)
    {
        $q = $this->db->where('id', $id)->where('ocupado', 0)->get('horarios_lavadores');
        if ($q->num_rows() == 1) {
            return $q->row();
        } else {
            return false;
        }
    }
    //Obtiene municipios por estado
    public function getMunicipiosByEstado($id_estado = '')
    {
        return $this->db->where('id_estado', $id_estado)->get('cat_municipios')->result();
    }
    //Obtiene sucursales por estado
    public function getSucursalesByEstado($id_estado = '')
    {
        return $this->db->where('id_estado', $id_estado)->get('sucursales')->result();
    }
    //Obtiene marcas por modelo
    public function getModelosByMarca($id_marca)
    {
        return $this->db->where('id_marca', $id_marca)->get('cat_modelos')->result();
    }
    //Obtener las fechas
    public function getFechasLavadores($fecha = '', $id_sucursal)
    {
        if ($fecha != '') {
            $this->db->where('fecha >=', $fecha);
        } else {
            $this->db->where('fecha >=', date('Y-m-d'));
        }
        return $this->db
            ->select('DISTINCT(fecha) as fecha')
            ->where('id_sucursal', $id_sucursal)
            ->order_by('fecha', 'asc')
            ->get('horarios_lavadores')
            ->result();
    }
    public function getFechasByLavador($id_lavador = '')
    {
        return $this->db->where('fecha >=', date('Y-m-d'))
            ->select('DISTINCT(fecha)')
            ->where('id_lavador', $id_lavador)
            ->order_by('fecha', 'asc')
            ->get('horarios_lavadores')
            ->result();
    }
    public function getFechasByLavadorEdit($id_lavador = '')
    {
        return $this->db
            ->select('fecha')
            ->where('id_lavador', $id_lavador)
            ->order_by('fecha', 'asc')
            ->get('horarios_lavadores')
            ->result();
    }
    public function getHorariosDisponibles($fecha, $id_lavador = '', $id_sucursal= '')
    {
        /*if ($fecha == date('Y-m-d')) {
            $this->db->where('hora >=', date('H:i'));
        }
        if ($id_lavador != '') {
            $this->db->where('id_lavador', $id_lavador);
            $this->db->select('id,hora');
        } else {
            $this->db->select('DISTINCT(hora) as hora');
            $this->db->where('id_sucursal', $id_sucursal);
        }*/
        
        return $this->db->where('fecha',$fecha)
            ->where('activo', 1)
            ->where('ocupado', 0)
            ->order_by('hora', 'asc')
            ->get('horarios_lavadores')
            ->result();
    }
    public function getFechasDisponiblesLavador($id_lavador)
    {
        return $this->db->where('fecha >=', date('Y-m-d'))
            ->where('id_lavador', $id_lavador)
            ->select('DISTINCT(fecha) as fecha')
            ->get('horarios_lavadores')
            ->result();
    }
    public function getHorariosByLavador($fecha, $id_lavador, $id_horario)
    {
        $query_where = '';
        if ($this->session->userdata('id_rol') != 1) {
            $query_where = ' and id_sucursal=' . $this->session->userdata('id_sucursal');
        }
        $query = "SELECT id, hora,ocupado FROM horarios_lavadores WHERE fecha = '" . $fecha . "' AND activo = '1' AND id_lavador = " . $id_lavador . " AND (ocupado =0 or id = " . $id_horario . ")" . $query_where;
        return $this->db->query($query)->result();
    }
    public function getIdLavador()
    {
        $q = $this->db
            ->where('fecha', $_POST['fecha'])
            ->where('hora', $_POST['hora'])
            ->where('id_sucursal', $_POST['id_sucursal_ubicacion'])
            ->where('activo', 1)
            ->where('ocupado', 0)
            ->get('horarios_lavadores');
        if ($q->num_rows() == 1) {
            return $q->row();
        } else {
            $id_lavador_siguiente = $this->getLavadorSiguiente();
            $array_lavadores = [];
            foreach ($q->result() as $l => $lavador) {
                $array_lavadores[] = $lavador->id_lavador;
            }
            //Puede ser que existen 3 lavadores con el horario por ej. 18:00 (4,5,6) pero el lavador siguiente es el 1 entonces debo validar
            if (!in_array($id_lavador_siguiente, $array_lavadores)) {
                $id_lavador_siguiente = $array_lavadores[0];
            }
            $q = $this->db->where('fecha', $_POST['fecha'])
                ->where('hora', $_POST['hora'])
                ->where('activo', 1)
                ->where('ocupado', 0)
                ->where('id_lavador', $id_lavador_siguiente)
                ->get('horarios_lavadores');
            return $q->row();
        }
    }
    //obtener el lavador que sigue
    public function getLavadorSiguiente()
    {
        $lavadores = $this->getLavadores();
        $q = $this->db->limit(1)->select('s.id_lavador')
            ->order_by('s.id', 'desc')
            ->where('id_lavador !=', '')
            ->where('s.id_sucursal', $_POST['id_sucursal_ubicacion'])
            ->join('ubicacion_servicio u', 's.id_ubicacion = u.id')
            ->get('servicio_lavado s');
        if ($q->num_rows() == 1) {
            $id_lavador = $q->row()->id_lavador;
            //Validar si es el último lavador, regresar al primero
            if ($id_lavador == null) {
                return $lavadores[0]->lavadorId;
            }
            if ($id_lavador >= count($lavadores)) {
                return $lavadores[0]->lavadorId;
            } else {
                $q = $this->db->limit(1)->select('lavadorId')->order_by('lavadorId', 'asc')->where('activo', 1)->where('lavadorId >', $id_lavador)->get('lavadores');
                if ($q->num_rows() == 1) {
                    return $q->row()->lavadorId;
                } else {
                    return 1;
                }
            }
        } else {
            return 1;
        }
    }
    public function validarCupon($cupon)
    {
        $query = "select * from cupones
        where activo = 1
        and cupon = " . $cupon . "
        and '" . date('Y-m-d') . "' between desde and hasta";
        $cupon = $this->db->query($query)->result();
        if (count($cupon) > 0) {
            return $cupon[0];
        } else {
            return false;
        }
    }
    //obtiene los lavadores
    public function getLavadores($id_sucursal = '')
    {
        if ($this->input->post() && $id_sucursal == '') {
            $this->db->where('id_sucursal', $_POST['id_sucursal_ubicacion']);
        } else {
            $this->db->where('id_sucursal', $id_sucursal);
        }
        return $this->db
            ->where('activo', 1)
            ->get('lavadores')
            ->result();
    }
    public function getDatosAux($fecha = '', $id_lavador = '')
    {
        return $this->db->select('a.id, a.hora, a.fecha, a.ocupado, a.activo,u.nombre,u.apellido_paterno,u.apellido_materno,u.telefono,ser.servicioNombre,au.placas,cs.estatus,cs.color,cs.color_letra')
            ->where('a.fecha', $fecha)
            ->where('a.id_lavador', $id_lavador)
            ->order_by('a.hora')
            ->join('servicio_lavado s', 's.id_horario = a.id')
            ->join('usuarios u', 'u.id = s.id_usuario')
            ->join('servicios ser', 'ser.servicioId = s.id_servicio')
            ->join('autos au', 'au.id = s.id_auto')
            ->join('cat_estatus_lavado cs', 'cs.id = s.id_estatus_lavado')
            ->get('horarios_lavadores a')
            ->result();
    }
    public function guardarUsuario($id = 0)
    {
        $data['usuarioID'] = get_guid();
        $data['nombre'] = $this->input->post('nombre');
        $data['apellido_paterno'] = $this->input->post('apellido_paterno');
        $data['apellido_materno'] = $this->input->post('apellido_materno');
        $data['email'] = $this->input->post('email');
        $data['telefono'] = $this->input->post('telefono');
        $data['password'] = substr($this->input->post('telefono'), -5);
        $data['id_sucursal'] = $this->input->post('id_sucursal');
        $data['token'] = $this->input->post('token');
        if ($id == 0) {
            return $this->Mgeneral->save_register('usuarios', $data);
        } else {
            return $this->db->where('id', $id)->update('usuarios', $data);
        }
    }
    //Validar si existe el usuario
    public function existeUsuario($id = 0)
    {
        if ($id != 0) {
            $this->db->where('id !=', $id);
        }
        $existe_usuario = $this->Mgeneral->get_result('email', $_POST['email'], 'usuarios');
        if (count($existe_usuario) > 0) {
            return true;
        }
        return false;
    }
    //Obtiene solamente los horarios de un operador de un día
    public function getDatosHorarios($fecha = '', $id_lavador = '')
    {
        return $this->db->select('a.id, a.hora, a.fecha, a.ocupado, a.activo')->where('a.fecha', $fecha)->where('a.id_lavador', $id_lavador)->order_by('a.hora')->get('horarios_lavadores a')->result();
    }
    public function tablero_lavadores($fecha = '', $valor = '', $tiempo_aumento = 30, $tipo = 0, $id_sucursal = 0)
    { // el último argumento es para saber si hace el cociente en base a 0 o a 1
        $tmpl = array(
            'table_open' => '<table class="table table-bordered table-hover table-hover">'
        );
        $this->table->set_template($tmpl);
        $this->table->set_heading('Horario', '#Servicio', 'Cliente', 'Placas', 'Valoraciones');
        $contador_asesores = 0;
        $lavadores = $this->getLavadores($id_sucursal);

        $array_lavadores = array();
        foreach ($lavadores as $key => $value) {
            $array_lavadores[$key] = $value;
            $array_lavadores[$key]->datos_servicio = $this->getDatosAux(date2sql($fecha), $value->lavadorId);

            $array_lavadores[$key]->datos_horarios = $this->getDatosHorarios(date2sql($fecha), $value->lavadorId);
        }
        if (count($array_lavadores) > 0) {
            foreach ($lavadores as $c => $value) {
                if ($contador_asesores % 2 == $tipo) {
                    if ($value->activo == 0) {
                        $etiqueta = "(Desactivado)";
                        $clase_operador = 'desactivado';;
                    } else {
                        $etiqueta = "";
                        $clase_operador = 'activo';
                    }
                    $row = array();
                    $row[] = array('class' => 'text-center col-sm-1 ' . $clase_operador, 'data' => $value->lavadorNombre . $etiqueta, 'colspan' => 5);
                    $this->table->add_row($row);
                    //EMPIEZA A PINTAR EL TIEMPO
                    $time = '08:00';
                    $contador = 0;
                    while ($contador < $valor) {
                        $row = array();
                        $bandera = false;
                        if (count($value->datos_servicio) > 0) {
                            foreach ($value->datos_servicio as $key => $val) {
                                if ($val->hora == $time && !$bandera) {
                                    $activo = $val->activo;
                                    $id_horario = $val->id;
                                    $cliente = $val->nombre . ' ' . $val->apellido_paterno . ' ' . $val->apellido_materno;
                                    $placas = $val->placas;;
                                    $servicioNombre = $val->servicioNombre;
                                    $estatus = $val->estatus;
                                    $color = $val->color;
                                    $color_letra = $val->color_letra;
                                    $bandera = true;
                                }
                            }
                        } //count
                        if ($bandera) {
                            //$activo = $this->horario_activo($value->id,$fecha,$time);
                            if ($activo == 0) {
                                $style = "background-color: #7576F6 !important;";
                            } else {
                                $style = "";
                            } //activo horario
                            $row[] = array('width' => '10%', 'data' => $time, 'style' => $style);
                            $acciones = '';
                            $row[] = array('data' => $servicioNombre, 'style' => $style);
                            $row[] = array('data' => $cliente, 'style' => $style);
                            $row[] = array('data' => $placas, 'style' => $style);
                            $row[] = array('data' => $estatus, 'style' => $style . 'color:' . $color_letra . ';font-size:15px;background-color:' . $color);
                        } else {
                            $bandera_horarios = false;
                            $activo_horario = 1;
                            if (count($value->datos_horarios) > 0) {
                                foreach ($value->datos_horarios as $key => $val) {
                                    if ($val->hora == $time && !$bandera_horarios) {
                                        $activo_horario = $val->activo;
                                        $bandera_horarios = true;
                                    }
                                }
                            } //count
                            if ($activo_horario == 0) {
                                $clase_horario = 'desactivado';
                            } else {
                                $clase_horario = '';
                            }

                            $row[] = array('width' => '10%', 'data' => $time, 'class' => $clase_horario);
                            $row[] = array('data' => '', 'colspan' => 6, 'class' => $clase_horario);
                        }

                        $this->table->add_row($row);

                        $timestamp = strtotime($time) + $tiempo_aumento * 60;
                        $time = date('H:i', $timestamp);
                        $contador++;
                    } // while
                    //$row[]=$item->asesor;


                } //$contador_asesores%2==$tipo
                $contador_asesores++;
            }
        } else {
            $row = array();
            $row[] = array('class' => 'text-center col-sm-1', 'data' => 'No hay registros para mostrar', 'colspan' => 16);
            $this->table->add_row($row);
        }
        return $this->table->generate();
    }
    public function getById($id, $tabla)
    {
        return $this->db->where('id', $id)->get($tabla)->row();
    }
    //Get nombre lavador 
    public function getLavadorById($id)
    {
        return $this->db->where('lavadorId', $id)->get('lavadores')->row();
    }
    public function getDatosFacturacionByIdUsuario($id_usuario = 0)
    {
        return $this->db->where('id_usuario', $id_usuario)->get('datos_facturacion')->row();
    }
    public function getAll()
    {
        if ($this->session->userdata('id_rol') != 1 && $this->session->userdata('id_rol') != 4) {
            $this->db->where('id_sucursal', $this->session->userdata('id_sucursal'));
        }
        if ($this->session->userdata('id_rol') == 4) {
            $this->db->where('id_usuario_creo', $this->session->userdata('id'));
        }
        return $this->db->order_by('fecha_creacion_servicio', 'asc')->get('v_listado_servicios')->result();
    }
    //Datos del usuario
    public function getDataUser($id = '')
    {
        return $this->db->where('u.id', $id)
            ->select('u.id as id_usuario,u.email,u.password,u.nombre,u.apellido_paterno,u.apellido_materno,u.telefono,u.id_sucursal,d.referencia,d.razon_social,d.domicilio,d.email_facturacion,d.id_cfdi,
                        d.id_forma_pago,d.id_metodo_pago,d.rfc,d.cp,ub.numero_int,ub.numero_ext,ub.colonia,ub.id_estado,ub.id_municipio,ub.latitud,ub.longitud,ub.calle,ub.id as id_ubicacion,d.id as id_facturacion')
            ->join('datos_facturacion d', 'u.id = d.id_usuario', 'left')
            ->join('ubicacion_usuario ub', 'u.id = ub.id_usuario', 'left')
            ->get('usuarios u')
            ->row();
    }
    public function getVehiculosUser($id_usuario = '')
    {
        return $this->db->where('a.id_usuario', $id_usuario)
            ->join('cat_marcas cm', 'cm.id = a.id_marca')
            ->join('cat_modelos cmd', 'cmd.id = a.id_modelo')
            ->select('a.*,cm.marca,cmd.modelo')
            ->get('autos a')->result();
    }
    public function getAutoByPlacas($placas)
    {
        return $this->db->where('placas', $placas)->get('autos')->row();
    }
    public function getIdHorarioActual($id = '')
    {
        $q = $this->db->where('id', $id)->select('id_horario')->get('servicio_lavado');
        if ($q->num_rows() == 1) {
            return $q->row()->id_horario;
        } else {
            return '';
        }
    }
    //Información del horario
    public function getInfoHorario($id)
    {
        return $this->db->where('h.id', $id)->join('lavadores l', 'l.lavadorId=h.id_lavador')->get('horarios_lavadores h')->row();
    }
    public function upload_evidencia($myfile = '', $formato = '')
    {
        $result = array();
        if (isset($_FILES[$myfile])) {
            ini_set('max_file_uploads', '600');
            ini_set('post_max_size', '50M');
            ini_set('upload_max_filesize', '50M');
            $config['upload_path'] = './statics/evidencia/';
            $config['allowed_types'] = 'png|jpg|jpeg';
            $config['max_size'] = '21200';
            $config['encrypt_name']  = true;
            $this->load->library('Upload2', $config);
            if (!$this->upload2->do_upload('file')) {
                $result[0] = array('isok' => false, 'client_name' => '', 'error' => $this->upload2->display_errors());
            } else {
                $ret = array();
                $contadorFILES = 0;
                $dataTMPARR = $this->upload2->data();
                foreach ($dataTMPARR as $indice => $dataTMP) {


                    $result[$contadorFILES]['isok'] = $dataTMP['isok'];
                    if ($dataTMP['isok']) {
                        $result[$contadorFILES]['client_name'] = $dataTMP['client_name'];
                        $result[$contadorFILES]['file_name'] = $dataTMP['file_name'];
                        $result[$contadorFILES]['path'] = $dataTMP['full_path'];
                    } else {

                        $result[$contadorFILES]['client_name'] = $dataTMP['file_name'];
                        $result[$contadorFILES]['error'] = $this->upload2->display_errors($indice);
                    }
                    $contadorFILES++;
                }
            }
        }
        return $result;
    }
    public function getSiglasSucursal($id_sucursal = '')
    {
        $q = $this->db->where('id', $id_sucursal)->select('nomenclatura')->get('sucursales');
        if ($q->num_rows() == 1) {
            return $q->row()->nomenclatura;
        } else {
            return '';
        }
    }
    public function getSiglasEstado($id_estado = '')
    {
        $q = $this->db->where('id', $id_estado)->select('siglas')->get('estados');
        if ($q->num_rows() == 1) {
            return $q->row()->siglas;
        } else {
            return '';
        }
    }
    //Obtener el #Folio
    public function getFolio()
    {
        $q = $this->db->order_by('id', 'desc')->where('folio_control is not null')->limit(1)->select('folio_control')->get('servicio_lavado');
        if ($q->num_rows() == 0) {
            return 0;
        } else {
            return $q->row()->folio_control;
        }
    }
    //Obtener info de CP por agencia
    public function getDataAgencia($id_agencia = '')
    {
        return $this->db->where('id', $id_agencia)->get('contacto_proactivo_agencias')->row();
    }
    public function getDataCatAgencia($id_agencia)
    {
        $q = $this->db->where('id', $id_agencia)->get('catalogo_agencias');

        if ($q->num_rows() == 0) {
            return '';
        } else {
            return $q->row();
        }
    }
    public function getPrecioServicio($id_modelo = '', $id_servicio)
    {
        $id_tipo_auto = $this->Mgeneral->get_row('id', $id_modelo, 'cat_modelos')->id_tipo_auto;
        $q = $this->db->where('id_tipo_auto', $id_tipo_auto)->where('id_servicio', $id_servicio)->select('precio')->get('precios_servicios');
        if ($q->num_rows() == 0) {
            return 0;
        } else {
            return $q->row()->precio;
        }
    }
    //Tel lavador
    public function getTelLavador($lavadorId = '')
    {
        $q = $this->db->where('lavadorId', $lavadorId)->get('lavadores');
        if ($q->num_rows() == 1) {
            return $q->row()->lavadorTelefono;
        } else {
            return '';
        }
    }
}
