@layout('layout')
@section('included_css')
    <link href="statics/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <style>
        .fa {
            margin-right: 8px;
        }

    </style>
@endsection
@section('contenido')
    <h1 class="text-center">Proactivo</h1>
    <div class="row">
        <div class='col-sm-3'>
            <label for="">Selecciona la agencia</label>
            {{ $id_agencia }}
            <span class="error error_id_agencia"></span>
        </div>
        <div class="col-sm-2">
            <br>
            <button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="tabla-items"></div>
        </div>
    </div>
@endsection

@section('included_js')
    @include('main/scripts_dt')
    <script src="statics/js/moment.js"></script>
    <script src="statics/js/bootstrap-datetimepicker.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        const buscar = () => {
            if ($("#id_agencia").val() != '') {
                $(".error_id_agencia").empty();
                var url = site_url + "/proactivo_agencias/generar_mes";
                ajaxLoad(url, {
                    id_agencia: $("#id_agencia").val()
                }, "tabla-items", "POST", function() {
                    inicializar_tabla_local();
                });
            } else {
                $(".error_id_agencia").empty().append('Es necesario seleccionar la agencia').css('color', 'red')
            }
        }
        $("body").on('click', "#generar", function() {
            ConfirmCustom("¿Está seguro de generar los registros para el contacto proactivo?", generarRegistros, "",
                "Confirmar", "Cancelar");
        });

        function generarRegistros() {
            var url = site_url + "/proactivo_agencias/generar_registros_proactivo";
            ajaxJson(url, {
                "mes": $("#month").val(),
                "anio": $("#year").val(),
                "id_agencia": $("#id_agencia_send").val()
            }, "POST", "async", function(result) {
                if (result == 1) {
                    ExitoCustom("Registros generados con éxito", function() {
                        location.reload();
                    });
                } else {
                    ErrorCustom("Error al generar los registros por favor intenta otra vez.");
                }
            });
        }
        $("#buscar").on('click', buscar);

        function inicializar_tabla_local() {
            $('#tbl_proactivo').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
        }

    </script>
@endsection
