@layout('layout')
@section('contenido')
    <div class="content mt-3">
        <form action="" method="post" novalidate="novalidate" id="alta_usuario">
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="cc-exp" class="control-label mb-1">Nombre</label>
                        <input id="nombre" required name="nombre" type="text"
                            class="form-control cc-exp form-control-sm" value="<?php echo $row->nombre;?>" placeholder="nombre">
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div align="right">
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info ">
                            <i class="fa fa-edit fa-lg"></i>&nbsp;
                            <span id="payment-button-amount">Editar</span>
                            <span id="payment-button-sending" style="display:none;">Sending…</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <br>
        
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script>
        var site_url = "{{site_url()}}";
        $(document).ready(function() {
            $("#cargando").hide();
            $('#alta_usuario').submit(function(event) {
                event.preventDefault();
                $("#enviar").hide();
                $("#cargando").show();
                var url_sis =
                    "<?php echo base_url(); ?>index.php/inventario_lavador_n/alta_editar/<?php echo $row->id;?>";
                // Get form
                var form = $('#alta_usuario')[0];
                // Create an FormData object
                var data = new FormData(form);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url_sis,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function(data) {
                        //  $("#result").text(data);
                        console.log("SUCCESS : ", data);
                        //  $("#btnSubmit").prop("disabled", false);
                        exito_redirect("DATOS GUARDADOS CON EXITO", "success",
                            "<?php echo base_url(); ?>index.php/inventario_lavador_n/alta"
                        );
                        $("#enviar").show();
                        $("#cargando").hide();
                    },
                    error: function(e) {
                        console.log("ERROR : ", e);
                        exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                        $("#enviar").show();
                        $("#cargando").hide();

                    }
                });
            });

        });

    </script>
@endsection
