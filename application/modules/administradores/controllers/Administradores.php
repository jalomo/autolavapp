<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Administradores extends MX_Controller
{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'companies', 'url'));
        $this->load->library('form_validation');
        date_default_timezone_set('America/Mexico_City');
    }

    public function index()
    {
        $data['titulo'] = "Estado";
        $data['titulo_dos'] = "Alta Estado";
        $data['usuarios'] = $this->Mgeneral->get_table('admin');
        $data['tipo_usuario'] = $this->Mgeneral->get_table('cat_roles');
        $data['sucursales'] = $this->Mgeneral->get_table('sucursales');
        $data['cargos'] = $this->Mgeneral->get_table('ca_cargos');
        $this->blade->render('administradores/index', $data);
    }

    public function guarda_admin()
    {
        $post = $this->input->post('Registro');
        //echo '<pre>'; print_r($post); exit();
        if ($post) {
            $post['adminPassword'] = md5(sha1($post['adminPassword']));
            $post['adminStatus'] = 1;
            $post['adminFecha'] = date('Y-m-d');
            $post['id_sucursal'] =  $this->input->post('id_sucursal');
            $post['id_cargo'] =  $this->input->post('id_cargo');
            $post['idRol'] =  $this->input->post('idRol');
            $id = $this->Mgeneral->save_admin($post);
            echo $id;
            redirect('administradores/index');
        } else {
        }
    }

    public function editar($id)
    {
        $data['titulo'] = "Estado";
        $data['titulo_dos'] = "Alta Estado";
        //$data['usuarios'] = $this->Mgeneral->get_table('admin');
        $data['sucursales'] = $this->Mgeneral->get_table('sucursales');
        $data['cargos'] = $this->Mgeneral->get_table('ca_cargos');
        $data['tipo_usuario'] = $this->Mgeneral->get_table('cat_roles');
        $data['row'] = $this->Mgeneral->get_row('adminId', $id, 'admin');

        $this->blade->render('administradores/editar', $data);
    }

    public function guarda_admin_editar($id)
    {
        $post = $this->input->post('Registro');
        $datos = $this->Mgeneral->get_row('adminId', $id, 'admin');
        //echo '<pre>'; print_r($datos);exit();
        if ($post) {
            if ($post['adminPassword'] != $datos->adminPassword) {
                $post['adminPassword'] = md5(sha1($post['adminPassword']));
            }
            $post['id_sucursal'] =  $this->input->post('id_sucursal');
            $post['id_cargo'] =  $this->input->post('id_cargo');
            $post['idRol'] =  $this->input->post('idRol');
            $id = $this->Mgeneral->update_admin($post, $id);
            echo $id;
            redirect('administradores/index');
        } else {
        }
    }
}
