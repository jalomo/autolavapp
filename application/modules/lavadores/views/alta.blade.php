@layout('layout')
@section('contenido')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="ui-typography">
                <div class="row">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Alta de lavador</strong>
                        </div>
                        <div class="card-body">
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <form action="" method="post" novalidate="novalidate" id="alta_usuario">
                                        <div class="row">
                                            <!--div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número Lavador</label>
                                                    <input id="numero_lavador" name="numero_lavador" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="Número Lavador" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div-->
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Nombre</label>
                                                    <input id="lavadorNombre" name="lavadorNombre" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="Nombre" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Email</label>
                                                    <input id="lavadorEmail" name="lavadorEmail" type="email"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="Email" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Contraseña</label>
                                                    <input id="lavadorPassword" name="lavadorPassword" type="password"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="Contraseña" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Teléfono</label>
                                                    <input id="lavadorTelefono" name="lavadorTelefono" type="text"
                                                        class="form-control cc-exp form-control-sm" placeholder="Nombre"
                                                        autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Tipo sangre</label>
                                                    <select name="tipo_sangre" id="tipo_sangre" class="form-control">
                                                        <option value="A/+">A/+</option>
                                                        <option value="A/-">A/-</option>
                                                        <option value="O/+">O/+</option>
                                                        <option value="O/-">O/-</option>
                                                        <option value="B/+">B/+</option>
                                                        <option value="B/-">B/-</option>
                                                        <option value="AB/+">AB/+</option>
                                                        <option value="AB/-">AB/-</option>
                                                    </select>
                                                    <!--input id="tipo_sangre" name="tipo_sangre" type="text"
                                                                    class="form-control cc-exp form-control-sm"
                                                                    placeholder="tipo sangre" autocomplete="cc-exp" value="">
                                                                <span class="help-block" data-valmsg-for="cc-exp"
                                                                    data-valmsg-replace="true"></span-->
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Fecha entrada</label>
                                                    <input id="fecha_entrada" name="fecha_entrada" type="text"
                                                        class="form-control cc-exp form-control-sm"
                                                        placeholder="Fecha entrada" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Fecha vencimiento</label>
                                                    <input id="fecha_vencimiento" name="fecha_vencimiento" type="text"
                                                        class="form-control cc-exp form-control-sm"
                                                        placeholder="Fecha vencimiento" autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                            
                                        <div class="row">
                                            
                                            
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Número licencia</label>
                                                    <input id="numero_licencia" name="numero_licencia" type="text"
                                                        class="form-control cc-exp form-control-sm" value=""
                                                        placeholder="Número licencia" autocomplete="cc-exp">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Fecha expedición</label>
                                                    <input id="fecha_expedicion" name="fecha_expedicion" type="text"
                                                        class="form-control cc-exp form-control-sm" placeholder="Fecha "
                                                        autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            
                                        
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Fecha expiración</label>
                                                    <input id="fecha_expiracion" name="fecha_expiracion" type="text"
                                                        class="form-control cc-exp form-control-sm" placeholder="Fecha "
                                                        autocomplete="cc-exp" value="">
                                                    <span class="help-block" data-valmsg-for="cc-exp"
                                                        data-valmsg-replace="true"></span>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                        @if ($this->session->userdata('id_rol') == 1)
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="cc-exp" class="control-label mb-1">Sucursal</label>
                                                    <select name="id_sucursal" id="id_sucursal" class="form-control">
                                                        <option value="">-- Selecciona sucursal --</option>
                                                        @foreach ($sucursales as $s => $sucursal)
                                                            <option value="{{ $sucursal->id }}">{{ $sucursal->sucursal }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @endif
                                        
                                        </div>

                                        <div class="row">
                                        <div class="col-sm-4">
                                                <div class="control-group">
                                                    <label class="control-label" for="basicinput">Imagen(165px X
                                                        165px)</label>
                                                    <div class="controls">
                                                        <input type="file" id="image" name="image" placeholder=""
                                                            class="span8" required="">
                                                        <!--span class="help-inline">500px X 500px</span-->
                                                    </div>
                                                </div>
                                           </div>
                                           <div class="col-sm-4">
                                                <div class="control-group">
                                                    <label class="control-label" for="basicinput">Documento</label>
                                                    <div class="controls">
                                                        <input type="file" id="documento" name="documento" placeholder=""
                                                            class="span8" required="">
                                                        <!--span class="help-inline">500px X 500px</span-->
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        <div align="right">
                                            <button type="button" id="guardar" type="submit" class="btn btn-lg btn-info ">
                                                <i class="fa fa-edit fa-lg"></i>&nbsp;
                                                <span id="payment-button-amount">Guardar</span>
                                                <span id="payment-button-sending" style="display:none;">Sending…</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .card -->
                </div>
                <!--/.col-->
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Número</th>
                                        <th>Imagen</th>
                                        <th>QR</th>
                                        <th>Nombre</th>
                                        <th>Email</th>
                                        <th>Contraseña</th>
                                        <th>Sucursal</th>
                                        <th>Documento</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (is_array($rows)): ?>
                                    <?php foreach ($rows as $row): ?>
                                    <tr>
                                        <td><?php echo nomenclatura_sucursal($row->id_sucursal) . '-' .
                                            $row->lavadorId; ?></td>

                                        <td><img height="50" src="<?php
                                            echo base_url();
                                            echo $row->lavadorFoto;
                                            ?>"></td>
                                        <td><img height="50" src="<?php
                                            echo base_url();
                                            echo $row->qr;
                                            ?>">
                                        </td>
                                        <td><?php echo $row->lavadorNombre; ?></td>
                                        <td><?php echo $row->lavadorEmail; ?></td>
                                        <td><?php echo $row->lavadorPassword; ?></td>
                                        <td><?php echo $row->sucursal; ?></td>
                                        <td>
                                            <a target="_blank"
                                                href="<?php echo base_url() . $row->url_documento; ?>">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a
                                                href="<?php echo base_url(); ?>index.php/lavadores/lista_horarios/<?php echo $row->lavadorId; ?>">
                                                <button type="button" class="btn btn-info">Lista citas</button>
                                            </a>
                                            <a
                                                href="<?php echo base_url(); ?>index.php/lavadores/editar/<?php echo $row->lavadorId; ?>">
                                                <button type="button" class="btn btn-info">Editar</button>
                                            </a>
                                            <a target="_blank"
                                                href="<?php echo base_url(); ?>index.php/gafete/index/<?php echo $row->id_id; ?>">
                                                <button type="button" class="btn btn-info">Gafete</button>
                                            </a>
                                            <a target="_blank"
                                                href="<?php echo base_url(); ?>index.php/lavadores/inventario/alta/<?php echo $row->lavadorId; ?>">
                                                <button type="button" class="btn btn-info">Inventario</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('included_js')
    @include('main/scripts_dt')


    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function() {

            // $( "#datepicker" ).datepicker();
            $("#fecha_entrada").datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $("#fecha_vencimiento").datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $("#fecha_expedicion").datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $("#fecha_expiracion").datepicker({
                dateFormat: 'yy-mm-dd'
            });

        });
        $("#cargando").hide();
        $('#guardar').on('click', function(event) {
            event.preventDefault();
            $("#enviar").hide();
            $("#cargando").show();
            var url_sis =
                "<?php echo base_url(); ?>index.php/lavadores/alta_guardar";
            // Get form
            var form = $('#alta_usuario')[0];
            // Create an FormData object
            var data = new FormData(form);
            console.log(data);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: url_sis,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function(data) {
                    exito_redirect("DATOS GUARDADOS CON EXITO", "success",
                        "<?php echo base_url(); ?>index.php/lavadores/alta"
                    );
                    $("#enviar").show();
                    $("#cargando").hide();

                },
                error: function(e) {
                    //$("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                    //$("#btnSubmit").prop("disabled", false);
                    exito("<h3>ERROR intente de nuevo<h3/> <br/>" + aux, "danger");
                    $("#enviar").show();
                    $("#cargando").hide();

                }
            });
        });

    </script>
@endsection
