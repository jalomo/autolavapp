@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Otras salidas Serv. Exce</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="frm-productos">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="orden">Orden</label>
                            <input id="orden" placeholder="1006" value="" name="orden" type="text" class="form-control">
                            <div id="orden_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-8 mt-4">
                        <button type="button" id="btn-buscar" class="btn-primary btn col-md-4">
                            <i class="fas fa-search"></i> buscar
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="select">Cliente</label>
                            <select name="id_cliente" class="form-control " id="id_cliente">
                                <option value=""> Seleccionar</option>
                                @foreach ($catalogo_clientes as $cliente)
                                @if (isset($data_venta) && $data_venta->cliente_id == $cliente->id)
                                <option selected value="{{ $cliente->id}}"> {{ $cliente->numero_cliente .' - '. $cliente->nombre .' '. $cliente->apellido_materno}} </option>
                                @else
                                <option value="{{ $cliente->id}}"> {{ $cliente->numero_cliente .' - '. $cliente->nombre .' '. $cliente->apellido_materno}} </option>

                                @endif
                                @endforeach
                            </select>
                            <div id='id_cliente_error' class='invalid-feedback'></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="select">Nombre completo</label>
                            <input class="form-control" disabled type="text" id="nombre_cliente" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="select">RFC</label>
                            <input class="form-control" type="text" disabled id="rfc_cliente">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "placas", "Placas", '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "no_serie", "No de serie", '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "asesor", "Asesor", '', true); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table class=" table table-bordered" id="tbl_salidas">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Descripción</th>
                                    <th>Número de pieza</th>
                                    <th>Precio unitario</th>
                                    <th>Cantidad</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Descripción</th>
                                    <th>Número de pieza</th>
                                    <th>Precio unitario</th>
                                    <th>Cantidad</th>
                                    <th>-</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button id="btn-confirmar-venta-mpm" type="button" class="btn btn-primary"> Aceptar y continuar </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h3>Listado ordenes abiertas</h3>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class=" table table-bordered" id="tbl_ordenes_abiertas">
                <thead>
                    <tr>
                        <th>No. Orden</th>
                        <th>Cliente</th>
                        <th>Asesor</th>
                        <th>Técnico</th>
                        <th>Serie</th>
                        <th>Placas</th>
                        <th>Estatus</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No. Orden</th>
                        <th>Cliente</th>
                        <th>Asesor</th>
                        <th>Técnico</th>
                        <th>Serie</th>
                        <th>Placas</th>
                        <th>Estatus</th>
                        <th>-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection

@section('included_js')
<script>
    // $(document).ready(function() {
        let orden = $("#orden").val();
        var tabla_salidas = $('#tbl_salidas').DataTable({
            "ajax": {
                url: base_url + "refacciones/salidas/ajax_data_salidas_producto",
                type: 'POST',
                data: {
                    orden: function() {
                        return $('#orden').val()
                    }
                }
            },
            columns: [{
                    'data': function(data) {
                        if (data.id) {
                            return data.id
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.descripcion) {
                            return data.descripcion
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.descripcion) {
                            return data.num_pieza
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.precio_unitario) {
                            return data.precio_unitario
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.cantidad) {
                            return data.cantidad
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function(data) {
                        return "--";
                    }
                }
            ]
        });

        //continuar flujo de venta
        $("#btn-confirmar-venta-mpm").on('click', function() {

            if ($("#id_cliente").val() == '') {
                return utils.displayWarningDialog('Selecciona el cliente ...', "warning", function(data) {})
            }

            if ($("#orden").val() == '') {
                return utils.displayWarningDialog('Indicar numero de orden ...', "warning", function(data) {})
            }

            ajax.post('api/ventas/mpm', {
                numero_orden: $('#orden').val(),
                cliente_id: $("#id_cliente").val(),
                venta_total: 0,
                tipo_venta_id: 2, //ventanilla taller
                almacen_id: 1,
                tipo_precio_id: 1,
                precio_id: 1
            }, (data, headers) => {
                toastr.warning('<strong>Procesando ...</strong>');
                if (headers.status == 200) {
                    utils.displayWarningDialog('Venta registrada ...', "success", function(data) {
                        window.location.href = PATH + "/refacciones/salidas/detalleVenta/" + data.folio_id
                    })
                }
            });
        });

        $("#id_cliente").on('change', function(e) {
            let id_cliente = $("#id_cliente").val();
            ajax.get(`api/clientes/${id_cliente}`, {}, (data, headers) => {
                let response = data[0];
                $("#nombre_cliente").val(`${response.nombre} ${response.apellido_materno} ${response.apellido_paterno}`);
                $("#rfc_cliente").val(response.rfc);
            })
        });

    // });

    var tabla_ordenes = $('#tbl_ordenes_abiertas').DataTable({
        ajax: base_url + "refacciones/Salidas/ajax_ordenes_abiertas",
        columns: [{
                'data': function(data) {
                    return data.id_cita
                }
            },
            {
                'data': function(data) {
                    return data.cliente
                }
            },
            {
                'data': function(data) {
                    return data.asesor
                }
            },
            {
                'data': function(data) {
                    return data.tecnico
                }
            },
            {
                'data': function(data) {
                    return data.serie
                }
            },
            {
                'data': function(data) {
                    return data.placas
                }
            },
            {
                'data': function(data) {
                    return data.estatus_orden
                }
            },
            {
                'data': function(data) {
                    return "<button onclick='cargar_operaciones(" + data.id_cita + ")' class='btn btn-primary'>" +
                        "<i class='fas fa-tasks'></i>" +
                        "</button>";
                }
            }
        ]
    });

    function cargar_operaciones(id_cita) {
        $("#orden").val(id_cita);
        $.ajax({
            type: 'GET',
            url: base_url + "refacciones/salidas/ajax_data_salidas/" + $("#orden").val(),
            dataType: "json",
            success: function(response) {
                if (response.asesor.length > 0) {
                    console.log(response);
                    let asesor = response.asesor;
                    $("#placas").val(asesor[0].vehiculo_placas);
                    $("#no_serie").val(asesor[0].vehiculo_numero_serie);
                    $("#asesor").val(asesor[0].asesor);
                    tabla_salidas.ajax.reload();
                    toastr.warning('<strong>Cargando productos ...</strong>');
                } else {
                    utils.displayWarningDialog('No se encontraron resultados ...', "warning", function(data) {})
                }
            }
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#menu_refacciones").addClass("show");
        $("#refacciones_salidas").addClass("show");
        $("#refacciones_salidas").addClass("active");
        $("#refacciones_salidas_sub").addClass("show");
        $("#refacciones_salidas_sub").addClass("active");
        $("#otras_salidas").addClass("active");
        $("#M02").addClass("active");
    });
</script>
@endsection