@layout('layout')
@section('contenido')
<?php echo form_open('administradores/guarda_admin_editar/' . $row->adminId); ?>
<div class="form-group">
    <span>
        <?php echo form_label('Nombre del Admin:', 'nombreAdmin'); ?>
    </span>
    <span>
        <?php echo form_input(array(
            'id' => '',
            'class' => 'form-control cc-exp ',
            'name' => 'Registro[adminNombre]',
            'value' => '' . $row->adminNombre . ''
        )); ?>
    </span>
</div>
<div class="form-group">
    <span>
        <?php echo form_label('Nombre de Usuario: ', 'nombreUsurio'); ?>
    </span>
    <span>
        <?php echo form_input(array(
            'id' => '',
            'class' => 'form-control cc-exp ',
            'name' => 'Registro[adminUsername]',
            'value' => '' . $row->adminUsername . ''
        )); ?>
    </span>
</div>
<div class="form-group">
    <span>
        <?php echo form_label('Password: ', 'password'); ?>
    </span>
    <span>
        <?php echo form_password(array(
            'id' => '',
            'class' => 'form-control cc-exp ',
            'name' => 'Registro[adminPassword]',
            'value' => '' . $row->adminPassword . ''
        )); ?>
    </span>
</div>
<div class="form-group">
    <span>
        <?php echo form_label('Teléfono: ', 'telefono'); ?>
    </span>
    <span>
        <?php echo form_input(array(
            'id' => '',
            'class' => 'form-control cc-exp ',
            'name' => 'Registro[telefono]',
            'value' => '' . $row->telefono . ''
        )); ?>
    </span>
</div>
<div class="form-group">
    <span>
        <?php echo form_label('Sucursales: ', 'Sucursales'); ?>
    </span>
    <span>
        <select id="id_sucursal" name="id_sucursal" class="form-control">
            <option value=""></option>
            <?php foreach ($sucursales as $sucu) : ?>
                <?php if ($sucu->id == $row->id_sucursal) : ?>
                    <option value="<?php echo $sucu->id; ?>" selected><?php echo $sucu->sucursal; ?></option>
                <?php else : ?>
                    <option value="<?php echo $sucu->id; ?>"><?php echo $sucu->sucursal; ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
    </span>
</div>
<div class="form-group">
    <span>
        <?php echo form_label('Cargos: ', 'Cargos'); ?>
    </span>
    <span>
        <select id="id_cargo" name="id_cargo" class="form-control">
            <option value=""></option>
            <?php foreach ($cargos as $cargo) : ?>
                <?php if ($cargo->id == $row->id_cargo) : ?>
                    <option value="<?php echo $cargo->id; ?>" selected><?php echo $cargo->nombre; ?></option>
                <?php else : ?>
                    <option value="<?php echo $cargo->id; ?>"><?php echo $cargo->nombre; ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
    </span>
</div>
<div class="form-group">
            <span>
                <?php echo form_label('Tipo usuario: ', 'Cargos'); ?>
            </span>
            <span>
                <select id="idRol" name="idRol" class="form-control">
                    <option value=""></option>
                    <?php foreach ($tipo_usuario as $tipo_user) : ?>
                        <?php if ($tipo_user->id == $row->idRol) : ?>
                            <option value="<?php echo $tipo_user->id; ?>" selected><?php echo $tipo_user->rol; ?></option>
                        <?php else : ?>
                             <option value="<?php echo $tipo_user->id; ?>"><?php echo $tipo_user->rol; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select>
            </span>
        </div>

<div>
    <?php echo form_submit(array(
        'id' => '',
        'class' => 'btn btn-lg btn-info ',
        'value' => 'Guardar'
    )); ?>
</div>
<?php echo form_close(); ?>






@endsection
@section('included_js')
@include('main/scripts_dt')

@endsection