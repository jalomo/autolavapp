@layout('layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">Productos</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">Lista</li>
    </ol>
    <div class="row mb-3">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
            <a class="btn btn-primary col-md-12" href="{{ base_url('refacciones/productos/crear') }}">Regresar</a>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="factura">Imagen de producto</label>
                <input type="file" class="form-control-file" id="imagen" name="imagen">

                <input type="hidden" id="producto_id" name="producto_id" value="{{ $data->id }}">
                <div id="imagen_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-12">
            <button type="button" id="btnsubir" onclick="subirfotografia()" class="btn btn-primary">
                Subir
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla_imagenes" width="100%" cellspacing="0"></table>
        </div>
    </div>
</div>
@endsection

@section('included_js')
<script src="{{ base_url('js/custom/jquery.dataTable.1.10.20.min.js') }}" crossorigin="anonymous"></script>
<script>
    const subirfotografia = ()=> {
        $.isLoading({ text: "Procesando...." });
        let imagen = $('#imagen')[0].files[0];
        var paqueteDeDatos = new FormData();
        paqueteDeDatos.append('imagen_producto', imagen);
        paqueteDeDatos.append('producto_id', $("#producto_id").val());
            ajax.postFile(`api/productos/agregarimagen`, paqueteDeDatos, function(response, header) {
                if(header.status == 200){
                    $.isLoading('hide');
                }
            });
    }

    var tabla_imagenes = $('#tabla_imagenes').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/imagenesproducto/imagenes-producto/"+document.getElementById('producto_id').value,
			type: 'GET',
		},
		columns: [{
				title: "#",
				data: 'id',
			},
			{
				title: "Nombre de archivo",
				data: 'nombre_archivo',
			},
			{
				title: "Principal",
				data: 'principal',
            },
            {
                title: "-",
                render: function (data, type, {id}) {
                    return '-'
                }
            }
		
			
		]
	});
</script>

@endsection