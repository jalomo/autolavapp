<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mmapa_servicios extends CI_Model
{

    public function save_register($table, $data)
      {
          $this->db->insert($table, $data);
          return $this->db->insert_id();
      }

      public function delete_row($id_tabla,$id,$tabla){
        $this->db->delete($tabla, array($id_tabla=>$id));
     }

     public function get_result($campo,$value,$tabla){
        return $this->db->where($campo,$value)->get($tabla)->result();
    }

    public function get_row($campo,$value,$tabla){
        return $this->db->where($campo,$value)->get($tabla)->row();
    }

    public function get_table($table){
        $data = $this->db->get($table)->result();
        return $data;
    }

}
