<div class="row">
    <div class="col-sm-12">
        <figure class="highcharts-figure">
            <div id="container"></div>
        </figure>
    </div>
</div>
<script>
    var series = [];
    series.push({
        name: 'Cantidad servicios',
        type: 'column',
        yAxis: 1,
        data: [
            parseFloat("{{$cantidad_servicios_sucursal[1]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[2]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[3]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[4]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[5]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[6]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[7]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[8]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[9]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[10]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[11]['total']}}"),
            parseFloat("{{$cantidad_servicios_sucursal[12]['total']}}"),
        ],
        tooltip: {
            valueSuffix: ''
        }
    })
    @foreach($cantidad_metodo_pago as $i => $info)
            var data = [];
            @foreach($info as $in =>$data)
                data.push(parseFloat("{{$data['total']}}"));
            @endforeach
            series.push({
                name: '{{$i}}',
                type: 'spline',
                tooltip: {
                    valueSuffix: ''
                },
                data: data
            })
        @endforeach

    Highcharts.chart('container', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Cantidad de servicios totales y por métodos de pago'
        },
        xAxis: [{
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
            ],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Cantidad total de servicios',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Cantidad por método de pago',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        // legend: {
        //     layout: 'horizontal',
        //     align: 'center',
        //     x: 0,
        //     verticalAlign: 'bottom',
        //     y: 0,
        //     floating: true,
        //     backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
        //         'rgba(255,255,255,0.25)'
        // },
        series: series
    });

</script>
